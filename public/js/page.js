var sub_cat=0;
var base_url = window.location.origin;
$(document).ready(function(){
    $('input[name="pea_event_reserve_price"]').val(0);
    var pathname = window.location.pathname;
    if(pathname=='/buyer-live-auction')
    setTimeout(update_buyer_current, 2000);
    setTimeout(update_buyer_current_invite_acceptance,2000);
    if(pathname=='/live-auction')
    setTimeout(update_current, 2000);

    // var hfExcelUploadSuccessVal = $('#hfExcelUploadSuccess').val();
    // var hfExcelUploadFailureVal = $('#hfExcelUploadFailure').val();

    // if(hfExcelUploadSuccessVal != null || hfExcelUploadFailureVal != null)
    // {
    //     if(hfExcelUploadFailureVal != null)
    //     {
    //         iziToast.error({
    //             timeout: 3000,
    //             id: 'error',
    //             title: 'Error',
    //             message: hfExcelUploadFailureVal,
    //             position: 'topRight',
    //             transitionIn: 'fadeInDown'
    //         });
    //     }
    //     else if(hfExcelUploadSuccessVal != null)
    //     {
    //         iziToast.success({
    //             timeout: 3000,
    //             id: 'success',
    //             title: 'Success',
    //             message: hfExcelUploadSuccessVal,
    //             position: 'bottomRight',
    //             transitionIn: 'bounceInLeft'
    //         });
    //     }
    // }

	$('.select2').select2();
	$("#buy-new-pack").click(function(){
		$(".new-package-list").hide();
	});
	$("#buy-new-pack").click(function(){
		$(".new-package-list").show();
	});
	//$('#makeEditable').SetEditable({ $addButton: $('#but_add')});

	

	
	$(function() {
  $('.datetimes').daterangepicker({
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'DD/MM/YYYY hh:mm A'
    }
  });
});

var today = new Date();

$(function() {
	
	$('.reportdate').daterangepicker({
  singleDatePicker: true,
  showDropdowns: true,
  maxDate: today,
  timePicker: true,
  autoUpdateInput:true,
 startDate: moment().startOf('minute').add(5, 'minute'),
  endDate: moment().startOf('hour').add(32, 'hour'), 
  locale: {
  format:'YYYY-MM-DD hh:mm A',
  }
  }, function(start, end, label) {
  var years = moment().diff(start, 'years');
  });

  $('.reportdate').val('');
	
  $('.datetimes1').daterangepicker({
  singleDatePicker: true,
  showDropdowns: true,
  minDate: today,
  timePicker: true,
  autoUpdateInput:true,
  startDate: moment().startOf('minute').add(30, 'minute'),
  endDate: moment().startOf('hour').add(32, 'hour'),
  locale: {
  format:'YYYY-MM-DD hh:mm A',
  }
  }, function(start, end, label) {
  var years = moment().diff(start, 'years');
  });
  
  $('.datetimes2').daterangepicker({
  singleDatePicker: true,
  showDropdowns: true,
  minDate: today,
  timePicker: true,
  autoUpdateInput:true,
  startDate: moment().startOf('hour').add(24, 'hour'),
  endDate: moment().startOf('hour').add(32, 'hour'),
  locale: {
  format:'YYYY-MM-DD hh:mm A',
  }
  }, function(start, end, label) {
  var years = moment().diff(start, 'years');
  });
  });
  
  $('.datetimes1').on('apply.daterangepicker', function(ev, picker) {
       $('#event-detail-form').valid();
      });
	  
	 $('body').on('click','#event-content', function(){ 
		  $("#event-detail-form").validate().element('#pec_event_end_dt');
	 })
  
  
 $("#new_event").validate({
	 
	rules: {
    // simple rule, converted to {required:true}
    pci_comp_name: {
    required: true
    },
    // compound rule
    pec_event_name: { 
		required: true,
    },
    pec_event_id: {
		required: true,
    },
    pec_event_dt: {
		required: true
    }
    
    },
	
  submitHandler: function(form) {
  
  var formdate=$('#new_event').serializeArray();

 
     //   return false;
          $.ajax({
              type: "POST",
              url: base_url+'/addevent',
              data: formdate,
              dataType: 'JSON',
			  beforeSend: function(){
				  $('.preloader').show();
			  },
              success: function( msg ) {
               console.log(msg);
			   $('.preloader').hide();
               if(msg['status']==true){
                //$("#status").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"Thanks for signing up for Purchase Quick. <br/> Please verify your email address."+'</div>');
				iziToast.success({
					timeout: 3000,
					id: 'success',
					title: 'Success',
					message: 'Your Event Added Successfully',
					position: 'bottomRight',
					transitionIn: 'bounceInLeft',
					onOpened: function(instance, toast){
					},
					onClosed: function(instance, toast, closedBy){
						console.info('closedBy: ' + closedBy);
					}
				});
                }
                else{
                //$("#status").html('<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"The email address you have entered is already registered."+'</div>');
				iziToast.error({
						timeout: 3000,
						id: 'error',
						title: 'Error',
						message: 'Event Creation Failed.',
						position: 'topRight',
						transitionIn: 'fadeInDown'
					});
                }
          
          }
          });

  }

});

 $("#package_conformation").validate({
	 
	rules: {
    // simple rule, converted to {required:true}
    pci_comp_name: {
    required: true
    },
    // compound rule
    pci_comp_phone: { 
		required: true,
    },
    pmp_pkg_name: {
		required: true,
    },
    pmp_pkg_amount: {
		required: true
    },
	pmp_pkg_eventcount:{
		required: true
	},
	pmp_pkg_validity:{
		required: true
	},
	pli_comp_id:{
		required: true
	},
	pli_sno:{
		required: true
	}
	
    },
	messages: {
        pli_comp_id: "All fields are required",
        pli_sno: "All fields are required",
    },
	
  submitHandler: function(form) {
  
  var formdate=$('#package_conformation').serializeArray();
  var RequestData = {
    key: '6634852',
    txnid: '123456789544',
    hash: '23XzhtXPBs',
    amount: '100',
    firstname: 'Jaysinh',
    email: 'dummyemail@dummy.com',
    phone: '6111111111',
    productinfo: 'Bag',
    surl : 'https://sucess-url.in',
    furl: 'https://fail-url.in',
  
}

var handler = {

    responseHandler: function(BOLT){
        console.log(BOLT);
      // your payment response Code goes here, BOLT is the response object

    },
    catchException: function(BOLT){
console.log(BOLT);
      // the code you use to handle the integration errors goes here

    }
}
  bolt.launch( RequestData , handler ); 
  alert("fsd");
         return false;
          $.ajax({
				type: "POST",
				url: base_url+'/addevent',
				data: formdate,
				dataType: 'JSON',
				beforeSend: function(){
				  $('.preloader').show();
				},
				success: function( msg ) 
				{
				 $('.preloader').hide();
         
				//console.log(msg['packid']);
          
				if(msg['status'] == true)
				{
					window.location.href	=	base_url+'/payment-status/success/'+msg['url']+'/'+msg['packid'];	
				}
				else{		
				if(msg['packid']==0)				
						window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
					}
				}
          });

  }

});

$('#report-submit').click(function()
{
    var event_name = $('#pec_event_name').val();
    var start_date = $('#pec_event_start_dt').val();
    var end_date = $('#pec_event_end_dt').val();

    if(event_name == "" && start_date == "")
    {
        $("#pec_event_name").prop('required',true);
    }
    else if(event_name == "" && start_date != "" && end_date == "")
    {
        $("#pec_event_end_dt").prop('required',true);
    }

});

$("#event-detail-form").validate({
	rules: {
    // simple rule, converted to {required:true}
    pec_event_name: {
    required: true
    },
    pec_event_category: {
        required: true
    },
    pec_event_type:{
		 required: true,
	    valueNotEquals: "0"
    },
    pec_event_dt: {
		required: true
    },
	pec_event_end_dt:{onfocusout: false, greaterThan: "#pec_event_start_dt" }
	
	
    },
	messages: {
        pec_event_end_dt: "Event End Date Must be greater than start Date",
    },

	
  submitHandler: function(form) {
  
  var formdate=$('#event-detail-form').serializeArray();

        // return false;
          $.ajax({
				type: "POST",
				url: base_url+'/createnNewEvent',
				data: formdate,
				dataType: 'JSON',
				beforeSend: function(){
				  $('.preloader').show();
				},
				success: function( msg ) 
				{
					$('.preloader').hide();         
					
					if(msg['status'] == true)
					{
						//alert("Event Created Successfully ");
						$('.preloader').show();  
						iziToast.success({
							timeout: 2500,
							id: 'success',
							title: 'Success',
							message: 'Event Created Successfully',
							position: 'bottomRight',
							transitionIn: 'bounceInLeft',
							onOpened: function(instance, toast){
								location.reload();	
							},
							onClosed: function(instance, toast, closedBy){		
								
								$('.preloader').hide();
								console.info('closedBy: ' + closedBy);
							}
							
						});
					}
					else{		
						iziToast.error({
							timeout: 3000,
							id: 'error',
							title: 'Error',
							message: 'Event Creation Failed.',
							position: 'topRight',
							transitionIn: 'fadeInDown'
						});
						 //window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
					}
				}
          });

  }

});

// add item for created event

//excel upload
$("#report-form").validate({
	rules: {
        excel : {
            required: true
        }
    },

  submitHandler: function(form) {  
  
  var data = new FormData(form);

  var file_data = $('#excel').prop('files')[0];

  data.append('file', file_data);

          $.ajax({
				type: "POST",
				url: base_url+'/excelitemupload',
				
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
				beforeSend: function(){
				  $('.preloader').show();
				},
                success: function(data){
					$('.preloader').show();  
                    if(data['status'] == true)
					{            
						iziToast.success({
							timeout: 2500,
							id: 'success',
							title: 'Success',
							message: data['message'],
							position: 'bottomRight',
							transitionIn: 'bounceInLeft',
							onOpened: function(instance, toast){
								location.reload();	
							},
							onClosed: function(instance, toast, closedBy){		
								
								$('.preloader').hide();
								console.info('closedBy: ' + closedBy);
							}
							
						});
					}
					else{		
                        iziToast.error({
                            timeout: 2500,
                            id: 'error',
                            title: 'Error',
                            message: data['message'],
                            position: 'topRight',
                            transitionIn: 'fadeInDown',						
                            onOpened: function(instance, toast){
                                $('.preloader').show();
                            },
                            onClosed: function(instance, toast, closedBy){							
                                $('.preloader').hide();
                            }
                        });
					}
                }
          });
  }

});

$("#upload-form-closed").validate({
	rules: {
        excel : {
            required: true
        }
    },

  submitHandler: function(form) {  
  
  var data = new FormData(form);

  var file_data = $('#excel').prop('files')[0];

  data.append('file', file_data);

          $.ajax({
				type: "POST",
				url: base_url+'/excelcloseditemupload',
				
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
				beforeSend: function(){
				  $('.preloader').show();
				},
                success: function(data){
					$('.preloader').show();  
                    if(data['status'] == true)
					{            
						iziToast.success({
							timeout: 2500,
							id: 'success',
							title: 'Success',
							message: data['message'],
							position: 'bottomRight',
							transitionIn: 'bounceInLeft',
							onOpened: function(instance, toast){
								location.reload();	
							},
							onClosed: function(instance, toast, closedBy){		
								
								$('.preloader').hide();
								console.info('closedBy: ' + closedBy);
							}
							
						});
					}
					else{		
                        iziToast.error({
                            timeout: 2500,
                            id: 'error',
                            title: 'Error',
                            message: data['message'],
                            position: 'topRight',
                            transitionIn: 'fadeInDown',						
                            onOpened: function(instance, toast){
                                $('.preloader').show();
                            },
                            onClosed: function(instance, toast, closedBy){							
                                $('.preloader').hide();
                            }
                        });
					}
                }
          });
  }

});

// add  event creation 
 $.validator.addMethod("valueNotEquals", function(e, t, a) {
    return a !== e
}, "Value must not equal arg."),
jQuery.validator.addMethod("greaterThan", 
function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }

    return isNaN(value) && isNaN($(params).val()) 
        || (Number(value) > Number($(params).val())); 
},'Must be greater than {0}.');

// add item for created event

// add  event creation 
$.validator.addMethod("valueNotEquals", function(e, t, a) {
    return a !== e
}, "Value must not equal arg.")


//init set values
$('#no_of_time_extension').val(0);
$('#time_extension').val(3);

$("#event-add-items").validate({
	rules: {
    // simple rule, converted to {required:true}
    prod_market: {
        required: true
    },
    prod_cat: {
    required: true
    },
    prod_st_price: {
        required: true
        },
    prod_sub_cat: {
       
        valueNotEquals: "0"
        },
   
    product_name: {
		required: true
    },
    spec: {
      required: true
      },
      prod_unit: {
        required: true
        },
        prod_quant: {
          required: true
          },
          pea_event_reserve_price: {
                required: false
            },
            time_extension: {
                required: true
            },
            no_of_time_extension: {
                required: true,
            },         
            prod_min_decrement: {
              required: true
              },
              location: {
                required: true
                },
				pec_event_start_dt : {
					required:true
				},
				pec_event_end_dt : {
					required : true
                },
                pea_event_emd_value : {
                    required : true
                }
    },

    messages: {
        prod_sub_cat: {
            valueNotEquals: "Please select Products  !"
        }
    },
  submitHandler: function(form) {
  
  
  var data = new FormData(form);

  var file_data = $('#term_upload').prop('files')[0];

  data.append('file', file_data);

          $.ajax({
				type: "POST",
				url: base_url+'/addProductEvent',
				
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
				beforeSend: function(){
				  $('.preloader').show();
				},
                success: function(data){
					$('.preloader').show();  
                    if(data['status'] == true)
					{            
						iziToast.success({
							timeout: 2500,
							id: 'success',
							title: 'Success',
							message: 'Event Item Added Successfully',
							position: 'bottomRight',
							transitionIn: 'bounceInLeft',
							onOpened: function(instance, toast){
								location.reload();	
							},
							onClosed: function(instance, toast, closedBy){		
								
								$('.preloader').hide();
								console.info('closedBy: ' + closedBy);
							}
							
						});
					}
					else{		
					iziToast.error({
						timeout: 3000,
						id: 'error',
						title: 'Error',
						message: 'Event Item Added Failed',
						position: 'topRight',
						transitionIn: 'fadeInDown',						
						onOpened: function(instance, toast){
							$('.preloader').show();
						},
						onClosed: function(instance, toast, closedBy){							
							$('.preloader').hide();
						}
					});					
					//  location.reload();
					// window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
					}
                }
          });

  }

});

$("#wallet-form").validate({

    rules: {
        // simple rule, converted to {required:true}
        wallet_amount : {
            required: true
        },
        payment_method : {
            required: true
        }

        
        },

});

$("#even-completed").validate({
	rules: {
    // simple rule, converted to {required:true}
    
    },

	
  submitHandler: function(form) {
  
  var formdate=$('#even-completed').serializeArray();

        // return false;
          $.ajax({
				type: "POST",
				url: base_url+'/completedEvent',
				data: formdate,
				dataType: 'JSON',
				beforeSend: function(){
				  $('.preloader').show();
				},
				success: function( msg ) 
				{
					$('.preloader').hide();       
					
					if(msg['status'] == true)
					{           
						window.location.href	=	base_url+'/upcoming-event';
					}
					else{		
						iziToast.error({
							timeout: 3000,
							id: 'error',
							title: 'Error',
							message: 'Even Completed Failed',
							position: 'topRight',
							transitionIn: 'fadeInDown'
						});								
					  //  location.reload();
						// window.location.href	=	base_url+'/upcoming-event/fail/'+msg['url'];	
						}
				}
          });

  }

});



$("#invite-vendorform").validate({
	rules: {
    // simple rule, converted to {required:true}

    pmc_market_name: {
        required: true
        },
    pmc_cat_name: {
    required: true
    },
   
    pms_subcat_name: {
		required: true
    },
    pea_event_spec: {
      required: true
      },
      pmu_unit_name: {
        required: true
        },
        pea_event_unit_quantity: {
          required: true
          },
          pea_event_max_dec: {
            required: true
            },
            pea_event_reserve_price: {
                required: true
            },
            pea_event_location: {
              required: true
              },
	
    },

	
  submitHandler: function(form) {
  
  var formdate=$('#invite-vendorform').serializeArray();

        // return false;
          $.ajax({
				type: "POST",
				url: base_url+'/editProductItem',
				data: formdate,
				dataType: 'JSON',
				beforeSend: function(){
				  $('.preloader').show();
				},
				success: function( msg ) 
				{
					$('.preloader').show();
					if(msg['status'] == true)
					{
			   
					iziToast.success({
						timeout: 3000,
						id: 'success',
						title: 'Success',
						message: 'product Updated Successfully',
						position: 'bottomRight',
						transitionIn: 'bounceInLeft',
						onOpened: function(instance, toast){
							location.reload();	
						},
						onClosed: function(instance, toast, closedBy){							
							$('.preloader').hide();
							console.info('closedBy: ' + closedBy);
						}
						
					});
					}
					else{	
						iziToast.error({
						timeout: 2500,
						id: 'error',
						title: 'Error',
						message: 'product Updated failed',
						position: 'topRight',
						transitionIn: 'fadeInDown',
						onOpened: function(instance, toast){
							$('.preloader').show();
						},
						onClosed: function(instance, toast, closedBy){							
							$('.preloader').hide();
						}
					});			
					  //  location.reload();
						// window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
						}
				}
          });

  }

});

$("#prod_market").on("change", function() {
 
    var market = $(this).val();
   
    var cat_type= $("#com_cat_type").val();
    $.ajax({
        type: "POST",
        url: base_url + "/getCategories",
        data: {
            market: market,
            cat_type:cat_type
        },
        dataType: "JSON",
        beforeSend: function(){
            $('.preloader').show();
          },
        success: function(e) {
              $('.preloader').hide();
              $("#prod_cat").empty();
              $("#prod_cat").append("<option value='0'>-- Choose Your Categories --</option>")
              $.each(e, function(e, t) {
              $("#prod_cat ").append($("<option></option>").attr("value", t.pmca_id).text(t.pmca_cat_name))
            })
        }
    })
  })

$("#prod_cat").on("change", function() {

 
  var categories = $(this).val();
  var cat_type= $("#com_cat_type").val();
  $.ajax({
      type: "POST",
      url: base_url + "/getSubproduct",
      data: {
          cat: categories,
          cat_type:cat_type
      },
      dataType: "JSON",
	  beforeSend: function(){
		  $('.preloader').show();
		},
      success: function(e) {
			$('.preloader').hide();
			$("#prod_sub_cat").empty();
			$("#prod_sub_cat").append("<option value='0'>-- Choose Your Products --</option>")
			$.each(e, function(e, t) {
            $("#prod_sub_cat ").append($("<option></option>").attr("value", t.pms_id).text(t.pms_subcat_name))
          })
      }
  })
})

$(".finalizedBid").on("click", function() {

    var id= $(this).attr('id');
    var sellID=  $(this).attr('data-mail');
   
    $.ajax({
     type: "POST",
     url: base_url + "/finalizedBid",
     data: {
         lotID: id,
         sellerID:sellID
     },
     dataType: "JSON",
	 beforeSend: function(){
		$('.preloader').show();
	},
     success: function(msg) {
       $('.preloader').hide();
        if(msg['status'] == true)
         {

			location.reload();
 
         }
         else{	
			iziToast.error({
				timeout: 2500,
				id: 'error',
				title: 'Error',
				message: 'Bit add failed',
				position: 'topRight',
				transitionIn: 'fadeInDown'
			});		
		 //  location.reload();
         // window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
         }
     }
 })

 return false;
})

$(".declinedBid").on("click", function() {

    var id= $(this).attr('id');
    var sellID=  $(this).attr('data-mail');
   
    $.ajax({
     type: "POST",
     url: base_url + "/rejectBid",
     data: {
         lotID: id,
         sellerID:sellID
     },
     dataType: "JSON",
	 beforeSend: function(){
		$('.preloader').show();
	},
     success: function(msg) {
       $('.preloader').hide();
        if(msg['status'] == true)
         {
			location.reload(); 
         }
         else{	
			iziToast.error({
				timeout: 2500,
				id: 'error',
				title: 'Error',
				message: $msg['message'],
				position: 'topRight',
				transitionIn: 'fadeInDown'
			});		
		   //location.reload();
         // window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
         }
     }
 })

 return false;
})

$(".declineAllBid").on("click", function() {

    var lotid=  $(this).attr('data-mail');
   
    $.ajax({
     type: "POST",
     url: base_url + "/rejectAllBid",
     data: {
         lotID: lotid
     },
     dataType: "JSON",
	 beforeSend: function(){
		$('.preloader').show();
	},
     success: function(msg) {
       $('.preloader').hide();
        if(msg['status'] == true)
         {
			location.reload(); 
         }
         else{	
			iziToast.error({
				timeout: 2500,
				id: 'error',
				title: 'Error',
				message: msg['message'],
				position: 'topRight',
				transitionIn: 'fadeInDown'
			});		
		   //location.reload();
         // window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
         }
     }
 })

 return false;
})

$('.rescheduleLot').click(function()
{
    var lotID=  $(this).attr('data-mail');
    var eventId=  $(this).attr('data-event');    

    $('#rescheduleForm').modal({
        backdrop: 'static',
        keyboard: false
      })
      .one('click', '#reschedule-model', function(e) {

        var startDate = $('#event_start_dt').val();
        var endDate = $('#event_end_dt').val();
          
        $.ajax({
            type: "POST",
            url: base_url + "/rescheduleLotItem",
            data: {
                startDate: startDate,
                endDate: endDate,
                lotID: lotID,
                eventId: eventId
            },      
            dataType: "JSON",
            beforeSend: function(){
              $('.preloader').show();
            },
            success: function(msg) {
               $('.preloader').hide();
                if(msg['status'] == true)
                {
                    $('#rescheduleForm').modal('hide');
                    location.reload();

                    //update_current();
                }
                else{		
                    iziToast.error({
                        timeout: 2500,
                        id: 'error',
                        title: 'Error',
                        message: msg['message'],
                        position: 'topRight',
                        transitionIn: 'fadeInDown'
                    });	
                    //  location.reload();
                    // window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
                }
            }
        })
      });
});

$('#ownv').click(function()
  {
    $('#addressFormv').modal({
            backdrop: 'static',
            keyboard: false
          });
  });

$('.generatePO').click(function()
{
    var lotID=  $(this).attr('data-lot-id');
    var currentBid=  $(this).attr('data-bidid');  

    $('#hfPOLotId').val(lotID);
    $('#hfCurrentBid').val(currentBid);
    
    $('#addressForm').modal({
            backdrop: 'static',
            keyboard: false
          });

    // $('#addressForm').modal({
    //     backdrop: 'static',
    //     keyboard: false
    //   })
    //   .one('click', '#address-model', function(e) {

    //     var radioValue = $("input[name='deliver_address']:checked").val();
    //     if(radioValue){
    //         $.ajax({
    //             type: "POST",
    //             url: base_url + "/purchaseorder",
    //             data: {
    //                 addressId: radioValue,
    //                 lotID: lotID,
    //                 currentBid: currentBid
    //             },      
    //             dataType: "JSON",
    //             beforeSend: function(){
    //               $('.preloader').show();
    //             },
    //             success: function(msg) {
    //                $('.preloader').hide();
    //                 if(msg['status'] == true)
    //                 {
    //                     iziToast.success({
    //                         timeout: 2500,
    //                         id: 'success',
    //                         title: 'Success',
    //                         message: msg['message'],
    //                         position: 'bottomRight',
    //                         transitionIn: 'bounceInLeft',
    //                         onOpened: function(instance, toast){
    //                             location.reload();
    //                       },						
    //                     });
    //                 }
    //                 else{		
    //                     iziToast.error({
    //                         timeout: 2500,
    //                         id: 'error',
    //                         title: 'Error',
    //                         message: msg['message'],
    //                         position: 'topRight',
    //                         transitionIn: 'fadeInDown'
    //                     });	
                            
    //                 }
    //             }
    //         })
    //     }
    //     else
    //     {
    //         alert('Select any one of the address to proceed');
    //     }          
        
    //   });
});

$("#generate_po_form").validate({
	rules: {
    // simple rule, converted to {required:true}
        deliver_address: {
            required: true
        },
        logo_upload: {
            required: true
        },
        deliveryDateOption: {
            required: true
        }
    },
    messages: {
        prod_sub_cat: {
            valueNotEquals: "Please select the required fields !"
        }
    },

  submitHandler: function(form) {  
  
    var data = new FormData(form);

    var file_data = $('#logo_upload').prop('files')[0];

    data.append('file', file_data);

          $.ajax({
				type: "POST",
				url: base_url+'/purchaseorder',				
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
				beforeSend: function(){
				  $('.preloader').show();
				},
                success: function(data){
					$('.preloader').show();  
                    if(data['status'] == true)
					{            
						iziToast.success({
							timeout: 2500,
							id: 'success',
							title: 'Success',
							message: data['message'],
							position: 'bottomRight',
							transitionIn: 'bounceInLeft',
							onOpened: function(instance, toast){
								location.reload();	
							},
							
						});
					}
					else{		
                        iziToast.error({
                            timeout: 3000,
                            id: 'error',
                            title: 'Error',
                            message: data['message'],
                            position: 'topRight',
                            transitionIn: 'fadeInDown',
                            onClosed: function(instance, toast, closedBy){		
								
								$('.preloader').hide();
								console.info('closedBy: ' + closedBy);
							}
                        });		
					}
                }
          });

  }

});

$('.generateInvoice').click(function()
{
    var lotID=  $(this).attr('data-lot-id');
    var currentBid=  $(this).attr('data-bidid');  

    $('#hfPOLotId').val(lotID);
    $('#hfCurrentBid').val(currentBid);
    
    $('#invoiceForm').modal({
            backdrop: 'static',
            keyboard: false
          });

});

$("#generate_invoice_form").validate({
	rules: {
    // simple rule, converted to {required:true}
        deliver_address: {
            required: true
        },
        logo_upload: {
            required: true
        },
       
    },
    messages: {
        prod_sub_cat: {
            valueNotEquals: "Please select the required fields !"
        }
    },

  submitHandler: function(form) {  
  
    var data = new FormData(form);

    var file_data = $('#logo_upload').prop('files')[0];

    data.append('file', file_data);

          $.ajax({
				type: "POST",
				url: base_url+'/generate_invoice',				
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
				beforeSend: function(){
				  $('.preloader').show();
				},
                success: function(data){
					$('.preloader').show();  
                    if(data['status'] == true)
					{            
						iziToast.success({
							timeout: 2500,
							id: 'success',
							title: 'Success',
							message: data['message'],
							position: 'bottomRight',
							transitionIn: 'bounceInLeft',
							onOpened: function(instance, toast){
								location.reload();	
							},
							
						});
					}
					else{		
                        iziToast.error({
                            timeout: 3000,
                            id: 'error',
                            title: 'Error',
                            message: data['message'],
                            position: 'topRight',
                            transitionIn: 'fadeInDown',
                            onClosed: function(instance, toast, closedBy){		
								
								$('.preloader').hide();
								console.info('closedBy: ' + closedBy);
							}
                        });		
					}
                }
          });

  }

});

$('.sendInvoice').click(function()
{
    var lotID=  $(this).attr('data-lot-id');
    var poId=  $(this).attr('data-po-id');
    var currentBid=  $(this).attr('data-bidid');    

    $.ajax({
        type: "POST",
        url: base_url + "/sendInvoice",
        data: {
            poId: poId,
            lotID: lotID,
            currentBid: currentBid
        },      
        dataType: "JSON",
        beforeSend: function(){
          $('.preloader').show();
        },
        success: function(msg) {
           $('.preloader').hide();
            if(msg['status'] == true)
            {
                iziToast.success({
                    timeout: 2500,
                    id: 'success',
                    title: 'Success',
                    message: msg['message'],
                    position: 'bottomRight',
                    transitionIn: 'bounceInLeft',
                    onOpened: function(instance, toast){
                        location.reload();
                  },						
                });
            }
            else{		
                iziToast.error({
                    timeout: 2500,
                    id: 'error',
                    title: 'Error',
                    message: msg['message'],
                    position: 'topRight',
                    transitionIn: 'fadeInDown'
                });	
                    
            }
        }
    })
});

$('#invoice-cancel-model').click( function () {
    $('#invoiceForm').modal('hide');
});



$('.sendPO').click(function()
{
    var lotID=  $(this).attr('data-lot-id');
    var poId=  $(this).attr('data-po-id');
    var currentBid=  $(this).attr('data-bidid');    

    $.ajax({
        type: "POST",
        url: base_url + "/sendPO",
        data: {
            poId: poId,
            lotID: lotID,
            currentBid: currentBid
        },      
        dataType: "JSON",
        beforeSend: function(){
          $('.preloader').show();
        },
        success: function(msg) {
           $('.preloader').hide();
            if(msg['status'] == true)
            {
                iziToast.success({
                    timeout: 2500,
                    id: 'success',
                    title: 'Success',
                    message: msg['message'],
                    position: 'bottomRight',
                    transitionIn: 'bounceInLeft',
                    onOpened: function(instance, toast){
                        location.reload();
                  },						
                });
            }
            else{		
                iziToast.error({
                    timeout: 2500,
                    id: 'error',
                    title: 'Error',
                    message: msg['message'],
                    position: 'topRight',
                    transitionIn: 'fadeInDown'
                });	
                    
            }
        }
    })
});

$('#address-cancel-model').click( function () {
    $('#addressForm').modal('hide');
});

$('.acceptPO').click(function()
{
    var lotID=  $(this).attr('data-lot-id');
    var poId=  $(this).attr('data-po-id');
    var currentBid=  $(this).attr('data-bidid');    

    $.ajax({
        type: "POST",
        url: base_url + "/acceptPO",
        data: {
            poId: poId,
            lotID: lotID,
            currentBid: currentBid
        },      
        dataType: "JSON",
        beforeSend: function(){
          $('.preloader').show();
        },
        success: function(msg) {
           $('.preloader').hide();
            if(msg['status'] == true)
            {
                iziToast.success({
                    timeout: 2500,
                    id: 'success',
                    title: 'Success',
                    message: msg['message'],
                    position: 'bottomRight',
                    transitionIn: 'bounceInLeft',
                    onOpened: function(instance, toast){
                        location.reload();
                  },						
                });
            }
            else{		
                iziToast.error({
                    timeout: 2500,
                    id: 'error',
                    title: 'Error',
                    message: msg['message'],
                    position: 'topRight',
                    transitionIn: 'fadeInDown'
                });	
                    
            }
        }
    })
});

$('.acceptInvoice').click(function()
{
    var lotID=  $(this).attr('data-lot-id');
    var poId=  $(this).attr('data-po-id');
    var currentBid=  $(this).attr('data-bidid');    

    $.ajax({
        type: "POST",
        url: base_url + "/acceptInvoice",
        data: {
            poId: poId,
            lotID: lotID,
            currentBid: currentBid
        },      
        dataType: "JSON",
        beforeSend: function(){
          $('.preloader').show();
        },
        success: function(msg) {
           $('.preloader').hide();
            if(msg['status'] == true)
            {
                iziToast.success({
                    timeout: 2500,
                    id: 'success',
                    title: 'Success',
                    message: msg['message'],
                    position: 'bottomRight',
                    transitionIn: 'bounceInLeft',
                    onOpened: function(instance, toast){
                        location.reload();
                  },						
                });
            }
            else{		
                iziToast.error({
                    timeout: 2500,
                    id: 'error',
                    title: 'Error',
                    message: msg['message'],
                    position: 'topRight',
                    transitionIn: 'fadeInDown'
                });	
                    
            }
        }
    })
});

$('#bulk-upload').click(function()
{
    var eventId=  $(this).attr('data-id');
    var eventType=  $(this).attr('data-event-type');

    if(eventType == 2)
    {
        $('#hfEventId').val(eventId);

        $('#uploadForm-Reverse').modal({
            backdrop: 'static',
            keyboard: false
        })
    }
    else
    {
        $('#hfClosedEventId').val(eventId);

        $('#uploadForm-Closed').modal({
            backdrop: 'static',
            keyboard: false
        })
    }
});

$('#upload-model-cancel').click( function () {
    $('#uploadForm-Reverse').modal('hide');
});

$('#upload-closed-model-cancel').click( function () {
    $('#uploadForm-Closed').modal('hide');
});

$('#reschedule-model-cancel').click( function () {
    $('#rescheduleForm').modal('hide');
});

$('.seller-price-enter').each( function(){
    var form = $(this);

 form.validate({
	rules: {
    // simple rule, converted to {required:true}
    bid_price: {
    required: true
   /* max: function () {
    
        return parseInt($('#bid_amount').val());
    }*/
 
    },
   
},
messages: {
    bid_amount: {
                max: "Please enter less than "+$('#bid_amount').val()
    }
},

	
submitHandler: function(form) {

              // return false;
     
      
        }
      
      })
    });


$(".pq_seller_accept").on("click", function() {

   var id= $(this).attr('id');
   var invite_type= $(this).attr('data-type');

   $.ajax({
    type: "POST",
    url: base_url + "/inviteAccept",
    data: {
        inviteID: id,
        invite_type: invite_type
    },
    dataType: "JSON",
	beforeSend: function(){
		$('.preloader').show();
	  },
    success: function(msg) {
		$('.preloader').hide();
        if(msg['status'] == true)
        {
            location.reload();
        }
        else{		
            //alert("product add failed ");	
            
            iziToast.error({
                timeout: 2500,
                id: 'error',
                title: 'Error',
                message: 'Lot Expired',
                position: 'topRight',
                transitionIn: 'fadeInDown'
            });	

            location.reload();
            // window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
        }
    }
})

})

$("#pmc_market_name").on("change", function() {

 
    var market = $(this).val();
    var cat_type= $("#com_cat_type").val();
    $.ajax({
        type: "POST",
        url: base_url + "/getCategories",
        data: {
            market: market,
            cat_type:cat_type
        },
        dataType: "JSON",
		beforeSend: function(){
			$('.preloader').show();
		},
        success: function(e) {
			$('.preloader').hide();
           $("#pmc_cat_name").empty();
         //  $(".new_varient").append("<option value='0'>--Select variant --</option>")
             $.each(e, function(e, t) {
             
                $("#pmc_cat_name").append($("<option></option>").attr("value", t.pmca_id).text(t.pmca_cat_name))
            })

            $("#pmc_cat_name option[value="+cat+"]").prop("selected",true);
        }
    })
  })


  $('.removeItem').on("click", function() {
    var itemID = $(this).attr('data-trash-id');
    $.ajax({
        type: "POST",
        url: base_url + "/deleteProuct",
        data: {
            pmc_id: itemID
        },
        dataType: "JSON",
		beforeSend: function(){
			$('.preloader').show();
		},
        success: function(e) {
			$('.preloader').hide();
            location.reload();
            
        }
    })
  });

	"use strict";

function IterarCamposEdit(t, n) {
    function i(t) {
        if (null == colsEdi) return !0;
        for (var n = 0; n < colsEdi.length; n++)
            if (t == colsEdi[n]) return !0;
        return !1
    }
    var o = 0;
    t.each(function() {
        o++, "buttons" != $(this).attr("name") && i(o - 1) && n($(this))
    })
}

function FijModoNormal(t) {
    $(t).parent().find("#bAcep").hide(), $(t).parent().find("#bCanc").hide(), $(t).parent().find("#bEdit").show(), $(t).parent().find("#bElim").show(), $(t).parents("tr").attr("id", "")
}

function FijModoEdit(t) {


    $(t).parent().find("#bAcep").show(), $(t).parent().find("#bCanc").show(), $(t).parent().find("#bEdit").hide(), $(t).parent().find("#bElim").hide(), $(t).parents("tr").attr("id", "editing")
}

function ModoEdicion(t) {
    return "editing" == t.attr("id")
}

function rowAcep(t) {
    
    var n = $(t).parents("tr"),
        i = n.find("td");
    ModoEdicion(n) && (IterarCamposEdit(i, function(t) {
        var n = t.find("input").val();
        t.html(n)
    }), FijModoNormal(t), params.onEdit(n))
}

function rowCancel(t) {
    var n = $(t).parents("tr"),
        i = n.find("td");
    ModoEdicion(n) && (IterarCamposEdit(i, function(t) {
        var n = t.find("div").html();
        t.html(n)
    }), FijModoNormal(t))
}

function rowEdit(t) {
    var n = $(t).parents("tr"),
        i = n.find("td");
    ModoEdicion(n) || (IterarCamposEdit(i, function(t) {
        var n = t.html(),
            i = '<div style="display: none;">' + n + "</div>",
            o = '<input class="form-control input-sm" value="' + n + '">';
        t.html(i + o)
    }), FijModoEdit(t))
}
$('.addattr').click( function () {

    var form = $(this).closest("form");
   
    if(form.valid()){
        var enterbid_amount = parseInt(form.find('input[name="bid_price"]').val());
        var amount_che = form.find('input[name="bid_price"]').val();

        var numbersOnly = /^\d+$/;

        if(numbersOnly.test(amount_che)){
           
            var event_type=form.find('input[name="event_type"]').val();
            if(event_type==2){
                var reserve_bid=parseInt(form.find('input[name="reserve_bid"]').val());
                var current_bid=parseInt(form.find('input[name="current_bid"]').val());
                var max_dec=parseInt(form.find('input[name="max_dec"]').val());
                var start_price=parseInt(form.find('input[name="start_price"]').val());
                var first_bid;
                if(current_bid==0){
                    first_bid=1;
                    current_bid=start_price;
                }
            
                var    check_valid_amount=current_bid-enterbid_amount;
                if(first_bid==1){
                if(check_valid_amount==0){
                    valid=1;
                }else if(check_valid_amount>=max_dec){
                valid=1;
                }else{
                    valid=0;
                }
                }else{
                    if(check_valid_amount>=max_dec){
                        valid=1;
                        }else{
                        valid=0;
                        }
                }

                    
                if(current_bid>=enterbid_amount && enterbid_amount>=reserve_bid && valid==1){

                $('.event-full-amount').empty();
                $("#amnt-text").empty();
                $('.event-full-amount').html(inr(enterbid_amount));
                $("#amnt-text").html(convertNumberToWords(enterbid_amount)+' Only')

                var formdate=  $(form).serializeArray();
                $.ajax({
                    type: "POST",
                    url: base_url + "/validateLotBid",
                    data: formdate,
              
                    dataType: "JSON",
                    beforeSend: function(){
                      $('.preloader').show();
                    },
                    success: function(msg) {
                       $('.preloader').hide();

                        if(msg['status'] == true)
                        {
                            $('#invite_Pricing_Amount').modal({
                                backdrop: 'static',
                                keyboard: false
                            })
                            .one('click', '#confirm-model', function(e) {
                                
                                var formdate=  $(form).serializeArray();
                                $.ajax({
                                    type: "POST",
                                    url: base_url + "/addBidLotItem",
                                    data: formdate,
                            
                                    dataType: "JSON",
                                    beforeSend: function(){
                                    $('.preloader').show();
                                    },
                                    success: function(msg) {
                                    $('.preloader').hide();
                                        if(msg['status'] == true)
                                        {
                                        $('#bid_price').val($('#bid_price').val())
                                        $('#bid_amount').val($('#bid_price').val())
                                //location.reload();
                                $('#invite_Pricing_Amount').modal('hide');
                                form.find('input[name="bid_price"]').attr( "readonly", "true" ); 
                                
                            
                            
                            $(".addattr").hide();
                                $(".editattr").show();
            
                                update_current();
                            //  location.reload();
                                        }
                                        else{		
                                            //alert(msg['c_code']);
                                            if(msg['c_code'] == 3)
                                            {
                                                iziToast.error({
                                                    timeout: 2500,
                                                    id: 'error',
                                                    title: 'Error',
                                                    message: 'You already made a bid',
                                                    position: 'topRight',
                                                    transitionIn: 'fadeInDown'
                                                });	
                                            }
                                            else
                                            {
                                                //location.reload();
                                            }	
            
                                        //  location.reload();
                                            // window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
                                        }
                                    }
                                })
                            });
                            
                            $('#invite_Pricing_Amount').modal('show');
                        }
                        else{		
                            if(msg['c_code'] == 3)
                            {
                                iziToast.error({
                                    timeout: 2500,
                                    id: 'error',
                                    title: 'Error',
                                    message: 'You already made a bid',
                                    position: 'topRight',
                                    transitionIn: 'fadeInDown'
                                });	

                            }
                            else
                            {
                                location.reload();
                            }	                           
                        }
                    }
                });               
                               
                }else{
                    if(enterbid_amount<reserve_bid)
                        iziToast.error({
                            timeout: 2500,
                            id: 'error',
                            title: 'Error',
                            message: 'make bid reserve amount',
                            position: 'topRight',
                            transitionIn: 'fadeInDown'
                        });
                    if(current_bid<enterbid_amount)
                    iziToast.error({
                        timeout: 2500,
                        id: 'error',
                        title: 'Error',
                        message: 'given amount is high ',
                        position: 'topRight',
                        transitionIn: 'fadeInDown'
                    });

                    if(check_valid_amount<max_dec)
                    iziToast.error({
                        timeout: 2500,
                        id: 'error',
                        title: 'Error',
                        message: 'Kindly enter the multiple of  Minimum Decrement value',
                        position: 'topRight',
                        transitionIn: 'fadeInDown'
                    });
                    
                }
            }else{
                $('.event-full-amount').empty();
                $("#amnt-text").empty();
                $('.event-full-amount').html(inr(enterbid_amount));
                $("#amnt-text").html(convertNumberToWords(enterbid_amount)+' Only')

                var formdate=  $(form).serializeArray();
                $.ajax({
                    type: "POST",
                    url: base_url + "/validateLotBid",
                    data: formdate,
              
                    dataType: "JSON",
                    beforeSend: function(){
                      $('.preloader').show();
                    },
                    success: function(msg) {
                       $('.preloader').hide();

                        if(msg['status'] == true)
                        {
                            $('#invite_Pricing_Amount').modal({
                                backdrop: 'static',
                                keyboard: false
                            })
                            .one('click', '#confirm-model', function(e) {
                                
                                var formdate=  $(form).serializeArray();
                                $.ajax({
                                    type: "POST",
                                    url: base_url + "/addBidLotItem",
                                    data: formdate,
                            
                                    dataType: "JSON",
                                    beforeSend: function(){
                                    $('.preloader').show();
                                    },
                                    success: function(msg) {
                                    $('.preloader').hide();
                                        if(msg['status'] == true)
                                        {
                                        $('#bid_price').val($('#bid_price').val())
                                        $('#bid_amount').val($('#bid_price').val())
                                //location.reload();
                                $('#invite_Pricing_Amount').modal('hide');

                                
                                //form.find('input[name="bid_price"]').attr( "readonly", "true" ); 
                                form.find('input[name="bid_price"]').remove(); 
                                form.append('<span id="bid_closed">bid Closed</span>');
                                
                                $(".addattr").hide();
                                $(".editattr").hide();
                                        }
                                        else{		
                                /*     iziToast.error({
                                        timeout: 2500,
                                        id: 'error',
                                        title: 'Error',
                                        message: 'Product add failed',
                                        position: 'topRight',
                                        transitionIn: 'fadeInDown'
                                    });		*/
                                //  location.reload();
                                            // window.location.href	=	base_url+'/payment-status/fail/'+msg['url'];	
                                        }
                                    }
                                })
                            });
                            
                            $('#invite_Pricing_Amount').modal('show');
                        }
                        else{		
                            if(msg['c_code'] == 3)
                            {
                                iziToast.error({
                                    timeout: 2500,
                                    id: 'error',
                                    title: 'Error',
                                    message: 'You already made a bid',
                                    position: 'topRight',
                                    transitionIn: 'fadeInDown'
                                });	

                            }
                            else
                            {
                                location.reload();
                            }	                           
                        }
                    }
                });            
            }    
        }
        else
        {
            iziToast.error({
                timeout: 2500,
                id: 'error',
                title: 'Error',
                message: 'Decimals not allowed. The bidding price should be the type integer',
                position: 'topRight',
                transitionIn: 'fadeInDown'
            });
        }                
       
    }else{

        $('.seller-quote-val').attr( "readonly", "true" ); 
        $(".addattr").hide();
        $(".editattr").show();
    }

    });
   
    $('#confirm-model-cancel').click( function () {
        $('#invite_Pricing_Amount').modal('hide');       

    });

    $('#confirm-model-wallet-cancel').click(function(){
        $('#wallet_popup').modal('hide');
    });

    $('.editattr').click( function () {
        
    $('.seller-quote-val').removeAttr("readonly");
    $(".editattr").hide();
    $(".addattr").show();
    }); 
function rowElim(t) {
    
    $(t).parents("tr").remove(), params.onDelete()
}

function rowAgreg() {
    if (0 == $tab_en_edic.find("tbody tr").length) {
        var t = "";
        (i = $tab_en_edic.find("thead tr").find("th")).each(function() {
            "buttons" == $(this).attr("name") ? t += colEdicHtml : t += "<td></td>"
        }), $tab_en_edic.find("tbody").append("<tr>" + t + "</tr>")
    } else {
        var n = $tab_en_edic.find("tr:last");
        n.clone().appendTo(n.parent());
        var i = (n = $tab_en_edic.find("tr:last")).find("td");
        i.each(function() {
            "buttons" == $(this).attr("name") || $(this).html("")
        })
    }
}

function TableToCSV(t) {
    var n = "",
        i = "";
    return $tab_en_edic.find("tbody tr").each(function() {
        ModoEdicion($(this)) && $(this).find("#bAcep").click();
        var o = $(this).find("td");
        n = "", o.each(function() {
            "buttons" == $(this).attr("name") || (n = n + $(this).html() + t)
        }), "" != n && (n = n.substr(0, n.length - t.length)), i = i + n + "\n"
    }), i
}
var $tab_en_edic = null,
    params = null,
    colsEdi = null,
    newColHtml = '<div class="btn-group table-action-btn"><button id="bEdit" type="button" class="btn btn-sm btn-edit-row" onclick="rowEdit(this);"><span class="fa fa-pencil" > </span></button><button id="bElim" type="button" class="btn btn-sm btn-delete-row" onclick="rowElim(this);"><span class="fa fa-trash" > </span></button><button id="bAcep" type="button" class="btn btn-sm btn-save-row" style="display:none;" onclick="rowAcep(this);"><span class="fa fa-check"> </span></button><button id="bCanc" type="button" class="btn btn-sm btn-remove-row" style="display:none;" onclick="rowCancel(this);"><span class="fa fa-times" > </span></button><button type="button" class="btn btn-sm btn-invite-row dropdown"><a href="javascript:" class="dropbtn"><span class="fa fa-envelope"></span></a><div class="dropdown-content"><a href="#" title="Own Vendor" data-toggle="modal" data-target="#invite_ownvendor">Own Vendor</a><a href="#" title="PQ Vendor" data-toggle="modal" data-target="#invite_PQvendor">PQ Vendor</a></div></button></div>',
    colEdicHtml = '<td name="buttons">' + newColHtml + "</td>";

});
function inr(rupee){var x=rupee;x=x.toString();var afterPoint='';if(x.indexOf('.')>0)
			afterPoint=x.substring(x.indexOf('.'),x.length);x=Math.floor(x);x=x.toString();var lastThree=x.substring(x.length-3);var otherNumbers=x.substring(0,x.length-3);if(otherNumbers!='')
			lastThree=','+lastThree;var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g,",")+lastThree+afterPoint;return res}
           
           
            function convertNumberToWords(amount) {
                var words = new Array();
                words[0] = '';
                words[1] = 'One';
                words[2] = 'Two';
                words[3] = 'Three';
                words[4] = 'Four';
                words[5] = 'Five';
                words[6] = 'Six';
                words[7] = 'Seven';
                words[8] = 'Eight';
                words[9] = 'Nine';
                words[10] = 'Ten';
                words[11] = 'Eleven';
                words[12] = 'Twelve';
                words[13] = 'Thirteen';
                words[14] = 'Fourteen';
                words[15] = 'Fifteen';
                words[16] = 'Sixteen';
                words[17] = 'Seventeen';
                words[18] = 'Eighteen';
                words[19] = 'Nineteen';
                words[20] = 'Twenty';
                words[30] = 'Thirty';
                words[40] = 'Forty';
                words[50] = 'Fifty';
                words[60] = 'Sixty';
                words[70] = 'Seventy';
                words[80] = 'Eighty';
                words[90] = 'Ninety';
                amount = amount.toString();
                var atemp = amount.split(".");
                var number = atemp[0].split(",").join("");
                var n_length = number.length;
                var words_string = "";
                if (n_length <= 9) {
                    var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
                    var received_n_array = new Array();
                    for (var i = 0; i < n_length; i++) {
                        received_n_array[i] = number.substr(i, 1);
                    }
                    for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
                        n_array[i] = received_n_array[j];
                    }
                    for (var i = 0, j = 1; i < 9; i++, j++) {
                        if (i == 0 || i == 2 || i == 4 || i == 7) {
                            if (n_array[i] == 1) {
                                n_array[j] = 10 + parseInt(n_array[j]);
                                n_array[i] = 0;
                            }
                        }
                    }
                    value = "";
                    for (var i = 0; i < 9; i++) {
                        if (i == 0 || i == 2 || i == 4 || i == 7) {
                            value = n_array[i] * 10;
                        } else {
                            value = n_array[i];
                        }
                        if (value != 0) {
                            words_string += words[value] + " ";
                        }
                        if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                            words_string += "Crores ";
                        }
                        if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                            words_string += "Lakhs ";
                        }
                        if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                            words_string += "Thousand ";
                        }
                        if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                            words_string += "Hundred and ";
                        } else if (i == 6 && value != 0) {
                            words_string += "Hundred ";
                        }
                    }
                    words_string = words_string.split("  ").join(" ");
                }
                return words_string;
            }	

            function update_current(){
                $.ajax({
                    type: "POST",
                    url: base_url + "/updatebid",
                  
              
                    dataType: "JSON",
                    beforeSend: function(){
                   
                    },
                    success: function(msg) {
                        $.each(msg, function(e, pqlist) {
                         var form= $('#bid_form_'+pqlist.bid_id+'');
                         if(pqlist.curent_amount!=null){
                        $('#current_'+pqlist.bid_id+'').text(pqlist.curent_amount);
                        $('#current_hidden_'+pqlist.bid_id+'').val(pqlist.curent_amount);}
                        else{
                            $('#current_'+pqlist.bid_id+'').text(0);
                            $('#current_hidden_'+pqlist.bid_id+'').val(0);
                        }
                        
                      var reserved= form.find('input[name="reserve_bid"]').val(); 

                      if(pqlist.curent_amount<=reserved && pqlist.curent_amount!=null){
                     form.find('input[name="bid_price"]').remove(); 
                        form.find(".reserved_reached").remove();
                         form.find("#reserved_reached_append").remove();
                         form.find(".btn-purple").remove();
                       
                          form.append('<span id="reserved_reached_append">bid reached reserved amount</span>');
                      }
                        })
               
                        },
                        complete: function() {
                            // Schedule the next request when the current one's complete
                            setTimeout(update_current, 2000);
                          }
                       
                    
                })
             
            }

            function  update_buyer_current(){
                //console.log("buyer live");
                $.ajax({
                    type: "POST",
                    url: base_url + "/buyerupdatebid",
                  
              
                    dataType: "JSON",
                    beforeSend: function(){
                   
                    },
                    success: function(msg) {

                        console.log("buyer live");
                        
                        $.each(msg, function(e, pqlist) {
                         $('#buyer_bid_'+pqlist.bid_id+'').html(pqlist.curent_amount);
                         //$('#buyer_lot_'+pqlist.lot_id+'').html('No.of Acceptance : '+pqlist.accept_count);
                        
                        })
               
                        },
                        complete: function() {
                            // Schedule the next request when the current one's complete
                            setTimeout(update_buyer_current, 2000);
                          }
                       
                    
                })
            }

            function  update_buyer_current_invite_acceptance(){
                //console.log("buyer live");
                $.ajax({
                    type: "POST",
                    url: base_url + "/buyerupdateinviteacceptance",
                    dataType: "JSON",
                    beforeSend: function(){
                   
                    },
                    success: function(msg) {

                        console.log("acceptance live");

                        $.each(msg, function(e, pqlist) {
                            $('#buyer_lot_'+pqlist.lot_id+'').html('No.of Acceptance : '+pqlist.accept_count);
                           
                           })

                        },
                        complete: function() {
                            // Schedule the next request when the current one's complete
                            setTimeout(update_buyer_current_invite_acceptance, 2000);
                          }
                       
                    
                })
            }


        function readPOImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(400)
                        .height(250);

                        // .width(150)
                        // .height(200);
                };
    
                reader.readAsDataURL(input.files[0]);
            }
        }

    function payEMDFunction(lot_id)
    {
        $.ajax({
            type: "POST",
            url: base_url + "/validateWalletAmount",
            data: { lot_id: lot_id },              
            dataType: "JSON",
            beforeSend: function(){
              $('.preloader').show();
            },
            success: function(msg) {
               $('.preloader').hide();

                if(msg['status'] == true)
                {
                    var emd_value = msg['emd_value'];
                    var current_balance = msg['current_balance'];

                    $('#wallet_popup').modal({
                        backdrop: 'static',
                        keyboard: false
                    }) 
                    .one('click', '#confirm-model', function(e) {
                        
                        $.ajax({
                            type: "POST",
                            url: base_url + "/payEmdValue",
                            data:  { lot_id: lot_id, current_balance: current_balance, emd_value: emd_value },                          
                            dataType: "JSON",
                            beforeSend: function(){
                                $('.preloader').show();
                            },
                            success: function(msg) 
                            {
                                $('.preloader').hide();
                                if(msg['status'] == true)
                                {
                                    location.reload();                                   
                                }
                                else
                                {		
                                    iziToast.error({
                                            timeout: 2500,
                                            id: 'error',
                                            title: 'Error',
                                            message: msg['message'],
                                            position: 'topRight',
                                            transitionIn: 'fadeInDown'
                                    });		
                                    //  location.reload();                                            
                                }
                            }
                        })
                    });

                    $('#wallet-text').html('Available Wallet Balance : INR '+msg['current_balance']);
                    $('#emd-text').html(msg['emd_value']);
                    
                    $('#wallet_popup').modal('show');                   
                }
                else{		
                    if(msg['c_code'] == 3)
                    {
                        iziToast.error({
                            timeout: 5000,
                            id: 'error',
                            title: 'Error',
                            message: 'You dont have enough balance in your wallet. Kindly recharge your wallet and try again.',
                            position: 'topRight',
                            transitionIn: 'fadeInDown'
                        });	

                    }
                    else
                    {
                        location.reload();
                    }	                           
                }
            }
        });

    }


    function bidwisePopFunction(lot_id,event_id)
    {
        $('#hfLotId').val(lot_id);
        $('#hfEventId').val(event_id);

        $('#Bid-Export-Report').modal({
            backdrop: 'static',
            keyboard: false
        }) 
        // .one('click', '#confirm-pdf-model', function(e) {

        //     var favorite = [];
        //     $.each($("input[name='biddetail']:checked"), function(){            
        //         favorite.push($(this).val());
        //         alert($(this).val());
        //     });

        //     alert('pdf');
        //     // $.ajax({
        //     //     type: "POST",
        //     //     url: base_url + "/payEmdValue",
        //     //     data:  { lot_id: pea_id_mod, current_balance: current_balance, emd_value: emd_value },                          
        //     //     dataType: "JSON",
        //     //     beforeSend: function(){
        //     //         $('.preloader').show();
        //     //     },
        //     //     success: function(msg) 
        //     //     {
        //     //         $('.preloader').hide();
        //     //         if(msg['status'] == true)
        //     //         {
        //     //             location.reload();                                   
        //     //         }
        //     //         else
        //     //         {		
        //     //             iziToast.error({
        //     //                     timeout: 2500,
        //     //                     id: 'error',
        //     //                     title: 'Error',
        //     //                     message: msg['message'],
        //     //                     position: 'topRight',
        //     //                     transitionIn: 'fadeInDown'
        //     //             });		
        //     //             //  location.reload();                                            
        //     //         }
        //     //     }
        //     // })
        // })
        // .one('click', '#confirm-excel-model', function(e) {
        //     alert('excel');
        //     // $.ajax({
        //     //     type: "POST",
        //     //     url: base_url + "/payEmdValue",
        //     //     data:  { lot_id: pea_id_mod, current_balance: current_balance, emd_value: emd_value },                          
        //     //     dataType: "JSON",
        //     //     beforeSend: function(){
        //     //         $('.preloader').show();
        //     //     },
        //     //     success: function(msg) 
        //     //     {
        //     //         $('.preloader').hide();
        //     //         if(msg['status'] == true)
        //     //         {
        //     //             location.reload();                                   
        //     //         }
        //     //         else
        //     //         {		
        //     //             iziToast.error({
        //     //                     timeout: 2500,
        //     //                     id: 'error',
        //     //                     title: 'Error',
        //     //                     message: msg['message'],
        //     //                     position: 'topRight',
        //     //                     transitionIn: 'fadeInDown'
        //     //             });		                                         
        //     //         }
        //     //     }
        //     // })
        // });
    }

    $("input[name='pea_emd_check']").click(function(){
        var radioValue = $("input[name='pea_emd_check']:checked").val();
        if(radioValue){
            if(radioValue == 1)
            {
                $('#pea_event_emd_value').show();
                $('.emd-div').show();
            }
            else
            {
                $('#pea_event_emd_value').hide();
                $('.emd-div').hide();
            }
        }
    });

    function bidwiseHighPopFunction(lot_id, event_id)
    {
        $('#hfLotId').val(lot_id);
        $('#hfEventId').val(event_id);

        $('#Bid-Export-Report').modal({
            backdrop: 'static',
            keyboard: false
        })        
    }


    function bidwiseOverallPopFunction(event_id, event_type)
    {
        $('#hfEventIt').val(event_id);
        $('#hfEventType').val(event_type);

        if(event_type == 1)
        {
            $('#lotdetail').each(function(){
                if ($(this).is(':enabled')) {
                    $('#lotdetail[value="9"]').not(this).prop('disabled', 'disabled');
                    $('#lotdetail[value="10"]').not(this).prop('disabled', 'disabled');
                    $('#lotdetail[value="11"]').not(this).prop('disabled', 'disabled');
                }
            });
        }
        else
        {
            $('#lotdetail').each(function(){                
                $('#lotdetail[value="9"]').removeAttr("disabled");
                $('#lotdetail[value="10"]').removeAttr("disabled");
                $('#lotdetail[value="11"]').removeAttr("disabled");                
            });
        }

        $.each($("input[id='lotdetail']"), function(){            
            $(this).prop("checked", false);            
        });
        
        $('#Bid-Overall-Export-Report').modal({
            backdrop: 'static',
            keyboard: false
        }) 
    }

    $('#confirm-bid-lot-cancel-model').click(function()
    {
        $('#Bid-Overall-Export-Report').modal('hide');
    });

    $('#confirm-bid-cancel-model').click(function()
    {
        $('#Bid-Export-Report').modal('hide');
    });

    $('#confirm-high-bid-cancel-model').click(function()
    {
        $('#High-Bid-Export-Report').modal('hide');
    });

    $('#select_all').click(function()
    {
        $.each($("input[id='biddetail']"), function(){            
            $(this).prop("checked", true);            
        });
        
    });
    
    $('#all_select_all').click(function()
    {
        var event_type = $('#hfEventType').val();

        if(event_type == 2)
        {
            $.each($("input[id='lotdetail']"), function(){            
                $(this).prop("checked", true);            
            });
        }
        else
        {            
            $.each($("input[id='lotdetail']"), function(){
                $(this).prop("checked", true);    
                $('#lotdetail[value="9"]').prop("checked", false); 
                $('#lotdetail[value="10"]').prop("checked", false); 
                $('#lotdetail[value="11"]').prop("checked", false); 
            });
        }
        
    });

    $('#all_unselect_all').click(function()
    {
        $.each($("input[id='lotdetail']"), function(){            
            $(this).prop("checked", false);            
        });
        
    });    

    $('#unselect_all').click(function()
    {
        $.each($("input[id='biddetail']"), function(){            
            $(this).prop("checked", false);            
        });
        
    });

$(document).ready(function(){

   // $('#confirm-cred').hide();

    $('.pay-seller-money').click(function()
    {
        var current_bid_id = $(this).attr('id');
        var accepted_amount = $(this).attr('data-bidid');
        var lot_id = $(this).attr('data-lot-id');

        $.ajax({
            type: "POST",
            url: base_url + "/validateWalletAmount",
            data: { lot_id: lot_id, current_bid_id: current_bid_id, accepted_amount: accepted_amount  },              
            dataType: "JSON",
            beforeSend: function(){
              $('.preloader').show();
            },
            success: function(msg) {
               $('.preloader').hide();

                if(msg['status'] == true)
                {                   
                    var current_balance = msg['current_balance'];
                    var total_amount = parseInt(accepted_amount)+parseInt(msg['total_emd']);

                    $('#wallet_popup').modal({
                        backdrop: 'static',
                        keyboard: false
                    }) 
                    .one('click', '#confirm-model', function(e) {

                        if(parseInt(current_balance) >= total_amount)
                        {
                            $.ajax({
                                type: "POST",
                                url: base_url + "/payToSeller",
                                data:  { lot_id: lot_id, current_balance: current_balance, accepted_amount: accepted_amount, current_bid_id: current_bid_id },                          
                                dataType: "JSON",
                                beforeSend: function(){
                                    $('.preloader').show();
                                },
                                success: function(msg) 
                                {
                                    $('.preloader').hide();
                                    if(msg['status'] == true)
                                    {
                                        location.reload();                                   
                                    }
                                    else
                                    {		
                                        iziToast.error({
                                                timeout: 2500,
                                                id: 'error',
                                                title: 'Error',
                                                message: msg['message'],
                                                position: 'topRight',
                                                transitionIn: 'fadeInDown'
                                        });		
                                        //  location.reload();                                            
                                    }
                                }
                            })
                        }
                        else
                        {
                            alert('Not enough amount available in your wallet');
                        }                        
                        
                    });                    

                    $('#emd-return').html('Total EMD to return : INR '+msg['total_emd']);
                    $('#total-pay').html('Total Amount deduct from your wallet : INR '+total_amount);
                    $('#wallet-text').html('Available Wallet Balance : INR '+msg['current_balance']);
                    $('#emd-text').html(accepted_amount);
                    
                    $('#wallet_popup').modal('show');                   
                }
                else{		
                    if(msg['c_code'] == 3)
                    {
                        iziToast.error({
                            timeout: 5000,
                            id: 'error',
                            title: 'Error',
                            message: 'You dont have enough balance in your wallet. Kindly recharge your wallet and try again.',
                            position: 'topRight',
                            transitionIn: 'fadeInDown'
                        });	

                    }
                    else
                    {
                        location.reload();
                    }	                           
                }
            }
        });
    });

    $('.invoice-seller').click(function() {
        var auctionId = $(this).attr('id');
        var accepted_amount = $(this).attr('data-bidid');
        var seller_id = $(this).attr('data-sellerid');
        var lotItemId = $(this).attr('data-lot-id');

        $.ajax({
            type: "POST",
            url: base_url + "/winnerInvoice",
            data: { auctionId: auctionId, lotItemId: lotItemId, accepted_amount: accepted_amount, seller_id: seller_id  },              
            dataType: "JSON",
            beforeSend: function(){
              $('.preloader').show();
            },
            success: function(msg) {
               $('.preloader').hide();

                if(msg['status'] == true)
                {                   
                    iziToast.success({
                        timeout: 2500,
                        id: 'success',
                        title: 'Success',
                        message: msg['message'],
                        position: 'topRight',
                        transitionIn: 'fadeInDown',
                        onOpened: function(instance, toast){
                            location.reload();	
                        },
                    });                 
                }
                else{		
                    iziToast.error({
                        timeout: 5000,
                        id: 'error',
                        title: 'Error',
                        message: msg['message'],
                        position: 'topRight',
                        transitionIn: 'fadeInDown'
                    });	                         
                }
            }
        });

    });

    var event_id = $('#bid_pec_event_name').val();
    if(event_id)
    {
        $('#bid_pec_lot_name').show();
    }
    else
    {
        $('#bid_pec_lot_name').hide();
    }

    // var comp_id = $('#pec_company_name').val();
    // if(comp_id)
    // {
    //     $('#pec_event_name').show();
    // }
    // else
    // {
    //     $('#pec_event_name').hide();
    // }

    $('#bid_pec_event_name').change(function()
    {
        var event_id = $('#bid_pec_event_name').val();
       
        $('#bid_pec_lot_name').show();

        $.ajax({
            type: "POST",
            url: base_url + "/eventLotData",
            data: { event_id: event_id },              
            dataType: "JSON",
            beforeSend: function(){
              $('.preloader').show();
            },
            success: function(msg) {
               
               $('.preloader').hide();

                if(msg['status'] == true)
                {    
                    $("#bid_pec_lot_name").empty();
                    $("#bid_pec_lot_name").append($("<option></option>").val(0).html('-- ALL --'));
                    $.each(msg['message'], function (key, value) {  
                        $("#bid_pec_lot_name").append($("<option></option>").val(value.pea_id).html(value.pea_event_spec));  
                    });                
                }
                else{		
                                            
                }
            }
        });
    });

    $('#pec_company_name').change(function()
    {
        var comp_id = $('#pec_company_name').val();

        $('#pec_event_name').show();
       
        $.ajax({
            type: "POST",
            url: base_url + "/sellerEventData",
            data: { comp_id: comp_id },              
            dataType: "JSON",
            beforeSend: function(){
              $('.preloader').show();
            },
            success: function(msg) {
               
               $('.preloader').hide();

                if(msg['status'] == true)
                {    
                    $("#pec_event_name").empty();
                    $("#pec_event_name").append($("<option></option>").val(0).html('-- ALL --'));
                    $.each(msg['message'], function (key, value) {  
                        $("#pec_event_name").append($("<option></option>").val(value.pec_sno).html(value.pec_event_id+' - '+value.pec_event_name+' [ '+value.pec_event_start_dt+'-'+value.pec_event_end_dt+' ] '));  
                    });                
                }
                else{		
                                            
                }
            }
        });
    });


$(".changeCategory").on("change", function() {
 
    var categories = $(this).val();
    var cat_type= $("#com_cat_type").val();

        $.ajax({
            type: "POST",
            url: base_url + "/getSubproduct",
            data: {
                cat: categories,
                cat_type:cat_type
            },
            dataType: "JSON",
            beforeSend: function(){
                $('.preloader').show();
                },
            success: function(e) {
                    $('.preloader').hide();
                    $("#pms_subcat_name").empty();
                    $.each(e, function(e, t) {
                        $("#pms_subcat_name ").append($("<option></option>").attr("value", t.pms_id).text(t.pms_subcat_name))
                    })
            }
        })        
  })

	
 $('body').on('click', '.btn-edit-row', function() {
     
        var event_type = $(this).attr('data-event-type');

		var pea_id = $(this).closest('tr').attr('id');
		var pea_id_mod = $(this).attr('data-id');
        var cat = $(this).attr('data-cat-id');
        var market_id = $(this).attr('data-market-id');
        sub_cat = $(this).attr('data-subcat-id');
        var unit = $(this).attr('data-unit-id');
        var currentRow = $(this).closest("tr"); 
        var pmc_market_name = currentRow.find("td:eq(0)").html();
		var pmc_cat_name = currentRow.find("td:eq(1)").html();
		var pms_subcat_name = currentRow.find("td:eq(2)").html();
		var pea_event_start_dt = currentRow.find("td:eq(3)").html();
		var pea_event_end_dt = currentRow.find("td:eq(4)").html();
		var pea_event_spec = currentRow.find("td:eq(5)").html();
		var pmu_unit_name = currentRow.find("td:eq(6)").html();
        var pea_event_unit_quantity = currentRow.find("td:eq(7)").html();		
        
        if(event_type == 2)
        {
            var pea_event_start_price = currentRow.find("td:eq(8)").html();
            var pea_event_max_dec = currentRow.find("td:eq(9)").html();
            var pea_event_reserve_price = currentRow.find("td:eq(10)").html();
            var pea_event_location = currentRow.find("td:eq(11)").html();
        }
        else
        {
            var pea_event_location = currentRow.find("td:eq(8)").html();
        }
		
		//$('#pmc_id').attr('id', pea_id);
		$('#pmc_id').val(pea_id);
        //$('#pmc_cat_name').val(pmc_cat_name);
        //$("#pmc_cat_name option[value="+cat+"]").prop("selected",true);

        $('#pmc_market_name').val(pmc_market_name);
        $("#pmc_market_name option[value="+market_id+"]").prop("selected",true);
   
        var cat_type= $("#com_cat_type").val();
        $.ajax({
            type: "POST",
            url: base_url + "/getCategories",
            data: {
                market: market_id,
                cat_type:cat_type
            },
            dataType: "JSON",
            beforeSend: function(){
                $('.preloader').show();
            },
            success: function(e) {
                $('.preloader').hide();
                $("#pmc_cat_name").empty();
                $("#pmc_cat_name").append("<option value='0'>-- Choose Your Categories --</option>")
                $.each(e, function(e, t) {
                    $("#pmc_cat_name ").append($("<option></option>").attr("value", t.pmca_id).text(t.pmca_cat_name))
                })
                $('#pmc_cat_name').val(pmc_cat_name);
                $("#pmc_cat_name option[value="+cat+"]").prop("selected",true);
            }
        })

        $.ajax({
            type: "POST",
            url: base_url + "/getSubproduct",
            data: {
                cat: cat,
                cat_type:cat_type
            },
            dataType: "JSON",
            beforeSend: function(){
                $('.preloader').show();
                },
            success: function(e) {
                    $('.preloader').hide();
                    $("#pms_subcat_name").empty();
                    $.each(e, function(e, t) {
                        $("#pms_subcat_name ").append($("<option></option>").attr("value", t.pms_id).text(t.pms_subcat_name))
                    })
                    $('#pms_subcat_name').val(pms_subcat_name);
                    $("#pms_subcat_name option[value="+sub_cat+"]").prop("selected",true);
            }
        })

        $('#pmc_cat_name').val(cat).trigger('change');
        $("#pmu_unit_name option[value="+unit+"]").prop("selected",true);
        $('#pea_event_spec').val(pea_event_spec);
        $('#pea_event_start_dt').val(pea_event_start_dt);
        $('#pea_event_end_dt').val(pea_event_end_dt);
          
        $('#pea_event_unit_quantity').val(pea_event_unit_quantity);
        //$("#pea_event_location option[value="+pea_event_location+"]").prop("selected",true);
        //$('#pea_event_location').val(pea_event_location);

        if(event_type == 2)
        {
            $('#pea_event_reserve_price').val(pea_event_reserve_price);
            $('#pea_event_start_price').val(pea_event_start_price);
            $('#pea_event_max_dec').val(pea_event_max_dec);
        }
        else
        {
            $('#pea_event_reserve_price').hide();
            $('#pea_event_start_price').hide();
            $('#pea_event_max_dec').hide();
        }
        $('#pea_event_location').val(pea_event_location).change();		
	
	});
	
	
	 $('body').on('click', '.own_vender', function() {
         
        $('.inviteSu').show();
        $('.success_val').hide();
        $('.alert-mail').remove();
        $('form').trigger("reset");
		var pea_id = $(this).closest('tr').attr('id');
		var pea_id_mod = $(this).attr('data-id');
		var event_name = $(this).attr('data-event_name');
		var event_start_dt = $(this).attr('data-event_start_dt');
		var event_end_dt = $(this).attr('data-event_end_dt');
        var term_condition = $(this).attr('data-term_condition');
        var sellerid = $(this).attr('data-seller');
        var event_type = $(this).attr('data-event');
        
        var currentRow = $(this).closest("tr"); 
        var pmc_market_name = currentRow.find("td:eq(0)").html();
		var pmc_cat_name = currentRow.find("td:eq(1)").html();
		var pms_subcat_name = currentRow.find("td:eq(2)").html();
		var pea_event_spec = currentRow.find("td:eq(3)").html();
		var pmu_unit_name = currentRow.find("td:eq(4)").html();
		var pea_event_unit_quantity = currentRow.find("td:eq(5)").html();
		var pea_event_location = currentRow.find("td:eq(6)").html();
		var pea_event_start_price = currentRow.find("td:eq(7)").html();
		var pea_event_max_dec = currentRow.find("td:eq(8)").html();
				
		
		//$('#pmc_id').attr('id', pea_id);
        $('#pmc1_id').val(pea_id);
        $('#market_name').val(pmc_market_name);
		$('#cat_name').val(pmc_cat_name);
		$('#subcat_name').val(pms_subcat_name);
		$('#event_spec').val(pea_event_spec);
		$('#unit_name').val(pmu_unit_name);
		$('#event_unit_quantity').val(pea_event_unit_quantity);
		$('#event_location').val(pea_event_location);
        $('#event_start_price').val(pea_event_start_price);
        $('#event_type').val(event_type);
        
		$('#event_max_dec').val(pea_event_max_dec);
		$('#pec_event_name').val(event_name);
		$('#pec_event_start_dt').val(event_start_dt);
        $('#pec_event_end_dt').val(event_end_dt);
        $('#pea_event_terms_condition').val(term_condition);
        $('#pec_loginID').val(sellerid);
        
		$('.invite_pqemails').val('');
		$('.invite_emails').val('');
	
	});
	
	$('body').on('click', '.pq_vender', function() {
        $('.invited-newlist').empty();
        var pea_id = $(this).closest('tr').attr('id');
        var cat_id = $(this).closest('tr').attr('date-catid');
		var pea_id_mod = $(this).attr('data-id');
		var event_name = $(this).attr('data-event_name');
		var event_start_dt = $(this).attr('data-event_start_dt');
        var event_end_dt = $(this).attr('data-event_end_dt');
        var sellerid = $(this).attr('data-seller');
        
		
		var currentRow = $(this).closest("tr"); 
		var pmc_cat_name = currentRow.find("td:eq(0)").html();
		var pms_subcat_name = currentRow.find("td:eq(1)").html();
		var pea_event_spec = currentRow.find("td:eq(4)").html();
		var pmu_unit_name = currentRow.find("td:eq(5)").html();
		var pea_event_unit_quantity = currentRow.find("td:eq(6)").html();
		var pea_event_location = currentRow.find("td:eq(7)").html();
		var pea_event_start_price = currentRow.find("td:eq(8)").html();
		var pea_event_max_dec = currentRow.find("td:eq(9)").html();
				
		
		//$('#pmc_id').attr('id', pea_id);
        $('#pmc1_id1').val(pea_id);
        $('#cat_id1').val(cat_id);
        
		$('#cat_name1').val(pmc_cat_name);
		$('#subcat_name1').val(pms_subcat_name);
		$('#event_spec1').val(pea_event_spec);
		$('#unit_name1').val(pmu_unit_name);
		$('#event_unit_quantity1').val(pea_event_unit_quantity);
		$('#event_location1').val(pea_event_location);
		$('#event_start_price1').val(pea_event_start_price);
		$('#event_max_dec1').val(pea_event_max_dec);
		$('#pec_event_name1').val(event_name);
		$('#pec_event_start_dt1').val(event_start_dt);
        $('#pec_event_end_dt1').val(event_end_dt);
        $('#pec_loginID').val(sellerid);
		
		$('.invite_pqemails').val('');
		$('.invite_emails').val('');
        
	
		 $.ajax({
              type: "POST",
              url: base_url+'/getPqvendor',
              data: { category_id : cat_id,location : pea_event_location,lotid:pea_id},
              dataType: 'JSON',			  
			  beforeSend: function(){
				  $('.preloader').show();
			  },
              success: function( msg ) {
               //console.log(msg);
			   $('.preloader').hide();
               if(msg.length>0){
                html='';
               
               html+='<table class="table table-striped" id="pq_vendor_select">';
               html+='<thead>';
               html+='	<tr>';
               html+='		<th>Select</th>';
               html+='		<th>Company</th>';
               html+='		<th>Email</th>';
               html+='		<th>Rating</th>';
               html+='	</tr>';
               html+='</thead>';
               html+='<tbody>';
               $.each(msg, function(e, pqlist) {
               html+='		<tr>';
               html+='		<td id="pq_company_id">';
               html+='			<label class="check-vendor">';
			   html+='				<input class="pq_vendor_email" type="checkbox" name="is_name'+pqlist.pmv_id+'" value='+pqlist.pmv_comp_email+'>';
               html+='				<span class="check-checkmark"></span>';
               html+='			</label>';
               html+='		</td>';
               html+='		<td id="pq_company">'+pqlist.pmv_comp_name+'</td>';
               html+='		<td id="pq_company_email">'+pqlist.pmv_comp_email+'</td>';
               html+='		<td class="vendor-star-rate">';
               html+='			<i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>';
               html+='		</td>';
               html+='	</tr>';
                });	
               html+='	</tbody>';
               html+='</table>';
               html+= '<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">';
               html+='<input type="submit" class="btn btn-purple" id="pq-vendor-invite" name="SUBMIT">';
               html+='</div>';
               html+='<input type="hidden" class="form-control invite_pqemails" id="invite_pqemails" name="pei_invite_details1">';
               $('.invited-newlist').append(html);	
               /*
				   iziToast.success({
						timeout: 2500,
						id: 'success',
						title: 'Success',
						message: 'Thanks for signing up for Purchase Quick. <br/> Please verify your email address.',
						position: 'bottomRight',
						transitionIn: 'bounceInLeft'						
					});
				$('form').trigger("reset");*/
                }
                else{
				/*	iziToast.error({
						timeout: 3000,
						id: 'error',
						title: 'Error',
						message: 'The email address you have entered is already registered.',
						position: 'topRight',
						transitionIn: 'fadeInDown'
                    });*/
                    html="<p class='no-vendor'>No PQ vendor Available</p> "
                    $('.invited-newlist').append(html);	
                }
          
          }
          });
	
	});
	


           $('body').on('click','.pq_vendor_email',function(){ 
			
			
			var array_values = [];
			$('input[type=checkbox]').each( function() {
				if( $(this).is(':checked') ) {
					array_values.push( $(this).val() );
				}
			});
			var arrayValues = array_values.join(',');
		
				
				 $('.invite_pqemails').val(arrayValues)
				 $('.invite_emails').val(arrayValues)
			  
          

        });
		

	$('body').on('click','.btn-close-mail ,.btn-close',function(){
		
		$('.invite_pqemails').val('');
		$('.invite_emails').val('');
		
    });
   
     
    $('.datetimes1').on('apply.daterangepicker', function(ev, picker) {
       $('#event-detail-form').valid();
      });
     
	$('body').on('click','#pq-vendor-invite',function(){
	
		  $.ajax({
              type: "POST",
              url: base_url+'/inviteSupplier',
              data: { pmc_cat_nam: $('#cat_name1').val(), pms_subcat_name: $('#subcat_name1').val(), pea_event_spec: $('#event_spec1').val(), pmu_unit_name: $('#unit_name1').val(), pea_event_unit_quantity: $('#event_unit_quantity1').val(), pea_event_start_price: $('#event_start_price1').val(), pea_event_max_dec: $('#event_max_dec1').val(), pea_event_location: $('#event_location1').val(), pmc_id: $('#pmc1_id1').val(), pec_event_name: $('#pec_event_name1').val(), pec_event_start_dt: $('#pec_event_start_dt1').val(), pec_event_end_dt: $('#pec_event_end_dt1').val(), pec_loginID: $('#pec_loginID').val(), pea_event_terms_condition: $('#pea_event_terms_condition').val(), pei_invite_details: $('#pei_invite_details1').val(), pei_invite_details1: $('#pei_invite_details1').val() ,join : "1"},
              dataType: 'JSON',
			  beforeSend: function(){
				  $('.preloader').show();
			  },
              success: function( msg ) {
               //console.log(msg);
               if(msg['status']==true){
				$('.preloader').hide();
                // $("#status").html('<div class="alert-mail alert alert-success">'+"Event Invited success fully."+'</div>');
				// $('.inviteSu').hide();
				// $('.success_val').show();
				iziToast.success({
							timeout: 2500,
							id: 'success',
							title: 'Success',
							message: 'Event Invited successfully',
							position: 'bottomRight',
							transitionIn: 'bounceInLeft',
							onOpened: function(instance, toast){
								location.reload();	
							},
							onClosed: function(instance, toast, closedBy){		
								
								$('.preloader').hide();
								console.info('closedBy: ' + closedBy);
							}
							
						});
                }
                else{
					 iziToast.error({
							timeout: 3000,
							id: 'error',
							title: 'Error',
							message: 'Please Enter Any your email address',
							position: 'topRight',
							transitionIn: 'fadeInDown'
						});
					$('.preloader').hide();
                //$("#status").html('<div class="alert-mail alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"Please Enter Any your email address."+'</div>');
				//$('.inviteSu').hide();
				//$('.success_val').show(); 
                }
          
          }
          });

	});	
	
	
  $('.success_val').hide();
  
	$("#inviteSupp").validate({
	
  rules: {
    // simple rule, converted to {required:true}
		pei_invite_details: {
			email: true
		}    
    },

  submitHandler: function(form) {
  
  var formdate = $('#inviteSupp').serializeArray();
  
  //console.log(formdate);
	
        //return false;
          $.ajax({
              type: "POST",
              url: base_url+'/inviteSupplier',
              data: formdate,
              dataType: 'JSON',
			  beforeSend: function(){
				  $('.preloader').show();
			  },
              success: function( msg ) {
               //console.log(msg);
               if(msg['status']==true){
				$('.preloader').hide();
                // $("#status").html('<div class="alert-mail alert alert-success">'+"Event Invited success fully."+'</div>');
				// $('.inviteSu').hide();
				// $('.success_val').show();
				iziToast.success({
							timeout: 2500,
							id: 'success',
							title: 'Success',
							message: 'Event Invited successfully',
							position: 'bottomRight',
							transitionIn: 'bounceInLeft',
							onOpened: function(instance, toast){
								location.reload();	
							},
							onClosed: function(instance, toast, closedBy){		
								
								$('.preloader').hide();
								console.info('closedBy: ' + closedBy);
							}
							
						});
                }
                else{
					 iziToast.error({
							timeout: 3000,
							id: 'error',
							title: 'Error',
							message: 'Please Enter Any your email address',
							position: 'topRight',
							transitionIn: 'fadeInDown'
						});
					$('.preloader').hide();
                //$("#status").html('<div class="alert-mail alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"Please Enter Any your email address."+'</div>');
				//$('.inviteSu').hide();
				//$('.success_val').show(); 
                }
          
          }
          });

  }

});

$("#add-bank-info-form").validate({	
   
    rules: {
        pbd_name: {
            required: true
        },
        pbd_email : {
            required: true
        },
        pbd_phone : {
            required: true
        },
        pbd_bank_account : {
            required: true
        },
        pbd_ifsc : {
            required: true
        },
        pbd_address1 : {
            required: true
        },
        pbd_city: {
            required: true
        },
        pbd_state: {
            required: true
        },
        pbd_pin: {
            required: true
        }
      
    },
  submitHandler: function(form) {
  
    var formdate=$('#add-bank-info-form').serializeArray();
      
       // return false;
          $.ajax({
              type: "POST",
              url: base_url+'/addNewBank',
              data: formdate,
              dataType: 'JSON',
              beforeSend: function(){
                    $('.preloader').show();
                },
                success: function( msg ) {				
                      
                    if(msg['status']==true)
                    {
                      $('.preloader').hide();
                      
                      iziToast.success({
                          timeout: 2500,
                          id: 'success',
                          title: 'Success',
                          message: msg['message'],
                          position: 'bottomRight',
                          transitionIn: 'bounceInLeft',
                          onOpened: function(instance, toast){
                            window.location.href	=	base_url+'/list-bank/'; 
                        },						
                      });
                      $('#force_reset_password').modal('hide');
                      //window.location = base_url;
                   
                    }
                    else{
                       $('.preloader').hide();
                       iziToast.error({
                          timeout: 3000,
                          id: 'error',
                          title: 'Error',
                          message: msg['message'],
                          position: 'topRight',
                          transitionIn: 'fadeInDown'
                      });
                    }
          
                }
        });
    }

});

$("#add-address-info-form").validate({	
   
    rules: {
        psa_address_line1: {
            required: true,
        },       
        psa_state : {
            required: true
        },
        psa_city : {
            required: true
        },
        psa_pincode : {
            required: true
        },
        pbd_country : {
            required: true
        }      
    },
  submitHandler: function(form) {
  
    var formdate=$('#add-address-info-form').serializeArray();
      
       // return false;
          $.ajax({
              type: "POST",
              url: base_url+'/addAddress',
              data: formdate,
              dataType: 'JSON',
              beforeSend: function(){
                    $('.preloader').show();
                },
                success: function( msg ) {				
                      
                    if(msg['status']==true)
                    {
                      $('.preloader').hide();
                      
                      iziToast.success({
                          timeout: 2500,
                          id: 'success',
                          title: 'Success',
                          message: msg['message'],
                          position: 'bottomRight',
                          transitionIn: 'bounceInLeft',
                          onOpened: function(instance, toast){
                            window.location.href	=	base_url+'/list-address/'; 
                        },						
                      });
                    }
                    else{
                       $('.preloader').hide();
                       iziToast.error({
                          timeout: 3000,
                          id: 'error',
                          title: 'Error',
                          message: msg['message'],
                          position: 'topRight',
                          transitionIn: 'fadeInDown'
                      });
                    }
                }
        });
    }

});


$("#update-address-info-form").validate({	
   
    rules: {
        psa_address_line1: {
            required: true,
        },       
        psa_state : {
            required: true
        },
        psa_city : {
            required: true
        },
        psa_pincode : {
            required: true
        },
        pbd_country : {
            required: true
        }      
    },
  submitHandler: function(form) {
  
    var formdate=$('#update-address-info-form').serializeArray();
      
       // return false;
          $.ajax({
              type: "POST",
              url: base_url+'/updateAddress',
              data: formdate,
              dataType: 'JSON',
              beforeSend: function(){
                    $('.preloader').show();
                },
                success: function( msg ) {				
                      
                    if(msg['status']==true)
                    {
                      $('.preloader').hide();
                      
                      iziToast.success({
                          timeout: 2500,
                          id: 'success',
                          title: 'Success',
                          message: msg['message'],
                          position: 'bottomRight',
                          transitionIn: 'bounceInLeft',
                          onOpened: function(instance, toast){
                            window.location.href	=	base_url+'/list-address/'; 
                        },						
                      });
                    }
                    else{
                       $('.preloader').hide();
                       iziToast.error({
                          timeout: 3000,
                          id: 'error',
                          title: 'Error',
                          message: msg['message'],
                          position: 'topRight',
                          transitionIn: 'fadeInDown'
                      });
                    }
                }
        });
    }

});

$(".delete_address").click(function() {	
   
    var addressId=  $(this).attr('data-id');
      
       // return false;
          $.ajax({
              type: "POST",
              url: base_url+'/deleteAddress',
              data: {'addressId':addressId},
              dataType: 'JSON',
              beforeSend: function(){
                    $('.preloader').show();
                },
                success: function( msg ) {				
                      
                    if(msg['status']==true)
                    {
                      $('.preloader').hide();
                      
                      iziToast.success({
                          timeout: 2500,
                          id: 'success',
                          title: 'Success',
                          message: msg['message'],
                          position: 'bottomRight',
                          transitionIn: 'bounceInLeft',
                          onOpened: function(instance, toast){
                            location.reload();
                            },							
                      });
                                         
                    }
                    else{
                       $('.preloader').hide();
                       iziToast.error({
                          timeout: 3000,
                          id: 'error',
                          title: 'Error',
                          message: msg['message'],
                          position: 'topRight',
                          transitionIn: 'fadeInDown'
                      });
                    }
          
                }
        });
    

});

$("#psa_state").on("change", function() {
 
    var state_id = $("#psa_state").val();

    $.ajax({
        type: "POST",
        url: base_url + "/getLocations",
        data: {
            state_id: state_id
        },
        dataType: "JSON",
        beforeSend: function(){
            $('.preloader').show();
          },
        success: function(e) {
              $('.preloader').hide();
              $("#psa_city").empty();
              $("#psa_city").append("<option value='0'>-- Choose Your Cities --</option>")
              $.each(e, function(e, t) {
              $("#psa_city ").append($("<option></option>").attr("value", t.pmci_city_id).text(t.pmci_city_name))
            })
        }
    })
  })


$("#confirm-bank").validate({	
    rules: {
        radioBeneId: {
            required: true
        },              
    },
  submitHandler: function(form) {
  
    var formdate=$('#confirm-bank').serializeArray();
      
       // return false;
          $.ajax({
              type: "POST",
              url: base_url+'/confirmBank',
              data: formdate,
              dataType: 'JSON',
              beforeSend: function(){
                    $('.preloader').show();
                },
                success: function( msg ) {				
                      
                    if(msg['status']==true)
                    {
                        window.location.href	=	base_url+'/accept-bank/'+msg['id']+'/'+msg['bene_id'];
                    }
                    else{
                       $('.preloader').hide();
                       iziToast.error({
                          timeout: 3000,
                          id: 'error',
                          title: 'Error',
                          message: msg['message'],
                          position: 'topRight',
                          transitionIn: 'fadeInDown'
                      });

                    }          
                }
        });
    }

});

$("#debit-amount").validate({	
    rules: {
        pbd_amount: {
            required: true
        },   
        login_username: {
            required: true
        },
        login_password: {
            required: true
        }
    },
  submitHandler: function(form) {
  
    var formdate=$('#debit-amount').serializeArray();

        var enter_amount = parseInt($('#pbd_amount').val());
        var balance_amount = parseInt($('#balance').val());

        if(enter_amount > balance_amount)
        {
            alert('Enter the valid amount');
            return false;
        }
        else
        {
            $.ajax({
                type: "POST",
                url: base_url+'/debitAmount',
                data: formdate,
                dataType: 'JSON',
                beforeSend: function(){
                        $('.preloader').show();
                    },
                    success: function( msg ) {				
                        
                        if(msg['status']==true)
                        {
                            $('.preloader').hide();
                            
                            iziToast.success({
                                timeout: 2500,
                                id: 'success',
                                title: 'Success',
                                message: msg['message'],
                                position: 'bottomRight',
                                transitionIn: 'bounceInLeft',
                                onOpened: function(instance, toast){
                                    window.location.href	=	base_url+'/wallet/'; 
                                },							
                            });
                            
                            //location.reload();
                                              
                        }
                        else
                        {
                            $('.preloader').hide();
                            iziToast.error({
                                timeout: 3000,
                                id: 'error',
                                title: 'Error',
                                message: msg['message'],
                                position: 'topRight',
                                transitionIn: 'fadeInDown'
                            });
                        }
            
                    }
            });
        }
    }

});

$("#delete-bank").validate({	
    rules: {
        radioBeneId: {
            required: true
        },              
    },
  submitHandler: function(form) {
  
    var formdate=$('#delete-bank').serializeArray();
      
       // return false;
          $.ajax({
              type: "POST",
              url: base_url+'/deleteBank',
              data: formdate,
              dataType: 'JSON',
              beforeSend: function(){
                    $('.preloader').show();
                },
                success: function( msg ) {				
                      
                    if(msg['status']==true)
                    {
                      $('.preloader').hide();
                      
                      iziToast.success({
                          timeout: 2500,
                          id: 'success',
                          title: 'Success',
                          message: msg['message'],
                          position: 'bottomRight',
                          transitionIn: 'bounceInLeft',
                          onOpened: function(instance, toast){
                            location.reload();
                            },							
                      });
                      
                      location.reload();
                   
                    }
                    else{
                       $('.preloader').hide();
                       iziToast.error({
                          timeout: 3000,
                          id: 'error',
                          title: 'Error',
                          message: msg['message'],
                          position: 'topRight',
                          transitionIn: 'fadeInDown'
                      });
                    }
          
                }
        });
    }

});

$("#bank-info-form").validate({	
   
    rules: {
        pbd_name: {
            required: true
        },
        pbd_email : {
            required: true
        },
        pbd_phone : {
            required: true
        },
        pbd_bank_account : {
            required: true
        },
        pbd_ifsc : {
            required: true
        },
        pbd_address1 : {
            required: true
        },
        pbd_city: {
            required: true
        },
        pbd_state: {
            required: true
        },
        pbd_pin: {
            required: true
        }
      
    },
  submitHandler: function(form) {
  
    var formdate=$('#bank-info-form').serializeArray();
      
       // return false;
          $.ajax({
              type: "POST",
              url: base_url+'/addBankInfo',
              data: formdate,
              dataType: 'JSON',
              beforeSend: function(){
                    $('.preloader').show();
                },
                success: function( msg ) {				
                      
                    if(msg['status']==true)
                    {
                      $('.preloader').hide();
                      
                      iziToast.success({
                          timeout: 2500,
                          id: 'success',
                          title: 'Success',
                          message: msg['message'],
                          position: 'bottomRight',
                          transitionIn: 'bounceInLeft'							
                      });
                      $('#force_reset_password').modal('hide');
                      //window.location = base_url;
                   
                    }
                    else{
                       $('.preloader').hide();
                       iziToast.error({
                          timeout: 3000,
                          id: 'error',
                          title: 'Error',
                          message: msg['message'],
                          position: 'topRight',
                          transitionIn: 'fadeInDown'
                      });
                    }
          
                }
        });
    }

});

$("#bank-confirm").validate({	
   
    rules: {
              
    },
  submitHandler: function(form) {
  
    var formdate=$('#bank-confirm').serializeArray();
      
       // return false;
          $.ajax({
              type: "POST",
              url: base_url+'/confirmBank',
              data: formdate,
              dataType: 'JSON',
              beforeSend: function(){
                    $('.preloader').show();
                },
                success: function( msg ) {				
                      
                    if(msg['status']==true)
                    {
                      $('.preloader').hide();
                      
                      iziToast.success({
                          timeout: 2500,
                          id: 'success',
                          title: 'Success',
                          message: msg['message'],
                          position: 'bottomRight',
                          transitionIn: 'bounceInLeft'							
                      });
                      $('#force_reset_password').modal('hide');
                      //window.location = base_url;
                   
                    }
                    else{
                       $('.preloader').hide();
                       iziToast.error({
                          timeout: 3000,
                          id: 'error',
                          title: 'Error',
                          message: msg['message'],
                          position: 'topRight',
                          transitionIn: 'fadeInDown'
                      });
                    }
          
                }
        });
    }

});

$("#force_reset").validate({
	
   
      rules: {
        new_password: "required",
        confirm_password: {
          equalTo: "#new_password"
        }
      },
    submitHandler: function(form) {
    
    var formdate = $('#new_password').val();
    
    var email =$('#email').val();
      
         // return false;
            $.ajax({
                type: "POST",
                url: base_url+'/restpass',
                data: {'password':formdate,'email':email},
                dataType: 'JSON',
                beforeSend: function(){
                    $('.preloader').show();
                  },
                  success: function( msg ) {				
                        
                      if(msg['status']==true)
                      {
                        $('.preloader').hide();
                        
						iziToast.success({
							timeout: 2500,
							id: 'success',
							title: 'Success',
							message: 'Password Reset Successfully',
							position: 'bottomRight',
							transitionIn: 'bounceInLeft'							
						});
                        $('#force_reset_password').modal('hide');
                        //window.location = base_url;
                     
                      }
                      else{
                         $('.preloader').hide();
						 iziToast.error({
							timeout: 3000,
							id: 'error',
							title: 'Error',
							message: 'Invalid Details.',
							position: 'topRight',
							transitionIn: 'fadeInDown'
						});
                      }
            
                }
            });
  
    }
  
  });
    
  
	$(function(){

	  $(':input[type=number]').on('mousewheel',function(e){ $(this).blur(); });

	});
	
	$('body').on('click','#add_mail',function(){


       $check_form= $("#inviteSupp").valid();

      if($check_form){
        if($('.invite_email').val()!=''){
            if($('.invite_email').val()==$('#pec_loginID').val()){
                iziToast.error({
					timeout: 3000,
					id: 'error',
					title: 'Error',
					message: 'please enter valid email',
					position: 'topRight',
					transitionIn: 'fadeInDown'
				});
            }else{
            
            var email=$('.invite_email').val();
            var lot=$('#pmc1_id').val();
			$.ajax({
            type: "POST",
            url: base_url+'/checkInvite',
            data: {
                email: email,
                lotid:lot,
               
            },
            dataType: 'JSON',
     
    
            success: function( msg ) {
             
            if(msg==0){
             var email = $('.invite_email').val();
             $('.add_invite_email').append('<div class="alert-mail alert btn-light" role="alert" style="width:auto !important"> <a href="#" class="close" id="alert_close" data-dismiss="alert" aria-label="close">&times;</a> <span>'+email+'</span>   </button> </div>');
             $("#pei_invite_details").val('');
              $(".invite_emails").val(function() {
                 return this.value + email.toLowerCase()+',';
             });
            }
            else if(msg==2)
			{
				 iziToast.error({
					timeout: 3000,
					id: 'error',
					title: 'Error',
					message: 'Given email as PQ buyer',
					position: 'topRight',
					transitionIn: 'fadeInDown'
				});
            }
            else{
				iziToast.error({
					timeout: 3000,
					id: 'error',
					title: 'Error',
					message: 'Given email already invited',
					position: 'topRight',
					transitionIn: 'fadeInDown'
				});
            }
        }
        });
    }

    }else{
		iziToast.error({
			timeout: 3000,
			id: 'error',
			title: 'Error',
			message: 'please enter valid email',
			position: 'topRight',
			transitionIn: 'fadeInDown'
		});
    }
      }

	
	});	
	
	$('body').on('click','#alert_close',function(){
            var remove_email=$(this).next('span').html();
           // console.log(remove_email);
            var new_array=[];
            existval=$('#pei_invite_details1').val();
            if(existval.length>0){
                var result =existval.split(','); 
                var i=0;
                $.each(result, function(key, email) {
                    console.log(email)
                    if(email!=remove_email && email!=''){
                        
                        new_array.push(email.toLowerCase());
                    }
                   

                });
                if(new_array==','){
                    $('#pei_invite_details1').val('');
                }else{
                    $('#pei_invite_details1').val(new_array.join(',')+',');
                }
               // console.log(new_array.join('; '));
            }
            
         $(this).parrent.remove();
      
    });
    
    $('body').on('click','.skippopup,.btn-close',function(){
        $.ajax({
            type: "POST",
            url: base_url+'/skippopup',
            dataType: 'JSON',
    
            success: function( msg ) {
             //console.log(msg);
             $('#force_reset_password').modal('hide');
        
        }
        });
   });

   //gowtham
   $("#event_name_report").on("change", function() {

 
    var eventID = $(this).val();
    //var cat_type= $("#com_cat_type").val();
    $.ajax({
        type: "POST",
        url: base_url + "/getreportlot",
        data: {
            eventIDreport: eventID,
  
        },
        dataType: "JSON",
  
        success: function(e) {
              $('.preloader').hide();
              $("#sub_lot").empty();
              $("#sub_lot").append("<option value=''>-- Choose Your Lot --</option>")
              $.each(e, function(e, t) {
              $("#sub_lot ").append($("<option></option>").attr("value", t.pea_id).text(t.pea_event_spec))
            })
        }
    })
  });

  $("#event_report_validate").validate({
	rules: {
    // simple rule, converted to {required:true}
    event_name_report: {
    required: true
    },
    sub_lot: {
        required: true
        }

    }

});
    

    });
    
