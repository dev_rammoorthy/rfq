<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PurchaseController@loginCheck');
Route::get('/login', 'PurchaseController@loginCheck');
Route::get('/home', 'PurchaseController@loginCheck');

Route::get('/register', function () {
    return view('auth.register');
});

Route::get('/register/{token}', function () {
    return view('auth.register');
});

Route::get('/sregister', function () {
    return view('auth.sellerreg');
});
Route::get('sregister/{token}', function () {
    return view('auth.sellerreg');
});

Route::get('/register-success', function () {
    return view('auth.regsucess');
});

Route::get('/register-fail', function () {
    return view('auth.regfail');
});
Route::group(['middleware' => 'loguser'],function(){
	
    Route::get('/event', 'PurchaseController@event_manager');
});
   
Route::get('/event-invite', function () {
    return view('event_manager.event-invite');
});

//Logistic

Route::resource('logistic','Logistic\LogisticController');
Route::post('logistic/api/pincode','Logistic\APIGetController@apipincode');

//RFQ

Route::resource('rfq','RFQ\RFQController');
Route::post('inviterfq','RFQ\RFQController@inviterfq');
Route::get('liverfq/{id}','RFQ\LiveRFQController@liverfq')->name('liverfq');;
Route::post('uploaddocument','RFQ\LiveRFQController@uploaddocument');
Route::post('paidlive','RFQ\LiveRFQController@paidlive');
Route::get('completedrfq','RFQ\CompletedRFQController@getcompleted');
Route::get('detailrfq/{id}','RFQ\CompletedRFQController@detailrfq');
Route::post('rfqaccept','RFQ\CompletedRFQController@rfqaccept');
Route::get('downloadtech/{id}','RFQ\LiveRFQController@downloadtech');
Route::get('downloadcomm/{id}','RFQ\LiveRFQController@downloadcomm');

Route::get('/upcoming-event', function () {
    return view('event_manager.upcoming-event');
    });
    Route::get('/seller-live', function () {
        return view('event_manager.seller.seller-live');
        });  
        Route::get('/seller-upcoming', function () {
            return view('event_manager.seller.seller-upcoming');
            }); 
        
    Route::get('/live-auction', function () {
    return view('event_manager.seller.seller-live');
});
Route::get('/buyer-live-auction', function () {
    return view('event_manager.live-event');
});
Route::get('/complete-auction', function () {
    return view('event_manager.seller.complete-event');
});
Route::get('/complete-auction-mis', function () {
    return view('event_manager.seller.complete-auction-mis');
});

Route::get('/buyer-complete', function () {
    return view('event_manager.buyer-complete');
});
Route::get('/buyer-complete-mis', function () {
    return view('event_manager.buyer-complete-mis');
});

// Route::get('/confirm-payment/{id}', function () {
    // return view('event_manager.confirm-payment');
// });

//paytm checkout
Route::get('/checkout', 'OrderController@create');
Route::post('/store', 'OrderController@store');
Route::post('/paytm-callback', 'OrderController@paytmCallback');
Route::get('/paytm-payment-redirect', 'OrderController@handlePaytmRequest');
Route::get('/paytm-payment-status/{status}/{id}', function () {
    return view('event_manager.paytm-event-register-success');
});

//instamojo checkout
Route::post('instamojo', 'CashfreeController@createRequest');
Route::post('/insta-callback', 'CashfreeController@InstaCallback');
Route::post('/insta-webhook', 'CashfreeController@InstaWebhook');
Route::get('/insta-payment-status/{status}/{id}', function () {
    return view('event_manager.insta-event-register-success');
});


Route::get('/datecheck', 'EventController@datecheck');

Route::get('/confirm-payment/{id}', 'PurchaseController@payment_conformation');

Route::get('/payment-status/{status}/{id}/{packid}', function () {
    return view('event_manager.event-register-success');
});
Route::get('/payment-status/{status}/{id}', function () {
    return view('event_manager.event-register-success');
});



Route::get('/reports', function () {
    return view('reports.overall-report');
});

Route::post('/reports', 'EventController@reports');
Route::post('/eventreport', 'EventController@eventreport');
Route::post('/lotreport', 'EventController@lotreport');
Route::post('/reportsPDF', 'EventController@reportsPDF');

Route::get('/bidwisereport', function () {
    return view('reports.bidwise-report');
});
Route::post('/bidwisereport', 'ReportController@bidwisereport');
Route::post('/eventLotData', 'ReportController@eventLotData');
Route::post('/sellerEventData', 'ReportController@sellerEventData');
Route::get('/bidchart/{id}', 'ReportController@bidreport');

//Reverse bid report
Route::get('/reversereport', function () {
    return view('reports.reverse-report');
});
Route::post('/reversereport', 'ReportController@reversereport');

//Close bid report
Route::get('/closereport', function () {
    return view('reports.close-report');
});
Route::post('/closereport', 'ReportController@closereport');

//Sellerwise bid report
Route::get('/sellerreport', function () {
    return view('reports.sellerwise-report');
});
Route::post('/sellerreport', 'ReportController@sellerreport');
Route::post('/lotsellerwisereport', 'ReportController@lotsellerwisereport');

Route::get('/sellerwisereport', function () {
    return view('reports.seller-wise-report');
});
Route::post('/sellerwisereport', 'ReportController@sellerwisereport');

//Lowest/Highest bid report
Route::get('/highlowreport', function () {
    return view('reports.highlow-report');
});

Route::post('/highlowreport', 'ReportController@highlowreport');
Route::post('/lothighlowreport', 'ReportController@lothighlowreport');

Route::get('/create-event/{id}', function () {
    return view('event_manager.add-event');
});
Route::get('/event-invite/{planId}/{id}', function () {
    return view('event_manager.event-invite');
    });
    Route::get('/profile', function () {
        return view('event_manager.profile');
        });
    Route::get('/forgot-password', function () {
        return view('auth.forgot');
        });
	Route::get('/rest-password/{id}', function () {
        return view('auth.password-rest');
        });
		
        Route::get('/need-clarification/{buyer_email}/{lotid}/{seller_email}/', function () {
            return view('event_manager.clarification');
            });

//wallet
// Route::get('/wallet', function () {
//     return view('event_manager.wallet');
// });
Route::get('/wallet', 'WalletController@wallet');
Route::get('/cashfree-failure', function () {
    return view('event_manager.cashfree-failure');
});

Route::post('/addmoney', 'WalletController@addmoney');
Route::post('/wallet-cashfree-callback', 'WalletController@cashfreecallback');
Route::post('/wallet-insta-callback', 'WalletController@walletinstacallback');
Route::get('/showmoney', 'WalletController@showmoney');
Route::post('/checkemdvalue', 'WalletController@checkemdvalue');
Route::post('/validateWalletAmount', 'WalletController@validateWalletAmount');
Route::post('/payEmdValue', 'WalletController@payEmdValue');
Route::post('/validateBuyerWallet', 'WalletController@validateBuyerWallet');
Route::post('/payToSeller', 'WalletController@payToSeller');
Route::get('/paidValidation', 'WalletController@paidValidation');

Route::get('/list-address', function () {
    return view('event_manager.address.list-address');
});
Route::get('/add-address', function () {
    return view('event_manager.address.add-address');
});
Route::post('/getLocations', 'AddressController@getLocations');
Route::post('/addAddress', 'AddressController@addAddress');
Route::post('/deleteAddress', 'AddressController@deleteAddress');
Route::get('/update-address/{id}', function () {
    return view('event_manager.address.update-address');
});
Route::post('/updateAddress', 'AddressController@updateAddress');

Route::get('/accepted-event', function () {
    return view('event_manager.order.accepted-event');
});
Route::post('/purchaseorder', 'OrderController@purchaseorder');
Route::post('/generate_invoice', 'OrderController@generate_invoice');
Route::post('/sendPO', 'OrderController@sendpotoseller');
Route::post('/sendInvoice', 'OrderController@sendInvoice');
Route::post('/acceptPO', 'OrderController@acceptPO');

Route::post('/acceptInvoice', 'OrderController@acceptInvoice');

Route::get('/accepted-invoice', function () {
    return view('event_manager.logistics.accepted-invoice');
});

Route::get('/my-invoice', function () {
    return view('event_manager.invoice.accepted-po');
});

Route::get('/documents', function () {
    return view('event_manager.document.document');
});

Route::get('/fdpi', 'OrderController@fdpi');

Route::get('/viewpo/{id}', 'OrderController@viewpo');

Route::get('/view-invoice/{id}', 'OrderController@viewinvoice');

Route::get('/wallethistory', function () {
    return view('event_manager.wallet-history');
});
Route::post('/wallethistory', 'WalletController@wallethistory');

Route::get('/emdhistory', function () {
    return view('event_manager.emd-history');
});
Route::post('/emdhistory', 'WalletController@emdhistory');

Route::get('/bank-info', function () {
    return view('event_manager.bank.bank-details');
});
Route::get('/list-bank', function () {
    return view('event_manager.bank.list-bank');
});
Route::get('/add-bank', function () {
    return view('event_manager.bank.add-bank');
});
Route::post('/addNewBank', 'BankController@addNewBank');
Route::post('/deleteBank', 'BankController@deleteBank');
Route::get('/confirm-bank', function () {
    return view('event_manager.bank.confirm-bank');
});
Route::post('/confirmBank', 'BankController@confirmBank');
Route::get('/accept-bank/{id}/{beneid}', function () {
    return view('event_manager.bank.accept-bank');
});
Route::post('/debitAmount', 'BankController@debitAmount');
Route::post('/transferLogin', 'BankController@transferLogin');


Route::post('/addBankInfo', 'BankController@addbankinfo');
Route::get('/getBankInfo', 'BankController@getBankInfo');

Route::get('/bank-confirm', function () {
    return view('event_manager.bank.confirm-bank-details');
});

Route::post('/winnerInvoice', 'ReportController@winnerInvoice');
Route::get('/chart', 'ChartController@index');
	
Route::get('/event_accept/{id}/{val}/{token}', 'PurchaseController@Accept_user_Event');
	
Route::get('users/confirm-email/{user_id}/{token}', 'PurchaseController@activateUser');

Route::get('404',['as'=>'404','uses'=>'PurchaseController@errorCode404']);
Route::get('405',['as'=>'405','uses'=>'PurchaseController@errorCode405']);

Route::get("/verifyemail/{token}", "PurchaseController@verifyuser");
Route::post("/companyDetails", "PurchaseController@getCompanyDetails");

Route::get("test-mail","PurchaseController@testmail");
Route::get("test-excel","EventController@export");
Route::get("getGeoLocation","PurchaseController@getGeoLocation");

Route::get("getActivationByToken","PurchaseController@getActivationByToken");

Route::get("/logout","PurchaseController@logout_user");

Route::post('/buyerRegister', 'PurchaseController@regiterBuyer');
Route::post('/checkEmailAvailability', 'PurchaseController@checkEmailAvailability');
Route::post('/sellerRegister', 'PurchaseController@regiterSeller');
Route::get('/addevent/{data}', 'PurchaseController@create_event');
Route::post('/buyerLogin', 'PurchaseController@buyerLogin');
Route::post('/inviteSupplier', 'PurchaseController@inviteSupplier');
Route::post('/get_Supplier_val', 'PurchaseController@get_Supplier');
Route::post('/confirm-email', 'PurchaseController@usersverify');
Route::post('/createnNewEvent', 'EventController@createNewEvent');
Route::post('/addProductEvent', 'EventController@addEventProduct');
Route::post('/getSubproduct', 'EventController@getSubProduct');
Route::post('/getCategories', 'EventController@getCategories');
Route::post('/completedEvent', 'EventController@completedEvent');
Route::post('/editProductItem', 'EventController@productEditItem');
Route::post('/deleteProuct', 'EventController@deleteProuctItem');
Route::post('/inviteAccept', 'EventController@acceptInviteItem');
Route::post('/addBidLotItem', 'EventController@addBidLotItem');
Route::post('/validateLotBid', 'EventController@validateLotBid');
Route::get('/downloadsamplelot', 'ExcelController@downloadsamplelot');
Route::get('/downloadsampleclosed', 'ExcelController@downloadsampleclosed');
Route::post('/excelitemupload', 'ExcelController@excelitemupload');
Route::post('/excelcloseditemupload', 'ExcelController@excelcloseditemupload');
Route::post('/rescheduleLotItem', 'EventController@rescheduleLotItem');
Route::post('/finalizedBid', 'EventController@buyerAcceptBid');
Route::post('/rejectBid', 'EventController@buyerRejectBid');
Route::post('/rejectAllBid', 'EventController@buyerRejectAllBid');
Route::get('/gotoSeller', 'PurchaseController@gotoSellerDashboard');
Route::get('/gotoBuyer', 'PurchaseController@gotoBuyerDashboard');
Route::post('/forgot', 'PurchaseController@forgotpassword');
Route::post('/restpass', 'PurchaseController@resetpassword');
Route::post('/skippopup', 'PurchaseController@skippopup');
Route::post('/checkInvite', 'EventController@lotInviteCheck');
Route::post('/needclarification', 'EventController@needClarification');
Route::post('/getPqvendor', 'EventController@getPqvendor');
Route::post('/updatebid', 'EventController@updatebidData');
Route::post('/buyerupdatebid', 'EventController@buyerupdatebidData');
Route::post('/buyerupdateinviteacceptance', 'EventController@buyerupdateInviteAcceptanceData');
Route::post('payment', ['as' => 'payment', 'uses' => 'PurchaseController@payment']);

# Status Route
Route::get('payment/status', ['as' => 'payment.status', 'uses' => 'PurchaseController@status']);


//gowtham
Route::get('/chartsReprts', function () {
    return view('charts.reportchat');
});

Route::post('/getreportlot', 'EventController@getreportlot');

Route::post('/chartsReprts', 'EventController@chartsReprts');

