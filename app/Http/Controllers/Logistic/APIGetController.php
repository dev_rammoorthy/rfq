<?php

namespace App\Http\Controllers\Logistic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Library\Logistic\LogisticLibrary;

class APIGetController extends Controller
{
    /*public function apimasternumber(){
    	$endpoint = "http://127.0.0.1:8080/get/masternumber/";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		$result=curl_exec($ch);
		curl_close($ch);
		$data=(json_decode($result, true));
		return $data;
    }

    public function apichildnumber(){
    	$endpoint = "http://127.0.0.1:8080/get/childnumber";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		$result=curl_exec($ch);
		curl_close($ch);
		$data=(json_decode($result, true));
		return $data;
	}*/

    public function apipincode(Request $request){

    	$data=LogisticLibrary::picode($request);
    	return $data;

    	/*$endpoint = "http://127.0.0.1:8080/get/pincode/".$request->input('pickup_id');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		$result=curl_exec($ch);
		curl_close($ch);
		$data=(json_decode($result, true));

		$dimensional=0;;
		for ($x = 0; $x <= $request->input('count'); $x++) {
            $box_length=$request->input('box_length'.$x.'');
            $box_breadth=$request->input('box_breadth'.$x.'');
            $box_height=$request->input('box_height'.$x.'');

            $dimensional+=((($box_length*$box_breadth*$box_height)/27000)*6)*$request->input('no_box'.$x.'');
        }

		$actual_weight=$request->input('total_weight');
		$charge_weight=max($dimensional,$actual_weight,20);
		$invoice_value=$request->input('invoice_value');
		$base_freight=$charge_weight*$data['rates'];
		$fuel_surcharge=$base_freight*0.15;
		$insure_value=($invoice_value*0.001);
		
		$freight_value=max($insure_value,100);
		$awb_charges=100;
		if($charge_weight < 100){
			$add_charge=0;
		}else if($charge_weight < 250){
			$add_charge=2;
		}else if($charge_weight < 500){
			$add_charge=3;
		}else{
			$add_charge=4;
		}
		$additional_handling_charges=($charge_weight/7)*$add_charge;
		
		$total_freight=$base_freight+$fuel_surcharge+$freight_value+$awb_charges+$additional_handling_charges;

		$sum_gst=$total_freight*0.18;

		return number_format((float)$total_freight+$sum_gst, 2, '.', '');*/


    }
}
