<?php

namespace App\Http\Controllers\Logistic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Redirect;
use Carbon\Carbon;
use App\Models\Auction\PQ_Live_Auction;
use App\Models\Users\PQ_Company_Info;
use App\Models\Users\PQ_Login_Info;
use App\Models\Logistic\PQ_Logistic_Delivery_Details;
use App\Models\Logistic\PQ_Logistic_Box_Details;
use App\PQ_EVENT_ITEM;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\LogisticExportLibrary;
use App\Exports\ExpoxtLogisticExport;
use DB;
use Mail;
use App\Models\Ship\PQ_Shipping_Address;
use App\Models\Order\PQ_Purchase_Orders;
use GuzzleHttp\Client;
use App\Library\Logistic\LogisticLibrary;
use App\Models\Invoice\PQ_Purchase_Invoice;

class LogisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
               
        $data=LogisticLibrary::picode($request);

        $insert=new PQ_Logistic_Delivery_Details;
        $insert->pq_po_id=$request->input('trans_id');
        $insert->total_boxes=$request->input('total_boxes');
        $insert->total_weight=$request->input('total_weight');
        $insert->pickup_appointment_date=Carbon::parse($request->input('appointment_date_time'))->format('Y-m-d');
        $insert->pickup_appointment_time=Carbon::parse($request->input('appointment_date_time'))->format('G:i');
        $insert->pickup_description=$request->input('pickup_description');
        $insert->ewaybill_number=$request->input('ewaybill_number');
        $insert->logistic_price=$data;
        $insert->save();

        for ($x = 0; $x <= $request->input('count'); $x++) {
            for ($i=1; $i <= $request->input('no_box'.$x.''); $i++) {
                if($x==0 && $i==1){
                    $master=LogisticLibrary::apimasternumber();
                    $master_id=$master['master_id'];
                    $child_id=NULL;
                    //$update_master=LogisticLibrary::updateapimasternumber($master['id']);
                }else{
                    $child=LogisticLibrary::apichildnumber();
                    $child_id=$child['child_id'];
                    $master_id=NULL;
                    //$update_child=LogisticLibrary::updateapichildnumber($child['id']);
                }
                $box=new PQ_Logistic_Box_Details;
                $box->logistic_details_id=$insert->id;
                $box->box_length=$request->input('box_length'.$x.'');
                $box->box_height=$request->input('box_height'.$x.'');
                $box->box_breadth=$request->input('box_breadth'.$x.'');
                $box->no_box=$request->input('no_box'.$x.'');
                $box->value=$i;
                $box->master_airway_number=$master_id;
                $box->child_airway_number=$child_id;
                $box->save();
            }
        }

        $get_insert_data=DB::table('pq_purchase_orders')
            ->select('sel_log.pli_con_name as sel_name','sel_log.pli_con_mob as sel_contact','sel_loc.psa_address_line1 as sel_add1','sel_loc.psa_address_line2 as sel_add2','sel_state.pmst_state_name as seller_state','sel_city.pmci_city_name as seller_city','sel_loc.psa_zipcode as seller_zipcode','sel_loc.psa_country as seller_country')

            ->join('pq_purchase_invoice','pq_purchase_orders.order_id','pq_purchase_invoice.ppi_order_id')
            ->join('pq_live_auction','pq_purchase_orders.live_action_id','pq_live_auction.pla_id')

            ->join('pq_logistic_delivery_details','pq_purchase_orders.id','pq_logistic_delivery_details.pq_po_id')
             ->join('pq_shipping_address as sel_loc','pq_purchase_invoice.ppi_seller_pickup_address','sel_loc.psa_id')
             ->join('pq_login_info as sel_log','sel_loc.psa_loginid','sel_log.pli_loginid')
             ->join('pq_master_states as sel_state','sel_loc.psa_state','sel_state.pmst_state_id')
             ->join('pq_master_cities as sel_city','sel_loc.psa_city','sel_city.pmci_city_id')

            ->addselect('buy_log.pli_con_name as buy_name','buy_log.pli_con_mob as buy_contact','buy_loc.psa_address_line1 as buy_add1','buy_loc.psa_address_line2 as buy_add2','buy_state.pmst_state_name as buyer_state','buy_city.pmci_city_name as buyer_city','buy_loc.psa_zipcode as buyer_zipcode','buy_loc.psa_country as buyer_country')

            ->addselect('pq_logistic_delivery_details.total_boxes','pq_logistic_delivery_details.total_weight','pq_logistic_delivery_details.pickup_appointment_date',DB::raw('DATE_FORMAT(pq_logistic_delivery_details.pickup_appointment_time, "%h:%i%p") as pickup_appointment_time'),'pq_logistic_delivery_details.pickup_description','pq_logistic_delivery_details.ewaybill_number','pq_purchase_invoice.ppi_invoice_id as invoice_number','pq_live_auction.pla_cur_bid','pq_logistic_delivery_details.id as logistic_id','pq_logistic_delivery_details.logistic_price')
            
            ->join('pq_shipping_address as buy_loc','pq_purchase_orders.buser_delivery_address','buy_loc.psa_id')
            ->join('pq_login_info as buy_log','buy_loc.psa_loginid','buy_log.pli_loginid')
             ->join('pq_master_states as buy_state','buy_loc.psa_state','buy_state.pmst_state_id')
            ->join('pq_master_cities as buy_city','sel_loc.psa_city','buy_city.pmci_city_id')
            
            ->where('pq_purchase_orders.id',$request->input('trans_id'))
            ->get()->first();

            $get_box_dts_mas=PQ_Logistic_Box_Details::where('logistic_details_id',$get_insert_data->logistic_id)->get()->first();

            $get_box_dts_child=PQ_Logistic_Box_Details::where('logistic_details_id',$get_insert_data->logistic_id)->pluck('child_airway_number')->toArray();
            $child_num=implode(", ", $get_box_dts_child);
            //$get_insert_data->invoice_value = NULL;
            $get_insert_data->master_air_way_number = $get_box_dts_mas->master_airway_number;
            $get_insert_data->child_air_way_number = ltrim($child_num,",");

            $set_ary=[];

            $box_dimensio=[];

            $box_dts=PQ_Logistic_Box_Details::where('logistic_details_id',$get_insert_data->logistic_id)->get();
            foreach ($box_dts as  $value) {
                if($value->no_box == $value->value){
                    $set_dim='No Box= '.$value->no_box.' Box Length= '.$value->box_length.' Box Height= '.$value->box_height.' Box Breadth= '.$value->box_breadth;
                    array_push($box_dimensio, $set_dim);
                }
            }

            $box_imp=implode(", ", $box_dimensio);

            $get_insert_data->box_detils = $box_imp;

        array_push($set_ary, $get_insert_data);

        (new ExpoxtLogisticExport($set_ary))->store('logistic/'.$get_insert_data->invoice_number.'.xlsx');


        $data = array('in_number' => $get_insert_data->invoice_number.'.xlsx');
        Mail::send('emails.Logistic.LogisticMail', $data,function($message) use($data)
        {
            $message->to('srinivasanit14@gmail.com')->subject('Logistic Details');
            $message->from('support@purchasequick.in','Purchase Quick');
            $message->attach(storage_path('app/logistic/'.$data['in_number']));
            //$message->attach(storage_path('app/logistic/INV2629277.xlsx'));
        });

        $url = 'http://127.0.0.1:8000/accepted-invoice';

        return Redirect::to($url);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $view_data['trans_id']=$id;
        $get_delivery=PQ_Purchase_Orders::find($id);
        $get_lot=PQ_Live_Auction::where('pla_id',$get_delivery->live_action_id)->get()->first();
        $view_data['in_voc_val']=$get_lot->pla_cur_bid;
        $view_data['buser_delivery_address']=$get_delivery->buser_delivery_address;
        return view('Logistic.CreateLogistic')->with('view_data', $view_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_insert_data=DB::table('pq_purchase_orders')
            ->select('sel_log.pli_con_name as sel_name','sel_log.pli_con_mob as sel_contact','sel_loc.psa_address_line1 as sel_add1','sel_loc.psa_address_line2 as sel_add2','sel_state.pmst_state_name as seller_state','sel_city.pmci_city_name as seller_city','sel_loc.psa_zipcode as seller_zipcode','sel_loc.psa_country as seller_country')

            ->join('pq_purchase_invoice','pq_purchase_orders.order_id','pq_purchase_invoice.ppi_order_id')
            ->join('pq_live_auction','pq_purchase_orders.live_action_id','pq_live_auction.pla_id')

            ->join('pq_logistic_delivery_details','pq_purchase_orders.id','pq_logistic_delivery_details.pq_po_id')
             ->join('pq_shipping_address as sel_loc','pq_purchase_invoice.ppi_seller_pickup_address','sel_loc.psa_id')
             ->join('pq_login_info as sel_log','sel_loc.psa_loginid','sel_log.pli_loginid')
             ->join('pq_master_states as sel_state','sel_loc.psa_state','sel_state.pmst_state_id')
             ->join('pq_master_cities as sel_city','sel_loc.psa_city','sel_city.pmci_city_id')

            ->addselect('buy_log.pli_con_name as buy_name','buy_log.pli_con_mob as buy_contact','buy_loc.psa_address_line1 as buy_add1','buy_loc.psa_address_line2 as buy_add2','buy_state.pmst_state_name as buyer_state','buy_city.pmci_city_name as buyer_city','buy_loc.psa_zipcode as buyer_zipcode','buy_loc.psa_country as buyer_country')

            ->addselect('pq_logistic_delivery_details.total_boxes','pq_logistic_delivery_details.total_weight','pq_logistic_delivery_details.pickup_appointment_date',DB::raw('DATE_FORMAT(pq_logistic_delivery_details.pickup_appointment_time, "%h:%i%p") as pickup_appointment_time'),'pq_logistic_delivery_details.pickup_description','pq_logistic_delivery_details.ewaybill_number','pq_purchase_invoice.ppi_invoice_id as invoice_number','pq_live_auction.pla_cur_bid','pq_logistic_delivery_details.id as logistic_id','pq_logistic_delivery_details.logistic_price')
            
            ->join('pq_shipping_address as buy_loc','pq_purchase_orders.buser_delivery_address','buy_loc.psa_id')
            ->join('pq_login_info as buy_log','buy_loc.psa_loginid','buy_log.pli_loginid')
             ->join('pq_master_states as buy_state','buy_loc.psa_state','buy_state.pmst_state_id')
            ->join('pq_master_cities as buy_city','sel_loc.psa_city','buy_city.pmci_city_id')
            
            ->where('pq_purchase_orders.id',29)
            ->get()->first();
        dd($get_insert_data);

        /*Mail::send('emails.register-conform',[],function($message)
        {
            $message->to('ramamoorthy@proctek.in')->subject('Event Register');
            $message->from('support@purchasequick.in','Purchase Quick');
            $message->attach(storage_path('app/public/logistic_.xls'));
        });*/

        // Mail::send('emails.register-conform',['name','kwuy'],function($message){
        //     $message->to('ramamoorthy@proctek.in','To Kuwy')->subject('Test Email');
        //     $message->from('support@purchasequick.in','hari');
        // });

        //dd('Mail send successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
