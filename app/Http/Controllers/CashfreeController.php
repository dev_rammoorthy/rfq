<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use DateTime;
use Crypt;

class CashfreeController extends Controller
{
    //
    public function createRequest(Request $request)
    {
        $input = $request->all();
        $order_id = uniqid() . ( string ) $input['pli_loginId'];
        $price = $input['pmp_pkg_amount'];
        $phone = $input['pci_comp_phone'];
        $comp_name = $input['pci_comp_name'];

        $session_val = session()->all(); 		
        session(['package_data' => $input ]);
        
        $apiEndpoint = "https://test.cashfree.com";
        $opUrl = $apiEndpoint."/api/v1/order/create";
        
        $cf_request = array();
        // $cf_request["appId"] = "450180a7d726f21a3e14e5621054"; -- pradeep acc
        //  $cf_request["secretKey"] = "cd63b792112a2d83f5a375929ac73c1a3af98583"; --pradeep acc
        $cf_request["appId"] = "44700864c72dd1d0649103ce0744";
        $cf_request["secretKey"] = "2d286d1b61ecf125ab8d391266b0e7ef63a13280"; 
        $cf_request["orderId"] = uniqid();
        $cf_request["orderAmount"] = $price;
        $cf_request["orderNote"] = "Buy Event";
        $cf_request["customerPhone"] = $phone;
        $cf_request["customerName"] = $comp_name;
        $cf_request["customerEmail"] = $input['pli_loginId'];
        $cf_request["returnUrl"] = url('/insta-callback');
        $cf_request["notifyUrl"] = "https://docs.cashfree.com/docs/rest/guide/";

        $timeout = 10;
        
        $request_string = "";
        foreach($cf_request as $key=>$value) {
            $request_string .= $key.'='.rawurlencode($value).'&';
        }
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$opUrl?");
        curl_setopt($ch,CURLOPT_POST, count($cf_request));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $request_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $curl_result=curl_exec ($ch);
        curl_close ($ch);

        $jsonResponse = json_decode($curl_result);
        if ($jsonResponse->{'status'} == "OK") {
            $paymentLink = $jsonResponse->{"paymentLink"};

            return Redirect::to($paymentLink);
            //Send this payment link to customer over email/SMS OR redirect to this link on browser
        } else {
            //Log request, $jsonResponse["reason"]
            print_r($jsonResponse);die;
        }
    }
    
    public function InstaCallback( Request $request ) {

        $input = $request->all();

        session(['cash_data' => $input ]);
        
        if($input['txStatus'] === 'SUCCESS')
        {
            $session_val = session()->all();
            $this->create_event($session_val);
        }
        else
        {
            $encrypted1 = Crypt::encryptString("0");
            Redirect::to('/insta-payment-status/fail/'.$encrypted1)->send();
            
            //echo 'payment failed';
        }
    }

    public function create_event($data)
	{
		$input = $data['package_data'];	
	 
		$dt 	= new DateTime();
		$date	= $dt->format('Y-m-d H:i:s');
		
		$str = preg_replace('/\D/', '', $input['pmp_pkg_validity']);
		
		$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));				
		
        try 
        {
			$package = DB::table('pq_buynow_package')
						->where('pbp_comp_id', '=', $input['pli_comp_id'])
						->where('pbp_pkg_id', '=', $input['pmp_id'])						
						->first();
						
			
			$packagellist = DB::table('pq_master_package')        
							->select('*')
							->where('pmp_id',$input['pmp_id'])
							->get();
			
            if(isset($package->pbp_pkg_cur_count))
            {
				$pack_count=$package->pbp_pkg_cur_count;
            }
            else
            {
				$pack_count=0;
			}
				
            if ((count($package)==0 )|| ($pack_count==0 ) )
            {
                // It does not exist - add to favorites button will show
                    
                    $package_data =	[
                            'pbp_comp_id' 		 	=>  $input['pli_comp_id'],
                            'pbp_login_id'  		=>  $input['pli_loginId'],
                            'pbp_pkg_id'  			=>  $packagellist[0]->pmp_id,
                            'pbp_pkg_eventcount' 	=>  $packagellist[0]->pmp_pkg_eventcount,
                            'pbp_pkg_cur_count'  	=>  $packagellist[0]->pmp_pkg_eventcount,
                            'pbp_pkg_start_dt'  	=>  $date,
                            'pbp_pkg_end_dt'  		=>  $enddate,
                            'pbp_pkg_status'  		=>  '1',                            
                        ];

                        if(($pack_count==0 )&&(count($package)==0 ))
                        {
                            $packageid = DB::table('pq_buynow_package')->insertGetId($package_data);
                        }
                        else
                        {                            
                            $packageid = DB::table('pq_buynow_package')
                                            ->where('pbp_id', '=', $package->pbp_id)
                                            ->update($package_data);
                        }					
                    
                    $encrypted = Crypt::encryptString($packageid);

                    if($packageid)
                    {								
                        Redirect::to('/insta-payment-status/success/'.$encrypted)->send();
                        
                    }

            } else {
                // It exists - remove from favorites button will show			
                    $encrypted1 = Crypt::encryptString("0");
                    Redirect::to('/insta-payment-status/fail/'.$encrypted1)->send();         
            }       
        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2
            );
        }

	}

    public function InstaWebhook( Request $request ) {

        $input = $request->all();
        
        print_r($input);
        echo 'hiiihello';
        die;


    }
}
