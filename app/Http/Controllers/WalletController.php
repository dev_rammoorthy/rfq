<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use DateTime;
use Crypt;
use App\GetOsDetails;
use View;
use Charts;
use Carbon\Carbon;
use Config;

class WalletController extends Controller
{
    //
    public function wallet()
    {
        $session = session()->all();
		
        $UserId = $session['pli_loginId'];
        
       // $date = \Carbon\Carbon::today()->subDays(30);

        // $theOtherKey = 'srini';
        // $encrypted = Crypt::encryptString('YASA7834SDS');
        // $decrypted = Crypt::decryptString($encrypted);

        // echo $encrypted.'<br>';
        // echo $decrypted;
        // die;       

        $history = DB::table('pq_wallet_history')
                        ->where('pwh_receiver_loginid', $UserId)
                        ->orWhere('pwh_loginid', $UserId)		
                        ->where(DB::raw('MONTH(`pwh_created_at`)'), date('m'))
                        //->where(DB::raw('DATE(`pwh_created_at`)'), '>', date($date))
                        ->get();
        
        $label = array();
        $values = array();

        foreach($history as $val)
        {
            if($val->pwh_receiver_loginid == $UserId && $val->pwh_loginid != $UserId)  
            {
                array_push($label, $val->pwh_wallet_id.'<br>Received<br>'.$val->pwh_created_at);
            } 
            elseif($val->pwh_loginid == $UserId && $val->pwh_receiver_loginid != $UserId)
            {
                array_push($label, $val->pwh_wallet_id.'<br>Spend<br>'.$val->pwh_created_at);
            }
            else
            {
                array_push($label, $val->pwh_wallet_id.'<br>Added<br>'.$val->pwh_created_at);
            }
           
            array_push($values, $val->pwh_balance);
        }

        $line  = Charts::create('line', 'highcharts')
                    ->title('Wallet Transaction History')
                    ->elementLabel('My wallet history')
                    ->labels($label)
                    ->values($values)
                    //->dimensions(500,500)
                    ->responsive(false);

        return view('event_manager.wallet', compact('line'));
    }

    public static function showmoney()
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $getModel = DB::table('pq_wallet_current')
                            ->where('pwc_login_id', '=', $login_id)
                            ->first();

        session(['wallet_data' => $getModel ]);

        return $getModel;
    }

    public static function checkemdvalue($item_id)
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $getModel = DB::table('pq_emd_paid_history')
                        ->where('peph_event_additem_id', '=', $item_id)
                        ->where('peph_loginid', '=', $login_id)
                        ->first();

        $getLiveModel = DB::table('pq_live_auction')
                        ->where('pla_lotid', '=', $item_id)
                        ->where('pla_seller_emailid', '=', $login_id)
                        ->first();

        $data = array(
            'emdData' => $getModel,
            'myBideData' => $getLiveModel
        );
                        
        return $data;
    }

    public static function wallethistorydetail()
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $getModel = DB::table('pq_wallet_history')
                        ->where('pwh_loginid', '=', $login_id)
                        ->orWhere('pwh_receiver_loginid', '=', $login_id)
                        ->orderBy('pwh_id', 'desc')
                        ->get();

        return $getModel;
    }

    public function wallethistory(Request $request)
    {
        $input = $request->all();
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];
        $wallet_id = $input['hfWalletId'];

        if($input['SUBMIT'] == "Sent")
        {
            $getModel = DB::table('pq_wallet_history')
                            ->where('pwh_wallet_id', '=', $wallet_id)
                            ->where('pwh_receiver_wallet_id', '!=', $wallet_id)
                            ->orderBy('pwh_id', 'desc')
                            ->get();
        }
        else if($input['SUBMIT'] == "Received")
        {
            $getModel = DB::table('pq_wallet_history')
                            ->where('pwh_receiver_wallet_id', '=', $wallet_id)
                            ->where('pwh_wallet_id', '!=', $wallet_id)
                            ->orderBy('pwh_id', 'desc')
                            ->get();
        }
        else
        {
            $getModel = DB::table('pq_wallet_history')
                            ->where('pwh_receiver_wallet_id', '=', $wallet_id)
                            ->where('pwh_wallet_id', '=', $wallet_id)
                            ->orderBy('pwh_id', 'desc')
                            ->get();
        }

        return View::make('event_manager.wallet-history')->with('wallet_history', $getModel);
    }

    public static function emdhistorydetail()
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];
        $wallet_data = $session_val['wallet_data'];

        $getModel = DB::table('pq_emd_paid_history')
                        ->where('peph_buyer_loginid', '=', $login_id)
                        ->orderBy('peph_id', 'desc')
                        ->get();

        return $getModel;
    }

    public function emdhistory(Request $request)
    {
        $input = $request->all();
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];
        $wallet_data = $session_val['wallet_data'];

        if($input['SUBMIT'] == "Returned")
        {
            $getModel = DB::table('pq_emd_paid_history')
                            ->where('peph_buyer_loginid', '=', $login_id)
                            ->where('peph_is_emd_returned', '=', '1')
                            ->orderBy('peph_id', 'desc')
                            ->get();
        }
        else
        {
            $getModel = DB::table('pq_emd_paid_history')
                            ->where('peph_buyer_loginid', '=', $login_id)
                            ->where('peph_is_emd_returned', '=', '0')
                            ->orderBy('peph_id', 'desc')
                            ->get();
        }       

        return View::make('event_manager.emd-history')->with('emd_history', $getModel);

    }

    public function validateWalletAmount(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();

       
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try{
          
          $addItemID=$input['lot_id'];
          $comp_id= $session_val['pli_comp_id'];
          $log_id= $session_val['pli_loginId'];

            $user_os 		= PurchaseController::getOS();	
		    $user_browser   = PurchaseController::getBrowser();

            $validateLot = DB::table('pq_event_additem')
                                ->where('pea_id', '=',  $addItemID)
                                ->first();

            if(isset($validateLot) && $validateLot != null)
            {
                if($validateLot->pea_is_emd_required == "1")
                {
                    $check_exist = DB::table('pq_wallet_current')
                                ->where('pwc_login_id', '=',  $log_id)
                                ->select('*')
                                ->first();

                    if(is_null($check_exist)) 
                    {
                        $result = array(
                            'status' => false,
                            'message' => 'Wallet not exist',
                            'c_code' => 3
                        );
                        
                    }else{ 

                        $sumOFEMD = DB::table('pq_emd_paid_history')
                                        ->where('peph_event_additem_id', '=',  $addItemID)
                                        ->sum('peph_emd_value');

                        $walletEmdVal = DB::table('pq_wallet_current')
                                                    ->where('pwc_login_id', $log_id)
                                                    ->where('pwc_current_balance', '>=', $validateLot->pea_event_emd_value)
                                                    ->first();

                        if(isset($walletEmdVal))
                        {
                            $result = array(
                                'status' => true,
                                'message' => 'Wallet balance available',	
                                'current_balance' => $walletEmdVal->pwc_current_balance,
                                'emd_value' => $validateLot->pea_event_emd_value,
                                'total_emd' => $sumOFEMD,
                                'c_code'  =>2
                            );

                            return response()->json($result);
                        }
                        else
                        {
                            $result = array(
                                'status' => false,
                                'message' => 'Wallet balance not available',
                                'c_code' => 3
                            );           
                        }
                        
                    }    
                }     
                else if($validateLot->pea_is_emd_required == "0")
                {
                    $walletEmdVal = DB::table('pq_wallet_current')
                                        ->where('pwc_login_id', $log_id)
                                        ->where('pwc_current_balance', '>=', $validateLot->pea_event_emd_value)
                                        ->first();

                    $result = array(
                        'status' => true,
                        'message' => 'EMD Not Applicable',	
                        'current_balance' => $walletEmdVal->pwc_current_balance,
                        'emd_value' => 0,
                        'total_emd' => 0,
                        'c_code'  =>5
                    );

                    return response()->json($result);
                }       
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Lot not exist',	
                    'c_code'  =>4
                );
            }            
            
       }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);
    }

    public function validateBuyerWallet(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();
       
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try{
          
          $addItemID=$input['lot_id'];
          $comp_id= $session_val['pli_comp_id'];
          $log_id= $session_val['pli_loginId'];
          $current_bid_id = $input['current_bid_id'];

            $user_os 		= PurchaseController::getOS();	
		    $user_browser   = PurchaseController::getBrowser();

            $validateLot = DB::table('pq_live_auction')
                                ->where('pla_id', '=',  $current_bid_id)
                                ->first();

            if(isset($validateLot) && $validateLot != null)
            {
                $check_exist = DB::table('pq_wallet_current')
                            ->where('pwc_login_id', '=',  $log_id)
                            ->select('*')
                            ->first();

                if(is_null($check_exist)) 
                {
                    $result = array(
                        'status' => false,
                        'message' => 'Wallet not exist',
                        'c_code' => 3
                    );
                    
                }else{ 

                    $walletEmdVal = DB::table('pq_wallet_current')
                                                ->where('pwc_login_id', $log_id)
                                                ->where('pwc_current_balance', '>=', $validateLot->pla_cur_bid)
                                                ->first();

                    if(isset($walletEmdVal))
                    {
                        $result = array(
                            'status' => true,
                            'message' => 'Wallet balance available',	
                            'current_balance' => $walletEmdVal->pwc_current_balance,
                            'c_code'  =>2
                        );

                        return response()->json($result);
                    }
                    else
                    {
                        $result = array(
                            'status' => false,
                            'message' => 'Wallet balance not available',
                            'c_code' => 3
                        );           
                    }
                    
                }                
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Lot not exist',	
                    'c_code'  =>4
                );
            }            
            
       }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);
    }

    public static function paidValidation($bid_id)
    {
        $validatePaidLot = DB::table('pq_live_auction')
                            ->where('pla_id', '=',  $bid_id)
                            ->first();  

        return $validatePaidLot;
    }

    public function payToSeller(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();
       
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try
        {          
          $addItemID=$input['lot_id'];
          $comp_id= $session_val['pli_comp_id'];
          $log_id= $session_val['pli_loginId'];
          $current_balance = $input['current_balance'];
          $accepted_amount = $input['accepted_amount'];
          $current_bid_id = $input['current_bid_id'];

            $user_os 		= PurchaseController::getOS();	
            $user_browser   = PurchaseController::getBrowser();
            
            $validateLot = DB::table('pq_live_auction')
                                ->where('pla_id', '=',  $current_bid_id)
                                ->first();

            $validateItemDetail = DB::table('pq_event_additem')
                                    ->where('pea_id', '=',  $addItemID)
                                    ->first();

            if(isset($validateLot) && $validateLot != null)
            {
                $validateWallet = DB::table('pq_wallet_current')
                                    ->where('pwc_login_id', '=',  $log_id)
                                    ->first();

                $validateSellerWallet = DB::table('pq_wallet_current')
                                        ->where('pwc_login_id', '=',  $validateLot->pla_seller_emailid)
                                        ->first();

                if(isset($validateWallet) && $validateWallet != null)
                {
                    if($validateItemDetail->pea_is_emd_required == 1)
                    {
                        $sumOFEMD = DB::table('pq_emd_paid_history')
                                        ->where('peph_event_additem_id', '=',  $addItemID)
                                        ->sum('peph_emd_value');

                        $current_balance = $validateWallet->pwc_current_balance - $validateLot->pla_cur_bid - $sumOFEMD;
                    }
                    else
                    {
                        $current_balance = $validateWallet->pwc_current_balance - $validateLot->pla_cur_bid;
                    }

                    $updateWallet = [
                        'pwc_current_balance' => $current_balance,
                        'pwc_updated_at' => $date
                    ];                    

                    $updateWalletValue = DB::table('pq_wallet_current')
                                    ->where('pwc_login_id', '=',  $log_id)
                                    ->update($updateWallet);

                    if($updateWalletValue) 
                    {
                        if(isset($validateSellerWallet))
                        {
                            $seller_cur_balance = $validateLot->pla_cur_bid + $validateSellerWallet->pwc_current_balance;

                            $updateSellerWallet = [
                                'pwc_current_balance' => $seller_cur_balance,
                                'pwc_updated_at' => $date
                            ];

                            $updateSellerWalletValue = DB::table('pq_wallet_current')
                                                            ->where('pwc_login_id', '=',  $validateLot->pla_seller_emailid)
                                                            ->update($updateSellerWallet);

                            if($updateSellerWalletValue)
                            {

                            }
                            else
                            {
                                $result = array(
                                    'status' => false,
                                    'message' => 'Seller wallet not updated',	
                                    'c_code'  =>2
                                );

                            }
        
                        }
                        else
                        {
                            $seller_cur_balance = $validateLot->pla_cur_bid;

                            $insertSellerWallet = [
                                'pwc_wallet_id' => 'WA'.uniqid(),
                                'pwc_login_id' => $validateLot->pla_seller_emailid,
                                'pwc_current_balance' => $seller_cur_balance,
                                'pwc_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                'pwc_browser' => $user_browser['name'],  
                                'pwc_browser_ver' =>  $user_browser['version'],
                                'pwc_os' => $user_os,
                                'pwc_updated_at' => $date
                            ];

                            $insertSellerWalletValue = DB::table('pq_wallet_current')
                                                ->where('pwc_login_id', '=',  $validateLot->pla_seller_emailid)
                                                ->insert($insertSellerWallet);

                            if($insertSellerWalletValue)
                            {

                            }
                            else
                            {
                                $result = array(
                                    'status' => false,
                                    'message' => 'Buyer wallet not inserted',	
                                    'c_code'  =>2
                                );
                            }
                           
                        }

                        $insertHistory = [
                            'pwh_wallet_id' => $validateWallet->pwc_wallet_id,
                            'pwh_balance' => $accepted_amount,
                            'pwh_do' => 2, //spend
                            'pwh_for' => '3',
                            'pwh_receiver_loginid' => $validateLot->pla_seller_emailid,
                            'pwh_receiver_wallet_id' => $validateSellerWallet->pwc_wallet_id,
                            'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                            'pwh_browser' => $user_browser['name'],  
                            'pwh_browser_ver' =>  $user_browser['version'],
                            'pwh_os' => $user_os,
                            'pwh_loginid' => $log_id,
                        ];

                        $historyInsert = DB::table('pq_wallet_history')->insert($insertHistory);

                        if($historyInsert)
                        {                 
                            // $sellerInsertHistory = [
                            //     'pwh_wallet_id' => $validateSellerWallet->pwc_wallet_id,
                            //     'pwh_balance' => $accepted_amount,
                            //     'pwh_do' => 1, //spend
                            //     'pwh_for' => '7',
                            //     'pwh_receiver_loginid' => $log_id,
                            //     'pwh_receiver_wallet_id' => $validateWallet->pwc_wallet_id,
                            //     'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                            //     'pwh_browser' => $user_browser['name'],  
                            //     'pwh_browser_ver' =>  $user_browser['version'],
                            //     'pwh_os' => $user_os,
                            //     'pwh_loginid' => $log_id,
                            // ];
    
                            // $sellerPaymentHistoryInsert = DB::table('pq_wallet_history')->insert($sellerInsertHistory);

                            if($validateItemDetail->pea_is_emd_required == 1)
                            {
                                $returnEMD = DB::table('pq_emd_paid_history')
                                                ->where('peph_event_additem_id', '=',  $validateLot->pla_lotid)
                                                ->get();

                                foreach($returnEMD as $returnVal)
                                {                                
                                    $validateSellerWalletCheck = DB::table('pq_wallet_current')
                                                                ->where('pwc_login_id', '=',  $returnVal->peph_loginid)
                                                                ->first();

                                    if(isset($validateSellerWalletCheck))
                                    {
                                        $latBalance = $returnVal->peph_emd_value + $validateSellerWalletCheck->pwc_current_balance;

                                        $sellerCurrentBalanceUpdate = [
                                            'pwc_current_balance' => $latBalance,
                                            'pwc_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                            'pwc_browser' => $user_browser['name'],  
                                            'pwc_browser_ver' =>  $user_browser['version'],
                                            'pwc_os' => $user_os,
                                            'pwc_updated_at' => $date
                                        ];

                                        $updateSellerWalletValue = DB::table('pq_wallet_current')
                                                                ->where('pwc_login_id', '=',  $returnVal->peph_loginid)
                                                                ->update($sellerCurrentBalanceUpdate);

                                        if($updateSellerWalletValue)
                                        {
                                            $emdInsert = [
                                                'pwh_wallet_id' => $validateWallet->pwc_wallet_id,
                                                'pwh_balance' => $returnVal->peph_emd_value,
                                                'pwh_do' => 2, //added
                                                'pwh_for' => '2',
                                                'pwh_receiver_loginid' => $returnVal->peph_loginid,
                                                'pwh_receiver_wallet_id' => $validateSellerWallet->pwc_wallet_id,
                                                'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                                'pwh_browser' => $user_browser['name'],  
                                                'pwh_browser_ver' =>  $user_browser['version'],
                                                'pwh_os' => $user_os,
                                                'pwh_loginid' => $log_id,
                                            ];
                    
                                            $sellerEmdInsert = DB::table('pq_wallet_history')->insert($emdInsert);

                                            if($sellerEmdInsert)
                                            {
                                                $pephUpdate = [
                                                    'peph_is_emd_returned' => '1'
                                                ];
                                                
                                                $returnEMDFlagUpdate = DB::table('pq_emd_paid_history')
                                                                ->where('peph_id', '=', $returnVal->peph_id)
                                                                ->update($pephUpdate);
                                            }

                                        }                                 
                                                                        
                                    }
                                    
                                }
                            }

                            $updateAuctionStatus = [
                                'pla_payment_status' => '1',
                                'pla_payment_date' => $date
                            ];
                            
                            $updatePaymentStatus = DB::table('pq_live_auction')
                                                    ->where('pla_id', '=',  $validateLot->pla_id)
                                                    ->update($updateAuctionStatus);

                            if($updatePaymentStatus)
                            {
                                $result = array(
                                    'status' => true,
                                    'message' => 'Paid to seller successfully',	
                                    'c_code'  =>1
                                );
                            }
                            else
                            {
                                $result = array(
                                    'status' => false,
                                    'message' => 'Payment not updated',	
                                    'c_code'  =>2
                                );
                            }
                        }
                        else
                        {
                            $result = array(
                                'status' => false,
                                'message' => 'Something Went Wrong',	
                                'c_code'  =>2
                            );
                        }      
                    }
                    else
                    {
                        $result = array(
                            'status' => false,
                            'message' => 'Buyer wallet not updated',	
                            'c_code'  =>2
                        );
                    }
                }
                else
                {
                    $result = array(
                        'status' => false,
                        'message' => 'Wallet not exist',	
                        'c_code'  =>2
                    );
                }               
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Lot not exist',	
                    'c_code'  =>2
                );
            }            
            
       }catch(\Exception $e){	

              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
        }

        return response()->json($result);

    }

    public function payEmdValue(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();

       
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        DB::beginTransaction();

        try{
          
          $addItemID=$input['lot_id'];
          $comp_id= $session_val['pli_comp_id'];
          $log_id= $session_val['pli_loginId'];
          $current_balance = $input['current_balance'];
          $emd_value = $input['emd_value'];

            $user_os 		= PurchaseController::getOS();	
            $user_browser   = PurchaseController::getBrowser();
            
            $validateLot = DB::table('pq_event_additem')
                                ->where('pea_id', '=',  $addItemID)
                                ->first();

            if(isset($validateLot) && $validateLot != null)
            {
                $validateWallet = DB::table('pq_wallet_current')
                                    ->where('pwc_login_id', '=',  $log_id)
                                    ->first();

                if(isset($validateWallet) && $validateWallet != null)
                {
                    $current_balance = $validateWallet->pwc_current_balance - $validateLot->pea_event_emd_value;

                    $updateWallet = [
                        'pwc_current_balance' => $current_balance,
                        'pwc_updated_at' => $date
                    ];

                    $updateWalletValue = DB::table('pq_wallet_current')
                                    ->where('pwc_login_id', '=',  $log_id)
                                    ->update($updateWallet);

                    $validateBuyer = DB::table('pq_wallet_current')
                                        ->where('pwc_login_id', '=',  $validateLot->pea_event_additem_loginid)
                                        ->first();

                    if($updateWalletValue) 
                    {
                        if(isset($validateBuyer) && $validateBuyer != null)
                        {
                            $buyerLatestBal = $validateLot->pea_event_emd_value + $validateBuyer->pwc_current_balance;

                            $updateBuyerData = [
                                'pwc_current_balance' => $buyerLatestBal,
                                'pwc_updated_at' => $date
                            ];

                            $updateBuyer = DB::table('pq_wallet_current')
                                            ->where('pwc_login_id', '=',  $validateLot->pea_event_additem_loginid)
                                            ->update($updateBuyerData);

                            if($updateBuyer)
                            {
                                $buyerInsertHistory = [
                                    'pwh_wallet_id' => $validateWallet->pwc_wallet_id,
                                    'pwh_balance' => $validateLot->pea_event_emd_value,
                                    'pwh_do' => 2, //spend
                                    'pwh_for' => '1',
                                    'pwh_receiver_loginid' => $validateLot->pea_event_additem_loginid,
                                    'pwh_receiver_wallet_id' => $validateBuyer->pwc_wallet_id,
                                    'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                    'pwh_browser' => $user_browser['name'],  
                                    'pwh_browser_ver' =>  $user_browser['version'],
                                    'pwh_os' => $user_os,
                                    'pwh_loginid' => $log_id,
                                ];
    
                                $buyerHistoryInsert = DB::table('pq_wallet_history')->insert($buyerInsertHistory);
                            }
    
                            $emdHistory = [
                                'peph_wallet_id' => $validateWallet->pwc_wallet_id,
                                'peph_loginid' => $log_id,
                                'peph_event_id' => $validateLot->pea_event_id,
                                'peph_event_additem_id' => $addItemID,
                                'peph_emd_value' => $validateLot->pea_event_emd_value,
                                'peph_buyer_loginid' => $validateLot->pea_event_additem_loginid,
                                'peph_buyer_wallet_id' => $validateBuyer->pwc_wallet_id,
                                'peph_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                'peph_browser' => $user_browser['name'],  
                                'peph_browser_ver' =>  $user_browser['version'],
                                'peph_os' => $user_os,
                            ];
    
                            $insertEmdHistory = DB::table('pq_emd_paid_history')->insert($emdHistory);
                        }
                        else
                        {
                            $buyerLatestBal = $validateLot->pea_event_emd_value;
                            $wallet_id = 'WA'.uniqid();

                            $updateBuyerData = [
                                'pwc_wallet_id' =>  $wallet_id,
                                'pwc_login_id' => $validateLot->pea_event_additem_loginid,
                                'pwc_current_balance' => $buyerLatestBal,
                                'pwc_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                'pwc_browser' => $user_browser['name'],  
                                'pwc_browser_ver' => $user_browser['version'],
                                'pwc_os' => $user_os,
                                'pwc_created_at' => $date
                            ];

                            $updateBuyer = DB::table('pq_wallet_current')
                                            ->where('pwc_login_id', '=',  $validateLot->pea_event_additem_loginid)
                                            ->insert($updateBuyerData);

                            $validateBuyer = DB::table('pq_wallet_current')
                                    ->where('pwc_login_id', '=',  $validateLot->pea_event_additem_loginid)
                                    ->first();

                            if($updateBuyer)
                            {               
                                $buyerInsertHistory = [
                                    'pwh_wallet_id' => $validateWallet->pwc_wallet_id,
                                    'pwh_balance' => $validateLot->pea_event_emd_value,
                                    'pwh_do' => 2, //spend
                                    'pwh_for' => '1',
                                    'pwh_receiver_loginid' => $validateLot->pea_event_additem_loginid,
                                    'pwh_receiver_wallet_id' => $validateBuyer->pwc_wallet_id,
                                    'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                    'pwh_browser' => $user_browser['name'],  
                                    'pwh_browser_ver' =>  $user_browser['version'],
                                    'pwh_os' => $user_os,
                                    'pwh_loginid' => $log_id,
                                ];
    
                                $buyerHistoryInsert = DB::table('pq_wallet_history')->insert($buyerInsertHistory);
                            }
    
                            $emdHistory = [
                                'peph_wallet_id' => $validateWallet->pwc_wallet_id,
                                'peph_loginid' => $log_id,
                                'peph_event_id' => $validateLot->pea_event_id,
                                'peph_event_additem_id' => $addItemID,
                                'peph_emd_value' => $validateLot->pea_event_emd_value,
                                'peph_buyer_loginid' => $validateLot->pea_event_additem_loginid,
                                'peph_buyer_wallet_id' => $validateBuyer->pwc_wallet_id,
                                'peph_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                'peph_browser' => $user_browser['name'],  
                                'peph_browser_ver' =>  $user_browser['version'],
                                'peph_os' => $user_os,
                            ];
    
                            $insertEmdHistory = DB::table('pq_emd_paid_history')->insert($emdHistory);
                        }

                        if($insertEmdHistory)
                        {
                            // $insertHistory = [
                            //     'pwh_wallet_id' => $validateWallet->pwc_wallet_id,
                            //     'pwh_balance' => $emd_value,
                            //     'pwh_do' => 1, //added
                            //     'pwh_for' => '6',
                            //     'pwh_receiver_loginid' => $validateLot->pea_event_additem_loginid,
                            //     'pwh_receiver_wallet_id' => $validateBuyer->pwc_wallet_id,
                            //     'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                            //     'pwh_browser' => $user_browser['name'],  
                            //     'pwh_browser_ver' =>  $user_browser['version'],
                            //     'pwh_os' => $user_os,
                            //     'pwh_loginid' => $log_id,
                            // ];

                            // $historyInsert = DB::table('pq_wallet_history')->insert($insertHistory);

                            // if($historyInsert)
                            // {
                            //     $result = array(
                            //         'status' => true,
                            //         'message' => 'EMD Paid Successfully',	
                            //         'c_code'  =>1
                            //     );
                            // }
                            // else
                            // {
                            //     DB::rollback();

                            //     $result = array(
                            //         'status' => false,
                            //         'message' => 'Something Went Wrong',	
                            //         'c_code'  =>2
                            //     );
                            // }

                            $result = array(
                                'status' => true,
                                'message' => 'EMD Paid Successfully',	
                                'c_code'  =>1
                            );
                        }
                        else
                        {
                            DB::rollback();

                            $result = array(
                                'status' => false,
                                'message' => 'Something Went Wrong',	
                                'c_code'  =>2
                            );
                        }
                    }
                    else
                    {
                        DB::rollback();
                    }
                }
                else
                {
                    $result = array(
                        'status' => false,
                        'message' => 'Wallet not exist',	
                        'c_code'  =>2
                    );
                }               
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Lot not exist',	
                    'c_code'  =>2
                );
            }            
            
       }catch(\Exception $e){	

            DB::rollback();

            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
            
            );
         }

        DB::commit();

        return response()->json($result);

    }

    public function addmoney(Request $request)
    {
        $input = $request->all();

        $amount = $input['wallet_amount'];
        $login_id = $input['loginid'];
        $payment_method = $input['payment_method'];

        if($payment_method == 2)
        {
            $apiEndpoint = "https://test.cashfree.com";
            $opUrl = $apiEndpoint."/api/v1/order/create";
            
            $cf_request = array();
           // $cf_request["appId"] = "450180a7d726f21a3e14e5621054"; -- pradeep acc
          //  $cf_request["secretKey"] = "cd63b792112a2d83f5a375929ac73c1a3af98583"; --pradeep acc
            $cf_request["appId"] = "44700864c72dd1d0649103ce0744";
            $cf_request["secretKey"] = "2d286d1b61ecf125ab8d391266b0e7ef63a13280"; 
            $cf_request["orderId"] = uniqid();
            $cf_request["orderAmount"] = $amount;
            $cf_request["orderNote"] = "Subscription";
            $cf_request["customerPhone"] = "8508591926";
            $cf_request["customerName"] = "SRINI";
            $cf_request["customerEmail"] = "srinivasanit14@gmail.com";
            $cf_request["returnUrl"] = url('/wallet-cashfree-callback');
            $cf_request["notifyUrl"] = "https://docs.cashfree.com/docs/rest/guide/";

            $timeout = 10;
            
            $request_string = "";
            foreach($cf_request as $key=>$value) {
                $request_string .= $key.'='.rawurlencode($value).'&';
            }
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"$opUrl?");
            curl_setopt($ch,CURLOPT_POST, count($cf_request));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $request_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            $curl_result=curl_exec ($ch);
            curl_close ($ch);

            $jsonResponse = json_decode($curl_result);
            if ($jsonResponse->{'status'} == "OK") {
                $paymentLink = $jsonResponse->{"paymentLink"};

                return Redirect::to($paymentLink);
                //Send this payment link to customer over email/SMS OR redirect to this link on browser
            } else {
                //Log request, $jsonResponse["reason"]
                print_r($jsonResponse);die;
            }
        }
        else
        {
            $ch = curl_init();

            //testing
            curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/api/1.1/payment-requests/');
    
            //live
            //curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                        array("X-Api-Key:test_dddff182ff25a04b635de22e553",
                            "X-Auth-Token:test_80eae5c5eec06cd26cfb543e31f"));  //testing keys
                        /* array("X-Api-Key:a842dddcc83f15650c3461bf235e12ec",
                            "X-Auth-Token:dfeb19afa0a5b1e5ea11484dcd3128d4")); */ //live
            $payload = Array(
                'purpose' => 'Wallet add',
                'amount' => $amount,
                //'amount' => 10,
                'phone' => '8508591926',
                'buyer_name' => 'Hello',
                'redirect_url' => url('/wallet-insta-callback'),
                'send_email' => false,
                'webhook' => 'http://instamojo.com/webhook/',
                'send_sms' => false,
                'email' => $login_id,
                'allow_repeated_payments' => false
            );
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
            $response = curl_exec($ch);
            curl_close($ch); 
            
            $data = json_decode($response, true);
            $url = $data['payment_request']['longurl'];
    
            return Redirect::to($url);
        }

    }

    public function cashfreecallback(Request $request)
    {
        $input = $request->all();  

        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        if($input['txStatus'] === 'SUCCESS')
        {
            $this->addmoneytowallet($login_id, $input['orderAmount']);
            $url = 'http://127.0.0.1:8000/wallet';
            return Redirect::to($url);
        }
        else
        {
            $url = 'http://127.0.0.1:8000/cashfree-failure';
            return Redirect::to($url);
            
            //echo 'payment failed';
        }
    }

    public function walletinstacallback(Request $request)
    {
        $input = $request->all();        

        print_r($input);
        die;

        if($input['txStatus'] === 'SUCCESS')
        {
            $this->addmoneytowallet($login_id, $input['orderAmount']);
        }
        else
        {
            echo 'payment failed';
        }
        
    }

    public function addmoneytowallet($loginid, $amount)
    {
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');
        $user_os 		= GetOsDetails::getOS();
        $user_browser   = GetOsDetails::getBrowser();
        
        $wallet = DB::table('pq_wallet_current')
						->where('pwc_login_id', '=', $loginid)					
                        ->first();
                        
        if(isset($wallet))
        {
            $updated_amount = $wallet->pwc_current_balance + $amount;

            $update_array = [
                'pwc_current_balance' => $updated_amount,
                'pwc_ipaddress' => $_SERVER['REMOTE_ADDR'],
                'pwc_browser' => $user_browser['name'],  
                'pwc_browser_ver' =>  $user_browser['version'],
                'pwc_os' => $user_os,
                'pwc_updated_at' => $date
            ];

            $updateModel = DB::table('pq_wallet_current')
                            ->where('pwc_wallet_id', '=',  $wallet->pwc_wallet_id)
                            ->update($update_array);

            if($updateModel)
            {
                $history_insert = [
                    'pwh_wallet_id' => $wallet->pwc_wallet_id,
                    'pwh_balance' => $amount,
                    'pwh_do' => 1, //added
                    'pwh_for' => '5',
                    'pwh_receiver_loginid' => $loginid,
                    'pwh_receiver_wallet_id' => $wallet->pwc_wallet_id,
                    'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                    'pwh_browser' => $user_browser['name'],  
                    'pwh_browser_ver' =>  $user_browser['version'],
                    'pwh_os' => $user_os,
                    'pwh_loginid' => $loginid,
                ];

                $historyInsert = DB::table('pq_wallet_history')->insert($history_insert);

                $url = 'http://127.0.0.1:8000/wallet';
                return Redirect::to($url);
            }
        }
        else
        {
            $wallet_id = 'WA'.uniqid();

            $insert_array = [
                'pwc_wallet_id' => $wallet_id,
                'pwc_login_id' => $loginid,
                'pwc_current_balance' => $amount,
                'pwc_ipaddress' => $_SERVER['REMOTE_ADDR'],
                'pwc_browser' => $user_browser['name'],  
                'pwc_browser_ver' =>  $user_browser['version'],
                'pwc_os' => $user_os
            ];

            $moneyInsert = DB::table('pq_wallet_current')->insert($insert_array);

            if($moneyInsert)
            {
                $history_insert = [
                    'pwh_wallet_id' => $wallet_id,
                    'pwh_balance' => $amount,
                    'pwh_do' => 1, //added
                    'pwh_for' => '5',
                    'pwh_receiver_loginid' => $loginid,
                    'pwh_receiver_wallet_id' => $wallet_id,
                    'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                    'pwh_browser' => $user_browser['name'],  
                    'pwh_browser_ver' =>  $user_browser['version'],
                    'pwh_os' => $user_os,
                    'pwh_loginid' => $loginid,
                ];

                $historyInsert = DB::table('pq_wallet_history')->insert($history_insert);

                if($historyInsert)
                {
                    echo 'successfully insert';
                }
                else
                {
                    echo 'something went wrong';
                }
            }
        }
    }
}
