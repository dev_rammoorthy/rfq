<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Crypt;
use Session;
use DateTime;
use Illuminate\Support\Facades\DB;
use Dompdf\Dompdf;
use setasign\Fpdi\Fpdi;
use Mail; 
use Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('event_manager.confirm-payment');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //
        $input = $request->all();
        $order_id = uniqid() . ( string ) $input['pli_loginId'];
        $price = $input['pmp_pkg_amount'];

		$session_val = session()->all(); 		
		session(['package_data' => $input ]);

        $data_for_request = $this->handlePaytmRequest($order_id, $price);

        $paytm_txn_url = 'https://securegw-stage.paytm.in/theia/processTransaction';
        $paramList = $data_for_request['paramList'];
        $checkSum = $data_for_request['checkSum'];

        return view('event_manager.paytm-merchant-form', compact('paytm_txn_url', 'paramList', 'checkSum'));
    }

    public function handlePaytmRequest($order_id, $amount) {
        // Load all functions of encdec_paytm.php and config-paytm.php
        $this->getAllEncDecFunc();
        $this->getConfigPaytmSettings();

        $checkSum = "";
        $paramList = array();

            // Create an array having all required parameters for creating checksum.
        $paramList["MID"] = 'Websit57239737375544';
        $paramList["ORDER_ID"] = $order_id;
        $paramList["CUST_ID"] = $order_id;
        $paramList["INDUSTRY_TYPE_ID"] = 'Retail';
        $paramList["CHANNEL_ID"] = 'WEB';
        $paramList["TXN_AMOUNT"] = $amount;
        $paramList["WEBSITE"] = 'WEBSTAGING';
        $paramList["CALLBACK_URL"] = url( '/paytm-callback' );
        $paytm_merchant_key = '31Q9BhP7U9JVip77';

        //Here checksum string will return by getChecksumFromArray() function.
        $checkSum = getChecksumFromArray( $paramList, $paytm_merchant_key );

        return array(
            'checkSum' => $checkSum,
                'paramList' => $paramList
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    function getAllEncDecFunc()
    {
        function encrypt_e($input, $ky) {
            $key   = html_entity_decode($ky);
            $iv = "@@@@&&&&####$$$$";
            $data = openssl_encrypt ( $input , "AES-128-CBC" , $key, 0, $iv );
            return $data;
        }

        function decrypt_e($crypt, $ky) {
            $key   = html_entity_decode($ky);
            $iv = "@@@@&&&&####$$$$";
            $data = openssl_decrypt ( $crypt , "AES-128-CBC" , $key, 0, $iv );
            return $data;
        }

        function generateSalt_e($length) {
            $random = "";
            srand((double) microtime() * 1000000);

            $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
            $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
            $data .= "0FGH45OP89";

            for ($i = 0; $i < $length; $i++) {
                $random .= substr($data, (rand() % (strlen($data))), 1);
            }

            return $random;
        }

        function checkString_e($value) {
            if ($value == 'null')
                $value = '';
            return $value;
        }

        function getChecksumFromArray($arrayList, $key, $sort=1) {
            if ($sort != 0) {
                ksort($arrayList);
            }
            $str = getArray2Str($arrayList);
            $salt = generateSalt_e(4);
            $finalString = $str . "|" . $salt;
            $hash = hash("sha256", $finalString);
            $hashString = $hash . $salt;
            $checksum = encrypt_e($hashString, $key);
            return $checksum;
        }
        function getChecksumFromString($str, $key) {
            
            $salt = generateSalt_e(4);
            $finalString = $str . "|" . $salt;
            $hash = hash("sha256", $finalString);
            $hashString = $hash . $salt;
            $checksum = encrypt_e($hashString, $key);
            return $checksum;
        }

        function verifychecksum_e($arrayList, $key, $checksumvalue) {
            $arrayList = removeCheckSumParam($arrayList);
            ksort($arrayList);
            $str = getArray2StrForVerify($arrayList);
            $paytm_hash = decrypt_e($checksumvalue, $key);
            $salt = substr($paytm_hash, -4);

            $finalString = $str . "|" . $salt;

            $website_hash = hash("sha256", $finalString);
            $website_hash .= $salt;

            $validFlag = "FALSE";
            if ($website_hash == $paytm_hash) {
                $validFlag = "TRUE";
            } else {
                $validFlag = "FALSE";
            }
            return $validFlag;
        }

        function verifychecksum_eFromStr($str, $key, $checksumvalue) {
            $paytm_hash = decrypt_e($checksumvalue, $key);
            $salt = substr($paytm_hash, -4);

            $finalString = $str . "|" . $salt;

            $website_hash = hash("sha256", $finalString);
            $website_hash .= $salt;

            $validFlag = "FALSE";
            if ($website_hash == $paytm_hash) {
                $validFlag = "TRUE";
            } else {
                $validFlag = "FALSE";
            }
            return $validFlag;
        }

        function getArray2Str($arrayList) {
            $findme   = 'REFUND';
            $findmepipe = '|';
            $paramStr = "";
            $flag = 1;	
            foreach ($arrayList as $key => $value) {
                $pos = strpos($value, $findme);
                $pospipe = strpos($value, $findmepipe);
                if ($pos !== false || $pospipe !== false) 
                {
                    continue;
                }
                
                if ($flag) {
                    $paramStr .= checkString_e($value);
                    $flag = 0;
                } else {
                    $paramStr .= "|" . checkString_e($value);
                }
            }
            return $paramStr;
        }

        function getArray2StrForVerify($arrayList) {
            $paramStr = "";
            $flag = 1;
            foreach ($arrayList as $key => $value) {
                if ($flag) {
                    $paramStr .= checkString_e($value);
                    $flag = 0;
                } else {
                    $paramStr .= "|" . checkString_e($value);
                }
            }
            return $paramStr;
        }

        function redirect2PG($paramList, $key) {
            $hashString = getchecksumFromArray($paramList);
            $checksum = encrypt_e($hashString, $key);
        }

        function removeCheckSumParam($arrayList) {
            if (isset($arrayList["CHECKSUMHASH"])) {
                unset($arrayList["CHECKSUMHASH"]);
            }
            return $arrayList;
        }

        function getTxnStatus($requestParamList) {
            return callAPI(PAYTM_STATUS_QUERY_URL, $requestParamList);
        }

        function getTxnStatusNew($requestParamList) {
            return callNewAPI(PAYTM_STATUS_QUERY_NEW_URL, $requestParamList);
        }

        function initiateTxnRefund($requestParamList) {
            $CHECKSUM = getRefundChecksumFromArray($requestParamList,PAYTM_MERCHANT_KEY,0);
            $requestParamList["CHECKSUM"] = $CHECKSUM;
            return callAPI(PAYTM_REFUND_URL, $requestParamList);
        }

        function callAPI($apiURL, $requestParamList) {
            $jsonResponse = "";
            $responseParamList = array();
            $JsonData =json_encode($requestParamList);
            $postData = 'JsonData='.urlencode($JsonData);
            $ch = curl_init($apiURL);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                         
            'Content-Type: application/json', 
            'Content-Length: ' . strlen($postData))                                                                       
            );  
            $jsonResponse = curl_exec($ch);   
            $responseParamList = json_decode($jsonResponse,true);
            return $responseParamList;
        }

        function callNewAPI($apiURL, $requestParamList) {
            $jsonResponse = "";
            $responseParamList = array();
            $JsonData =json_encode($requestParamList);
            $postData = 'JsonData='.urlencode($JsonData);
            $ch = curl_init($apiURL);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                         
            'Content-Type: application/json', 
            'Content-Length: ' . strlen($postData))                                                                       
            );  
            $jsonResponse = curl_exec($ch);   
            $responseParamList = json_decode($jsonResponse,true);
            return $responseParamList;
        }
        function getRefundChecksumFromArray($arrayList, $key, $sort=1) {
            if ($sort != 0) {
                ksort($arrayList);
            }
            $str = getRefundArray2Str($arrayList);
            $salt = generateSalt_e(4);
            $finalString = $str . "|" . $salt;
            $hash = hash("sha256", $finalString);
            $hashString = $hash . $salt;
            $checksum = encrypt_e($hashString, $key);
            return $checksum;
        }
        function getRefundArray2Str($arrayList) {	
            $findmepipe = '|';
            $paramStr = "";
            $flag = 1;	
            foreach ($arrayList as $key => $value) {		
                $pospipe = strpos($value, $findmepipe);
                if ($pospipe !== false) 
                {
                    continue;
                }
                
                if ($flag) {
                    $paramStr .= checkString_e($value);
                    $flag = 0;
                } else {
                    $paramStr .= "|" . checkString_e($value);
                }
            }
            return $paramStr;
        }
        function callRefundAPI($refundApiURL, $requestParamList) {
            $jsonResponse = "";
            $responseParamList = array();
            $JsonData =json_encode($requestParamList);
            $postData = 'JsonData='.urlencode($JsonData);
            $ch = curl_init($apiURL);	
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $refundApiURL);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  
            $jsonResponse = curl_exec($ch);   
            $responseParamList = json_decode($jsonResponse,true);
            return $responseParamList;
        }
    }

    function getConfigPaytmSettings()
    {
        define('PAYTM_ENVIRONMENT', 'TEST'); // PROD
        define('PAYTM_MERCHANT_KEY', '31Q9BhP7U9ddJVip77'); //Change this constant's value with Merchant key received from Paytm.
        define('PAYTM_MERCHANT_MID', 'Websit5723d9d737375544'); //Change this constant's value with MID (Merchant ID) received from Paytm.
        define('PAYTM_MERCHANT_WEBSITE', 'WEBSTAGING'); //Change this constant's value with Website name received from Paytm.

        $PAYTM_STATUS_QUERY_NEW_URL='https://securegw-stage.paytm.in/merchant-status/getTxnStatus';
        $PAYTM_TXN_URL='https://securegw-stage.paytm.in/theia/processTransaction';
        if (PAYTM_ENVIRONMENT == 'PROD') {
            $PAYTM_STATUS_QUERY_NEW_URL='https://securegw.paytm.in/merchant-status/getTxnStatus';
            $PAYTM_TXN_URL='https://securegw.paytm.in/theia/processTransaction';
        }

        define('PAYTM_REFUND_URL', '');
        define('PAYTM_STATUS_QUERY_URL', $PAYTM_STATUS_QUERY_NEW_URL);
        define('PAYTM_STATUS_QUERY_NEW_URL', $PAYTM_STATUS_QUERY_NEW_URL);
        define('PAYTM_TXN_URL', $PAYTM_TXN_URL);
    }

    public function paytmCallback( Request $request ) {
            $order_id = $request['ORDERID'];
    
            $user_id = auth()->id();
            //$user = User::find( $user_id );

            $input = $request->all();

            session(['payment_data' => $input ]);
    
            if ( 'TXN_SUCCESS' === $request['STATUS'] ) {

                $session_val = session()->all();
                $this->create_event($session_val);
                
                    // $transaction_id = $request['TXNID'];
                    // $order = Order::where( 'order_id', $order_id )->first();
                    // $order->status = 'complete';
                    // $order->payment_received = 'yes';
                    // $order->payment_id = $transaction_id;
                    // $order->save();
                    // return view( 'order-complete', compact( 'order', 'status' ) );
    
            } else if( 'TXN_FAILURE' === $request['STATUS'] ){                
                $encrypted1 = Crypt::encryptString("0");
                Redirect::to('/paytm-payment-status/fail/'.$encrypted1)->send();
            }
    }

    public function create_event($data)
	{
		$input = $data['package_data'];	
	 
		$dt 	= new DateTime();
		$date	= $dt->format('Y-m-d H:i:s');
		
		$str = preg_replace('/\D/', '', $input['pmp_pkg_validity']);
		
		$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));				
		
        try 
        {
			$package = DB::table('pq_buynow_package')
						->where('pbp_comp_id', '=', $input['pli_comp_id'])
						->where('pbp_pkg_id', '=', $input['pmp_id'])						
						->first();
						
			
			$packagellist = DB::table('pq_master_package')        
							->select('*')
							->where('pmp_id',$input['pmp_id'])
							->get();
			
            if(isset($package->pbp_pkg_cur_count))
            {
				$pack_count=$package->pbp_pkg_cur_count;
            }
            else
            {
				$pack_count=0;
			}
				
            if ((count($package)==0 )|| ($pack_count==0 ) )
            {
                // It does not exist - add to favorites button will show
                    
                    $package_data =	[
                            'pbp_comp_id' 		 	=>  $input['pli_comp_id'],
                            'pbp_login_id'  		=>  $input['pli_loginId'],
                            'pbp_pkg_id'  			=>  $packagellist[0]->pmp_id,
                            'pbp_pkg_eventcount' 	=>  $packagellist[0]->pmp_pkg_eventcount,
                            'pbp_pkg_cur_count'  	=>  $packagellist[0]->pmp_pkg_eventcount,
                            'pbp_pkg_start_dt'  	=>  $date,
                            'pbp_pkg_end_dt'  		=>  $enddate,
                            'pbp_pkg_status'  		=>  '1',                            
                        ];

                        if(($pack_count==0 )&&(count($package)==0 ))
                        {
                            $packageid = DB::table('pq_buynow_package')->insertGetId($package_data);
                        }
                        else
                        {                            
                            $packageid = DB::table('pq_buynow_package')
                                            ->where('pbp_id', '=', $package->pbp_id)
                                            ->update($package_data);
                        }					
                    
                    $encrypted = Crypt::encryptString($packageid);

                    if($packageid)
                    {								
                        Redirect::to('/paytm-payment-status/success/'.$encrypted)->send();
                        
                    }

            } else {
                // It exists - remove from favorites button will show			
                    $encrypted1 = Crypt::encryptString("0");
                    Redirect::to('/paytm-payment-status/fail/'.$encrypted1)->send();         
            }       
        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2
            );
        }

       
    }
    
    public function purchaseorder(Request $request)
    {
        $input = $request->all();
     
        $session_val = session()->all();
        $log_id= $session_val['pli_loginId'];

		$dt 	= new DateTime();
        $todayDate	= $dt->format('Y-m-d H:i:s');

        if($request->hasFile('file')){

            $validator = Validator::make($request->all(), [
                'file'   => 'mimes:png,jpg,jpeg'
            ]);
            if ($validator->fails()) {
                
                $result = array(
                    'status' => false,
                    'message' => "please upload only jpg,png,jpeg image",				
                );
                return response()->json($result);
            }
        }

        if($input['deliveryDateOption'] == 1)
        {
            $validator1 = Validator::make($request->all(), [
                'payment_date' => 'required'
            ]);
            if ($validator1->fails()) {
                
                $result1 = array(
                    'status' => false,
                    'message' => "please choose one approximate payment date",				
                );
                return response()->json($result1);
            }
        }
        else
        {
            $validator2 = Validator::make($request->all(), [
                'otherOptionDeliveryAddress' => 'required'
            ]);
            if ($validator2->fails()) {
                
                $result2 = array(
                    'status' => false,
                    'message' => "please fill the open text field",				
                );
                return response()->json($result2);
            }
        }

        try 
        {
            $addressId = $input['deliver_address'];

            $logo = $input['logo_upload'];
            $lotID = $input['hfPOLotId'];
            $currentBid = $input['hfCurrentBid'];
            $deliver_date_option = $input['deliveryDateOption'];
            $date = $input['payment_date'];
            $other = $input['otherOptionDeliveryAddress'];
            
            $sellerCorpAddress = DB::table('pq_live_auction')
                                ->select('pq_company_info.*', 'pq_event_additem.pea_event_id', 'pq_event_additem.pea_id', 'pq_live_auction.pla_id', 'pq_event_additem.pea_event_spec', 'pq_event_additem.pea_event_unit_quantity', 'pq_master_unit.pmu_unit_name')
                                ->Join('pq_company_info', 'pq_live_auction.pla_seller_compid' , '=', 'pq_company_info.pci_comp_id')
                                ->Join('pq_event_additem', 'pq_live_auction.pla_lotid' , '=', 'pq_event_additem.pea_id')    
                                ->Join('pq_master_unit', 'pq_event_additem.pea_event_unit' , '=', 'pq_master_unit.pmu_id')                              
                                ->where('pq_live_auction.pla_cur_bid', $currentBid)
                                ->where('pq_live_auction.pla_lotid', $lotID)
                                ->first();

            $shippingAddress = DB::table('pq_shipping_address')
                                ->select('pq_shipping_address.*', 'pq_company_info.pci_comp_name', 'pq_company_info.pci_comp_phone')
                                ->Join('pq_login_info', 'pq_shipping_address.psa_loginid' , '=', 'pq_login_info.pli_loginid')
                                ->Join('pq_company_info', 'pq_login_info.pli_comp_id' , '=', 'pq_company_info.pci_comp_id')
                                ->where('pq_shipping_address.psa_id', $addressId)
                                ->first();

            $dompdf = new Dompdf();

            $orderIdGenerate = 'PO'.$sellerCorpAddress->pea_id.''.$sellerCorpAddress->pla_id;

            $pdfroot = 'C:/xampp/htdocs/PQ/public/PO/'.$orderIdGenerate.'.pdf';

            if($deliver_date_option == 1)
            {
                $paymentDate = DB::table('pq_master_delivery')
                                ->select('pq_master_delivery.pmd_delivery_time', 'pq_master_day_type.pmdt_day_type')
                                ->Join('pq_master_day_type', 'pq_master_delivery.pmd_day_type' , '=', 'pq_master_day_type.pmdt_id')
                                ->where('pq_master_delivery.pmd_id', $date)
                                ->first();

                $printPaymentDate = $paymentDate->pmd_delivery_time.' '.$paymentDate->pmdt_day_type;
            }
            else
            {
                $printPaymentDate = $other;
            }

            $insertPO = [
                'order_id' => $orderIdGenerate,
                'event_id'  => $sellerCorpAddress->pea_event_id,
                'lot_id'    => $lotID,
                'live_action_id'    => $sellerCorpAddress->pla_id,
                'buser_delivery_address' => $addressId,
                'seller_confirmed'  => 'false',
                'ppo_is_master_payment_date' => $deliver_date_option,
                'ppo_payment_date' => ($deliver_date_option == 1) ? $date : $other,
                'document_path'     => '/PO/'.$orderIdGenerate.'.pdf',
                'loginid'   => $log_id
            ];


            $poInsert = DB::table('pq_purchase_orders')
                        ->insertGetId($insertPO);            

            if($poInsert)
            {
                

                if($request->hasFile('file')){
                    $extension = $request->file('file');
                    $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
                    $dir = 'buyer-logo';
                    $filename = $orderIdGenerate.'.'.$extension;
                        $request->file('file')->move($dir, $filename);
                    $path=  $dir.'/'. $filename;

                    $updateItemFileLocation =	[    
                        'ppo_buyer_logo' =>   $path                        
                    ];
    
                    $upadteFileLocation = DB::table('pq_purchase_orders')
                                            ->where('id', $poInsert)
                                            ->update($updateItemFileLocation);

                    $dompdf->loadHtml('
                        <style>
                            footer {
                                position: fixed; 
                                bottom: -60px; 
                                left: 0px; 
                                right: 0px;
                                height: 50px; 
        
                                /** Extra personal styles **/
                                border-top: 2px solid #D3D3D3;
                                color:black;
                                text-align: center;
                                line-height: 35px;
                            }
        
                            td, th {    
                                padding: 8px;
                                font-size: 14px 
                            }
        
                            #items td, th {    
                                border-bottom: 1px solid #dddddd;
                            }
        
                        </style>
                        <body>             
                            <footer>
                                Copyright &copy; 2019 Simplified Procurement Solutions(OPC) Pvt Ltd. All Rights Reserved.
                            </footer>  
                            <table width="100%">
                                <tr>                        
                                    <td> 
                                        <img src="'.$path.'" height="100" width="200" /> 
        
                                        <h3>Simplified Procurement Solutions(OPC) Pvt Ltd</h3>
                                        <p>
                                            Chamier Towers, 37/6 1st floor, Pasumpon Muthuramalinga Thevar Rd,
                                            <br> 
                                            Teynampet, Chennai, Tamil Nadu 600028
                                            <br>
                                            044 4343 7474
                                        </p>
                                    </td>    
                                    <td align="right">
                                        <h1>PURCHASE ORDER</h1>
                                        <p>
                                            #'.$orderIdGenerate.'
                                        </p>
                                    </td>                        
                                </tr>
                                <tr>
                                    <td align="left">
                                        <p>Vendor Address</p>
                                        <h4> '.$sellerCorpAddress->pci_comp_name.' </h4>
                                        <p>
                                            '.$sellerCorpAddress->pci_comp_address.'
                                            <br> 
                                            '.$sellerCorpAddress->pci_comp_location.'                                    
                                            <br>
                                            '.$sellerCorpAddress->pci_comp_phone.'
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <p>Shipping Address</p>
                                        <h4> '.$shippingAddress->pci_comp_name.' </h4>
                                        <p>
                                            '.$shippingAddress->psa_address_line1.'
                                            <br> 
                                            '.$shippingAddress->psa_address_line2.'
                                            <br>
                                            '.$shippingAddress->psa_country.'
                                            <br>
                                            '.$shippingAddress->psa_zipcode.'
                                            <br>
                                            '.$shippingAddress->pci_comp_phone.'
                                        </p>
                                    </td>
                                    <td align="right">
                                        <p> Date : '.$todayDate.'</p>
                                    </td>
                                </tr>
        
                                <table width="100%" id="items">                        
                                        <tr style="background-color: lightgray;">           
                                            <th>#</th>
                                            <th width="50%">Spec</th>
                                            <th>Qty</th>
                                            <th>Rate</th>
                                            <th>Amount</th>    
                                        </tr>        
                                        <tr>
                                            <td>1</td>
                                            <td>'.$sellerCorpAddress->pea_event_spec.'</td>
                                            <td>'.$sellerCorpAddress->pea_event_unit_quantity.' 
                                                ' .$sellerCorpAddress->pmu_unit_name.'
                                            </td>
                                            <td>INR '.$currentBid.'</td>
                                            <td>INR '.$currentBid.'</td>
                                        </tr>
                                        <tr>
                                            
                                            <td colspan="4" style="text-align:right">SubTotal</td>
                                            <td>INR '.$currentBid.'</td>
                                        </tr>  
                                        <tr>
                                            <td colspan="4" style="text-align:right">GST</td>
                                            <td>INR 0</td>
                                        </tr>   
                                        <tr>
                                            <td colspan="4" style="text-align:right">Total</td>
                                            <td>INR '.$currentBid.'</td>
                                        </tr>     
                                                
                                </table>

                                <tr>
                                    <p> Payment Date : '.$printPaymentDate.' </p> 
                                    <p> Place of delivery : 
                                            '.$shippingAddress->psa_address_line1.',
                                        
                                            '.$shippingAddress->psa_address_line2.',
                                        
                                            '.$shippingAddress->psa_country.',
                                        
                                            '.$shippingAddress->psa_zipcode.'
                                    </p>
                                    <p> Deliverables : All the items mentioned above </p> <br>
                                    <p> Notes : This is system generated Purchase Order</p> <br> <br>
                                </tr>        
                            
                            </table>
                            <br>
        
                        </body>
                        ');
        
                        // (Optional) Setup the paper size and orientation
                        //$dompdf->setPaper('A4', 'landscape');
        
                        // Render the HTML as PDF
                        $dompdf->render();
        
                        // Output the generated PDF to Browser
                        //$dompdf->stream('Bid Report');
        
                        $pdf_string =   $dompdf->output();
                        file_put_contents($pdfroot, $pdf_string ); 
                }

                $result = array(
                    'status' => true,
                    'message' => 'Purchase Order Generated Successfully',
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Something Went Wrong',
                    'c_code'  =>2
                );
            }

        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    public function generate_invoice(Request $request)
    {
        $input = $request->all();
     
        $session_val = session()->all();
        $log_id= $session_val['pli_loginId'];

		$dt 	= new DateTime();
        $todayDate	= $dt->format('Y-m-d H:i:s');

        if($request->hasFile('file')){

            $validator = Validator::make($request->all(), [
                'file'   => 'mimes:png,jpg,jpeg'
            ]);
            if ($validator->fails()) {
                
                $result = array(
                    'status' => false,
                    'message' => "please upload only jpg,png,jpeg image",				
                );
                return response()->json($result);
            }
        }

        try 
        {
            $addressId = $input['deliver_address'];

            $logo = $input['logo_upload'];
            $lotID = $input['hfPOLotId'];
            $currentBid = $input['hfCurrentBid'];
            
            $sellerCorpAddress = DB::table('pq_live_auction')
                                ->select('pq_purchase_orders.ppo_payment_date', 'pq_purchase_orders.ppo_is_master_payment_date', 'pq_shipping_address.*', 'pq_purchase_orders.order_id', 'pq_company_info.*', 'pq_event_additem.pea_event_id', 'pq_event_additem.pea_id', 'pq_live_auction.pla_id', 'pq_event_additem.pea_event_spec', 'pq_event_additem.pea_event_unit_quantity', 'pq_master_unit.pmu_unit_name')
                                ->Join('pq_company_info', 'pq_live_auction.pla_seller_compid' , '=', 'pq_company_info.pci_comp_id')
                                ->Join('pq_event_additem', 'pq_live_auction.pla_lotid' , '=', 'pq_event_additem.pea_id')    
                                ->Join('pq_purchase_orders', 'pq_event_additem.pea_id' , '=', 'pq_purchase_orders.lot_id')
                                ->Join('pq_shipping_address', 'pq_purchase_orders.buser_delivery_address' , '=', 'pq_shipping_address.psa_id')
                                ->Join('pq_master_unit', 'pq_event_additem.pea_event_unit' , '=', 'pq_master_unit.pmu_id')                              
                                ->where('pq_live_auction.pla_cur_bid', $currentBid)
                                ->where('pq_live_auction.pla_lotid', $lotID)
                                ->first();

            $shippingAddress = DB::table('pq_shipping_address')
                                ->select('pq_shipping_address.*', 'pq_company_info.pci_comp_name', 'pq_company_info.pci_comp_phone')
                                ->Join('pq_login_info', 'pq_shipping_address.psa_loginid' , '=', 'pq_login_info.pli_loginid')
                                ->Join('pq_company_info', 'pq_login_info.pli_comp_id' , '=', 'pq_company_info.pci_comp_id')
                                ->where('pq_shipping_address.psa_id', $addressId)
                                ->first();

            if($sellerCorpAddress->ppo_is_master_payment_date ==  '2')
            {
                $due_date = $sellerCorpAddress->ppo_payment_date;
            }
            else
            {
                $paymentDate = DB::table('pq_purchase_orders')
                                ->select('pq_master_delivery.pmd_delivery_time', 'pq_master_day_type.pmdt_day_type')
                                ->Join('pq_master_delivery', 'pq_purchase_orders.ppo_payment_date' , '=', 'pq_master_delivery.pmd_id')
                                ->Join('pq_master_day_type', 'pq_master_delivery.pmd_day_type' , '=', 'pq_master_day_type.pmdt_id')
                                ->where('pq_purchase_orders.ppo_payment_date', $sellerCorpAddress->ppo_payment_date)
                                ->first();

                $due_date = $paymentDate->pmd_delivery_time.' '.$paymentDate->pmdt_day_type;
            }

            $duedate = date('Y-m-d H:i:s', strtotime('+30 days'));

            $dompdf = new Dompdf();

            $orderIdGenerate = 'INV'.$sellerCorpAddress->pea_id.''.$sellerCorpAddress->pla_id;

            $pdfroot = 'C:/xampp/htdocs/PQ/public/Invoice/'.$orderIdGenerate.'.pdf';


            $insertInvoice = [
                'ppi_order_id' => $sellerCorpAddress->order_id,
                'ppi_invoice_id' => $orderIdGenerate,
                'ppi_seller_pickup_address' => $addressId,
                'ppi_document_path'  => '/Invoice/'.$orderIdGenerate.'.pdf',
                'ppi_due_date' => 10,
                'ppi_loginid'   => $log_id
            ];


            $invoiceInsert = DB::table('pq_purchase_invoice')
                        ->insertGetId($insertInvoice);            

            if($invoiceInsert)
            {                
                if($request->hasFile('file')){
                    $extension = $request->file('file');
                    $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
                    $dir = 'seller-logo';
                    $filename = $orderIdGenerate.'.'.$extension;
                        $request->file('file')->move($dir, $filename);
                    $path=  $dir.'/'. $filename;

                    $updateItemFileLocation =	[    
                        'ppi_seller_logo' =>   $path                        
                    ];
    
                    $upadteFileLocation = DB::table('pq_purchase_invoice')
                                            ->where('ppi_id', $invoiceInsert)
                                            ->update($updateItemFileLocation);

                    $dompdf->loadHtml('
                        <style>
                            footer {
                                position: fixed; 
                                bottom: -60px; 
                                left: 0px; 
                                right: 0px;
                                height: 50px; 
        
                                /** Extra personal styles **/
                                border-top: 2px solid #D3D3D3;
                                color:black;
                                text-align: center;
                                line-height: 35px;
                            }
        
                            td, th {    
                                padding: 8px;
                                font-size: 14px 
                            }
        
                            #items td, th {    
                                border-bottom: 1px solid #dddddd;
                            }
        
                        </style>
                        <body>             
                            <footer>
                                Copyright &copy; 2019 Simplified Procurement Solutions(OPC) Pvt Ltd. All Rights Reserved.
                            </footer>  
                            <table width="100%">
                                <tr>                        
                                    <td> 
                                        <img src="'.$path.'" height="100" width="200" /> 
        
                                        <h3>Simplified Procurement Solutions(OPC) Pvt Ltd</h3>
                                        <p>
                                            Chamier Towers, 37/6 1st floor, Pasumpon Muthuramalinga Thevar Rd,
                                            <br> 
                                            Teynampet, Chennai, Tamil Nadu 600028
                                            <br>
                                            044 4343 7474
                                        </p>
                                    </td>    
                                    <td align="right">
                                        <h1>Invoice</h1>
                                        <p>
                                            #'.$orderIdGenerate.'
                                        </p>
                                    </td>                        
                                </tr>
                                <tr>
                                    <td align="left">
                                        <p>Pickup Address</p>
                                        <h4> '.$shippingAddress->pci_comp_name.' </h4>
                                        <p>
                                            '.$shippingAddress->psa_address_line1.'
                                            <br> 
                                            '.$shippingAddress->psa_address_line2.'
                                            <br>
                                            '.$shippingAddress->psa_country.'
                                            <br>
                                            '.$shippingAddress->psa_zipcode.'
                                            <br>
                                            '.$shippingAddress->pci_comp_phone.'
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <p>Delivery Address</p>
                                        <h4> '.$sellerCorpAddress->pci_comp_name.' </h4>
                                        <p>
                                            '.$sellerCorpAddress->psa_address_line1.'
                                            <br> 
                                            '.$sellerCorpAddress->psa_address_line2.'
                                            <br>
                                            '.$sellerCorpAddress->psa_country.'
                                            <br>
                                            '.$sellerCorpAddress->psa_zipcode.'
                                            <br>
                                            '.$sellerCorpAddress->pci_comp_phone.'
                                        </p>
                                    </td>
                                    <td align="right">
                                        <p> Date : '.$todayDate.'</p> 
                                        <p> Due Date : '.$due_date.'</p>
                                    </td>
                                </tr>
        
                                <table width="100%" id="items">                        
                                        <tr style="background-color: lightgray;">           
                                            <th>#</th>
                                            <th width="50%">Spec</th>
                                            <th>Qty</th>
                                            <th>Rate</th>
                                            <th>Amount</th>    
                                        </tr>        
                                        <tr>
                                            <td>1</td>
                                            <td>'.$sellerCorpAddress->pea_event_spec.'</td>
                                            <td>'.$sellerCorpAddress->pea_event_unit_quantity.' 
                                                ' .$sellerCorpAddress->pmu_unit_name.'
                                            </td>
                                            <td>INR '.$currentBid.'</td>
                                            <td>INR '.$currentBid.'</td>
                                        </tr>
                                        <tr>
                                            
                                            <td colspan="4" style="text-align:right">SubTotal</td>
                                            <td>INR '.$currentBid.'</td>
                                        </tr>  
                                        <tr>
                                            <td colspan="4" style="text-align:right">GST</td>
                                            <td>INR 0</td>
                                        </tr>   
                                        <tr>
                                            <td colspan="4" style="text-align:right">Total</td>
                                            <td>INR '.$currentBid.'</td>
                                        </tr>     
                                                
                                </table>

                                <tr>                                    
                                    <p> Thanks for your business </p> <br>
                                    <p> Notes : This is system generated Purchase Invoice</p> <br> <br>
                                </tr>        
                            
                            </table>
                            <br>
        
                        </body>
                        ');
        
                        // (Optional) Setup the paper size and orientation
                        //$dompdf->setPaper('A4', 'landscape');
        
                        // Render the HTML as PDF
                        $dompdf->render();
        
                        // Output the generated PDF to Browser
                        //$dompdf->stream('Bid Report');
        
                        $pdf_string =   $dompdf->output();
                        file_put_contents($pdfroot, $pdf_string ); 
                }

                $result = array(
                    'status' => true,
                    'message' => 'Purchase Invoice Generated Successfully',
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Something Went Wrong',
                    'c_code'  =>2
                );
            }

        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    public static function fdpi()
    {
        // initiate FPDI
        $pdf = new Fpdi();
        // add a page
        $pdf->AddPage();
        // set the source file
        $pdf->setSourceFile("C:/xampp/htdocs/PQ/public/PO/PO2630274.pdf");
        // import page 1
        $tplId = $pdf->importPage(1);
        // use the imported page and place it at point 10,10 with a width of 100 mm
        $pdf->useTemplate($tplId, 10, 10, 100);

        $pdf->Output();    
    }

    public function viewpo($id)
    {
        $PO = DB::table('pq_purchase_orders')
                            ->where('id', $id)
                            ->first();

        $path = 'C:/xampp/htdocs/PQ/public/PO/'.$PO->order_id.'.pdf';

        // initiate FPDI
        $pdf = new Fpdi();

        $pageCount = $pdf->setSourceFile($path);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            // import a page
            $templateId = $pdf->importPage($pageNo);
            // get the size of the imported page
            $size = $pdf->getTemplateSize($templateId);

            // create a page (landscape or portrait depending on the imported page size)
            if ($size[0] > $size[1]) {
                $pdf->AddPage('L', array($size[0], $size[1]));
            } else {
                $pdf->AddPage('P', array($size[0], $size[1]));
            }

            // use the imported page
            $pdf->useTemplate($templateId);

        }

        // add a page
       // $pdf->AddPage();
        // set the source file
        //$pdf->setSourceFile($path);
        // import page 1
        //$tplId = $pdf->importPage(1);
        // use the imported page and place it at point 10,10 with a width of 100 mm
        //$pdf->useTemplate($tplId, 5, 5, 200);
        

        $pdf->Output();    
    }

    public function viewinvoice($id)
    {
        $PO = DB::table('pq_purchase_invoice')
                            ->where('ppi_id', $id)
                            ->first();

        $path = 'C:/xampp/htdocs/PQ/public/Invoice/'.$PO->ppi_invoice_id.'.pdf';

        // initiate FPDI
        $pdf = new Fpdi();

        $pageCount = $pdf->setSourceFile($path);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            // import a page
            $templateId = $pdf->importPage($pageNo);
            // get the size of the imported page
            $size = $pdf->getTemplateSize($templateId);

            // create a page (landscape or portrait depending on the imported page size)
            if ($size[0] > $size[1]) {
                $pdf->AddPage('L', array($size[0], $size[1]));
            } else {
                $pdf->AddPage('P', array($size[0], $size[1]));
            }

            // use the imported page
            $pdf->useTemplate($templateId);

        }

        // add a page
       // $pdf->AddPage();
        // set the source file
        //$pdf->setSourceFile($path);
        // import page 1
        //$tplId = $pdf->importPage(1);
        // use the imported page and place it at point 10,10 with a width of 100 mm
        //$pdf->useTemplate($tplId, 5, 5, 200);
        

        $pdf->Output();    
    }

    public function sendpotoseller(Request $request)
    {
        $input = $request->all();
     
        $session_val = session()->all();
        $log_id= $session_val['pli_loginId'];

        $dt 	= new DateTime();
        $updateDate	= $dt->format('Y-m-d H:i:s');
        $datetime		= $dt->format('Y-m-d H:i:s');
        $date			= $dt->format('Y-m-d');
        $year			= $dt->format('Y');

        try 
        {
            $poId = $input['poId'];
            $lotID = $input['lotID'];
            $currentBid = $input['currentBid'];  

            $poDetail = DB::table('pq_purchase_orders')
                                        ->select('pq_purchase_orders.document_path' ,'pq_purchase_orders.order_id' ,'pq_live_auction.pla_seller_emailid as emailid','pq_master_cat_new.pmca_cat_name','pq_event_create.pec_event_name' ,'pq_company_info.pci_comp_name as seller_name', 'pq_live_auction.pla_cur_bid as seller_bidamount', 'pq_event_additem.pea_event_start_dt as start_date', 'pq_event_additem.pea_event_end_dt as end_date')    
                                        ->Join('pq_live_auction', 'pq_purchase_orders.live_action_id' , '=', 'pq_live_auction.pla_id')
                                        ->Join('pq_company_info', 'pq_live_auction.pla_seller_compid' , '=', 'pq_company_info.pci_comp_id')
                                        ->Join('pq_event_additem', 'pq_live_auction.pla_lotid' , '=', 'pq_event_additem.pea_id')
                                        ->Join('pq_event_create', 'pq_event_additem.pea_event_id' , '=', 'pq_event_create.pec_event_id')
                                        ->Join('pq_master_cat_new', 'pq_event_additem.pea_event_cat' , '=', 'pq_master_cat_new.pmca_id')
                                        ->where('pq_purchase_orders.id', $poId)
                                        ->first();

            $start= new DateTime($poDetail->start_date);
            $end= new DateTime($poDetail->end_date);
            $start_date=$start->format('d M');
            $end_date=$end->format('d M');
            $date_format = $start_date.' -'. $end_date.', '.$year;
                                        
            $data = array(
                'date_format'=>$date_format,
                'seller_company' => $poDetail->seller_name,
                'seller_name' => $poDetail->seller_name,
                'seller_bidamount' => $poDetail->seller_bidamount,
                'event_name' => $poDetail->pec_event_name,
                'event_cat_name'   => $poDetail->pmca_cat_name,
                'email' => $poDetail->emailid,
                'order_id' => $poDetail->order_id,
                'document_path' => $poDetail->document_path
            );

            $poUpdateData = [
                'ppo_is_po_sent' => '1',
                'updated_at' => $updateDate
            ];

            $poUpdate = DB::table('pq_purchase_orders')
                            ->where('id', $poId)
                            ->update($poUpdateData);

            if($poUpdate)
            {
                Mail::send('emails.purchase-order',$data,function($message) use ($data)
                {	
                    $file = 'C:/xampp/htdocs/PQ/public'.$data['document_path'];
                    $message->to($data['email'])->subject('Purchase Order - Purchase Quick');
                    $message->from('support@purchasequick.in','Purchase Quick');            
                    $message->attach($file);        
                });

                $result = array(
                    'status' => true,
                    'message' => 'PO sent to the seller successfully',
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'PO not updated',
                    'c_code'  =>2
                );
            }

        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    public function sendInvoice(Request $request)
    {
        $input = $request->all();
     
        $session_val = session()->all();
        $log_id= $session_val['pli_loginId'];

        $dt 	= new DateTime();
        $updateDate	= $dt->format('Y-m-d H:i:s');
        $datetime		= $dt->format('Y-m-d H:i:s');
        $date			= $dt->format('Y-m-d');
        $year			= $dt->format('Y');

        try 
        {
            $poId = $input['poId'];
            $lotID = $input['lotID'];
            $currentBid = $input['currentBid'];  

            $poDetail = DB::table('pq_purchase_invoice')
                                        ->select('pq_purchase_invoice.ppi_document_path' ,'pq_purchase_orders.order_id' ,'pq_event_create.pec_event_loginid as emailid','pq_master_cat_new.pmca_cat_name','pq_event_create.pec_event_name' ,'pq_company_info.pci_comp_name as seller_name', 'pq_live_auction.pla_cur_bid as seller_bidamount', 'pq_event_additem.pea_event_start_dt as start_date', 'pq_event_additem.pea_event_end_dt as end_date')    
                                        ->Join('pq_purchase_orders', 'pq_purchase_invoice.ppi_order_id' , '=', 'pq_purchase_orders.order_id')
                                        ->Join('pq_live_auction', 'pq_purchase_orders.live_action_id' , '=', 'pq_live_auction.pla_id')
                                        ->Join('pq_company_info', 'pq_live_auction.pla_seller_compid' , '=', 'pq_company_info.pci_comp_id')
                                        ->Join('pq_event_additem', 'pq_live_auction.pla_lotid' , '=', 'pq_event_additem.pea_id')
                                        ->Join('pq_event_create', 'pq_event_additem.pea_event_id' , '=', 'pq_event_create.pec_event_id')
                                        ->Join('pq_master_cat_new', 'pq_event_additem.pea_event_cat' , '=', 'pq_master_cat_new.pmca_id')
                                        ->where('pq_purchase_invoice.ppi_id', $poId)
                                        ->first();

            $start= new DateTime($poDetail->start_date);
            $end= new DateTime($poDetail->end_date);
            $start_date=$start->format('d M');
            $end_date=$end->format('d M');
            $date_format = $start_date.' -'. $end_date.', '.$year;
                                        
            $data = array(
                'date_format'=>$date_format,
                'seller_company' => $poDetail->seller_name,
                'seller_name' => $poDetail->seller_name,
                'seller_bidamount' => $poDetail->seller_bidamount,
                'event_name' => $poDetail->pec_event_name,
                'event_cat_name'   => $poDetail->pmca_cat_name,
                'email' => $poDetail->emailid,
                'order_id' => $poDetail->order_id,
                'document_path' => $poDetail->ppi_document_path
            );

            $poUpdateData = [
                'ppi_is_invoice_sent' => '1',
                'ppi_updated_date' => $updateDate
            ];

            $poUpdate = DB::table('pq_purchase_invoice')
                            ->where('ppi_id', $poId)
                            ->update($poUpdateData);

            if($poUpdate)
            {
                Mail::send('emails.purchase-order',$data,function($message) use ($data)
                {	
                    $file = 'C:/xampp/htdocs/PQ/public'.$data['document_path'];
                    $message->to($data['email'])->subject('Invoice - Purchase Quick');
                    $message->from('support@purchasequick.in','Purchase Quick');            
                    $message->attach($file);        
                });

                $result = array(
                    'status' => true,
                    'message' => 'Invoice sent to the buyer successfully',
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Invoice not updated',
                    'c_code'  =>2
                );
            }

        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    public function acceptPO(Request $request)
    {
        $input = $request->all();
     
        $session_val = session()->all();
        $log_id= $session_val['pli_loginId'];

        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try 
        {
            $poId = $input['poId'];
            $lotID = $input['lotID'];
            $currentBid = $input['currentBid'];  

            $updateData = [
                'seller_confirmed' => 'true',
                'updated_at' => $date
            ];

            $poDetail = DB::table('pq_purchase_orders')
                                    ->where('id', $poId)
                                    ->update($updateData);

            if($poDetail)
            {
                $result = array(
                    'status' => true,
                    'message' => 'PO accepted successfully',
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Something went wrong',
                    'c_code'  =>2
                );
            }

        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    public function acceptInvoice(Request $request)
    {
        $input = $request->all();
     
        $session_val = session()->all();
        $log_id= $session_val['pli_loginId'];

        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try 
        {
            $poId = $input['poId'];
            $lotID = $input['lotID'];
            $currentBid = $input['currentBid'];  

            $updateData = [
                'ppi_is_buyer_confirmed' => '1',
                'ppi_updated_date' => $date
            ];

            $poDetail = DB::table('pq_purchase_invoice')
                                    ->where('ppi_id', $poId)
                                    ->update($updateData);

            if($poDetail)
            {
                $result = array(
                    'status' => true,
                    'message' => 'Invoice accepted successfully',
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Something went wrong',
                    'c_code'  =>2
                );
            }

        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    public static function getDeliveryDays()
    {
        $deliveryDetails = DB::table('pq_master_delivery')
                        ->Join('pq_master_day_type', 'pq_master_delivery.pmd_day_type' , '=', 'pq_master_day_type.pmdt_id')
                        ->get();

        return $deliveryDetails;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
