<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use DateTime;
use Crypt;
use App\GetOsDetails;
use View;
use GuzzleHttp\Exception\GuzzleException;
use App\Http\Controllers\PurchaseController;
use GuzzleHttp\Client;
use Hash;

class AddressController extends Controller
{
    public static function listAddress()
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $getModel = DB::table('pq_shipping_address')
                            ->select('pq_shipping_address.*', 'pq_master_states.pmst_state_name', 'pq_master_cities.pmci_city_name')
                            ->Join('pq_master_states', 'pq_shipping_address.psa_state' , '=', 'pq_master_states.pmst_state_id')
                            ->Join('pq_master_cities', 'pq_shipping_address.psa_city' , '=', 'pq_master_cities.pmci_city_id')
                            ->where('pq_shipping_address.psa_loginid', '=', $login_id)
                            ->get();

        return $getModel;
    }

    public static function getStateNames()
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $getModel = DB::table('pq_master_states')
                            ->where('pmst_display', '=', '1')
                            ->get();

        return $getModel;
    }

    public function getLocations(Request $request)
    {
        $data = $request->all();

        $state_id = $data['state_id'];

        $getModel = DB::table('pq_master_cities')
                    ->where('pmci_state_id', '=', $state_id)
                    ->get();

        return $getModel;

    }

    //Add new address
    public function addAddress(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try
        {  
            $user_os 		= PurchaseController::getOS();	
            $user_browser   = PurchaseController::getBrowser();
           
            
            $insertValue = [
                'psa_loginid' => $login_id,
                'psa_address_line1' => $input['psa_address_line1'],
                'psa_address_line2' => $input['psa_address_line2'],
                'psa_state' => $input['psa_state'],
                'psa_city' => $input['psa_city'],
                'psa_country' => $input['pbd_country'],
                'psa_zipcode' => $input['psa_pincode'],
                'psa_created_date' => $date,
                'psa_os' => $user_os,
                'psa_ipaddress' => $_SERVER['REMOTE_ADDR'],
                'psa_browser' => $user_browser['name'],
                'psa_browser_ver' => $user_browser['version']
            ];

            //add address
            $getModel = DB::table('pq_shipping_address')
                        ->where('psa_loginid', '=', $login_id)
                        ->insert($insertValue);

            if($getModel)
            {
                $result = array(
                    'status' => true,
                    'message' => 'Address added successfully',	
                    'c_code'  =>1
                );  
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Something Went Wrong',
                    'c_code'  => 2
                );
            }            

        }catch(\Exception $e){	

            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    //delete address
    public function deleteAddress(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        try
        {
            $deleteId = $input['addressId'];

            $deletModel = DB::table('pq_shipping_address')
                            ->where('psa_id', '=', $deleteId)
                            ->delete();

            if($deletModel)
            {
                $result = array(
                    'status' => true,
                    'message' => 'Address deleted successfully',
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Something Went Wrong',
                    'c_code'  =>2
                );
            }

        }catch(\Exception $e){	

            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    public static function getAddress($addressId)
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $getModel = DB::table('pq_shipping_address')
                            ->where('psa_id', '=', $addressId)
                            ->first();

        return $getModel;
    }

    public static function getCities($addressId)
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $getModel = DB::table('pq_master_cities')
                            ->where('pmci_state_id', '=', $addressId)
                            ->get();

        return $getModel;
    }

    
    public function updateAddress(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try
        {
            $updateId = $input['addressId'];

            $user_os 		= PurchaseController::getOS();	
            $user_browser   = PurchaseController::getBrowser();

            $updateData = [
                'psa_address_line1' => $input['psa_address_line1'],
                'psa_address_line2' => $input['psa_address_line2'],
                'psa_state' => $input['psa_state'],
                'psa_city' => $input['psa_city'],
                'psa_zipcode' => $input['psa_pincode'],
                'psa_updated_date' => $date,
                'psa_os' => $user_os,
                'psa_ipaddress' => $_SERVER['REMOTE_ADDR'],
                'psa_browser' => $user_browser['name'],
                'psa_browser_ver' => $user_browser['version']
            ];

            $updateModel = DB::table('pq_shipping_address')
                            ->where('psa_id', '=', $updateId)
                            ->update($updateData);

            if($updateModel)
            {
                $result = array(
                    'status' => true,
                    'message' => 'Address updated successfully',
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Something Went Wrong',
                    'c_code'  =>2
                );
            }

        }catch(\Exception $e){	

            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }
   
   
}
