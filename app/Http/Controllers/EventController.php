<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Mail\reminder;
use Validator;
use App\EmailChecker;
use Mail;  
use App\GetListuser;
use Illuminate\Support\Facades\Cache;
use DateTime;
use Exception;
use URL;
use Hash;
use Crypt;
use Session;
use File;
use App\GetOsDetails;
use App\purchaseInvite;
use View;
use Carbon\Carbon;

use App\Exports\EventExport;
use App\Exports\EventItemExport;
use App\Exports\LotBidExport;
use Maatwebsite\Excel\Facades\Excel;
use Dompdf\Dompdf;
use Charts;

class EventController extends Controller
{

    public function export() 
    {
        return Excel::download(new EventExport, 'events.xlsx');

    }

    public function datecheck()
    {
        $dt 	= new DateTime();
        $date=$dt->format('Y-m-d H:i:s');

        echo $date;
        die;
    }
	
	public function createNewEvent(Request $request)
	{
		$input = $request->all();
	   
        $packageID=$input['package_id'];
        $loginid=$input['loginid'];
        $event_name=$input['pec_event_name'];
        $event_type=$input['pec_event_type'];
        $event_cat=$input['pec_event_category'];
       // $event_start=new DateTime($input['pec_event_start_dt']);
       // $event_end=$input['pec_event_end_dt'];

        $user_os 		= PurchaseController::getOS();	
		$user_browser   = PurchaseController::getBrowser();	
      
        
      //  print_r( $eventid);die;
        
        $dt 	= new DateTime();
        $date=$dt->format('Y-m-d H:i:s');
        $st=new DateTime($input['pec_event_start_dt']);
        $et=new DateTime($input['pec_event_end_dt']);
        $event_start=$st->format('Y-m-d H:i:s');
        $event_end=$et->format('Y-m-d H:i:s');
		
	//	$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));
				
	  try{
		 
            $Pkg_curCount=$this->eventPackageBalance($packageID,$loginid);
            $current_count=$Pkg_curCount->pbp_pkg_cur_count;
            $pkg_buyID=$Pkg_curCount->pbp_id;
          //  print_r($Pkg_curCount);die;

          if($current_count>0){

          
            $eventid=$this->generateUid($loginid);

            $EventInsertData =	[
                'pec_event_id' 		 	=>  $eventid,
                'pec_event_name'  		=>  $event_name,
                'pec_event_start_dt'  	=>  $event_start,
                'pec_event_category'    =>  $event_cat,
                'pec_event_type' 	    =>  $event_type,
                'pec_event_end_dt' 	    =>  $event_end,
                'pec_event_loginid'  	=>  $loginid,
                'pec_event_pkg_id'  	=>  $packageID,
                'pec_event_pkg_id'  	=>  $packageID,
                'pec_buynow_pkg_id'     =>  $pkg_buyID,
                'pec_event_create_dt'  	=>  $date,
                'pec_ipaddress'         =>  $_SERVER['REMOTE_ADDR'],
                'pec_browser'           =>  $user_browser['name'],        
                'pec_browser_ver'       =>  $user_browser['version'],    
                'pec_os'                =>  $user_os         
                
            ];

           // print_r( $EventInsertData);die;
         
             $eventInsert = DB::table('pq_event_create')->insert($EventInsertData);


            if( $eventInsert){


                // update event count minus current count from pq_buynow_package table
                $update_array= [
                    'pbp_pkg_cur_count' =>   $current_count - 1 ,
                                         
                ];
                $updateData= DB::table('pq_buynow_package')
                ->where('pbp_pkg_id', $packageID)
                ->update($update_array);

                $result = array(
                    'status' => true,
                    'message' => 'Event Created'

                );
            }else{
                $result = array(
                    'status' => false,
                    'message' => 'Some went Wrong'               
                );

            }
             

            //print_r( $eventid);die;
          }else{
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
           
            );
          }
			
			
			//echo $str;
			//print_r(count($package));die;
					
       
       // print_r($insertData);die;
        }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);
    }
    
    public static function eventPackageBalance($packageID,$loginID){

       
        $current_pkg_count = DB::table('pq_buynow_package')
        
        ->select('*')
        ->where('pq_buynow_package.pbp_login_id',$loginID)
        ->where('pq_buynow_package.pbp_pkg_id',$packageID)
        ->first();
    //  print_r( $current_pkg_count );die;
        return $current_pkg_count;

    }

    
    public function generateUid($logID){
        $dt = new DateTime();
        $date= $dt->format('ymd');
        $com_type= $this->getCompanyType($logID);
       
        $orderObj = DB::table('pq_event_create')->select('pec_event_id')->latest('pec_sno')->first();
        if ($orderObj) {
            $orderNr = $orderObj->pec_event_id;
            $removed1char = substr($orderNr, 9);
            $dateformat=str_pad($removed1char + 1, 4, "0", STR_PAD_LEFT);

          
            $uid=$date. $dateformat;
         
           if($com_type->company_type == '1'){
            $generateOrder_nr = 'PQP'.$uid;
           }elseif($com_type->company_type == '2'){
            $generateOrder_nr = 'PQS'.$uid;
           }
           elseif($com_type->company_type == '3'){
            $generateOrder_nr = 'PQB'.$uid;
           }
           
           
        } else {

            if($com_type->company_type == '1'){
                $generateOrder_nr = 'PQP'.$date . str_pad(1, 4, "0", STR_PAD_LEFT);
               }elseif($com_type->company_type == '2'){
                $generateOrder_nr = 'PQS'.$date . str_pad(1, 4, "0", STR_PAD_LEFT);
               }
               elseif($com_type->company_type == '3'){
                $generateOrder_nr = 'PQB'.$date . str_pad(1, 4, "0", STR_PAD_LEFT);
               }
               
            
        }
     // print_r();

        return $generateOrder_nr;
      //  return $orderObj;
    }

    public static function getCompanyType($logID)
	{
        $company_type = DB::table('pq_login_info')
        ->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_login_info.pli_comp_id')
        ->select('pci_comp_type_cat as company_type')
        ->where('pq_login_info.pli_loginid',$logID)
        ->first();
      
        return $company_type;
    }
    public static function getEventData($logID)
	{
        $eventData = DB::table('pq_event_create')
        ->Join('pq_master_package', 'pq_master_package.pmp_id' , '=', 'pq_event_create.pec_event_pkg_id')
        ->Join('pq_buynow_package', 'pq_buynow_package.pbp_id' , '=', 'pq_event_create.pec_buynow_pkg_id')
        ->select('*')
        ->where('pq_event_create.pec_event_loginid',$logID)
        ->where('pq_event_create.pec_event_status','0')
        ->where('pq_event_create.pec_event_start_dt', '>', NOW())
        ->orderBy('pq_event_create.pec_sno','DESC')
        ->get();

        return $eventData;
    }

    public static function upcomingEvent($logID)
	{

        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select 
        *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
        
      FROM
        `pq_event_create` 
      WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
        AND `pq_event_create`.`pec_event_status` = '1' 
        AND `pq_event_create`.`pec_event_start_dt` >= NOW()
        ";
        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function upcomingEvent1($logID)
	{
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select 
        *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`
        ) AS no_item  
        
      FROM
        `pq_event_create`
      JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
      WHERE 
        (`pq_event_create`.`pec_event_loginid` = '".$logID."' AND `pq_event_create`.`pec_event_status` = '1' AND `pq_event_create`.`pec_event_start_dt` >= NOW() AND `pq_event_create`.`pec_event_end_dt` >= NOW())
        OR  (`pq_event_create`.`pec_event_loginid` = '".$logID."' AND `pq_event_create`.`pec_event_status` = '1' AND `pq_event_additem`.`pea_event_start_dt` >= NOW() AND `pq_event_additem`.`pea_event_end_dt` >= NOW())
        GROUP BY pq_event_create.`pec_event_id` 
        ";

        //OR  (NOW() BETWEEN `pq_event_additem`.`pea_event_start_dt` AND `pq_event_additem`.`pea_event_end_dt`)

        $eventData=DB::select( $sql);
        return $eventData;
        
    }

    public static function sellerUpcomingEvent($logID)
	{
        $json_date='{"email" : "'.$logID.'"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select pq_event_create.*,pq_company_info.`pci_comp_name` FROM pq_event_invite_detail
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
         WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         
         AND `pq_event_create`.`pec_event_start_dt` >= NOW() AND `pq_event_create`.`pec_event_end_dt` >= NOW()
         GROUP BY pq_event_create.`pec_event_id`  order by `pq_event_create`.`pec_event_start_dt` ASC
         ";
  

        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function sellerUpcomingEvent1($logID)
	{
        $json_date='{"email" : "'.$logID.'"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select pq_event_create.*,pq_company_info.`pci_comp_name` FROM pq_event_invite_detail
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
        WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         
        AND (`pq_event_create`.`pec_event_start_dt` >= NOW() AND `pq_event_create`.`pec_event_end_dt` >= NOW() )
        OR  (`pq_event_additem`.`pea_event_start_dt` >= NOW() AND `pq_event_additem`.`pea_event_end_dt` >= NOW())
        GROUP BY pq_event_create.`pec_event_id`  
        order by `pq_event_create`.`pec_event_start_dt` ASC
         ";
  

        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function seller_Completed_Aution($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select pq_event_create.*,pq_company_info.`pci_comp_name` FROM pq_event_invite_detail
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
        JOIN `pq_live_auction` ON  pq_live_auction.`pla_seller_emailid`='".$logID."'
         WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         AND `pq_event_create`.`pec_event_status` = '1' 
         AND   `pq_event_create`.`pec_event_end_dt` <= NOW()
         GROUP BY pq_event_create.`pec_event_id`  order by  `pq_event_create`.`pec_event_end_dt` DESC
         ";
        $completed=DB::select( $sql);
        return $completed;

    }

    //Seller completed events new
    public static function seller_Completed_Aution1($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select pq_event_create.*,pq_company_info.`pci_comp_name` FROM pq_event_invite_detail
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
        JOIN `pq_live_auction` ON  pq_live_auction.`pla_seller_emailid`='".$logID."'
         WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         AND `pq_event_create`.`pec_event_status` = '1' 
         AND (`pq_event_create`.`pec_event_end_dt` <= NOW())
         OR  (`pq_event_additem`.`pea_event_start_dt` <= NOW() AND `pq_event_additem`.`pea_event_end_dt` <= NOW())
         GROUP BY pq_event_create.`pec_event_id`  order by  `pq_event_create`.`pec_event_end_dt` DESC
         ";
        $completed=DB::select( $sql);
        return $completed;

    }

    public static function sellerLiveEvent($logID)
	{
       // $json_date='{"email" : "'.$logID.'","accept": "1"}'; changes given by AR 23.11.2018
       $json_date='{"email" : "'.$logID.'"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select pq_event_create.*,pq_company_info.`pci_comp_name` FROM pq_event_invite_detail   
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`     
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
         WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         AND `pq_event_create`.`pec_event_status` = '1' 
         AND  `pq_event_create`.`pec_event_start_dt` <= NOW() AND `pq_event_create`.`pec_event_end_dt` >= NOW()
         AND  `pq_event_additem`.`pea_event_start_dt` <= NOW() AND `pq_event_additem`.`pea_event_end_dt` >= NOW()
         GROUP BY pq_event_create.`pec_event_id`  order by `pq_event_create`.`pec_event_end_dt` ASC
         ";
       

        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function buyerLiveEvent($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
          AND `pq_event_create`.`pec_event_end_dt` >= NOW() 
          AND `pq_event_additem`.`pea_event_start_dt` <= NOW() 
          AND `pq_event_additem`.`pea_event_end_dt` >= NOW() 
          GROUP BY `pq_event_create`.`pec_sno` 
         ";
     

        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function buyerLiveEvent1($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              
              FROM
                `pq_event_create`
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND `pq_event_create`.`pec_event_type` = '2' 
                AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
          AND `pq_event_create`.`pec_event_end_dt` >= NOW()
          GROUP BY `pq_event_create`.`pec_sno` 
         ";
     

        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function buyerCompletedEvent($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
          AND `pq_event_create`.`pec_event_end_dt` <= NOW() 
         ";
     

        $eventData=DB::select( $sql);
        return $eventData;
    }

    //Buyer completed event
    public static function buyerCompletedEvent1($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
              WHERE 
                (`pq_event_create`.`pec_event_loginid` = '".$logID."' AND `pq_event_create`.`pec_event_status` = '1' AND `pq_event_create`.`pec_event_start_dt` <= NOW() AND `pq_event_create`.`pec_event_end_dt` <= NOW())
                OR (`pq_event_create`.`pec_event_loginid` = '".$logID."' AND `pq_event_create`.`pec_event_status` = '1' AND `pq_event_additem`.`pea_event_start_dt` <= NOW() AND `pq_event_additem`.`pea_event_end_dt` <= NOW())
                GROUP BY pq_event_create.`pec_event_id` 
         ";

        //OR (NOW() BETWEEN `pq_event_additem`.`pea_event_start_dt` AND `pq_event_additem`.`pea_event_end_dt`)
     

        $eventData=DB::select( $sql);
        return $eventData;
    }

    //buyer accepted events
    public static function buyerAcceptedEvent($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
              JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                AND `pq_live_auction`.`pla_auction_status` = '1' 
                AND `pq_event_create`.`pec_event_status` = '1' 
                GROUP BY pq_event_create.`pec_event_id` 
         ";

        //OR (NOW() BETWEEN `pq_event_additem`.`pea_event_start_dt` AND `pq_event_additem`.`pea_event_end_dt`)
     
        $eventData=DB::select( $sql);
        return $eventData;
    }

    //seller accepted po
    public static function sellerAcceptedPO($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
              JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
              JOIN `pq_purchase_orders` ON `pq_purchase_orders`.`lot_id`=pq_event_additem.`pea_id`
              WHERE `pq_live_auction`.`pla_seller_emailid` = '".$logID."'
                AND `pq_purchase_orders`.`seller_confirmed` = 'true'
                AND `pq_live_auction`.`pla_auction_status` = '1'
                AND `pq_event_create`.`pec_event_status` = '1' 
                GROUP BY pq_event_create.`pec_event_id` 
         ";

        //OR (NOW() BETWEEN `pq_event_additem`.`pea_event_start_dt` AND `pq_event_additem`.`pea_event_end_dt`)
     
        $eventData=DB::select( $sql);
        return $eventData;
    }

    //buyer accepted invoice
    public static function buyerAcceptedInvoice($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
              JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
              JOIN `pq_purchase_orders` ON `pq_purchase_orders`.`lot_id`=pq_event_additem.`pea_id`
              JOIN `pq_purchase_invoice` ON `pq_purchase_invoice`.`ppi_order_id`=pq_purchase_orders.`order_id`
              WHERE `pq_live_auction`.`pla_seller_emailid` = '".$logID."'
                AND `pq_purchase_orders`.`seller_confirmed` = 'true'
                AND `pq_purchase_invoice`.`ppi_is_buyer_confirmed` = '1'
                AND `pq_live_auction`.`pla_auction_status` = '1'
                AND `pq_event_create`.`pec_event_status` = '1' 
                GROUP BY pq_event_create.`pec_event_id` 
         ";

        //OR (NOW() BETWEEN `pq_event_additem`.`pea_event_start_dt` AND `pq_event_additem`.`pea_event_end_dt`)
     
        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function buyerReceivedPO($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
              JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
              JOIN `pq_purchase_orders` ON `pq_purchase_orders`.`lot_id`=pq_event_additem.`pea_id`
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                AND `pq_purchase_orders`.`seller_confirmed` = 'true'
                AND `pq_live_auction`.`pla_auction_status` = '1' 
                AND `pq_event_create`.`pec_event_status` = '1' 
                GROUP BY pq_event_create.`pec_event_id` 
         ";

        //OR (NOW() BETWEEN `pq_event_additem`.`pea_event_start_dt` AND `pq_event_additem`.`pea_event_end_dt`)
     
        $eventData=DB::select( $sql);
        return $eventData;
    }

    //buyer PO generated events
    public static function buyerPOGenEvent($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
              JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
              JOIN `pq_purchase_orders` ON `pq_purchase_orders`.`lot_id`=pq_event_additem.`pea_id`
              LEFT JOIN `pq_purchase_invoice` ON `pq_purchase_invoice`.`ppi_order_id`=pq_purchase_orders.`order_id`
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                AND `pq_purchase_orders`.`ppo_is_po_sent` = '1' 
                AND `pq_live_auction`.`pla_auction_status` = '1' 
                AND `pq_event_create`.`pec_event_status` = '1' 
                GROUP BY pq_event_create.`pec_event_id` 
         ";

        //OR (NOW() BETWEEN `pq_event_additem`.`pea_event_start_dt` AND `pq_event_additem`.`pea_event_end_dt`)
     
        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function sellerPOEvents($logID)
    {
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
              JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
              WHERE `pq_live_auction`.`pla_seller_emailid` = '".$logID."'
                AND `pq_live_auction`.`pla_auction_status` = '1'
                AND `pq_event_create`.`pec_event_status` = '1' 
                GROUP BY pq_event_create.`pec_event_id` 
         ";

        //OR (NOW() BETWEEN `pq_event_additem`.`pea_event_start_dt` AND `pq_event_additem`.`pea_event_end_dt`)
     
        $eventData=DB::select( $sql);
        return $eventData;
    }

    //seller received po events
    public static function sellerPOGenEvents($logID)
    {
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
              JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
              JOIN `pq_purchase_orders` ON `pq_purchase_orders`.`lot_id`=pq_event_additem.`pea_id`
              LEFT JOIN `pq_purchase_invoice` ON `pq_purchase_invoice`.`ppi_order_id`=pq_purchase_orders.`order_id`
              WHERE `pq_live_auction`.`pla_seller_emailid` = '".$logID."'
                AND `pq_purchase_orders`.`ppo_is_po_sent` = '1'
                AND `pq_live_auction`.`pla_auction_status` = '1'
                AND `pq_event_create`.`pec_event_status` = '1' 
                GROUP BY pq_event_create.`pec_event_id` 
         ";

        //OR (NOW() BETWEEN `pq_event_additem`.`pea_event_start_dt` AND `pq_event_additem`.`pea_event_end_dt`)
     
        $eventData=DB::select( $sql);
        return $eventData;
    }
    
    
    public static function buyerCompletedEventMIS($logID)
	{
        $sql="select 
       * 
       FROM
         `pq_event_additem` 
         INNER JOIN `pq_master_cat` 
           ON `pq_master_cat`.`pmc_id` = `pq_event_additem`.`pea_event_cat` 
         LEFT JOIN `pq_event_invite_detail` 
           ON `pq_event_invite_detail`.`pei_event_additem_id` = `pq_event_additem`.`pea_id` 
         INNER JOIN `pq_master_subcat` 
           ON `pq_master_subcat`.`pms_id` = `pq_event_additem`.`pea_event_subcat` 
         INNER JOIN `pq_master_unit` 
           ON `pq_master_unit`.`pmu_id` = `pq_event_additem`.`pea_event_unit` 
           INNER JOIN `pq_event_create` 
           ON `pq_event_create`.`pec_event_id` = `pq_event_additem`.`pea_event_id`
             LEFT JOIN `pq_live_auction` 
           ON pq_live_auction.`pla_lotid`=pq_event_additem.`pea_id`
           LEFT JOIN `pq_company_info` 
    ON `pq_company_info`.`pci_comp_id` = `pq_live_auction`.`pla_seller_compid`   
           
       WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
         AND `pq_event_create`.`pec_event_status` = '1' 
         AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
         AND `pq_event_create`.`pec_event_end_dt` <= NOW()";
     

        $eventData=DB::select( $sql);
        return $eventData;
    }
    public static function getSellerInviteItem($eventID,$logID){

        $accept_status='{"email" : "'.$logID.'" ,"accept": "1"}';
        $reject_status='{"email" : "'.$logID.'" ,"reject": "1"}';
        $json_date='{"email" : "'.$logID.'"}';
        $sql = " select 
                    * ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$accept_status."'
                    ) as accept
                    ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$reject_status."'
                    ) as reject
                    FROM
                    pq_event_invite_detail 
                    
                    JOIN `pq_event_additem` 
                        ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
                    JOIN `pq_event_create` 
                        ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
                    LEFT JOIN  `pq_live_auction` 
                        ON `pq_live_auction`.`pla_lotid` = pq_event_additem.`pea_id`  and pq_live_auction.pla_seller_emailid='".$logID."'
                    JOIN `pq_master_market` 
                        ON `pq_master_market`.`pmm_id` = pq_event_additem.`pea_event_market` 
                    JOIN `pq_master_cat_new` 
                        ON `pq_master_cat_new`.`pmca_id` = pq_event_additem.`pea_event_cat` 
                    JOIN `pq_master_subcat_new` 
                        ON `pq_master_subcat_new`.`pms_id` = pq_event_additem.`pea_event_subcat` 
                    JOIN `pq_master_unit` 
                        ON `pq_master_unit`.`pmu_id` = pq_event_additem.`pea_event_unit` 
                    WHERE json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$json_date."'
                    )
                    AND `pq_event_additem`.`pea_event_start_dt` <= NOW()
                    AND `pq_event_additem`.`pea_event_end_dt` >= NOW()
                    AND pq_event_additem.`pea_event_id`='".$eventID."'";

   $eventData=DB::select( $sql);
   return $eventData;
    }

    public static function getSellerInviteItem1($eventID,$logID){

        $accept_status='{"email" : "'.$logID.'" ,"accept": "1"}';
        $reject_status='{"email" : "'.$logID.'" ,"reject": "1"}';
        $json_date='{"email" : "'.$logID.'"}';
        $sql = " select 
                    * ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$accept_status."'
                    ) as accept
                    ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$reject_status."'
                    ) as reject
                    FROM
                    pq_event_invite_detail 
                    
                    JOIN `pq_event_additem` 
                        ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
                    JOIN `pq_event_create` 
                        ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
                    LEFT JOIN  `pq_live_auction` 
                        ON `pq_live_auction`.`pla_lotid` = pq_event_additem.`pea_id`  and pq_live_auction.pla_seller_emailid='".$logID."'
                    JOIN `pq_master_market` 
                        ON `pq_master_market`.`pmm_id` = pq_event_additem.`pea_event_market` 
                    JOIN `pq_master_cat_new` 
                        ON `pq_master_cat_new`.`pmca_id` = pq_event_additem.`pea_event_cat` 
                    JOIN `pq_master_subcat_new` 
                        ON `pq_master_subcat_new`.`pms_id` = pq_event_additem.`pea_event_subcat` 
                    JOIN `pq_master_unit` 
                        ON `pq_master_unit`.`pmu_id` = pq_event_additem.`pea_event_unit` 
                    WHERE json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$json_date."'
                    )
                    AND `pq_event_additem`.`pea_event_start_dt` >= NOW()
                    AND pq_event_additem.`pea_event_id`='".$eventID."'";

            $eventData=DB::select( $sql);
            return $eventData;
    }

    //Seller completed events
    public static function getSellerInviteItem2($eventID,$logID){

        $accept_status='{"email" : "'.$logID.'" ,"accept": "1"}';
        $reject_status='{"email" : "'.$logID.'" ,"reject": "1"}';
        $json_date='{"email" : "'.$logID.'"}';
        $sql = " select 
                    * ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$accept_status."'
                    ) as accept
                    ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$reject_status."'
                    ) as reject
                    FROM
                    pq_event_invite_detail 
                    
                    JOIN `pq_event_additem` 
                        ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
                    JOIN `pq_event_create` 
                        ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
                    LEFT JOIN  `pq_live_auction` 
                        ON `pq_live_auction`.`pla_lotid` = pq_event_additem.`pea_id`  and pq_live_auction.pla_seller_emailid='".$logID."'
                    JOIN `pq_master_market` 
                        ON `pq_master_market`.`pmm_id` = pq_event_additem.`pea_event_market` 
                    JOIN `pq_master_cat_new` 
                        ON `pq_master_cat_new`.`pmca_id` = pq_event_additem.`pea_event_cat` 
                    JOIN `pq_master_subcat_new` 
                        ON `pq_master_subcat_new`.`pms_id` = pq_event_additem.`pea_event_subcat` 
                    JOIN `pq_master_unit` 
                        ON `pq_master_unit`.`pmu_id` = pq_event_additem.`pea_event_unit` 
                    WHERE json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$json_date."'
                    )
                    AND `pq_event_additem`.`pea_event_start_dt` <= NOW()
                    AND `pq_event_additem`.`pea_event_end_dt` <= NOW()
                    AND pq_event_additem.`pea_event_id`='".$eventID."'";

   $eventData=DB::select( $sql);
   return $eventData;
    }

    public static function getSellerMisReport($logID){

        $accept_status='{"email" : "'.$logID.'" ,"accept": "1"}';
        $reject_status='{"email" : "'.$logID.'" ,"reject": "1"}';
        $json_date='{"email" : "'.$logID.'"}';
        $sql = " select 
                    * ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$accept_status."'
                    ) as accept
                    ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$reject_status."'
                    ) as reject
                    FROM
                    pq_event_invite_detail 
                    
                    JOIN `pq_event_additem` 
                        ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
                    JOIN `pq_event_create` 
                        ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
                    LEFT JOIN  `pq_live_auction` 
                        ON `pq_live_auction`.`pla_lotid` = pq_event_additem.`pea_id`  and pq_live_auction.pla_seller_emailid='".$logID."'
                    JOIN `pq_master_cat` 
                        ON `pq_master_cat`.`pmc_id` = pq_event_additem.`pea_event_cat` 
                    JOIN `pq_master_subcat` 
                        ON `pq_master_subcat`.`pms_id` = pq_event_additem.`pea_event_subcat` 
                    JOIN `pq_master_unit` 
                        ON `pq_master_unit`.`pmu_id` = pq_event_additem.`pea_event_unit` 
                    WHERE json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$json_date."'
                    ) ";

   $eventData=DB::select( $sql);
   return $eventData;
    }

    public static function getCurrentBitAmount($lotID){
        $sql='select MIN(pq_live_auction.`pla_cur_bid`) as cur_bid FROM `pq_live_auction`
    WHERE pq_live_auction.`pla_lotid`='.$lotID;
    $eventData=DB::select( $sql);
    return $eventData;
    }
    public function updatebidData(Request $request){

        $session_val = session()->all();
        $input=array();
        try
        { 
           // $input = $request->all();	
         if(isset($session_val['pli_loginId'])){
           $input['email']=$session_val['pli_loginId'];		  
         }else{
            $input['email']="";		  
         }
         

            $validator = Validator::make($input, [
              'email' => 'required|email' 
             
             
                        
          ]);



          if ($validator->fails()) {
          
              $result = array(
                  'status' => false,
                  'message' =>  $validator->errors(),						
          
                  );

             return response()->json($result);
          
          }else{
            $logID=$session_val['pli_loginId'];		  
            $json_date='{"email" : "'.$logID.'"}';
            //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
            $sql = "
                    SELECT 
                    pq_event_additem.`pea_id` as bid_id,(SELECT MIN(pla_cur_bid) AS cur_bid FROM `pq_live_auction` WHERE `pla_lotid`=pq_event_additem.`pea_id` GROUP BY `pla_lotid`) AS curent_amount
                    FROM
                    `pq_event_invite_detail`
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
                    JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
                    JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
                
                    WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' ) AND  `pq_event_create`.`pec_event_start_dt` <= NOW() 
                    AND `pq_event_create`.`pec_event_end_dt` >= NOW()";
            
    
            $eventData=DB::select( $sql);


            $result= $eventData;
          }
            }catch(\Exception $e){	

               
                 $result = array(
                        'status' => false,
                        'message' =>  $e->getMessage(),
                        'c_code'  =>1		
          
                    );
             }

             return response()->json($result);
    }

    public function buyerupdatebidData(Request $request){

        $session_val = session()->all();
        $input=array();
        try
        { 
           // $input = $request->all();	
         if(isset($session_val['pli_loginId'])){
           $input['email']=$session_val['pli_loginId'];		  
         }else{
            $input['email']="";		  
         }
         

            $validator = Validator::make($input, [
              'email' => 'required|email' 
             
             
                        
          ]);



          if ($validator->fails()) {
          
              $result = array(
                  'status' => false,
                  'message' =>  $validator->errors(),						
          
                  );

             return response()->json($result);
          
          }else{
            $logID=$session_val['pli_loginId'];		  
            $json_date='{"email" : "'.$logID.'"}';
            //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
            $sql = "select 
            pq_live_auction.`pla_id` AS bid_id,
            pq_live_auction.`pla_lotid` AS lot_id,
            pq_live_auction.`pla_cur_bid` AS curent_amount,
            pq_event_invite_detail.`pei_invite_accept_count` AS accept_count
          FROM
            `pq_event_invite_detail` 
            JOIN `pq_event_additem` 
              ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
            JOIN `pq_event_create` 
              ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
            JOIN pq_live_auction 
              ON pq_event_invite_detail.`pei_event_additem_id` = `pq_live_auction`.`pla_lotid` 
          WHERE pq_event_invite_detail.`pei_invite_createby` = '".$logID."' 
            AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
            AND `pq_event_create`.`pec_event_end_dt` >= NOW()";
            

            $eventData=DB::select( $sql);


            $result= $eventData;
          }
            }catch(\Exception $e){	

               
                 $result = array(
                        'status' => false,
                        'message' =>  $e->getMessage(),
                        'c_code'  =>1		
          
                    );
             }

             return response()->json($result);
    }

    public function buyerupdateInviteAcceptanceData(Request $request){

        $session_val = session()->all();
        $input=array();
        try
        { 
           // $input = $request->all();	
            if(isset($session_val['pli_loginId'])){
                $input['email']=$session_val['pli_loginId'];		  
            }else{
                $input['email']="";		  
            }         

            $validator = Validator::make($input, [
                'email' => 'required|email'             
            ]);

            if ($validator->fails()) {          
                $result = array(
                  'status' => false,
                  'message' =>  $validator->errors(),
                );

                return response()->json($result);
          
            }else{
                $logID=$session_val['pli_loginId'];		  
                $json_date='{"email" : "'.$logID.'"}';
                //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
                $sql = "select 
                pq_event_invite_detail.`pei_event_additem_id` AS lot_id,
                pq_event_invite_detail.`pei_invite_accept_count` AS accept_count
            FROM
                `pq_event_invite_detail` 
                JOIN `pq_event_additem` 
                ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
                JOIN `pq_event_create` 
                ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
            WHERE pq_event_invite_detail.`pei_invite_createby` = '".$logID."' 
                AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
                AND `pq_event_create`.`pec_event_end_dt` >= NOW()";                

                $eventData=DB::select( $sql);

                $result= $eventData;
            }
        }catch(\Exception $e){	    

            $result = array(
                'status' => false,
                'message' =>  $e->getMessage(),
                'c_code'  =>1		
    
            );
        }

        return response()->json($result);
    }
    

    public static function getSellerInviteItemForReverse($eventID,$logID){

        $accept_status='{"email" : "'.$logID.'" ,"accept": "1"}';
        $reject_status='{"email" : "'.$logID.'" ,"reject": "1"}';
        $json_date='{"email" : "'.$logID.'"}';
        $sql = " select 
                    * ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$accept_status."'
                    ) as accept
                    ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$reject_status."'
                    ) as reject,
                    (SELECT MIN(pq_live_auction.`pla_cur_bid`) FROM `pq_live_auction` WHERE pq_live_auction.`pla_lotid` = pq_event_additem.`pea_id`) AS current_bid
  
                    FROM
                    pq_event_invite_detail 
                    
                    JOIN `pq_event_additem` 
                        ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
                    JOIN `pq_event_create` 
                        ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
                    LEFT JOIN  `pq_live_auction` 
                        ON `pq_live_auction`.`pla_lotid` = pq_event_additem.`pea_id`  and pq_live_auction.pla_seller_emailid='".$logID."'
                    JOIN `pq_master_market` 
                        ON `pq_master_market`.`pmm_id` = pq_event_additem.`pea_event_market` 
                    JOIN `pq_master_cat_new` 
                        ON `pq_master_cat_new`.`pmca_id` = pq_event_additem.`pea_event_cat` 
                    JOIN `pq_master_subcat_new` 
                        ON `pq_master_subcat_new`.`pms_id` = pq_event_additem.`pea_event_subcat` 
                    JOIN `pq_master_unit` 
                        ON `pq_master_unit`.`pmu_id` = pq_event_additem.`pea_event_unit` 
                    WHERE json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$json_date."'
                    )
                    AND `pq_event_additem`.`pea_event_start_dt` <= NOW()
                    AND `pq_event_additem`.`pea_event_end_dt` >= NOW() 
                    AND pq_event_additem.`pea_event_id`='".$eventID."'";
                    

   $eventData=DB::select( $sql);
   return $eventData;
    }
    public static function getEvent($eventID)
	{
        $eventData = DB::table('pq_event_create')
         ->select('*')
        ->where('pq_event_create.pec_event_id',$eventID)
        ->first();
      
        return $eventData;
    }
    public static function getMasterCat($cat_id)
    {
       if($cat_id!=3){
        $category = DB::table('pq_master_cat')
         ->select('*')
         ->where('pq_master_cat.pmc_cat_type',$cat_id)
        ->get();
        }
        else{
           $category = DB::table('pq_master_cat')
         ->select('*')
         ->whereIn('pq_master_cat.pmc_cat_type',array('1','2','3'))
        ->get();
        }
      
        return $category;

    }

    public static function getMarket($cat_id)
    {
        $market = DB::table('pq_master_market')
                    ->select('*')
                    ->get();
      
        return $market;
    }

    public function getCategories(Request $request){

        $input = $request->all();
        $market=$input['market'];
        $cat_type=$input['cat_type'];
        $otherText='OTHERS';
      /*  $others=DB::table('pq_master_subcat as sub_other')
        ->select('*')
        ->where('sub_other.pms_subcat_name',$otherText)
        ->where('sub_other.pms_cat_type',$cat_type);

         $category = DB::table('pq_master_subcat')
         ->select('*')
         ->where('pq_master_subcat.pms_catid',$categories)

         ->union($others)
        ->get();*/
        $category = DB::table('pq_master_cat_new')
         ->select('*')
         ->where('pq_master_cat_new.pmca_market_id',$market)
         ->get();


        return  $category;
    }
    
    public static function getUnit()
    {
        $unit = DB::table('pq_master_unit')
         ->select('*')
        ->get();
      
        return $unit;

    } 

    
    
    public function deleteProuctItem(Request $request){


        $input = $request->all();



    
       
          $dt 	= new DateTime();
          $date	= $dt->format('Y-m-d H:i:s');

          try{
            
            $pmc_id=$input['pmc_id'];
         
   
           
               $eventInsert = DB::table('pq_event_additem')
               ->where('pea_id', $pmc_id)
               ->delete();
  
  
              if( $eventInsert){

                  $result = array(
                      'status' => true,
                      'message' => 'Event item updated'
  
                  );
              
               
  
              //print_r( $eventid);die;
            }else{
              $result = array(
                  'status' => false,
                  'message' => 'Something Went Wrong',	
                  'c_code'  =>2	 			
             
              );
            }
              
              
              //echo $str;
              //print_r(count($package));die;
                      
         
         // print_r($insertData);die;
         }catch(\Exception $e){	
                $result = array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'c_code'  =>2	 			
               
                );
           }
  
           return response()->json($result);
          
    }
    public function buyerAcceptBid(Request $request){
        $input = $request->all();
    
 
         $dt 	= new DateTime();
         $date	= $dt->format('Y-m-d H:i:s');
         $session_val = session()->all();
     	$datetime		= $dt->format('Y-m-d H:i:s');
		$date			= $dt->format('Y-m-d');
		$year			= $dt->format('Y');
         
     //	$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));
                 
      try{
           $auctionID=$input['lotID'];
           $sellerID=$input['sellerID'];
           $check_exist = DB::table('pq_live_auction')
           ->where('pla_id', '=',  $auctionID)
           ->select('*')
           ->first();
        
             
            if( $check_exist ){
                $EventCompletedData =	[            
                    'pla_auction_status'  =>  '1',
                ];

                $EventCompletedAcceptHistoryData =	[            
                    'pbh_auction_status'  =>  '1',
                ];
          
         $updateData= DB::table('pq_live_auction')
         ->where('pla_id', $auctionID)
         ->update($EventCompletedData);
         $lotItem_id=$check_exist->pla_lotid;

         $findUpdateRow = DB::table('pq_live_auction')
         ->where('pla_lotid', $lotItem_id)
         ->where('pla_id', $auctionID)
         ->first();    

         $updateHistoryData= DB::table('pq_bid_history')
         ->where('pbh_live_auctionid', $auctionID)
         ->where('pbh_cur_bid', $findUpdateRow->pla_cur_bid)
         ->update($EventCompletedAcceptHistoryData);
         
           //  print_r( $check_exist->pla_seller_emailid);die;
          
              
             if($updateData){


                $EventCompletedRejectData =	[            
                    'pla_auction_status'  =>  '2',
                ];

                $EventCompletedRejectHistoryData =	[            
                    'pbh_auction_status'  =>  '2',
                ];

                $EventCompletedBiddingHistoryData =	[            
                    'pbh_auction_status'  =>  '3',
                ];
                
                $rejectData= DB::table('pq_live_auction')
                                ->where('pla_lotid', $lotItem_id)
                                ->where('pla_id','!=', $auctionID)
                                ->update($EventCompletedRejectData);      
                                
                $biddingHistoryData= DB::table('pq_bid_history')
                                    ->where('pbh_lotid', $lotItem_id)
                                    ->where('pbh_cur_bid', '!=',$findUpdateRow->pla_cur_bid)
                                    ->update($EventCompletedRejectHistoryData);

                $biddingHistoryUpdateData= DB::table('pq_bid_history')
                                    ->where('pbh_lotid', $lotItem_id)
                                    ->where('pbh_cur_bid', '!=',$findUpdateRow->pla_cur_bid)
                                    ->orderBy('pbh_id', 'desc')
                                    ->first();

                $rejectHistoryData= DB::table('pq_bid_history')
                                    ->where('pbh_lotid', $lotItem_id)
                                    ->where('pbh_id', '!=',$biddingHistoryUpdateData->pbh_id)
                                    ->where('pbh_cur_bid', '!=',$findUpdateRow->pla_cur_bid)
                                    ->update($EventCompletedBiddingHistoryData);
            
                $result = array(
                     'status' => true,
                     'message' => 'Event Completed',	
                     'c_code'  =>1
                );
            
                 $sql=  "select `pec_event_name`,`pmc_cat_name`,pec_event_start_dt,pec_event_end_dt,`pms_subcat_name`,`pli_con_name` AS seller_name,`pci_comp_name` AS seller_company,`pla_cur_bid` FROM `pq_event_create`

                 JOIN `pq_event_additem` ON pq_event_additem.`pea_event_id`=pq_event_create.`pec_event_id`
                 
                 JOIN  pq_live_auction ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
                 JOIN pq_login_info ON `pq_login_info`.`pli_loginid` = pq_live_auction.`pla_seller_emailid`
                 JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id` = pq_login_info.`pli_comp_id`
                 JOIN `pq_master_cat` ON `pq_master_cat`.`pmc_id` = pq_event_additem.`pea_event_cat`
                 JOIN `pq_master_subcat` ON `pq_master_subcat`.`pms_id` = pq_event_additem.`pea_event_subcat`
                 WHERE pq_live_auction.`pla_lotid`=".$lotItem_id." and pq_login_info.`pli_loginid`='".$sellerID."' ";
      
                 $winner_data= DB::select( $sql);
      

                   $data=$winner_data[0];
                   $start= new DateTime($data->pec_event_start_dt);
                   $end= new DateTime($data->pec_event_end_dt);
                   $start_date=$start->format('d M');
                   $end_date=$end->format('d M');
      
                   $date_format=$start_date.' -'. $end_date.', '.$year;
                   $data 		= array('event_name'=>$data->pec_event_name,'date_format'=>$date_format,'start_date'=>$data->pec_event_start_dt,'end_date'=>$data->pec_event_end_dt,'email'=>$check_exist->pla_seller_emailid,'event_cat_name'=>$data->pmc_cat_name,'seller_name'=>$data->seller_name,'year'=>$year,'seller_company'=>$data->seller_company,'seller_bidamount'=>$data->pla_cur_bid,'date'=>$date);
                   $data_val		= array_merge($data,$session_val);
                
                //winning mail send to seller
                 Mail::send('emails.event-winner',$data_val,function($message) use ($data_val)
                 {				 
                     $message->to($data_val['email'])->subject('Congratulation - Purchase Quick');
                     $message->from('support@purchasequick.in','Purchase Quick');
                 });
                
             //print_r( $eventid);die;
           }else{
             $result = array(
                 'status' => false,
                 'message' => 'Some went wrong',	
                 'c_code'  =>2	 			
            
             );
           }
        }else{
            $result = array(
                'status' => false,
                'message' => 'Some went wrong',	
                'c_code'  =>2	 			
           
            );
          }
             
             
             //echo $str;
             //print_r(count($package));die;
                     
        
        // print_r($insertData);die;
        }catch(\Exception $e){	
               $result = array(
                   'status' => false,
                   'message' => $e->getMessage(),
                   'c_code'  =>2	 			
              
               );
          }
 
          return response()->json($result);

    }

    public function buyerRejectBid(Request $request){
        $input = $request->all();    
 
        $dt 	= new DateTime();
        $current_date	= $dt->format('Y-m-d H:i:s');
        $session_val = session()->all();
     	$datetime		= $dt->format('Y-m-d H:i:s');
		$date			= $dt->format('Y-m-d');
        $year			= $dt->format('Y');
        
        $user_os 		= PurchaseController::getOS();	
        $user_browser   = PurchaseController::getBrowser();
                          
        try{
           $auctionID=$input['lotID'];
           $sellerID=$input['sellerID'];
           $log_id= $session_val['pli_loginId'];
           
           $check_exist = DB::table('pq_live_auction')
                            ->Join('pq_event_additem', 'pq_live_auction.pla_lotid' , '=', 'pq_event_additem.pea_id')
                            ->where('pq_live_auction.pla_id', '=',  $auctionID)
                            ->select('pq_live_auction.*', 'pq_event_additem.pea_is_emd_required', 'pq_event_additem.pea_id')
                            ->first();        
             
            if($check_exist){

                if($check_exist->pea_is_emd_required == '1')
                {
                    $validateWallet = DB::table('pq_wallet_current')
                                        ->where('pwc_login_id', '=',  $log_id)
                                        ->first();

                    if(isset($validateWallet))
                    {
                        $sumEMD = DB::table('pq_emd_paid_history')
                                        ->where('peph_event_additem_id', '=',  $check_exist->pea_id)
                                        ->where('peph_is_emd_returned', '=',  '0')
                                        ->where('peph_loginid', '=',  $check_exist->pla_seller_emailid)
                                        ->sum('peph_emd_value');

                        if($validateWallet->pwc_current_balance > $sumEMD)
                        {
                            $current_balance = $validateWallet->pwc_current_balance - $sumEMD;

                            $returnEMD = DB::table('pq_emd_paid_history')
                                        ->where('peph_event_additem_id', '=',  $check_exist->pea_id)
                                        ->where('peph_is_emd_returned', '=',  '0')
                                        ->where('peph_loginid', '=',  $check_exist->pla_seller_emailid)
                                        ->first();

                            if(@count($returnEMD) > 0)
                            {                              
                                $validateSellerWalletCheck = DB::table('pq_wallet_current')
                                                            ->where('pwc_login_id', '=',  $returnEMD->peph_loginid)
                                                            ->first();

                                if(isset($validateSellerWalletCheck))
                                {
                                    $latBalance = $returnEMD->peph_emd_value + $validateSellerWalletCheck->pwc_current_balance;

                                    $sellerCurrentBalanceUpdate = [
                                        'pwc_current_balance' => $latBalance,
                                        'pwc_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                        'pwc_browser' => $user_browser['name'],  
                                        'pwc_browser_ver' =>  $user_browser['version'],
                                        'pwc_os' => $user_os,
                                        'pwc_updated_at' => $current_date
                                    ];

                                    $updateSellerWalletValue = DB::table('pq_wallet_current')
                                                            ->where('pwc_login_id', '=',  $returnEMD->peph_loginid)
                                                            ->update($sellerCurrentBalanceUpdate);

                                    if($updateSellerWalletValue)
                                    {
                                        $emdInsert = [
                                            'pwh_wallet_id' => $validateWallet->pwc_wallet_id,
                                            'pwh_balance' => $returnEMD->peph_emd_value,
                                            'pwh_do' => 2, //added
                                            'pwh_for' => '2',
                                            'pwh_receiver_loginid' => $returnEMD->peph_loginid,
                                            'pwh_receiver_wallet_id' => $validateSellerWalletCheck->pwc_wallet_id,
                                            'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                            'pwh_browser' => $user_browser['name'],  
                                            'pwh_browser_ver' =>  $user_browser['version'],
                                            'pwh_os' => $user_os,
                                            'pwh_loginid' => $log_id,
                                        ];
                
                                        $sellerEmdInsert = DB::table('pq_wallet_history')->insert($emdInsert);

                                        if($sellerEmdInsert)
                                        {
                                            $pephUpdate = [
                                                'peph_is_emd_returned' => '1'
                                            ];
                                            
                                            $returnEMDFlagUpdate = DB::table('pq_emd_paid_history')
                                                            ->where('peph_id', '=', $returnEMD->peph_id)
                                                            ->update($pephUpdate);
                                        }

                                    }                                 
                                                                    
                                }                                   
                                
                            }

                            $updateWallet = [
                                'pwc_current_balance' => $current_balance,
                                'pwc_updated_at' => $current_date
                            ];                    
        
                            $updateWalletValue = DB::table('pq_wallet_current')
                                            ->where('pwc_login_id', '=',  $log_id)
                                            ->update($updateWallet);
                        }                            
                    }
                }

                $LotRejectedData =	[            
                    'pla_auction_status'  =>  '2',
                ];

                $LotRejectedHistoryData =	[            
                    'pbh_auction_status'  =>  '2',
                ];
          
                $updateData= DB::table('pq_live_auction')
                                ->where('pla_id', $auctionID)
                                ->update($LotRejectedData);
                $lotItem_id=$check_exist->pla_lotid;

                $findUpdateRow = DB::table('pq_live_auction')
                                    ->where('pla_lotid', $lotItem_id)
                                    ->where('pla_id', $auctionID)
                                    ->first();    

                $updateHistoryData= DB::table('pq_bid_history')
                                        ->where('pbh_live_auctionid', $auctionID)
                                        ->where('pbh_cur_bid', $findUpdateRow->pla_cur_bid)
                                        ->update($LotRejectedHistoryData);
                
                if($updateData){                    
                
                    $result = array(
                        'status' => true,
                        'message' => 'Item Rejected',	
                        'c_code'  =>1
                    );            
                    
                }else{
                    $result = array(
                        'status' => false,
                        'message' => 'Some went wrong',	
                        'c_code'  =>2
                    );
                }
            }else{
                $result = array(
                    'status' => false,
                    'message' => 'Not Found',	
                    'c_code'  =>2	 			
            
                );
            }             
        }catch(\Exception $e){	
               $result = array(
                   'status' => false,
                   'message' => $e->getMessage(),
                   'c_code'  =>2	 			
              
               );
        }
 
        return response()->json($result);

    }

    public function buyerRejectAllBid(Request $request){
        $input = $request->all();    
 
        $dt 	= new DateTime();
        $current_date	= $dt->format('Y-m-d H:i:s');
        $session_val = session()->all();
     	$datetime		= $dt->format('Y-m-d H:i:s');
		$date			= $dt->format('Y-m-d');
        $year			= $dt->format('Y');
        
        $user_os 		= PurchaseController::getOS();	
        $user_browser   = PurchaseController::getBrowser();
                          
        try{
           $lotId=$input['lotID'];
           $log_id= $session_val['pli_loginId'];

           $validate_item = DB::table('pq_event_additem')
                                ->where('pea_id', '=',  $lotId)
                                ->select('*')
                                ->first();    

            if(isset($validate_item))
            {
                $check_exist = DB::table('pq_live_auction')
                                ->where('pla_lotid', '=',  $lotId)
                                ->select('*')
                                ->get();        
 
                if($check_exist){                

                    if($validate_item->pea_is_emd_required == '1')
                    {
                        $validateWallet = DB::table('pq_wallet_current')
                                            ->where('pwc_login_id', '=',  $log_id)
                                            ->first();

                        if(isset($validateWallet))
                        {
                            $sumEMD = DB::table('pq_emd_paid_history')
                                            ->where('peph_event_additem_id', '=',  $lotId)
                                            ->where('peph_is_emd_returned', '=',  '0')
                                            ->sum('peph_emd_value');

                            if($validateWallet->pwc_current_balance > $sumEMD)
                            {
                                $current_balance = $validateWallet->pwc_current_balance - $sumEMD;

                                $returnEMD = DB::table('pq_emd_paid_history')
                                            ->where('peph_event_additem_id', '=',  $lotId)
                                            ->where('peph_is_emd_returned', '=',  '0')
                                            ->get();

                                if(@count($returnEMD) > 0)
                                {
                                    foreach($returnEMD as $returnVal)
                                    {                                
                                        $validateSellerWalletCheck = DB::table('pq_wallet_current')
                                                                    ->where('pwc_login_id', '=',  $returnVal->peph_loginid)
                                                                    ->first();

                                        if(isset($validateSellerWalletCheck))
                                        {
                                            $latBalance = $returnVal->peph_emd_value + $validateSellerWalletCheck->pwc_current_balance;

                                            $sellerCurrentBalanceUpdate = [
                                                'pwc_current_balance' => $latBalance,
                                                'pwc_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                                'pwc_browser' => $user_browser['name'],  
                                                'pwc_browser_ver' =>  $user_browser['version'],
                                                'pwc_os' => $user_os,
                                                'pwc_updated_at' => $current_date
                                            ];

                                            $updateSellerWalletValue = DB::table('pq_wallet_current')
                                                                    ->where('pwc_login_id', '=',  $returnVal->peph_loginid)
                                                                    ->update($sellerCurrentBalanceUpdate);

                                            if($updateSellerWalletValue)
                                            {
                                                $emdInsert = [
                                                    'pwh_wallet_id' => $validateWallet->pwc_wallet_id,
                                                    'pwh_balance' => $returnVal->peph_emd_value,
                                                    'pwh_do' => 2, //added
                                                    'pwh_for' => '2',
                                                    'pwh_receiver_loginid' => $returnVal->peph_loginid,
                                                    'pwh_receiver_wallet_id' => $validateSellerWalletCheck->pwc_wallet_id,
                                                    'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                                    'pwh_browser' => $user_browser['name'],  
                                                    'pwh_browser_ver' =>  $user_browser['version'],
                                                    'pwh_os' => $user_os,
                                                    'pwh_loginid' => $log_id,
                                                ];
                        
                                                $sellerEmdInsert = DB::table('pq_wallet_history')->insert($emdInsert);

                                                if($sellerEmdInsert)
                                                {
                                                    $pephUpdate = [
                                                        'peph_is_emd_returned' => '1'
                                                    ];
                                                    
                                                    $returnEMDFlagUpdate = DB::table('pq_emd_paid_history')
                                                                    ->where('peph_id', '=', $returnVal->peph_id)
                                                                    ->update($pephUpdate);
                                                }

                                            }                                 
                                                                            
                                        }
                                        
                                    }
                                }

                                $updateWallet = [
                                    'pwc_current_balance' => $current_balance,
                                    'pwc_updated_at' => $current_date
                                ];                    
            
                                $updateWalletValue = DB::table('pq_wallet_current')
                                                ->where('pwc_login_id', '=',  $log_id)
                                                ->update($updateWallet);
                            }                            
                        }
                    }

                    $updateData= DB::table('pq_live_auction')
                                    ->where('pla_lotid', $lotId)
                                    ->update(['pla_auction_status'  =>  '2']);

                    foreach($check_exist as $val)
                    {
                        $updateHistoryData= DB::table('pq_bid_history')
                                                ->where('pbh_lotid', $lotId)
                                                ->where('pbh_cur_bid', $val->pla_cur_bid)
                                                ->update(['pbh_auction_status'  =>  '2']);
                    }
                    
                    if($updateData){                    
                    
                        $result = array(
                            'status' => true,
                            'message' => 'Rejected All',	
                            'c_code'  =>1
                        );            
                        
                    }else{
                        $result = array(
                            'status' => false,
                            'message' => 'Some went wrong',	
                            'c_code'  =>2
                        );
                    }
                }else{
                    $result = array(
                        'status' => false,
                        'message' => 'Not Found',	
                        'c_code'  =>2	 			

                    );
                }         
            }
           
               
        }catch(\Exception $e){	
               $result = array(
                   'status' => false,
                   'message' => $e->getMessage(),
                   'c_code'  =>2	 			
              
               );
        }
 
        return response()->json($result);

    }


    public function needClarification(Request $request){
        $input = $request->all();
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');
        $year			= $dt->format('Y');
        try{
            $buyerID = Crypt::decryptString($input['buyerID']);	  
            $sellerID = Crypt::decryptString($input['sellerID']);
            $lotID = Crypt::decryptString($input['lotID']);
            $clarification=$input['clarification'];
            $user_os 		= GetOsDetails::getOS();
            $user_browser   = GetOsDetails::getBrowser();

            $clarificationInsertData=[

         
                'peic_event_additem_id'        => $lotID,
                'peic_email'                   => $sellerID,
                'peic_seller_remarks'          => $clarification,
                'peic_insert_dt'               => $date,
                'peic_buyer_remaksdt'          => $date,
                'peic_ipaddress'               => $_SERVER['REMOTE_ADDR'],
                'peic_browser'                 => $user_browser['name'],    
                'peic_browser_ver'             => $user_browser['version'],
                'peic_os'                      => $user_os
            ];

         	
   
          
        $eventInsert = DB::table('pq_event_invite_clarify')->insert($clarificationInsertData);
        if($eventInsert){
             
            $eventLotData=purchaseInvite::getEventAdditem($lotID);
            
           
          
            
            
            $data 		= array('year'=>$year,
            'selleremail'=>$sellerID,
            'buyeremail'=>$buyerID,
            'clarification'=>$clarification,
            'pec_event_name'=>$eventLotData->pec_event_name,
            'pmc_cat_name'=>$eventLotData->pmc_cat_name,
            'pms_subcat_name'=>$eventLotData->pms_subcat_name,
            'pea_event_spec'=>$eventLotData->pea_event_spec,
            'pea_event_unit_quantity'=>$eventLotData->pea_event_unit_quantity,
            'pmu_unit_name'=>$eventLotData->pmu_unit_name,
            'pec_event_start_dt'=>$eventLotData->pec_event_start_dt,
            'pec_event_end_dt'=>$eventLotData->pec_event_end_dt,
            'pea_event_location'=>$eventLotData->pea_event_location

        
        );
        Mail::send('emails.event-clarification',$data,function($message) use ($data)
        {	
        
            $message->to($data['buyeremail'])->subject('Clarification-Purchase Quick');						
            $message->from('support@purchasequick.in','Purchase Quick');
            
        });
                                    
            $result = array(
                'status'  => true,
                'message' => "Invite Send success"			

            );	

        }else{
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
           
            );
        }
         
           
        }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }
         return response()->json($result);

    }
    public function addBidLotItem(Request $request){
        $input = $request->all();

        $session_val = session()->all();

       
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try{
          
          $addItemID=$input['lotID'];
          $comp_id= $session_val['pli_comp_id'];
          $log_id= $session_val['pli_loginId'];
          $cur_bid=$input['bid_price'];
          $event_type=$input['event_type'];

            $user_os 		= PurchaseController::getOS();	
		    $user_browser   = PurchaseController::getBrowser();

            $validateDate = DB::table('pq_event_additem')
                                ->where('pea_id', '=',  $addItemID)
                                ->where('pea_event_start_dt', '<=',  $date)
                                ->where('pea_event_end_dt', '>=',  $date)
                                ->first();

            if(isset($validateDate) && $validateDate != null)
            {
                $check_exist = DB::table('pq_live_auction')
                            ->where('pla_lotid', '=',  $addItemID)
                            ->where('pla_seller_emailid', '=',  $log_id)
                            ->select('*')
                            ->first();

                //time diff calculation between lot end date and current date
                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $validateDate->pea_event_end_dt);
                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date);
                $diff_in_hours = $to->diffInMinutes($from);

                if($event_type == 2)
                {
                    if($diff_in_hours <=  $validateDate->pea_time_extension)
                    {
                        if($validateDate->pea_no_of_extension == 0)
                        {
                            $extension_count = $validateDate->pea_extn_count + 1;
                            $minutesToAdd = $validateDate->pea_time_extension;

                            $newtimestamp = strtotime(''.$validateDate->pea_event_end_dt.' + '.$minutesToAdd.' minute');
                            $lat_end_time = date('Y-m-d H:i:s', $newtimestamp);
                            //     $dateTime = new DateTime($validateDate->pea_event_end_dt);                            
                            // $lat_end_time = $dateTime->modify('+{$minutesToAdd} minutes');

                            $updateDateExtension = [
                                'pea_extn_count' => $extension_count,          
                                'pea_event_end_dt' => $lat_end_time                                              
                            ];

                            $validateDateVal = DB::table('pq_event_additem')
                                    ->where('pea_id', '=',  $addItemID)
                                    ->where('pea_event_start_dt', '<=',  $date)
                                    ->where('pea_event_end_dt', '>=',  $date)
                                    ->update($updateDateExtension);

                            $validateEvent = DB::table('pq_event_create')
                                    ->where('pec_event_id', '=',  $validateDate->pea_event_id)
                                    ->first();

                            //if($lat_end_time >= $validateEvent->pec_event_end_dt)
                            $latdate=strtotime($lat_end_time);
                            $enddate=strtotime($validateEvent->pec_event_end_dt);

                            if($latdate >= $enddate)
                            {
                                $updateEventEndDateExtension = [        
                                    'pec_event_end_dt' => $lat_end_time                                              
                                ];

                                $validateEvent = DB::table('pq_event_create')
                                                ->where('pec_event_id', '=',  $validateDate->pea_event_id)
                                                ->update($updateEventEndDateExtension);
                            }
                        }
                        else
                        {
                            if($validateDate->pea_extn_count != $validateDate->pea_no_of_extension)
                            {
                                $extension_count = $validateDate->pea_extn_count + 1;
                                $minutesToAdd = $validateDate->pea_time_extension;

                                $newtimestamp = strtotime(''.$validateDate->pea_event_end_dt.' + '.$minutesToAdd.' minute');
                                $lat_end_time = date('Y-m-d H:i:s', $newtimestamp);
                                //     $dateTime = new DateTime($validateDate->pea_event_end_dt);                            
                                // $lat_end_time = $dateTime->modify('+{$minutesToAdd} minutes');
        
                                $updateDateExtension = [
                                    'pea_extn_count' => $extension_count,          
                                    'pea_event_end_dt' => $lat_end_time                                              
                                ];

                                $validateDateVal = DB::table('pq_event_additem')
                                        ->where('pea_id', '=',  $addItemID)
                                        ->where('pea_event_start_dt', '<=',  $date)
                                        ->where('pea_event_end_dt', '>=',  $date)
                                        ->update($updateDateExtension);


                                $validateEvent = DB::table('pq_event_create')
                                    ->where('pec_event_id', '=',  $validateDate->pea_event_id)
                                    ->first();

                                //if($lat_end_time >= $validateEvent->pec_event_end_dt)
                                $latdate=strtotime($lat_end_time);
                                $enddate=strtotime($validateEvent->pec_event_end_dt);

                                if($latdate >= $enddate)
                                {
                                    $updateEventEndDateExtension = [        
                                        'pec_event_end_dt' => $lat_end_time                                                  
                                    ];

                                    $validateEvent = DB::table('pq_event_create')
                                                    ->where('pec_event_id', '=',  $validateDate->pea_event_id)
                                                    ->update($updateEventEndDateExtension);
                                }
                            }
                        }
                    }
                }

                //time extension calc
                // $sql = "
                //     SELECT DATE_SUB(pea_event_end_dt, INTERVAL ".$validateDate->pea_time_extension." MINUTE) as endtime
                //     FROM
                //         `pq_event_additem` 
                //     WHERE `pq_event_additem`.`pea_id` = '".$addItemID."'
                // ";     
                // $eventData=DB::select($sql);
                // return $eventData['endtime'];

                if(is_null($check_exist)) 
                {
                    $EventInsertData =	[
                        'pla_lotid' => $addItemID,
                        'pla_seller_compid'=> $comp_id,
                        'pla_seller_emailid'=> $log_id,
                        'pla_cur_bid'=> $cur_bid,
                        'pla_cur_bid_datetime'=>  $date,
                        'pla_auction_status'=> '0',
                        'pla_ipaddress'  =>  $_SERVER['REMOTE_ADDR'],
                        'pla_browser'    =>  $user_browser['name'],        
                        'pla_browser_ver'  =>  $user_browser['version'],    
                        'pla_os'        =>  $user_os                        
                    ];
                   

                    // print_r( $EventInsertData);die;

                    $eventInsert = DB::table('pq_live_auction')->insertGetId($EventInsertData);

                    if($eventInsert){

                        $HistoryInsertData =	[
                            'pbh_lotid' => $addItemID,
                            'pbh_live_auctionid' => $eventInsert,
                            'pbh_seller_compid'=> $comp_id,
                            'pbh_seller_emailid'=> $log_id,
                            'pbh_cur_bid'=> $cur_bid,
                            'pbh_cur_bid_datetime'=>  $date,
                            'pbh_auction_status'=> '0',
                            'pbh_ipaddress'  =>  $_SERVER['REMOTE_ADDR'],
                            'pbh_browser'    =>  $user_browser['name'],        
                            'pbh_browser_ver'  =>  $user_browser['version'],    
                            'pbh_os'        =>  $user_os                            
                        ];

                        $historyInsert = DB::table('pq_bid_history')->insert($HistoryInsertData);

                        $result = array(
                            'status' => true,
                            'message' => 'Bit item inserted'

                        );
                    }else{
                        $result = array(
                            'status' => false,
                            'message' => 'Something Went Wrong',	
                            'c_code'  =>2	 			
                    
                        );
                    }
                }else{ 

                    $lastRecordValidation = DB::table('pq_bid_history')
                                                ->where('pbh_lotid', $addItemID)
                                                ->orderBy('pbh_id', 'desc')
                                                ->first();

                    if($lastRecordValidation->pbh_seller_emailid == $log_id)
                    {
                        $result = array(
                            'status' => false,
                            'message' => 'You already made a bid',	
                            'c_code'  =>3 
                        );

                        return response()->json($result);
                    }
                    else
                    {
                        $update_array=[                    
                            'pla_cur_bid'=> $cur_bid,
                            'pla_cur_bid_datetime'=>  $date,
                            'pla_ipaddress'  =>  $_SERVER['REMOTE_ADDR'],
                            'pla_browser'    =>  $user_browser['name'],        
                            'pla_browser_ver'  =>  $user_browser['version'],    
                            'pla_os'        =>  $user_os                        
                        ];
    
                        $eventInsert = DB::table('pq_live_auction')
                                            ->select('pla_id')
                                            ->where('pla_lotid', $addItemID)
                                            ->where('pla_seller_emailid', '=',  $log_id)
                                            ->update($update_array);
                        
                        if($eventInsert){
    
                            $bidHistory = DB::table('pq_live_auction')
                                            ->select('pla_id')
                                            ->where('pla_lotid', $addItemID)
                                            ->where('pla_seller_emailid', '=',  $log_id)
                                            ->first();
    
                            $HistoryInsertData =	[
                                'pbh_lotid' => $addItemID,
                                'pbh_live_auctionid' => $bidHistory->pla_id,
                                'pbh_seller_compid'=> $comp_id,
                                'pbh_seller_emailid'=> $log_id,
                                'pbh_cur_bid'=> $cur_bid,
                                'pbh_cur_bid_datetime'=>  $date,
                                'pbh_auction_status'=> '0',
                                'pbh_ipaddress'  =>  $_SERVER['REMOTE_ADDR'],
                                'pbh_browser'    =>  $user_browser['name'],        
                                'pbh_browser_ver'  =>  $user_browser['version'],    
                                'pbh_os'        =>  $user_os                            
                            ];
    
                            $historyInsert = DB::table('pq_bid_history')->insert($HistoryInsertData);
    
                            $result = array(
                                'status' => true,
                                'message' => 'Bit item updated'
                            );
    
                        }else{
                                $result = array(
                                    'status' => false,
                                    'message' => 'Something Went Wrong',	
                                    'c_code'  =>2	 			
                            
                                );
                        }
                    }
                    
                }        

                if($eventInsert){

                    $result = array(
                        'status' => true,
                        'message' => 'Bit item updated'

                    );

                //print_r( $eventid);die;
                }else{
                    $result = array(
                        'status' => false,
                        'message' => 'Something Went Wrong',	
                        'c_code'  =>2	 			
                
                    );
                }
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Event Completed | You cannot accept',	
                    'c_code'  =>2	 			
                
                );
            }            
            
            //echo $str;
            //print_r(count($package));die; 
            // print_r($insertData);die;

       }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);


    }

    public function validateLotBid(Request $request){
        $input = $request->all();

        $session_val = session()->all();

       
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try{
          
          $addItemID=$input['lotID'];
          $comp_id= $session_val['pli_comp_id'];
          $log_id= $session_val['pli_loginId'];
          $cur_bid=$input['bid_price'];

            $user_os 		= PurchaseController::getOS();	
		    $user_browser   = PurchaseController::getBrowser();

            $validateDate = DB::table('pq_event_additem')
                                ->where('pea_id', '=',  $addItemID)
                                ->where('pea_event_start_dt', '<=',  $date)
                                ->where('pea_event_end_dt', '>=',  $date)
                                ->first();

            if(isset($validateDate) && $validateDate != null)
            {
                $check_exist = DB::table('pq_live_auction')
                            ->where('pla_lotid', '=',  $addItemID)
                            ->where('pla_seller_emailid', '=',  $log_id)
                            ->select('*')
                            ->first();

                if(is_null($check_exist)) 
                {
                    $result = array(
                        'status' => true,
                        'message' => 'New Bid',
                        'c_code' => 2
                    );

                    
                }else{ 

                    $lastRecordValidation = DB::table('pq_bid_history')
                                                ->where('pbh_lotid', $addItemID)
                                                ->orderBy('pbh_id', 'desc')
                                                ->first();

                    if($lastRecordValidation->pbh_seller_emailid == $log_id)
                    {
                        $result = array(
                            'status' => false,
                            'message' => 'You already made a bid',	
                            'c_code'  =>3 
                        );

                        return response()->json($result);
                    }
                    else
                    {
                        $result = array(
                            'status' => true,
                            'message' => 'Bid updation',
                            'c_code' => 2
                        );           
                    }
                    
                }                
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Event Completed | You cannot accept',	
                    'c_code'  =>2	 			
                
                );
            }            
            
            //echo $str;
            //print_r(count($package));die;                   
       
            // print_r($insertData);die;

       }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);


    }

    public function acceptInviteItem(Request $request){


        $input = $request->all();
       
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try
        {
            $session_val = session()->all();
          
            $pmc_id=$input['inviteID'];
            $invite_flag=$input['invite_type'];
            $loginID=$session_val['pli_loginId'];

            $validateDate = DB::table('pq_event_additem')
                                ->where('pea_id', '=',  $pmc_id)
                                ->where('pea_event_start_dt', '<=',  $date)
                                ->where('pea_event_end_dt', '>=',  $date)
                                ->orWhere('pea_event_start_dt', '>=', $date)
                                ->first(); 
                          
            if(isset($validateDate) && $validateDate != null)
            {
                $check_exist = DB::table('pq_event_invite_detail')
                                ->where('pei_event_additem_id', '=',  $pmc_id)
                                ->select('*')
                                ->first();

                $json_array=json_decode($check_exist->pei_invite_details,true);           
            
                foreach($json_array as $key => $value) {
                    if (in_array($loginID, $value)) {
                        unset($json_array[$key]);
                    }
                }           
                
                $json_date =array();
                $json_date=array_values($json_array);

                if($invite_flag==1) { 
                    $jsonformat=[
                        'bid'=> "0",
                        'email' => $loginID,
                        'accept'=>  "1",
                        'reject'=>  "0",
                    ];
                }else{
                    $jsonformat=[
                        'bid'=> "0",
                        'email' => $loginID,
                        'accept'=>  "0",
                        'reject'=>  "1",
                    ];
                }

                array_push($json_date,$jsonformat);

                //encode json format 

                $format_json=json_encode($json_date);
                if($invite_flag==1) { 
                    $accept_count=$check_exist->pei_invite_accept_count+1 ;                
                    
                    $update_array= [
                        'pei_invite_details' =>   $format_json   ,
                        'pei_invite_accept_count' =>    $accept_count                              
                    ];
                    
                }else{
                    $update_array= [
                        'pei_invite_details' =>   $format_json   
                                        
                    ];
                }

            
                $inviteUpdate = DB::table('pq_event_invite_detail')
                                    ->where('pei_event_additem_id', $pmc_id)
                                    ->update($update_array);  
            
                if( $inviteUpdate)
                {
                    $result = array(
                        'status' => true,
                        'message' => 'Event item updated'  
                    );
    
                //print_r( $eventid);die;
                }else{
                    $result = array(
                        'status' => false,
                        'message' => 'Something Went Wrong',	
                        'c_code'  =>2	 			
                    
                    );
                }
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Event Completed | You cannot accept',	
                    'c_code'  =>2	 			
                
                );
            }
              
              
              //echo $str;
              //print_r(count($package));die;
                      
         
         // print_r($insertData);die;
         }catch(\Exception $e){	
                $result = array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'c_code'  =>2	 			
               
                );
           }
  
           return response()->json($result);

                 
    }

    
    public function productEditItem(Request $request){


        $input = $request->all();


    
       
          $dt 	= new DateTime();
          $date	= $dt->format('Y-m-d H:i:s');

          try{
            $prod_market=$input['pmc_market_name'];
            $prod_cat=$input['pmc_cat_name'];
            $prod_sub_cat=$input['pms_subcat_name'];
            $spec=$input['pea_event_spec'];
            $prod_unit=$input['pmu_unit_name'];
            $prod_st_price=$input['pea_event_start_price'];
            $prod_min_decrement=$input['pea_event_max_dec'];
            $location=$input['pea_event_location'];
            $quantity=$input['pea_event_unit_quantity'];
            $reserve_price = $input['pea_event_reserve_price'];
            $pmc_id=$input['pmc_id'];
			
			$st=new DateTime($input['pea_event_start_dt']);
			$et=new DateTime($input['pea_event_end_dt']);
			$event_start=$st->format('Y-m-d H:i:s');
			$event_end=$et->format('Y-m-d H:i:s');
         
            
             // print_r($Pkg_curCount);die;
  
      

  
              $EventItemData =	[
                'pea_event_market'              =>  $prod_market,
                  'pea_event_cat' 		 	    =>  $prod_cat,
                  'pea_event_subcat'  		    =>  $prod_sub_cat,
                  'pea_event_spec'  	        =>  $spec,
                  'pea_event_unit' 	            =>  $prod_unit,
                  'pea_event_unit_quantity'     =>  $quantity,
                  'pea_event_start_price'  	    =>  $prod_st_price,
                  'pea_event_max_dec'  		    =>  $prod_min_decrement,
                  'pea_event_reserve_price'     =>  $reserve_price,
                  'pea_event_location'  	    =>  $location,
                  'pea_event_additem_dt'        =>  $date,
				  'pea_event_start_dt' => $event_start,
				  'pea_event_end_dt' => $event_end      
                  
              ];
             
              
            //  DB::enableQueryLog();
               $eventInsert = DB::table('pq_event_additem')
               ->where('pea_id', '=',  $pmc_id)
               ->update($EventItemData);

               //$query = DB::getQueryLog();

              if($eventInsert){

                  $result = array(
                      'status' => true,
                      'message' => 'Event item updated'
  
                  );
              
               
  
              //
            }else{
              $result = array(
                  'status' => false,
                  'message' => 'Something Went Wrong',	
                  'c_code'  =>2	 			
             
              );
            }
              
              
              //echo $str;
              //print_r(count($package));die;
                      
         
         // print_r($insertData);die;
         }catch(\Exception $e){	
                $result = array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'c_code'  =>2	 			
               
                );
           }
  
           return response()->json($result);
          
    }

    public function addEventProduct(Request $request){

        $input = $request->all();
		
          $dt 	= new DateTime();
          $date	= $dt->format('Y-m-d H:i:s');
         if($request->hasFile('file')){
          $validator = Validator::make($request->all(), [
            'file'   => 'mimes:doc,pdf,docx|nullable'
        ]);
        if ($validator->fails()) {
			
            $result = array(
                'status' => false,
                'message' => "please upload only doc,pdf,docx file",				
        
                );
                return response()->json($result);
          
        
        }
    }
    
    

      //	$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));
                  
        try{
            $prod_market = $input['prod_market'];
            $prod_cat=$input['prod_cat'];
            $prod_sub_cat=$input['prod_sub_cat'];
            $spec=$input['spec'];
            $prod_unit=$input['prod_unit'];
            $prod_st_price= isset($input['prod_st_price']) ? $input['prod_st_price'] : null;
            $prod_min_decrement= isset($input['prod_min_decrement']) ? $input['prod_min_decrement'] : null;
            $location=$input['location'];
            $quantity=$input['prod_quant'];
            $time_extension= isset($input['time_extension']) ? $input['time_extension'] : null ;
            $no_of_time_extension= isset($input['no_of_time_extension']) ? $input['no_of_time_extension'] : null;
            $loginid=$input['loginid'];
            $eventID=$input['eventID'];
            $emd = isset($input['pea_event_emd_value']) ? $input['pea_event_emd_value'] : 0;
            $emd_check = $input['pea_emd_check'];
			
            if(isset($input['pea_event_reserve_price'])){
                $reserve_price=  $input['pea_event_reserve_price'];
            }else{
                $reserve_price=  0;
            }
         
            
             // print_r($Pkg_curCount);die;
            
      
			$st=new DateTime($input['pec_event_start_dt']);
			$et=new DateTime($input['pec_event_end_dt']);
			$event_start=$st->format('Y-m-d H:i:s');
            $event_end=$et->format('Y-m-d H:i:s');
            
            $user_os 		= PurchaseController::getOS();	
		    $user_browser   = PurchaseController::getBrowser();	

  
              $EventItemData =	[
                  'pea_event_id'                =>  $eventID,
                  'pea_event_market'            =>  $prod_market,
                  'pea_event_cat' 		 	    =>  $prod_cat,
                  'pea_event_subcat'  		    =>  $prod_sub_cat,
                  'pea_event_spec'  	        =>  $spec,
                  'pea_event_unit' 	            =>  $prod_unit,
                  'pea_event_unit_quantity'     =>  $quantity,
                  'pea_event_start_price'  	    =>  $prod_st_price,
                  'pea_event_max_dec'  		    =>  $prod_min_decrement,
                  'pea_event_location'  	    =>  $location,
                  'pea_event_additem_dt'  	    =>  $date,
                  'pea_event_additem_loginid'  	=>  $loginid,
                  'pea_event_reserve_price'     =>  $reserve_price,
                  'pea_is_emd_required'         =>  $emd_check,
                  'pea_event_emd_value'         =>  $emd,
				  'pea_event_start_dt' => $event_start,
                  'pea_event_end_dt' => $event_end,
                  'pea_time_extension' => $time_extension,
                  'pea_no_of_extension' => $no_of_time_extension,
                  'pea_extn_count' => 0,
                  'pea_ipaddress'         =>  $_SERVER['REMOTE_ADDR'],
                  'pea_browser'           =>  $user_browser['name'],        
                  'pea_browser_ver'       =>  $user_browser['version'],    
                  'pea_os'                =>  $user_os  
              ];
             
            //  print_r( $EventItemData);die;
           
               $eventInsert = DB::table('pq_event_additem')->insertGetId($EventItemData);
  
  
              if( $eventInsert){

                if($request->hasFile('file')){
                $extension = $request->file('file');
                $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
                $dir = 'pq_terms/'.date('Y').'/'.date('m').'/'.date('d').'/'. $eventID;
                $filename = $eventInsert.'.'.$extension;
                $request->file('file')->move($dir, $filename);
                $path=  $dir.'/'. $filename;
                $updateItemFileLocation =	[


                    'pea_event_additem_dt'  	    =>  $date,
                    'pea_event_terms_condition' =>   $path
                    
                ];

                $upadteFileLocation = DB::table('pq_event_additem')
               ->where('pea_id', $eventInsert)
               ->update($updateItemFileLocation);
            }


                  $result = array(
                      'status' => true,
                      'message' => 'Event item added'
  
                  );
              
               
  
              //print_r( $eventid);die;
            }else{
              $result = array(
                  'status' => false,
                  'message' => 'Something Went Wrong',	
                  'c_code'  =>2	 			
             
              );
            }
              
              
              //echo $str;
              //print_r(count($package));die;
                      
         
         // print_r($insertData);die;
         }catch(\Exception $e){	
                $result = array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'c_code'  =>2	 			
               
                );
           }
        
  
           return response()->json($result);
    }

    public function completedEvent(Request $request){
        $input = $request->all();
    
       //sssprint_r($input);die;
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');
        
    
        
    //	$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));
                
      try{
          $eventID=$input['eventID'];
          $loginid=$input['loginid'];
		  
			$lowestDate = DB::table('pq_event_additem')
							->where('pea_event_id', $eventID)
							->orderBy('pea_event_start_dt', 'asc')
							->first();
							
			$highestDate = DB::table('pq_event_additem')
							->where('pea_event_id', $eventID)
							->orderBy('pea_event_end_dt', 'desc')
							->first();	
          
           // print_r($Pkg_curCount);die;
		$EventCompletedData =	[            
            'pec_event_status' 		 	    =>  '1',
            'pec_event_create_dt' =>  $date,
			'pec_event_start_dt' => $lowestDate->pea_event_start_dt,
			'pec_event_end_dt' => $highestDate->pea_event_end_dt
                 
        ];

        $updateData= DB::table('pq_event_create')
        ->where('pec_event_id', $eventID)
        ->update($EventCompletedData);
       
    
          //  print_r( $EventItemData);die;
         
             
            if( $updateData){

                $result = array(
                    'status' => true,
                    'message' => 'Event Completed',	
                    'c_code'  =>1	 			
               
                );
               
            //print_r( $eventid);die;
          }else{
            $result = array(
                'status' => false,
                'message' => 'Some went wrong',	
                'c_code'  =>2	 			
           
            );
          }
            
            
            //echo $str;
            //print_r(count($package));die;
                    
       
       // print_r($insertData);die;
       }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);
    }

    public function getSubProduct(Request $request){

        $input = $request->all();
        $categories=$input['cat'];
        $cat_type=$input['cat_type'];
        $otherText='OTHERS';
      /*  $others=DB::table('pq_master_subcat as sub_other')
        ->select('*')
        ->where('sub_other.pms_subcat_name',$otherText)
        ->where('sub_other.pms_cat_type',$cat_type);

         $category = DB::table('pq_master_subcat')
         ->select('*')
         ->where('pq_master_subcat.pms_catid',$categories)

         ->union($others)
        ->get();*/
        $category = DB::table('pq_master_subcat_new')
         ->select('*')
         ->where('pq_master_subcat_new.pms_catid',$categories)
         ->get();


        return  $category;
    }

    public function lotInviteCheck(Request $request){

        $input = $request->all();

        $email=$input['email'];

        $lotID=$input['lotid'];
        $json_date='{"email" : "'.$email.'"}';
        $check_logintype=DB::table('pq_login_info')
        ->select('*')
        ->where('pli_loginid',$email)
        ->first();

       
if($check_logintype){
        if( $check_logintype->pli_login_status=='A' && $check_logintype->pli_login_type!='1'){
      
    
         $sql="select COUNT(pq_event_additem.`pea_id`) as invite_count FROM pq_event_invite_detail
            JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
            JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
            JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
            JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
            WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
             
             AND `pq_event_additem`.`pea_id`=".$lotID;
             $invite=DB::select( $sql);
             $invite_count=$invite[0];
             $result=  $invite_count->invite_count;
        }else{
            $result=2;
        }
    }else{
        

     $sql="select COUNT(pq_event_additem.`pea_id`) as invite_count FROM pq_event_invite_detail
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
        WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         
         AND `pq_event_additem`.`pea_id`=".$lotID;
         $invite=DB::select( $sql);
         $invite_count=$invite[0];
         
        $result=  $invite_count->invite_count;
    }
    return   $result;
    }

    
    public static function getAddedItems($eventID){
        $eventID=$eventID;
         $category = DB::table('pq_event_additem')
         ->Join('pq_master_cat_new', 'pq_master_cat_new.pmca_id' , '=', 'pq_event_additem.pea_event_cat')
         ->leftJoin('pq_event_invite_detail', 'pq_event_invite_detail.pei_event_additem_id' , '=', 'pq_event_additem.pea_id')
         ->Join('pq_master_market', 'pq_master_market.pmm_id' , '=', 'pq_event_additem.pea_event_market')
         ->Join('pq_master_subcat_new', 'pq_master_subcat_new.pms_id' , '=', 'pq_event_additem.pea_event_subcat')
         ->Join('pq_master_unit', 'pq_master_unit.pmu_id' , '=', 'pq_event_additem.pea_event_unit')
         ->select('pq_event_additem.pea_id',  
        
         'pq_master_market.pmm_market_name',
         'pq_master_cat_new.pmca_id',
         'pq_master_cat_new.pmca_cat_name',
         'pq_master_subcat_new.pms_id',
         'pq_master_subcat_new.pms_subcat_name',
         'pq_event_additem.pea_event_spec',
         'pq_master_unit.pmu_id',
         'pq_master_unit.pmu_unit_name',
         'pq_event_additem.pea_event_unit_quantity',
		 'pq_event_additem.pea_event_start_price',
         'pq_event_additem.pea_event_max_dec',
         'pq_event_additem.pea_event_terms_condition',
         'pq_event_additem.pea_event_location',
         'pq_event_additem.pea_event_reserve_price',
		 'pq_event_additem.pea_event_start_dt',
		 'pq_event_additem.pea_event_end_dt',
         'pq_event_invite_detail.*')
         ->where('pq_event_additem.pea_event_id',$eventID)
         ->where('pq_event_additem.pea_event_start_dt', '<=', NOW())
         ->where('pq_event_additem.pea_event_end_dt', '>=', NOW())
        ->get();        
       
        return  $category;
    }

    //buyer upcoming events
    public static function getAddedItems1($eventID){
        $eventID=$eventID;
         $category = DB::table('pq_event_additem')
         ->Join('pq_master_market', 'pq_master_market.pmm_id' , '=', 'pq_event_additem.pea_event_market')
         ->Join('pq_master_cat_new', 'pq_master_cat_new.pmca_id' , '=', 'pq_event_additem.pea_event_cat')
         ->leftJoin('pq_event_invite_detail', 'pq_event_invite_detail.pei_event_additem_id' , '=', 'pq_event_additem.pea_id')
         ->Join('pq_master_subcat_new', 'pq_master_subcat_new.pms_id' , '=', 'pq_event_additem.pea_event_subcat')
         ->Join('pq_master_unit', 'pq_master_unit.pmu_id' , '=', 'pq_event_additem.pea_event_unit')
         ->select('pq_event_additem.pea_id',  
        
         'pq_master_market.pmm_id',
         'pq_master_market.pmm_market_name',
         'pq_master_cat_new.pmca_id',
         'pq_master_cat_new.pmca_cat_name',
         'pq_master_subcat_new.pms_id',
         'pq_master_subcat_new.pms_subcat_name',
         'pq_event_additem.pea_event_spec',
         'pq_master_unit.pmu_id',
         'pq_master_unit.pmu_unit_name',
         'pq_event_additem.pea_event_unit_quantity',
		 'pq_event_additem.pea_event_start_price',
         'pq_event_additem.pea_event_max_dec',
         'pq_event_additem.pea_event_terms_condition',
         'pq_event_additem.pea_event_location',
         'pq_event_additem.pea_event_reserve_price',
		 'pq_event_additem.pea_event_start_dt',
		 'pq_event_additem.pea_event_end_dt',
         'pq_event_invite_detail.*')
         ->where('pq_event_additem.pea_event_id',$eventID)
         ->where('pq_event_additem.pea_event_start_dt', '>=', NOW())
         //->where('pq_event_additem.pea_event_end_dt', '>=', NOW())
        ->get();        
       
        return  $category;
    }

    //buyer completed events
    public static function getAddedItems2($eventID){
        $eventID=$eventID;
         $category = DB::table('pq_event_additem')
         ->Join('pq_master_cat_new', 'pq_master_cat_new.pmca_id' , '=', 'pq_event_additem.pea_event_cat')
         ->leftJoin('pq_event_invite_detail', 'pq_event_invite_detail.pei_event_additem_id' , '=', 'pq_event_additem.pea_id')
         ->Join('pq_master_subcat_new', 'pq_master_subcat_new.pms_id' , '=', 'pq_event_additem.pea_event_subcat')
         ->Join('pq_master_market', 'pq_master_market.pmm_id' , '=', 'pq_event_additem.pea_event_market')
         ->Join('pq_master_unit', 'pq_master_unit.pmu_id' , '=', 'pq_event_additem.pea_event_unit')
         ->select('pq_event_additem.pea_id',  
         
         'pq_master_market.pmm_market_name',
         'pq_master_cat_new.pmca_id',
         'pq_master_cat_new.pmca_cat_name',
         'pq_master_subcat_new.pms_id',
         'pq_master_subcat_new.pms_subcat_name',
         'pq_event_additem.pea_event_spec',
         'pq_event_additem.pea_event_id',
         'pq_master_unit.pmu_id',
         'pq_master_unit.pmu_unit_name',
         'pq_event_additem.pea_event_unit_quantity',
		 'pq_event_additem.pea_event_start_price',
         'pq_event_additem.pea_event_max_dec',
         'pq_event_additem.pea_event_terms_condition',
         'pq_event_additem.pea_event_location',
         'pq_event_additem.pea_event_reserve_price',
		 'pq_event_additem.pea_event_start_dt',
		 'pq_event_additem.pea_event_end_dt',
         'pq_event_invite_detail.*')
         ->where('pq_event_additem.pea_event_id',$eventID)
         ->where('pq_event_additem.pea_event_start_dt', '<=', NOW())
         ->where('pq_event_additem.pea_event_end_dt', '<=', NOW())
        ->get();        
       
        return  $category;
    }

    public static function sellerBidDetails($lotId){
        
        $category = DB::table('pq_live_auction')

        ->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_live_auction.pla_seller_compid')
        ->select('pq_live_auction.pla_cur_bid','pq_live_auction.pla_id',
        'pq_company_info.pci_comp_name', 'pq_live_auction.pla_auction_status',
        'pq_live_auction.pla_seller_emailid', 'pq_live_auction.pla_lotid'
        )
        ->where('pq_live_auction.pla_lotid',$lotId)
        ->get();

        return  $category;
    }

    public static function sellerWonBidDetails($lotId){
        
        $category = DB::table('pq_live_auction')

        ->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_live_auction.pla_seller_compid')
        ->leftJoin('pq_purchase_orders', 'pq_live_auction.pla_id' , '=', 'pq_purchase_orders.live_action_id')
        ->leftJoin('pq_purchase_invoice', 'pq_purchase_orders.order_id' , '=', 'pq_purchase_invoice.ppi_order_id')
         ->select('pq_live_auction.pla_cur_bid','pq_live_auction.pla_id',
        'pq_company_info.pci_comp_name', 'pq_live_auction.pla_auction_status',
        'pq_live_auction.pla_seller_emailid', 'pq_live_auction.pla_lotid',
        'pq_purchase_orders.id as po_id', 'pq_purchase_orders.seller_confirmed',
        'pq_purchase_orders.ppo_is_po_sent', 'pq_purchase_invoice.ppi_id',
        'pq_purchase_invoice.ppi_is_invoice_sent', 'pq_purchase_invoice.ppi_is_buyer_confirmed'
        )
        ->where('pq_live_auction.pla_lotid',$lotId)
        ->where('pq_live_auction.pla_auction_status', 1)
        ->get();

        return  $category;
    }

    public static function validateRejectAllData($lotId)
    {
        $category = DB::table('pq_live_auction')

        //->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_live_auction.pla_seller_compid')
        ->select('pq_live_auction.pla_cur_bid','pq_live_auction.pla_id',
        'pq_live_auction.pla_auction_status', 'pq_live_auction.pla_seller_emailid'
        )
        ->where('pq_live_auction.pla_lotid',$lotId)
        ->where('pq_live_auction.pla_auction_status', 2)
        ->get();

        return $category;
    }

    public static function validateAcceptData($lotId)
    {
        $category = DB::table('pq_live_auction')

        //->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_live_auction.pla_seller_compid')
        ->select('pq_live_auction.pla_cur_bid','pq_live_auction.pla_id',
        'pq_live_auction.pla_auction_status', 'pq_live_auction.pla_seller_emailid'
        )
        ->where('pq_live_auction.pla_lotid',$lotId)
        ->where('pq_live_auction.pla_auction_status', 1)
        ->get();

        return $category;
    }

    public function rescheduleLotItem(Request $request){
        $input = $request->all();
        $session_val = session()->all();
       
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try{
          
          $addItemID=$input['lotID'];
          $comp_id= $session_val['pli_comp_id'];
          $log_id= $session_val['pli_loginId'];
          $eventId = $input['eventId'];

          $st=new DateTime($input['startDate']);
			$et=new DateTime($input['endDate']);
			$event_start=$st->format('Y-m-d H:i:s');
			$event_end=$et->format('Y-m-d H:i:s');

            $user_os 		= PurchaseController::getOS();	
            $user_browser   = PurchaseController::getBrowser();
            
            $validateHistoryData = DB::table('pq_bid_history')
                                    ->where('pbh_lotid', '=',  $addItemID)
                                    ->delete();

            $validateHistoryData = DB::table('pq_live_auction')
                                    ->where('pla_lotid', '=',  $addItemID)
                                    ->delete();

            $updateLot = [
                'pea_event_start_dt' => $st,
                'pea_event_end_dt' => $et,
                'pea_extn_count' => 0,
                'pea_ipaddress'               => $_SERVER['REMOTE_ADDR'],
                'pea_browser'                 => $user_browser['name'],    
                'pea_browser_ver'             => $user_browser['version'],
                'pea_os'                      => $user_os
            ];

            $check_exist = DB::table('pq_event_additem')
                            ->where('pea_id', '=',  $addItemID)
                            ->update($updateLot);

            if($check_exist)
            {
                $check_exist_event = DB::table('pq_event_create')
                                    ->where('pec_event_id', '=',  $eventId)
                                    ->where('pec_event_end_dt', '<=', $et)
                                    ->update(['pec_event_end_dt' => $et ]);               
                
                $result = array(
                    'status' => true,
                    'message' => 'Rescheduled time updated'
                );

            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Something Went Wrong',	
                    'c_code'  =>2	 			
            
                );
            }

       }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);


    }

    
    public function getPqvendor(Request $request){
        
        $input = $request->all();

        $lotID= $input['lotid'];
        $catID= $input['category_id'];
        $location= $input['location'];
       
        $checkinvite_insert = DB::table('pq_event_invite_detail')
       ->select('*')    
       ->where('pei_event_additem_id', $lotID)
       ->first();
           
        if(!$checkinvite_insert){
            $pqvendor = DB::table('pq_master_vendors')
            ->select('*')    
            ->where('pmv_cat_id',$catID)
            ->where('pmv_comp_city', 'like', '%'.$location.'%')
            ->get();
        }else{
           $sql="select * FROM `pq_master_vendors` WHERE NOT FIND_IN_SET(pq_master_vendors.pmv_comp_email,(SELECT REPLACE(REPLACE(REPLACE(REPLACE(JSON_EXTRACT(`pei_invite_details`, '$[*].email'),'[',''),']',''),'\"',''),' ','') FROM pq_event_invite_detail WHERE pq_event_invite_detail.`pei_event_additem_id`='". $lotID."')) AND `pmv_cat_id`='".$catID."' and  `pmv_comp_city` LIKE '%".$location."%'";
           $invite=DB::select( $sql);

       
           $pqvendor= $invite;
        }
        

        return  $pqvendor;

    }
    public static function getLocation(){
        
        $location = DB::table('pq_master_cities')
        ->select('*')
        ->get();

        return  $location;

    }

    public static function reports(Request $request)
	{
		$input = $request->all();
	   
        $event_name = $input['pec_event_name'];
        $logID = $input['loginid'];

        $st=new DateTime($input['pec_event_start_dt']);
        $et=new DateTime($input['pec_event_end_dt']);
		
        $event_start=$st->format('Y-m-d');
        $event_end=$et->format('Y-m-d');

        // $json_date='{"email" : "'.$logID.'","accept": "1"}';
        // $sql = "
        // SELECT 
        //         *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
        //       FROM
        //         `pq_event_create` 
        //       JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
        //       WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
        //         AND `pq_event_create`.`pec_event_status` = '1'
        //         AND `pq_event_create`.`pec_event_name` LIKE '%".$eventName."%'
        //         AND (`pq_event_create`.`pec_event_start_dt` <= NOW() AND `pq_event_create`.`pec_event_end_dt` <= NOW())
        //         GROUP BY pq_event_create.`pec_event_id` 
        //  ";
     

        // $eventData=DB::select( $sql);

        // return View::make('reports.buyer-report')->with('event_data', $eventData);   
        
        if($input['pec_event_name'] != null && $input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{
			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
            //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
            $sql = "
            SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
				AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
                AND `pq_event_create`.`pec_event_status` = '1' 
              AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
            AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
         ";     

            $eventData=DB::select( $sql);
      					
		    return View::make('reports.overall-report')->with('event_data', $eventData);
		}
		elseif ($input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			 SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'				
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
                AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
            ";  
            $eventData=DB::select( $sql);      			
			return View::make('reports.overall-report')->with('event_data', $eventData);
			
		}
		elseif($input['pec_event_name'] != null)
		{	
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			SELECT 
					*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
				  FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
				    WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
					AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
                    AND `pq_event_create`.`pec_event_status` = '1' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_create.`pec_event_id`			 
			 ";     

			$eventData=DB::select( $sql);
      					
			return View::make('reports.overall-report')->with('event_data', $eventData);		
		}		
		else
		{
            return View::make('reports.overall-report')->with('event_data', $eventData = null);
		}	

    }

    //event lot details pdf/excel
    public static function eventreport(Request $request)
    {
        $input = $request->all();
	   
        $eventId = $input['hfEventIt'];

        $columns = isset($input['lotdetail']) ? $input['lotdetail'] : null; 

        $market_flag = 0;
        $category_flag = 0;
        $sub_flag = 0;
        $unit_flag = 0;  
        $quantity_flag = 0;  
        $startp_flag = 0;  
        $diff_flag = 0;  
        $floor_flag = 0;  
        $location_flag = 0;  

        if(isset($columns))
        {
            foreach($columns as $key => $value)
            {
                if($value == 2)
                {
                    $category_flag = 1;
                }
                else if($value == 3)
                {
                    $sub_flag = 1;
                }            
                else if($value == 7)
                {
                    $unit_flag = 1;
                }
                else if($value == 8)
                {
                    $quantity_flag = 1;
                }
                else if($value == 9)
                {
                    $startp_flag = 1;
                }
                else if($value == 10)
                {
                    $diff_flag = 1;
                }
                else if($value == 11)
                {
                    $floor_flag = 1;
                }
                else if($value == 13)
                {
                    $market_flag = 1;
                }
                else
                {
                    $location_flag = 1;
                }
            }
        }

        if($input['SUBMIT'] == 'Excel')
        {
            return (new EventItemExport($eventId, $market_flag, $category_flag, $sub_flag, $unit_flag, $quantity_flag, $startp_flag, $diff_flag, $floor_flag, $location_flag))->download('Event Items.xlsx');
        }
        else
        {            
            //return (new EventItemExport($eventId))->download('event_items.pdf');

            // instantiate and use the dompdf class
            $dompdf = new Dompdf();

            $items = DB::table('pq_event_additem')
                        ->select('pea_event_id', 'pq_master_market.pmm_market_name', 'pq_master_cat_new.pmca_cat_name','pq_master_subcat_new.pms_subcat_name', 'pea_event_start_dt', 'pea_event_end_dt',
                        'pea_event_spec', 'pq_master_unit.pmu_unit_name', 'pea_event_unit_quantity', 'pea_event_start_price', 'pea_event_max_dec',
                        'pea_event_reserve_price', 'pea_event_location')    
                        ->Join('pq_master_market', 'pq_event_additem.pea_event_market' , '=', 'pq_master_market.pmm_id')
                        ->Join('pq_master_cat_new', 'pq_event_additem.pea_event_cat' , '=', 'pq_master_cat_new.pmca_id')
                        ->Join('pq_master_subcat_new', 'pq_event_additem.pea_event_subcat' , '=', 'pq_master_subcat_new.pms_id')
                        ->Join('pq_master_unit', 'pq_event_additem.pea_event_unit' , '=', 'pq_master_unit.pmu_id')
                        ->where('pea_event_id', $eventId)
                        ->get();

            $event = DB::table('pq_event_create')
                        ->select('*')
                        ->where('pec_event_id', $eventId)
                        ->first();

            $pdfData = "";
            $pdfHeader = "";
            foreach($items as $key => $values)
            {
                $pdfData .= '<tr>';
                $pdfData .= '<th scope="row">'.$values->pea_event_id.'</th>';

                if($market_flag == 1)
                {
                    $pdfData .= '<td>'.$values->pmm_market_name.'</td>';
                }
                
                if($category_flag == 1)
                {
                    $pdfData .= '<td>'.$values->pmca_cat_name.'</td>';
                }

                if($sub_flag == 1)
                {
                    $pdfData .= '<td>'.$values->pms_subcat_name.'</td>';
                }

                $pdfData .= '<td>'.$values->pea_event_start_dt.'</td>';               
                $pdfData .= '<td>'.$values->pea_event_end_dt.'</td>';               
                $pdfData .= '<td>'.$values->pea_event_spec.'</td>';
                
                if($unit_flag == 1)
                {
                    $pdfData .= '<td>'.$values->pmu_unit_name.'</td>';
                }

                if($quantity_flag == 1)
                {
                    $pdfData .= '<td>'.$values->pea_event_unit_quantity.'</td>';
                }

                if($startp_flag == 1)
                {
                    $pdfData .= '<td>'.$values->pea_event_start_price.'</td>';
                }

                if($diff_flag == 1)
                {
                    $pdfData .= '<td>'.$values->pea_event_max_dec.'</td>';
                }

                if($floor_flag == 1)
                {
                    $pdfData .= '<td>'.$values->pea_event_reserve_price.'</td>';
                }

                if($location_flag == 1)
                {
                    $pdfData .= ' <td>'.$values->pea_event_location.'</td>';
                }

                $pdfData .= '
                            </tr>';
            }

            $pdfHeader .= '<tr>';            
            $pdfHeader .= '<th>Event Id</th>';
            
            if($market_flag == 1)
            {
                $pdfHeader .= '<th>Market Name</th>';
            }
            if($category_flag == 1)
            {
                $pdfHeader .= '<th>Category Name</th>';
            }
            if($sub_flag == 1)
            {
                $pdfHeader .= '<th>Sub Category Name</th>';
            }
            
            $pdfHeader .= '<th>Start Date</th>';            
            $pdfHeader .= '<th>End Date</th>'; 
            $pdfHeader .= '<th>Spec</th>';
            
            if($unit_flag == 1)
            {
                $pdfHeader .= '<th>Unit</th>';
            }
            if($quantity_flag == 1)
            {
                $pdfHeader .= '<th>Item Quantity</th>';
            }
            if($startp_flag == 1)
            {
                $pdfHeader .= ' <th>Start Price</th>';
            }
            if($diff_flag == 1)
            {
                $pdfHeader .= ' <th>Min Decrement</th>';
            }
            if($floor_flag == 1)
            {
                $pdfHeader .= ' <th>Floor Price</th>';
            }
            if($location_flag == 1)
            {
                $pdfHeader .= '<th>Location</th>';
            }

            if($event->pec_event_type == 2)
            {
                $event_type = 'Reverse Bid Auction';
            }
            else
            {
                $event_type = 'Closed Bid Auction';
            }

            $pdfHeader .= '</tr>';

            $dompdf->loadHtml('
            <style>
                footer {
                    position: fixed; 
                    bottom: -60px; 
                    left: 0px; 
                    right: 0px;
                    height: 50px; 

                    /** Extra personal styles **/
                    border-top: 2px solid #D3D3D3;
                    color:black;
                    text-align: center;
                    line-height: 35px;
                }
            </style>
            <body>
                <footer>
                    Copyright &copy; 2019 Simplified Procurement Solutions(OPC) Pvt Ltd. All Rights Reserved.
                </footer>  
                <table width="100%">
                    <tr>            
                        <td> <img src="C:/xampp/htdocs/PQ/public/new-img/logo.png" /> </td>            
                        <td align="right">
                            <h3>Simplified Procurement Solutions(OPC) Pvt Ltd</h3>
                            <p>
                                Chamier Towers, 37/6 1st floor, Pasumpon Muthuramalinga Thevar Rd,
                                <br> 
                                Teynampet, Chennai, Tamil Nadu 600028
                                <br>
                                044 4343 7474
                            </p>
                        </td>
                    </tr>
                </table>

                <h4>EVENT DETAILS</h4>
                <table width="100%" style="border:1px lightgray solid">                    
                    <tr>
                        <td><strong>Event Id :</strong> '.$event->pec_event_id.' </td>
                        <td><strong>Event Name :</strong> '.$event->pec_event_name.'</td>          
                        <td><strong>Start Date :</strong> '.$event->pec_event_start_dt.'</td>              
                    </tr>
                    <tr>     
                        <td><strong>End Date :</strong> '.$event->pec_event_end_dt.'</td>
                        <td><strong>Event Type :</strong> '.$event_type.'</td>                       
                    </tr>
                </table>
                <br>

                <h4>LIST OF ITEMS</h4>
                <table width="100%">
                    <thead style="background-color: lightgray;">
                        '.$pdfHeader.'
                    </thead>
                    <tbody>                        
                        '.$pdfData.'                        
                    </tbody>
                
                </table>

            </body>
            ');

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A4', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream('Event Items', array("Attachment" => false));

            exit(0);
        }
       
    }

    //overall search report pdf/excel
    public static function reportsPDF(Request $request)
    {
        $input = $request->all();
        
        $pdfData='';
        for($i=0; $i< @count($input['hfArrayEventId']); $i++ )
        {
            $pdfData .= '<tr>
                    <th scope="row">'.$input['hfArrayEventId'][$i].'</th>
                    <td>'. $input['hfArrayEventName'][$i] .'</td>
                    <td>'. $input['hfArrayStartDate'][$i] .'</td>
                    <td>'. $input['hfArrayEndDate'][$i] .'</td>
                    <td>'. $input['hfArrayEventType'][$i] .'</td>
                    <td>'. $input['hfArrayNoOfItems'][$i] .'</td>
                </tr>';
        }

        $dompdf = new Dompdf();

        $dompdf->loadHtml('
        <style>
            footer {
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                border-top: 2px solid #D3D3D3;
                color:black;
                text-align: center;
                line-height: 35px;
            }
        </style>
            <body>        
                <footer>
                    Copyright &copy; 2019 Simplified Procurement Solutions(OPC) Pvt Ltd. All Rights Reserved.
                </footer>          
                <table width="100%">
                    <tr>                  
                        <td> <img src="C:/xampp/htdocs/PQ/public/new-img/logo.png" /> </td>     
                        <td align="right">
                            <h3>Simplified Procurement Solutions(OPC) Pvt Ltd</h3>
                            <p>
                                Chamier Towers, 37/6 1st floor, Pasumpon Muthuramalinga Thevar Rd,
                                <br> 
                                Teynampet, Chennai, Tamil Nadu 600028
                                <br>
                                044 4343 7474
                            </p>
                        </td>
                    </tr>
                </table>
                <br>

                <h4>LIST OF EVENTS</h4>
                <table width="100%">
                    <thead style="background-color: lightgray;">
                        <tr>
                            <th>Event Id</th>
                            <th>Event Name</th>
                            <th>Event Start Date</th>
                            <th>Event End Date</th>
                            <th>Event Type</th>
                            <th>No Of Items</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        '.$pdfData.'                        
                    </tbody>
                
                </table>

            </body>
            ');

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A4', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream('Simplified Procurement Solutions(OPC) Events', array("Attachment" => false));
            
            exit(0);
        
    }

    //lot bidders details pdf/excel
    public static function lotreport(Request $request)
    {
        $input = $request->all();
	   
        $lotId = $input['hfLotId'];  
        $eventId = $input['hfEventId'];  
        $columns = isset($input['biddetail']) ? $input['biddetail'] : null; 

        $date_flag = 0;  
        $auction_status_flag = 0;  

        if(isset($columns))
        {
            foreach($columns as $key => $value)
            {
                if($value == 1)
                {
                    $lot_flag = 1;
                }
                else if($value == 2)
                {
                    $comp_flag = 1;
                }
                else if($value == 3)
                {
                    $bid_flag = 1;
                }
                else if($value == 4)
                {
                    $date_flag = 1;
                }
                else
                {
                    $auction_status_flag = 1;
                }
            }
        }
        
        if($input['SUBMIT'] == 'Excel')
        {
            return (new LotBidExport($lotId, $date_flag, $auction_status_flag))->download('Bid Report.xlsx');
        }
        else
        {          
            // instantiate and use the dompdf class
            $dompdf = new Dompdf();

            $items = DB::table('pq_bid_history')
                        ->select('pbh_lotid', 'pq_company_info.pci_comp_name', 'pbh_cur_bid', 'pbh_cur_bid_datetime', 
                            DB::raw('(CASE WHEN pbh_auction_status= "1" THEN "Win" WHEN pbh_auction_status= "2" THEN "Lose" WHEN pbh_auction_status= "3" THEN "Bidding" ELSE "Pending" END) AS auction_status'))    
                        ->Join('pq_company_info', 'pq_bid_history.pbh_seller_compid' , '=', 'pq_company_info.pci_comp_id')
                        ->where('pbh_lotid', $lotId)
                        ->orderBy('pbh_id', 'desc')
                        ->get();

            $event = DB::table('pq_event_additem')
                        ->select('pq_event_additem.*', 'pq_master_market.pmm_market_name', 'pq_master_cat_new.pmca_cat_name', 'pq_master_subcat_new.pms_subcat_name')
                        ->Join('pq_master_cat_new', 'pq_event_additem.pea_event_cat' , '=', 'pq_master_cat_new.pmca_id')
                        ->Join('pq_master_market', 'pq_event_additem.pea_event_market' , '=', 'pq_master_market.pmm_id')
                        ->Join('pq_master_subcat_new', 'pq_event_additem.pea_event_subcat' , '=', 'pq_master_subcat_new.pms_id')
                        ->where('pea_id', $lotId)
                        ->first();

            $eventDetail = DB::table('pq_event_create')
                        ->select('*')
                        ->where('pec_event_id', $eventId)
                        ->first();

            $pdfData = "";
            //$i=1;
            foreach($items as $key => $values)
            {
                $pdfData .= '<tr>';
                $pdfData .= '<th scope="row">'.$values->pbh_lotid.'</th>';                
                $pdfData .= ' <td>'.$values->pci_comp_name.'</td>';                
                $pdfData .= ' <td>'.$values->pbh_cur_bid.'</td>';
                
                if($date_flag == 1)
                {
                    $pdfData .= ' <td>'.$values->pbh_cur_bid_datetime.'</td>';
                }

                if($auction_status_flag == 1)
                {
                    $pdfData .= ' <td>'.$values->auction_status.'</td>';
                }

                $pdfData .= '</tr>';

                // if($i >= 3)
                // {
                //     if($values->auction_status != "Pending")                    
                //     {
                //         $pdfData .= '<tr>
                //             <th scope="row">'.$values->pbh_lotid.'</th>
                //             <td>'.$values->pci_comp_name.'</td>
                //             <td>'.$values->pbh_cur_bid.'</td>
                //             <td>'.$values->pbh_cur_bid_datetime.'</td>
                //             <td>Bidding</td>
                //         </tr>';
                //     }
                //     else
                //     {
                //         $pdfData .= '<tr>
                //             <th scope="row">'.$values->pbh_lotid.'</th>
                //             <td>'.$values->pci_comp_name.'</td>
                //             <td>'.$values->pbh_cur_bid.'</td>
                //             <td>'.$values->pbh_cur_bid_datetime.'</td>
                //             <td>'.$values->auction_status.'</td>
                //         </tr>';  
                //     }
    
                // }
                // else
                // {
                //     $pdfData .= '<tr>
                //         <th scope="row">'.$values->pbh_lotid.'</th>
                //         <td>'.$values->pci_comp_name.'</td>
                //         <td>'.$values->pbh_cur_bid.'</td>
                //         <td>'.$values->pbh_cur_bid_datetime.'</td>
                //         <td>'.$values->auction_status.'</td>
                //     </tr>';    
                // }
                
                // $i++;
            }

            $pdfHeader = '<tr>
                '; 

            $pdfHeader .= '
                    <th>Item Id</th>';  
           
            $pdfHeader .= '
                    <th>Seller Company Name</th>';  
           
            $pdfHeader .= '
                    <th>Bid Amount</th>';  

            if($date_flag == 1)
            {
                $pdfHeader .= '
                <th>Bid Date</th>';  
            }

            if($auction_status_flag == 1)
            {
                $pdfHeader .= '
                <th>Auction Status</th>';  
            }

            $pdfHeader .= '</tr>
            '; 

            if($eventDetail->pec_event_type == 2)
            {
                $reserve_price = $event->pea_event_start_price;
                $min_diff = $event->pea_event_max_dec;
                $floor_price = $event->pea_event_reserve_price;
                $event_type = 'Reverse Bid Auction';
            }
            else
            {
                $reserve_price = 'Not Applicable';
                $min_diff = 'Not Applicable';
                $floor_price = 'Not Applicable';
                $event_type = 'Closed Bid Auction';
            }

            $dompdf->loadHtml('
            <style>
                footer {
                    position: fixed; 
                    bottom: -60px; 
                    left: 0px; 
                    right: 0px;
                    height: 50px; 

                    /** Extra personal styles **/
                    border-top: 2px solid #D3D3D3;
                    color:black;
                    text-align: center;
                    line-height: 35px;
                }
            </style>
            <body>             
                <footer>
                    Copyright &copy; 2019 Simplified Procurement Solutions(OPC) Pvt Ltd. All Rights Reserved.
                </footer>  
                <table width="100%">
                    <tr>                        
                        <td> <img src="C:/xampp/htdocs/PQ/public/new-img/logo.png" /> </td>    
                        <td align="right">
                            <h3>Simplified Procurement Solutions(OPC) Pvt Ltd</h3>
                            <p>
                                Chamier Towers, 37/6 1st floor, Pasumpon Muthuramalinga Thevar Rd,
                                <br> 
                                Teynampet, Chennai, Tamil Nadu 600028
                                <br>
                                044 4343 7474
                            </p>
                        </td>
                    </tr>
                </table>
                <br>

                <h4>EVENT DETAILS</h4>
                <table width="100%" style="border:1px lightgray solid">                    
                    <tr>
                        <td><strong>Event Id:</strong> '.$eventDetail->pec_event_id.' </td>
                        <td><strong>Event Name:</strong> '.$eventDetail->pec_event_name.'</td>   
                        <td><strong>Start Date:</strong> '.$eventDetail->pec_event_start_dt.'</td>                     
                    </tr>
                    <tr>  
                        <td><strong>End Date:</strong> '.$eventDetail->pec_event_end_dt.'</td>
                        <td><strong>Event Type:</strong> '.$event_type.'</td>
                    </tr>
                </table>
                <br>

                <h4>ITEM DETAILS</h4>
                <table width="100%" style="border:1px lightgray solid;">                    
                    <tr>
                        <td><strong>Market Name :</strong> '.$event->pmm_market_name.' </td>
                        <td><strong>Category Name :</strong> '.$event->pmca_cat_name.' </td>
                        <td><strong>Sub Category Name :</strong> '.$event->pms_subcat_name.' </td>                                         
                    </tr>                        
                    <tr>                    
                        <td><strong>Spec :</strong> '.$event->pea_event_spec.'</td>        
                        <td><strong>Start Date :</strong> '.$event->pea_event_start_dt.'</td>   
                        <td><strong>End Date </strong> '.$event->pea_event_end_dt.'</td>                                          
                    </tr>                    
                    <tr>
                        <td><strong>Reserve Price :</strong> '.$reserve_price.'</td>     
                        <td><strong>Min Decrement :</strong> '.$min_diff.'</td>
                        <td><strong>Floor Price :</strong> '.$floor_price.'</td>                        
                    </tr> 
                    <tr>    
                        <td><strong>Location :</strong> '.$event->pea_event_location.'</td>  
                    </tr>              
                </table>
                <br/>

                <h4>BID DETAILS</h4>
                <table width="100%">
                    <thead style="background-color: lightgray;">
                        '.$pdfHeader.'
                    </thead>
                    <tbody>                        
                        '.$pdfData.'                        
                    </tbody>                
                </table>

            </body>
            ');

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A4', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream('Bid Report', array("Attachment" => false));

            exit(0);
        }
    }  


    //gowtham function
    public function getreportlot(Request $request)
	 {
		 
		  $input = $request->all();
   
          $eventID=$input['eventIDreport'];
		  
         $category = DB::table('pq_event_additem')
         ->Join('pq_master_cat', 'pq_master_cat.pmc_id' , '=', 'pq_event_additem.pea_event_cat')
         ->leftJoin('pq_event_invite_detail', 'pq_event_invite_detail.pei_event_additem_id' , '=', 'pq_event_additem.pea_id')
         ->select('pq_event_additem.pea_id',  
        

         'pq_event_additem.pea_event_spec',
         'pq_event_additem.pea_event_id',


         'pq_event_invite_detail.*')
         ->where('pq_event_additem.pea_event_id',$eventID)
         ->where('pq_event_additem.pea_event_start_dt', '<=', NOW())
         ->where('pq_event_additem.pea_event_end_dt', '<=', NOW())
        ->get();        
       
          $category;
		 
		 return response()->json($category);
	 }
	 
	  public function chartsReprts(Request $request)
	 {
		$input = $request->all();
		
        $eventname=$input['event_name_report'];
		
        $lotId=$input['sub_lot'];
		
		//print_r($lotId);
		//die();
        //$lotId=512;
		
		$searchedcharts = DB::table('pq_event_additem')         
                            ->Join('pq_event_create', 'pq_event_create.pec_event_id' , '=', 'pq_event_additem.pea_event_id')
                            ->select('pq_event_additem.pea_id',  
                            'pq_event_create.*',
                            'pq_event_additem.pea_event_id',
                            'pq_event_additem.pea_event_spec')
                            ->where('pq_event_additem.pea_id',$lotId)
                            ->get();     
		
		 $items = DB::table('pq_bid_history')
                        ->select('pbh_lotid', 'pq_company_info.pci_comp_name', 'pbh_cur_bid', 'pbh_cur_bid_datetime', 
                            DB::raw('(CASE WHEN pbh_auction_status= "1" THEN "Win" WHEN pbh_auction_status= "2" THEN "Lose" WHEN pbh_auction_status= "3" THEN "Bidding" ELSE "Pending" END) AS auction_status'))    
                        ->Join('pq_company_info', 'pq_bid_history.pbh_seller_compid' , '=', 'pq_company_info.pci_comp_id')
                        ->where('pbh_lotid', $lotId)
                        ->orderBy('pbh_id', 'asc')
                        ->get();

        $lotData= DB::table('pq_event_additem')
                    ->select('pq_event_additem.*')
                    ->where('pq_event_additem.pea_event_id', '=' ,$eventname)
                    ->where('pq_event_additem.pea_event_end_dt', '<=', NOW())
                    ->get();
						
	
			$dataPoints1 = array();
			$dataPoints2 = array();
			
			foreach($items as $m)
			{
			 $dataPoints1[] = $m->pci_comp_name.".".$m->pbh_cur_bid_datetime;
			 $dataPoints2[] = $m->pbh_cur_bid;
			}

			
		    $line_chart = Charts::create('line', 'highcharts')
						->title('Seller Wise Bidding History')
						->elementLabel('Seller Bidwise Reports')
						->labels($dataPoints1)
						->values($dataPoints2)
						->dimensions(1000,500)
						->responsive(true);
		
				
		return View::make('charts.reportchat',compact('line_chart','searchedcharts', 'lotData', 'lotId'));	
		
     }
     
     public static function reportCompletedEvent($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        $sql = "
        SELECT 
            pq_event_create.*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND (`pq_event_create`.`pec_event_start_dt` <= NOW() AND `pq_event_create`.`pec_event_end_dt` <= NOW())
                OR (`pq_event_additem`.`pea_event_start_dt` <= NOW() AND `pq_event_additem`.`pea_event_end_dt` <= NOW())
                GROUP BY pq_event_create.`pec_event_id` 
         ";

        $eventData=DB::select( $sql);
        return $eventData;
    }

    
}