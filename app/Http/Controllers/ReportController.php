<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use DateTime;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use File;
use View;
use Exception;
use Validator;
use App\Imports\EventItemImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LotBidExport;
use Dompdf\Dompdf;
use App\Exports\HighLowReportExport;
use App\Exports\SellerWiseReportExport;
use Mail;  
use Dompdf\Dompdf as PDF;
use Charts;

class ReportController extends Controller
{

	public static function reversereport(Request $request)
	{
		$input = $request->all();
	   
        $event_name = $input['pec_event_name'];
        $logID = $input['loginid'];

        $st=new DateTime($input['pec_event_start_dt']);
        $et=new DateTime($input['pec_event_end_dt']);
		
        $event_start=$st->format('Y-m-d');
        $event_end=$et->format('Y-m-d');

        if($input['pec_event_name'] != null && $input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{
			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
            //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
            $sql = "
            SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
				AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
				AND `pq_event_create`.`pec_event_status` = '1' 
				AND `pq_event_create`.`pec_event_type` = '2' 
              AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
            AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
         ";     

            $eventData=DB::select( $sql);
      					
		    return View::make('reports.reverse-report')->with('event_data', $eventData);
		}
		elseif ($input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			 SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'				
				AND `pq_event_create`.`pec_event_status` = '1' 
				AND `pq_event_create`.`pec_event_type` = '2' 
                AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
                AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
            ";  
            $eventData=DB::select( $sql);      			
			return View::make('reports.reverse-report')->with('event_data', $eventData);
			
		}
		elseif($input['pec_event_name'] != null)
		{
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			SELECT 
					*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
				  FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
				    WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
					AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
					AND `pq_event_create`.`pec_event_status` = '1' 
					AND `pq_event_create`.`pec_event_type` = '2' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_create.`pec_event_id`			 
			 ";     

			$eventData=DB::select( $sql);
      					
			return View::make('reports.reverse-report')->with('event_data', $eventData);		
		}		
		else
		{
            return View::make('reports.reverse-report')->with('event_data', $eventData = null);
		}	

    }

	public static function closereport(Request $request)
	{
		$input = $request->all();
	   
        $event_name = $input['pec_event_name'];
        $logID = $input['loginid'];

        $st=new DateTime($input['pec_event_start_dt']);
        $et=new DateTime($input['pec_event_end_dt']);
		
        $event_start=$st->format('Y-m-d');
        $event_end=$et->format('Y-m-d');

        if($input['pec_event_name'] != null && $input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{
			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
            //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
            $sql = "
            SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
				AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
				AND `pq_event_create`.`pec_event_status` = '1' 
				AND `pq_event_create`.`pec_event_type` = '1' 
              AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
            AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
         ";     

            $eventData=DB::select( $sql);
      					
		    return View::make('reports.close-report')->with('event_data', $eventData);
		}
		elseif ($input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			 SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'				
				AND `pq_event_create`.`pec_event_status` = '1' 
				AND `pq_event_create`.`pec_event_type` = '1' 
                AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
                AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
            ";  
            $eventData=DB::select( $sql);      			
			return View::make('reports.close-report')->with('event_data', $eventData);
			
		}
		elseif($input['pec_event_name'] != null)
		{
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			SELECT 
					*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
				  FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
				    WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
					AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
					AND `pq_event_create`.`pec_event_status` = '1' 
					AND `pq_event_create`.`pec_event_type` = '1' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_create.`pec_event_id`			 
			 ";     

			$eventData=DB::select( $sql);
      					
			return View::make('reports.close-report')->with('event_data', $eventData);		
		}		
		else
		{
            return View::make('reports.close-report')->with('event_data', $eventData = null);
		}	

    }

	public static function highlowreport(Request $request)
	{
		$input = $request->all();
	   
        $event_name = $input['pec_event_name'];
        $logID = $input['loginid'];

        $st=new DateTime($input['pec_event_start_dt']);
        $et=new DateTime($input['pec_event_end_dt']);
		
        $event_start=$st->format('Y-m-d');
        $event_end=$et->format('Y-m-d');

        // $json_date='{"email" : "'.$logID.'","accept": "1"}';
        // $sql = "
        // SELECT 
        //         *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
        //       FROM
        //         `pq_event_create` 
        //       JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id` 
        //       WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
        //         AND `pq_event_create`.`pec_event_status` = '1'
        //         AND `pq_event_create`.`pec_event_name` LIKE '%".$eventName."%'
        //         AND (`pq_event_create`.`pec_event_start_dt` <= NOW() AND `pq_event_create`.`pec_event_end_dt` <= NOW())
        //         GROUP BY pq_event_create.`pec_event_id` 
        //  ";
     

        // $eventData=DB::select( $sql);

        // return View::make('reports.buyer-report')->with('event_data', $eventData);   
        
        if($input['pec_event_name'] != null && $input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{
			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
            //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
            $sql = "
            SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
				AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
                AND `pq_event_create`.`pec_event_status` = '1' 
              AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
            AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
         ";     

            $eventData=DB::select( $sql);
      					
		    return View::make('reports.highlow-report')->with('event_data', $eventData);
		}
		elseif ($input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			 SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'				
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
                AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
            ";  
            $eventData=DB::select( $sql);      			
			return View::make('reports.highlow-report')->with('event_data', $eventData);
			
		}
		elseif($input['pec_event_name'] != null)
		{	
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			SELECT 
					*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
				  FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
				    WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
					AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
                    AND `pq_event_create`.`pec_event_status` = '1' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_create.`pec_event_id`			 
			 ";     

			$eventData=DB::select( $sql);
      					
			return View::make('reports.highlow-report')->with('event_data', $eventData);		
		}		
		else
		{
            return View::make('reports.highlow-report')->with('event_data', $eventData = null);
		}	

    }

	//pdf lot bid detail for highest/lowest report
	public static function lothighlowreport(Request $request)
    {
        $input = $request->all();
	   
        $lotId = $input['hfLotId'];  
        $eventId = $input['hfEventId'];        

        $columns = isset($input['biddetail']) ? $input['biddetail'] : null; 
        
        $address_flag = 0;
        $phone_flag = 0;
        $date_flag = 0;  
        $status_flag = 0;  

        if(isset($columns))
        {
            foreach($columns as $key => $value)
            {
                if($value == 3)
                {
                    $address_flag = 1;
                }
                else if($value == 4)
                {
                    $phone_flag = 1;
                }
                else if($value == 6)
                {
                    $date_flag = 1;
                }
                else if($value == 7)
                {
                    $status_flag = 1;
                }
            }
        }

        if($input['SUBMIT'] == 'Excel')
        {
            return (new HighLowReportExport($lotId, $address_flag, $phone_flag, $date_flag, $status_flag))->download('Bid Report.xlsx');
        }
        else
        {          
            // instantiate and use the dompdf class
            $dompdf = new Dompdf();

            $items = DB::table('pq_bid_history')
						->select('pbh_lotid', 'pq_company_info.pci_comp_name', 'pq_company_info.pci_comp_address', 'pq_company_info.pci_comp_phone',
						'pbh_cur_bid', 'pbh_cur_bid_datetime', 
                            DB::raw('(CASE WHEN pbh_auction_status= "1" THEN "Win" WHEN pbh_auction_status= "2" THEN "Lose" WHEN pbh_auction_status= "3" THEN "Bidding" ELSE "Pending" END) AS auction_status'))    
                        ->Join('pq_company_info', 'pq_bid_history.pbh_seller_compid' , '=', 'pq_company_info.pci_comp_id')
                        ->where('pbh_lotid', $lotId)
                        ->orderBy('pbh_id', 'desc')
                        ->first();

            $event = DB::table('pq_event_additem')
                        ->select('pq_event_additem.*', 'pq_master_market.pmm_market_name', 'pq_master_cat_new.pmca_cat_name', 'pq_master_subcat_new.pms_subcat_name')
                        ->Join('pq_master_market', 'pq_event_additem.pea_event_market' , '=', 'pq_master_market.pmm_id')
                        ->Join('pq_master_cat_new', 'pq_event_additem.pea_event_cat' , '=', 'pq_master_cat_new.pmca_id')
                        ->Join('pq_master_subcat_new', 'pq_event_additem.pea_event_subcat' , '=', 'pq_master_subcat_new.pms_id')
                        ->where('pea_id', $lotId)
                        ->first();

            $eventDetail = DB::table('pq_event_create')
                        ->select('*')
                        ->where('pec_event_id', $eventId)
                        ->first();

			$pdfData = "";     
			
			if($items)
			{
                $pdfData .= '<tr>
                            <th scope="row">'.$items->pbh_lotid.'</th>
                            <td>'.$items->pci_comp_name.'</td>';

                if($address_flag == 1)
                {
                    $pdfData .= '<td>'.$items->pci_comp_address.'</td>';
                }

                if($phone_flag == 1)
                {
                    $pdfData .= '<td>'.$items->pci_comp_phone.'</td>';
                }

                $pdfData .= '<td>'.$items->pbh_cur_bid.'</td>';

                if($date_flag == 1)
                {
                    $pdfData .= '<td>'.$items->pbh_cur_bid_datetime.'</td>';
                }

                if($status_flag == 1)
                {
                    $pdfData .= '<td>'.$items->auction_status.'</td>';
                }

                $pdfData .= '</tr>';
			}
			else
			{
				$pdfData .= '<tr>
							<td class="text-center">No Records Found</td>
						</tr>';
            } 

            $pdfHeader = '<tr>';         
            $pdfHeader .= '<th>Item Id</th>';
            $pdfHeader .= '<th>Seller Company Name</th>';

            if($address_flag == 1)
            {
                $pdfHeader .= '<th> Company Address </th>'; 
            }

            if($phone_flag == 1)
            {
                $pdfHeader .= '<th>Phone Number</th>'; 
            }

            $pdfHeader .= '<th>Bid Amount</th>';

            if($date_flag == 1)
            {
                $pdfHeader .= '<th>Bid Date</th>';
            }

            if($status_flag == 1)
            {
                $pdfHeader .= '<th>Auction Status</th>';
            }

            if($eventDetail->pec_event_type == 2)
            {
                $reserve_price = $event->pea_event_start_price;
                $min_diff = $event->pea_event_max_dec;
                $floor_price = $event->pea_event_reserve_price;
                $event_type = 'Reverse Bid Auction';
            }
            else
            {
                $reserve_price = 'Not Applicable';
                $min_diff = 'Not Applicable';
                $floor_price = 'Not Applicable';
                $event_type = 'Closed Bid Auction';
            }

            $pdfHeader .= '</tr>';            

            $dompdf->loadHtml('
            <style>
                footer {
                    position: fixed; 
                    bottom: -60px; 
                    left: 0px; 
                    right: 0px;
                    height: 50px; 

                    /** Extra personal styles **/
                    border-top: 2px solid #D3D3D3;
                    color:black;
                    text-align: center;
                    line-height: 35px;
                }
            </style>
            <body>
                <footer>
                    Copyright &copy; 2019 Simplified Procurement Solutions(OPC) Pvt Ltd. All Rights Reserved.
                </footer> 
                <table width="100%">
                    <tr>                        
                        <td> <img src="C:/xampp/htdocs/PQ/public/new-img/logo.png" /> </td>    
                        <td align="right">
                            <h3>Simplified Procurement Solutions(OPC) Pvt Ltd</h3>
                            <p>
                                Chamier Towers, 37/6 1st floor, Pasumpon Muthuramalinga Thevar Rd,
                                <br> 
                                Teynampet, Chennai, Tamil Nadu 600028
                                <br>
                                044 4343 7474
                            </p>
                        </td>
                    </tr>
                </table>
                <br>

                <h4>EVENT DETAILS</h4>
                <table width="100%" style="border:1px lightgray solid">                    
                    <tr>
                        <td><strong>Event Id:</strong> '.$eventDetail->pec_event_id.' </td>
                        <td><strong>Event Name:</strong> '.$eventDetail->pec_event_name.'</td>   
                        <td><strong>Start Date:</strong> '.$eventDetail->pec_event_start_dt.'</td>                     
                    </tr>
                    <tr>  
                        <td><strong>End Date:</strong> '.$eventDetail->pec_event_end_dt.'</td>
                        <td><strong>Event Type:</strong> '.$event_type.'</td>
                    </tr>
                </table>
                <br>

                <h4>ITEM DETAILS</h4>
                <table width="100%" style="border:1px lightgray solid;">                    
                    <tr>
                        <td><strong>Market Name :</strong> '.$event->pmm_market_name.' </td>
                        <td><strong>Category Name :</strong> '.$event->pmca_cat_name.' </td>
                        <td><strong>Sub Category Name :</strong> '.$event->pms_subcat_name.' </td>                                 
                    </tr>                        
                    <tr>       
                        <td><strong>Spec :</strong> '.$event->pea_event_spec.'</td>                             
                        <td><strong>Start Date :</strong> '.$event->pea_event_start_dt.'</td>   
                        <td><strong>End Date </strong> '.$event->pea_event_end_dt.'</td>                             
                    </tr>                    
                    <tr>
                        <td><strong>Reserve Price :</strong> '.$reserve_price.'</td>                  
                        <td><strong>Min Decrement :</strong> '.$min_diff.'</td>
                        <td><strong>Floor Price :</strong> '.$floor_price.'</td>                        
                    </tr>    
                    <tr>  
                        <td><strong>Location :</strong> '.$event->pea_event_location.'</td>       
                    </tr>           
                </table>
                <br/>

                <h4>BID DETAILS</h4>
                <table width="100%">
                    <thead style="background-color: lightgray;">                        
                        '.$pdfHeader.'
                    </thead>
                    <tbody>                        
                        '.$pdfData.'                        
                    </tbody>                
                </table>

            </body>
            ');

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A4', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream('Bid Report', array("Attachment" => false));

            exit(0);
        }
	} 
	
	//company dropdown
	public static function getSellerCompanyInfo($logId)
	{
		$data= DB::table('pq_login_info')
				->select('pq_company_info.*')
				->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_login_info.pli_comp_id')
				->where('pq_login_info.pli_loginid', '!=' ,$logId)
				//->where('pq_login_info.pli_login_type', '>', 1)
				->get();
				
		return $data;
    }

    public static function getEventNames($logId)
	{
		$data= DB::table('pq_event_create')
				->select('pq_event_create.*')
                ->where('pq_event_create.pec_event_loginid', '=' ,$logId)
                ->where('pq_event_create.pec_event_end_dt', '<=', NOW())
                ->orderBy('pec_sno','desc')
				->get();
				
		return $data;
    }

    public function eventLotData(Request $request)
    {
        $input = $request->all();
        $session = session()->all();
		
        $UserId = $session['pli_loginId'];

        try{

            $eventId = $input['event_id'];

            $data= DB::table('pq_event_additem')
                    ->select('pq_event_additem.*')
                    ->where('pq_event_additem.pea_event_id', '=' ,$eventId)
                    ->where('pq_event_additem.pea_event_end_dt', '<=', NOW())
                    ->get();

            if(isset($data))
            {
                $result = array(
                    'status' => true,
                    'message' => $data,
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Not Found',
                    'c_code'  =>2	 			
               
                );
            }

        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2	 			
           
            );
        }

        return response()->json($result);
    }

    public function sellerEventData(Request $request)
    {
        $input = $request->all();
        $session = session()->all();
		
        $UserId = $session['pli_loginId'];

        try{

            $seller_id = $input['comp_id'];

            $json_date='{"email" : "'.$UserId.'","accept": "1"}';
			$sql = "
			SELECT 
					*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
				  FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
                    JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`                  
                    WHERE `pq_event_create`.`pec_event_loginid` = '".$UserId."'
                    AND `pq_event_create`.`pec_event_status` = '1' 
                    AND `pq_live_auction`.`pla_seller_compid` = '".$seller_id."' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_create.`pec_event_id`			 
			 ";     

            $eventData=DB::select( $sql);

            if(isset($eventData))
            {
                $result = array(
                    'status' => true,
                    'message' => $eventData,
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Not Found',
                    'c_code'  =>2	 			
               
                );
            }

        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2	 			
           
            );
        }

        return response()->json($result);
    }
    
    public function bidwisereport(Request $request)
    {
        $input = $request->all();
	   
        $event_id = $input['bid_pec_event_name'];
        $lot_id = $input['bid_pec_lot_name'];

        $logID = $input['loginid'];
        
        $lotData= DB::table('pq_event_additem')
                    ->select('pq_event_additem.*')
                    ->where('pq_event_additem.pea_event_id', '=' ,$event_id)
                    ->where('pq_event_additem.pea_event_end_dt', '<=', NOW())
                    ->get();

        $eventTypeData= DB::table('pq_event_create')
                    ->select('pq_event_create.*')
                    ->where('pec_event_id', '=' ,$event_id)
                    ->first();
        $event_type = $eventTypeData->pec_event_type;
                    
		if($lot_id > 0)
		{
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			SELECT 
                *,pq_event_additem.*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item,  
                'pq_master_cat_new.pmca_id',
                'pq_master_cat_new.pmca_cat_name',
                'pq_master_subcat_new.pms_id',
                'pq_master_subcat_new.pms_subcat_name',
                'pq_master_unit.pmu_id',
                'pq_master_unit.pmu_unit_name',
                'pq_master_market.pmm_market_name',
                'pq_event_invite_detail.*'
                FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
                    JOIN `pq_master_unit` ON `pq_master_unit`.`pmu_id`=pq_event_additem.`pea_event_unit`
                    JOIN `pq_master_subcat_new` ON `pq_master_subcat_new`.`pms_id`=pq_event_additem.`pea_event_subcat`
                    JOIN `pq_master_cat_new` ON `pq_master_cat_new`.`pmca_id`=pq_event_additem.`pea_event_cat`
                    JOIN `pq_master_market` ON `pq_master_market`.`pmm_id`=pq_event_additem.`pea_event_market`
                    LEFT JOIN `pq_event_invite_detail` ON `pq_event_invite_detail`.`pei_event_additem_id`=pq_event_additem.`pea_id`
				    WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
					AND `pq_event_additem`.`pea_id` = '".$lot_id."'
					AND `pq_event_create`.`pec_event_status` = '1' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_additem.`pea_id`		 
			 ";     

            $eventData=DB::select( $sql);
            
            $data = [
                'event_data'  => $eventData,
                'event_id' => $event_id,
                'lot_id' => $lot_id,
                'lot_data' => $lotData,
                'event_type' => $event_type
            ];
            
      					
			return View::make('reports.bidwise-report')->with('event_data', $data);		
        }	
        else if($lot_id == 0)
        {
            $json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			SELECT 
            *,pq_event_additem.*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item,  
            'pq_master_cat_new.pmca_id',
            'pq_master_cat_new.pmca_cat_name',
            'pq_master_subcat_new.pms_id',
            'pq_master_subcat_new.pms_subcat_name',
            'pq_master_unit.pmu_id',
            'pq_master_unit.pmu_unit_name',
            'pq_master_market.pmm_market_name',
            'pq_event_invite_detail.*'
				  FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
                    JOIN `pq_master_unit` ON `pq_master_unit`.`pmu_id`=pq_event_additem.`pea_event_unit`
                    JOIN `pq_master_subcat_new` ON `pq_master_subcat_new`.`pms_id`=pq_event_additem.`pea_event_subcat`
                    JOIN `pq_master_cat_new` ON `pq_master_cat_new`.`pmca_id`=pq_event_additem.`pea_event_cat`
                    JOIN `pq_master_market` ON `pq_master_market`.`pmm_id`=pq_event_additem.`pea_event_market`
                    LEFT JOIN `pq_event_invite_detail` ON `pq_event_invite_detail`.`pei_event_additem_id`=pq_event_additem.`pea_id`
				    WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
					AND `pq_event_additem`.`pea_event_id` = '".$event_id."'
					AND `pq_event_create`.`pec_event_status` = '1' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_additem.`pea_id`			 
			 ";     

            $eventData=DB::select( $sql);
            
            $data = [
                'event_data'  => $eventData,
                'event_id' => $event_id,
                'lot_id' => $lot_id,
                'lot_data' => $lotData,
                'event_type' => $event_type
            ];
      					
			return View::make('reports.bidwise-report')->with('event_data', $data);
        }
		else
		{
            return View::make('reports.bidwise-report')->with('event_data', $eventData = null);
		}	
    }

    public function bidreport($id)
    {
        $session = session()->all();
		
        $UserId = $session['pli_loginId'];

        $history = DB::table('pq_bid_history')
                        ->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_bid_history.pbh_seller_compid')
                        ->where('pbh_lotid', $id)
                        ->get();

        $label = array();
        $values = array();

        foreach($history as $val)
        {            
            array_push($label, $val->pci_comp_name.'<br>Seller<br>'.$val->pbh_cur_bid_datetime);            
            
            array_push($values, $val->pbh_cur_bid);
        }

        $line  = Charts::create('line', 'highcharts')
                    ->title('Bid History')
                    ->elementLabel('Lot Bid History')
                    ->labels($label)
                    ->values($values)
                    //->dimensions(500,500)
                    ->responsive(false);

        return view('charts.chart', compact('line'));

    }

    public static function sellerreport(Request $request)
	{
		$input = $request->all();
	   
        $event_name = $input['pec_event_name'];
        $logID = $input['loginid'];

        $seller_company_id = $input['pec_company_name'];

        $st=new DateTime($input['pec_event_start_dt']);
        $et=new DateTime($input['pec_event_end_dt']);
		
        $event_start=$st->format('Y-m-d');
        $event_end=$et->format('Y-m-d');
        
        if($input['pec_event_name'] != null && $input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
            //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
            $sql = "
            SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
                JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
                JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
				AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND `pq_live_auction`.`pla_seller_compid` = '".$seller_company_id."' 
              AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
            AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
         ";     

            $eventData=DB::select( $sql);
      					
		    return View::make('reports.sellerwise-report')->with('event_data', $eventData);
		}
		elseif ($input['pec_event_start_dt'] != null && $input['pec_event_end_dt'] != null )
		{			
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			 SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
                JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
                JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`                  
               WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'				
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND `pq_live_auction`.`pla_seller_compid` = '".$seller_company_id."'
                AND `pq_event_create`.`pec_event_start_dt` >= '".$event_start."'
                AND `pq_event_create`.`pec_event_end_dt` <= '".$event_end."'
            ";  
            $eventData=DB::select( $sql);      			

			return View::make('reports.sellerwise-report')->with('event_data', $eventData);
			
		}
		elseif($input['pec_event_name'] != null)
		{	
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			SELECT 
					*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
				  FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
                    JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`                  
                    WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
					AND `pq_event_create`.`pec_event_name` LIKE '%".$event_name."%'
                    AND `pq_event_create`.`pec_event_status` = '1' 
                    AND `pq_live_auction`.`pla_seller_compid` = '".$seller_company_id."' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_create.`pec_event_id`			 
			 ";     

            $eventData=DB::select( $sql);
                  					
			return View::make('reports.sellerwise-report')->with('event_data', $eventData);		
		}		
		else
		{
            return View::make('reports.sellerwise-report')->with('event_data', $eventData = null);
		}	

    }

    public static function sellerwisereport(Request $request)
	{
		$input = $request->all();
	   
        $event_name = $input['pec_event_name'];
        $logID = $input['loginid'];

        $seller_company_id = $input['pec_company_name'];

        $companyInfo= DB::table('pq_event_create')
				->select('pq_event_create.*')
                ->Join('pq_event_additem', 'pq_event_additem.pea_event_id' , '=', 'pq_event_create.pec_event_id')
                ->Join('pq_live_auction', 'pq_live_auction.pla_lotid' , '=', 'pq_event_additem.pea_id')
                ->where('pq_live_auction.pla_seller_compid', '=' ,$seller_company_id)
                ->where('pq_event_create.pec_event_end_dt', '<=' ,NOW())
                ->orWhere('pq_event_additem.pea_event_end_dt', '<=' ,NOW())
                ->groupBy('pq_event_create.pec_event_id')
				->get();
       
		if($event_name > 0)
		{	
			$json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			SELECT 
					*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
				  FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
                    JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`                  
                    WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
					AND `pq_event_create`.`pec_sno` = '".$event_name."'
                    AND `pq_event_create`.`pec_event_status` = '1' 
                    AND `pq_live_auction`.`pla_seller_compid` = '".$seller_company_id."' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_create.`pec_event_id`			 
			 ";     

            $eventData=DB::select( $sql);

            $data = [
                'event_data'  => $eventData,
                'event_id' => $event_name,
                'seller_id' => $seller_company_id,
                'company_event_data' => $companyInfo
            ];
                  					
			return View::make('reports.seller-wise-report')->with('event_data', $data);		
		}		
		else
		{
            $json_date='{"email" : "'.$logID.'","accept": "1"}';
			$sql = "
			SELECT 
					*,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
				  FROM
                    `pq_event_create` 
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_event_id`=pq_event_create.`pec_event_id`
                    JOIN `pq_live_auction` ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`                  
                    WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                    AND `pq_event_create`.`pec_event_status` = '1' 
                    AND `pq_live_auction`.`pla_seller_compid` = '".$seller_company_id."' 
                    AND (`pq_event_create`.`pec_event_end_dt` <= NOW() OR `pq_event_additem`.`pea_event_end_dt` <= NOW())
                    GROUP BY pq_event_create.`pec_event_id`			 
			 ";     

            $eventData=DB::select( $sql);

            $data = [
                'event_data'  => $eventData,
                'event_id' => $event_name,
                'seller_id' => $seller_company_id,
                'company_event_data' => $companyInfo
            ];
                  					
			return View::make('reports.seller-wise-report')->with('event_data', $data);	
		}	

    }

    //pdf lot bid detail for sellerwise report
	public static function lotsellerwisereport(Request $request)
    {
        $input = $request->all();
	   
        $lotId = $input['hfLotId'];        
        $sellerId = $input['hfSellerId'];

        if($input['SUBMIT'] == 'Excel')
        {
            return (new SellerWiseReportExport($lotId, $sellerId))->download('Bid Report.xlsx');
        }
        else
        {          
            // instantiate and use the dompdf class
            $dompdf = new Dompdf();

            $items = DB::table('pq_bid_history')
                        ->select('pbh_lotid', 'pq_company_info.pci_comp_name', 'pbh_cur_bid', 'pbh_cur_bid_datetime', 
                            DB::raw('(CASE WHEN pbh_auction_status= "1" THEN "Win" WHEN pbh_auction_status= "2" THEN "Lose" WHEN pbh_auction_status= "3" THEN "Bidding" ELSE "Pending" END) AS auction_status'))    
                        ->Join('pq_company_info', 'pq_bid_history.pbh_seller_compid' , '=', 'pq_company_info.pci_comp_id')
                        ->where('pbh_lotid', $lotId)
                        ->where('pbh_seller_compid', $sellerId)
                        ->orderBy('pbh_id', 'desc')
                        ->get();

            $event = DB::table('pq_event_additem')
                        ->select('*')
                        ->where('pea_id', $lotId)
                        ->first();

            $pdfData = "";
            
            foreach($items as $key => $values)
            {
                $pdfData .= '<tr>
                            <th scope="row">'.$values->pbh_lotid.'</th>
                            <td>'.$values->pci_comp_name.'</td>
                            <td>'.$values->pbh_cur_bid.'</td>
                            <td>'.$values->pbh_cur_bid_datetime.'</td>
                            <td>'.$values->auction_status.'</td>
                        </tr>';  
            }

            $dompdf->loadHtml('
            <style>
                footer {
                    position: fixed; 
                    bottom: -60px; 
                    left: 0px; 
                    right: 0px;
                    height: 50px; 

                    /** Extra personal styles **/
                    border-top: 2px solid #D3D3D3;
                    color:black;
                    text-align: center;
                    line-height: 35px;
                }
            </style>
            <body>
                <footer>
                    Copyright &copy; 2019 Simplified Procurement Solutions(OPC) Pvt Ltd. All Rights Reserved.
                </footer> 
                <table width="100%">
                    <tr>               
                        <td> <img src="C:/xampp/htdocs/PQ/public/new-img/logo.png" /> </td>             
                        <td align="right">
                            <h3>Simplified Procurement Solutions(OPC) Pvt Ltd</h3>
                            <p>
                                Chamier Towers, 37/6 1st floor, Pasumpon Muthuramalinga Thevar Rd,
                                <br> 
                                Teynampet, Chennai, Tamil Nadu 600028
                                <br>
                                044 4343 7474
                            </p>
                        </td>
                    </tr>
                </table>
                <br>

                <h4>ITEM DETAILS</h4>
                <table width="100%" style="border:1px lightgray solid">                    
                    <tr>
                        <td><strong>Event Id:</strong> '.$event->pea_event_id.' </td>
                        <td><strong>Spec:</strong> '.$event->pea_event_spec.'</td>
                        <td><strong>Start Date:</strong> '.$event->pea_event_start_dt.'</td>
                    </tr>
                    <tr>                        
                        <td><strong>End Date:</strong> '.$event->pea_event_end_dt.'</td>
                        <td><strong>Reserve Price:</strong> '.$event->pea_event_start_price.'</td>
                        <td><strong>Min Decrement:</strong> '.$event->pea_event_max_dec.'</td>
                    </tr>
                    <tr>
                        <td><strong>Floor Price:</strong> '.$event->pea_event_reserve_price.'</td>
                        <td><strong>Location:</strong> '.$event->pea_event_location.'</td>
                    </tr>
                </table>
                <br/>

                <h4>BID DETAILS</h4>
                <table width="100%">
                    <thead style="background-color: lightgray;">
                        <tr>
                            <th>Item Id</th>
                            <th>Seller Company Name</th>
                            <th>Bid Amount</th>
                            <th>Bid Date</th>
                            <th>Auction Status</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        '.$pdfData.'                        
                    </tbody>                
                </table>

            </body>
            ');

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A4', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream('Bid Report', array("Attachment" => false));

            exit(0);
        }
    }

    public function winnerInvoice(Request $request)
    {
        $input = $request->all();
        
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');
        $session_val = session()->all();
        $datetime		= $dt->format('Y-m-d H:i:s');
        $date			= $dt->format('Y-m-d');
        $year			= $dt->format('Y');

        try{
           
           $auctionID=$input['auctionId'];
           $sellerID=$input['seller_id'];
           $lotItemId=$input['lotItemId'];

           $sql=  "select pq_event_additem.pea_event_unit_quantity,pq_event_create.pec_event_id,`pec_event_name`,`pmc_cat_name`,pec_event_start_dt,pec_event_end_dt,`pms_subcat_name`,`pli_con_name` AS seller_name,`pci_comp_name` AS seller_company,`pla_cur_bid` FROM `pq_event_create`

                 JOIN `pq_event_additem` ON pq_event_additem.`pea_event_id`=pq_event_create.`pec_event_id`
                 
                 JOIN  pq_live_auction ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
                 JOIN pq_login_info ON `pq_login_info`.`pli_loginid` = pq_live_auction.`pla_seller_emailid`
                 JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id` = pq_login_info.`pli_comp_id`
                 JOIN `pq_master_cat` ON `pq_master_cat`.`pmc_id` = pq_event_additem.`pea_event_cat`
                 JOIN `pq_master_subcat` ON `pq_master_subcat`.`pms_id` = pq_event_additem.`pea_event_subcat`
                 WHERE pq_live_auction.`pla_lotid`=".$lotItemId." and pq_login_info.`pli_loginid`='".$sellerID."' ";
      
                 $winner_data= DB::select( $sql);
      

                   $data=$winner_data[0];
                   $start= new DateTime($data->pec_event_start_dt);
                   $end= new DateTime($data->pec_event_end_dt);
                   $start_date=$start->format('d M');
                   $end_date=$end->format('d M');
      
                   $date_format = $start_date.' -'. $end_date.', '.$year;
                   $data 		= array(
                                    'event_id' => $data->pec_event_id,
                                    'quantity' => $data->pea_event_unit_quantity,
                                    'event_name'=>$data->pec_event_name,
                                    'date_format'=>$date_format,
                                    'start_date'=>$data->pec_event_start_dt,
                                    'end_date'=>$data->pec_event_end_dt,
                                    'email'=>$sellerID,
                                    'event_cat_name'=>$data->pmc_cat_name,
                                    'seller_name'=>$data->seller_name,  
                                    'year'=>$year,
                                    'seller_company'=>$data->seller_company,
                                    'seller_bidamount'=>$data->pla_cur_bid,
                                    'date'=>$date
                                );
                   $data_val		= array_merge($data,$session_val);
                
                //winning mail send to seller
                Mail::send('emails.event-winner-invoice',$data_val,function($message) use ($data_val)
                {	
                    $dompdf = new Dompdf();
                   
                    $html =
                    '
                    <!doctype html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <title>Invoice - #123</title>
                    
                        <style type="text/css">
                            @page {
                                margin: 0px;
                            }
                            body {
                                margin: 0px;      
                                background-image: url(images/logo.png);
                                background-color: white;
                                background-position: center;
                                background-repeat: no-repeat;                          
                            }
                            * {
                                font-family: Verdana, Arial, sans-serif;
                            }
                            a {
                                color: #fff;
                                text-decoration: none;
                            }
                            table {
                                font-size: x-small;
                            }
                            tfoot tr td {
                                font-weight: bold;
                                font-size: x-small;
                            }
                            .invoice table {
                                margin: 15px;
                            }
                            .invoice h3 {
                                margin-left: 15px;
                            }
                            .information {
                                background-color: #ff9800;
                                color: #FFF;
                            }
                            .information .logo {
                                margin: 5px;
                            }
                            .information table {
                                padding: 10px;
                            }
                        </style>
                    
                    </head>
                    <body>
                    
                    <div class="information">
                        <table width="100%">
                            <tr>
                                <td align="left" style="width: 40%;">
                                    <h3>Srinivasan</h3>
                                    <pre>
                                        Company Name: '.$data_val['seller_company'].'
                                        <br />
                                        Invoice Date: '.$data_val['date'].'
                                        <br />
                                        Status: Paid
                                    </pre>                  
                    
                                </td>
                                <td align="center">
                                    <img style="background:white" src="C:/xampp/htdocs/PQ/public/new-img/logo.png" alt="Logo" class="logo"/>
                                </td>
                                <td align="right" style="width: 40%;">
                    
                                    <h3>Simplified Procurement Solutions(OPC) Pvt Ltd</h3>
                                    <pre>
                                                            
                                        Chamier Towers, 37/6 1st floor,
                                        Pasumpon Muthuramalinga Thevar Rd, 
                                        Teynampet, Chennai,
                                        Tamil Nadu 600028
                                    </pre>
                                </td>
                            </tr>
                    
                        </table>
                    </div>
                    
                    
                    <br/>
                    
                    <div class="invoice">
                        <h3>Invoice specification</h3>
                        <table width="100%">
                            <thead style="background:black;color:white">
                                <tr>
                                    <th>Event Id</th>
                                    <th>Event Name</th>
                                    <th>Lot Spec</th>
                                    <th>Quantity</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Bid Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr style="background:#f5f5f5">
                                <td>'.$data_val['event_id'].'</td>
                                <td>'.$data_val['event_name'].'</td>
                                <td>'.$data_val['date'].'</td>
                                <td>'.$data_val['quantity'].'</td>
                                <td>'.$data_val['start_date'].'</td>
                                <td>'.$data_val['end_date'].'</td>
                                <td align="left">'.$data_val['seller_bidamount'].'</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                    
                            <tfoot style="background:#f5f5f5">
                            <tr>
                                <td colspan="5"></td>
                                <td align="left">Delivery Charges</td>
                                <td align="left" class="gray"> - </td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td align="left">Total</td>
                                <td align="left" class="gray">'.$data_val['seller_bidamount'].'</td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td align="left">Sub Total</td>
                                <td align="left" class="gray">'.$data_val['seller_bidamount'].'</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                    <div class="information" style="position: absolute; bottom: 0;">
                        <table width="100%">
                            <tr>
                                <td align="left" style="width: 50%;">
                                    Copyright &copy; 2019 Simplified Procurement Solutions(OPC) Pvt Ltd. All Rights Reserved.
                                </td>
                                <td align="right" style="width: 50%;">
                                    System of Trust
                                </td>
                            </tr>
                    
                        </table>
                    </div>
                    </body>
                    </html>
                    ';

                    $dompdf->loadHtml($html);

                    // (Optional) Setup the paper size and orientation
                    $dompdf->setPaper('A4', 'landscape');

                    // Render the HTML as PDF
                    $dompdf->render();

                    $file = $dompdf->output();

                    $message->to($data_val['email'])->subject('Invoice - Purchase Quick');
                    $message->from('support@purchasequick.in','Purchase Quick');            
                    $message->attachData($file, 'invoice.pdf');        
                });

                $result = array(
                    'status' => true,
                    'message' => 'Invoice sent successfully',
                    'c_code'  =>1	 		
                );

        }
        catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
           
            );
        }

        return response()->json($result);
    }
	
}
