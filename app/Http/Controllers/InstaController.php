<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use DateTime;
use Crypt;

class InstaController extends Controller
{
    //

    public function createRequest(Request $request)
    {
        $input = $request->all();
        $order_id = uniqid() . ( string ) $input['pli_loginId'];
        $price = $input['pmp_pkg_amount'];
        $phone = $input['pci_comp_phone'];
        $comp_name = $input['pci_comp_name'];

        $session_val = session()->all(); 		
		session(['package_data' => $input ]);

        $ch = curl_init();

        //testing
        //curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/api/1.1/payment-requests/');

        //live
        curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
                   /* array("X-Api-Key:test_dddff182ff25a04b635de22e553",
                        "X-Auth-Token:test_80eae5c5eec06cd26cfb543e31f")); */ //testing keys
                    array("X-Api-Key:a842dddcc83f15650c3461bf235e12ec",
                        "X-Auth-Token:dfeb19afa0a5b1e5ea11484dcd3128d4"));
        $payload = Array(
            'purpose' => 'Buying Event',
            'amount' => $price,
            //'amount' => 10,
            'phone' => $phone,
            'buyer_name' => $comp_name,
            'redirect_url' => url('/insta-callback'),
            'send_email' => false,
            'webhook' => 'http://instamojo.com/webhook/',
            'send_sms' => false,
            'email' => $input['pli_loginId'],
            'allow_repeated_payments' => false
        );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
        $response = curl_exec($ch);
        curl_close($ch); 
        
        $data = json_decode($response, true);
        $url = $data['payment_request']['longurl'];

        return Redirect::to($url);
    }

    public function InstaCallback( Request $request ) {

        $input = $request->all();

        session(['insta_data' => $input ]);

        if ('Credit' === $request['payment_status']) {

            $session_val = session()->all();
            $this->create_event($session_val);

        }
        else if('Failed' === $request['payment_status'])
        {
            $encrypted1 = Crypt::encryptString("0");
            Redirect::to('/insta-payment-status/fail/'.$encrypted1)->send();
        }
    }

    public function create_event($data)
	{
		$input = $data['package_data'];	
	 
		$dt 	= new DateTime();
		$date	= $dt->format('Y-m-d H:i:s');
		
		$str = preg_replace('/\D/', '', $input['pmp_pkg_validity']);
		
		$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));				
		
        try 
        {
			$package = DB::table('pq_buynow_package')
						->where('pbp_comp_id', '=', $input['pli_comp_id'])
						->where('pbp_pkg_id', '=', $input['pmp_id'])						
						->first();
						
			
			$packagellist = DB::table('pq_master_package')        
							->select('*')
							->where('pmp_id',$input['pmp_id'])
							->get();
			
            if(isset($package->pbp_pkg_cur_count))
            {
				$pack_count=$package->pbp_pkg_cur_count;
            }
            else
            {
				$pack_count=0;
			}
				
            if ((count($package)==0 )|| ($pack_count==0 ) )
            {
                // It does not exist - add to favorites button will show
                    
                    $package_data =	[
                            'pbp_comp_id' 		 	=>  $input['pli_comp_id'],
                            'pbp_login_id'  		=>  $input['pli_loginId'],
                            'pbp_pkg_id'  			=>  $packagellist[0]->pmp_id,
                            'pbp_pkg_eventcount' 	=>  $packagellist[0]->pmp_pkg_eventcount,
                            'pbp_pkg_cur_count'  	=>  $packagellist[0]->pmp_pkg_eventcount,
                            'pbp_pkg_start_dt'  	=>  $date,
                            'pbp_pkg_end_dt'  		=>  $enddate,
                            'pbp_pkg_status'  		=>  '1',                            
                        ];

                        if(($pack_count==0 )&&(count($package)==0 ))
                        {
                            $packageid = DB::table('pq_buynow_package')->insertGetId($package_data);
                        }
                        else
                        {                            
                            $packageid = DB::table('pq_buynow_package')
                                            ->where('pbp_id', '=', $package->pbp_id)
                                            ->update($package_data);
                        }					
                    
                    $encrypted = Crypt::encryptString($packageid);

                    if($packageid)
                    {								
                        Redirect::to('/insta-payment-status/success/'.$encrypted)->send();
                        
                    }

            } else {
                // It exists - remove from favorites button will show			
                    $encrypted1 = Crypt::encryptString("0");
                    Redirect::to('/insta-payment-status/fail/'.$encrypted1)->send();         
            }       
        }catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),	
                'c_code'  =>2
            );
        }

	}

    public function InstaWebhook( Request $request ) {

        $input = $request->all();
        
        print_r($input);
        echo 'hiiihello';
        die;


    }
}
