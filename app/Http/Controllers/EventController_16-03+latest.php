<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Mail\reminder;
use Validator;
use App\EmailChecker;
use Mail;  
use App\GetListuser;
use Illuminate\Support\Facades\Cache;
use DateTime;
use Exception;
use URL;
use Hash;
use Crypt;
use Session;
use File;
use App\GetOsDetails;
use App\purchaseInvite;

class EventController extends Controller

{
	
	public function createNewEvent(Request $request)
	{
		$input = $request->all();
	   
        $packageID=$input['package_id'];
        $loginid=$input['loginid'];
        $event_name=$input['pec_event_name'];
        $event_type=$input['pec_event_type'];
       // $event_start=new DateTime($input['pec_event_start_dt']);
       // $event_end=$input['pec_event_end_dt'];
      
        
      //  print_r( $eventid);die;
        
        $dt 	= new DateTime();
        $date=$dt->format('Y-m-d H:i:s');;
        $st=new DateTime($input['pec_event_start_dt']);
        $et=new DateTime($input['pec_event_end_dt']);
        $event_start=$st->format('Y-m-d H:i:s');
        $event_end=$et->format('Y-m-d H:i:s');
		
	//	$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));
				
	  try{
		 
            $Pkg_curCount=$this->eventPackageBalance($packageID,$loginid);
            $current_count=$Pkg_curCount->pbp_pkg_cur_count;
            $pkg_buyID=$Pkg_curCount->pbp_id;
          //  print_r($Pkg_curCount);die;

          if($current_count>0){

          
            $eventid=$this->generateUid($loginid);

            $EventInsertData =	[
                'pec_event_id' 		 	=>  $eventid,
                'pec_event_name'  		=>  $event_name,
                'pec_event_start_dt'  	=>  $event_start,
                'pec_event_type' 	    =>  $event_type,
                'pec_event_end_dt' 	    =>  $event_end,
                'pec_event_loginid'  	=>  $loginid,
                'pec_event_pkg_id'  	=>  $packageID,
                'pec_event_pkg_id'  	=>  $packageID,
                'pec_buynow_pkg_id'     =>  $pkg_buyID,
                'pec_event_create_dt'  	=>  $date
          
                
            ];

           // print_r( $EventInsertData);die;
         
             $eventInsert = DB::table('pq_event_create')->insert($EventInsertData);


            if( $eventInsert){


                // update event count minus current count from pq_buynow_package table
                $update_array= [
                    'pbp_pkg_cur_count' =>   $current_count - 1 ,
                                         
                ];
                $updateData= DB::table('pq_buynow_package')
                ->where('pbp_pkg_id', $packageID)
                ->update($update_array);

                $result = array(
                    'status' => true,
                    'message' => 'Event Created'

                );
            }else{
                $result = array(
                    'status' => false,
                    'message' => 'Some went Wrong'               
                );

            }
             

            //print_r( $eventid);die;
          }else{
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
           
            );
          }
			
			
			//echo $str;
			//print_r(count($package));die;
					
       
       // print_r($insertData);die;
        }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);
    }
    
    public static function eventPackageBalance($packageID,$loginID){

       
        $current_pkg_count = DB::table('pq_buynow_package')
        
        ->select('*')
        ->where('pq_buynow_package.pbp_login_id',$loginID)
        ->where('pq_buynow_package.pbp_pkg_id',$packageID)
        ->first();
    //  print_r( $current_pkg_count );die;
        return $current_pkg_count;

    }

    
    public function generateUid($logID){
        $dt = new DateTime();
        $date= $dt->format('ymd');
        $com_type= $this->getCompanyType($logID);
       
        $orderObj = DB::table('pq_event_create')->select('pec_event_id')->latest('pec_sno')->first();
        if ($orderObj) {
            $orderNr = $orderObj->pec_event_id;
            $removed1char = substr($orderNr, 9);
            $dateformat=str_pad($removed1char + 1, 4, "0", STR_PAD_LEFT);

          
            $uid=$date. $dateformat;
         
           if($com_type->company_type == '1'){
            $generateOrder_nr = 'PQP'.$uid;
           }elseif($com_type->company_type == '2'){
            $generateOrder_nr = 'PQS'.$uid;
           }
           elseif($com_type->company_type == '3'){
            $generateOrder_nr = 'PQB'.$uid;
           }
           
           
        } else {

            if($com_type->company_type == '1'){
                $generateOrder_nr = 'PQP'.$date . str_pad(1, 4, "0", STR_PAD_LEFT);
               }elseif($com_type->company_type == '2'){
                $generateOrder_nr = 'PQS'.$date . str_pad(1, 4, "0", STR_PAD_LEFT);
               }
               elseif($com_type->company_type == '3'){
                $generateOrder_nr = 'PQB'.$date . str_pad(1, 4, "0", STR_PAD_LEFT);
               }
               
            
        }
     // print_r();

        return $generateOrder_nr;
      //  return $orderObj;
    }

    public static function getCompanyType($logID)
	{
        $company_type = DB::table('pq_login_info')
        ->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_login_info.pli_comp_id')
        ->select('pci_comp_type_cat as company_type')
        ->where('pq_login_info.pli_loginid',$logID)
        ->first();
      
        return $company_type;
    }
    public static function getEventData($logID)
	{
        $eventData = DB::table('pq_event_create')
        ->Join('pq_master_package', 'pq_master_package.pmp_id' , '=', 'pq_event_create.pec_event_pkg_id')
        ->Join('pq_buynow_package', 'pq_buynow_package.pbp_id' , '=', 'pq_event_create.pec_buynow_pkg_id')
        ->select('*')
        ->where('pq_event_create.pec_event_loginid',$logID)
        ->where('pq_event_create.pec_event_status','0')
        ->where('pq_event_create.pec_event_start_dt', '>', NOW())
        ->orderBy('pq_event_create.pec_sno','DESC')
        ->get();

        return $eventData;
    }

    public static function upcomingEvent($logID)
	{

        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select 
        *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
      FROM
        `pq_event_create` 
      WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
        AND `pq_event_create`.`pec_event_status` = '1' 
        AND `pq_event_create`.`pec_event_start_dt` >= NOW()";

        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function sellerUpcomingEvent($logID)
	{
        $json_date='{"email" : "'.$logID.'"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select pq_event_create.*,pq_company_info.`pci_comp_name` FROM pq_event_invite_detail
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
         WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         
         AND `pq_event_create`.`pec_event_start_dt` >= NOW() AND `pq_event_create`.`pec_event_end_dt` >= NOW()
         GROUP BY pq_event_create.`pec_event_id`  order by `pq_event_create`.`pec_event_start_dt` ASC
         ";
  

        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function seller_Completed_Aution($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select pq_event_create.*,pq_company_info.`pci_comp_name` FROM pq_event_invite_detail
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
        JOIN `pq_live_auction` ON  pq_live_auction.`pla_seller_emailid`='".$logID."'
         WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         AND `pq_event_create`.`pec_event_status` = '1' 
         AND   `pq_event_create`.`pec_event_end_dt` <= NOW()
         GROUP BY pq_event_create.`pec_event_id`  order by  `pq_event_create`.`pec_event_end_dt` DESC
         ";
        $completed=DB::select( $sql);
        return $completed;

    }

    public static function sellerLiveEvent($logID)
	{
       // $json_date='{"email" : "'.$logID.'","accept": "1"}'; changes given by AR 23.11.2018
       $json_date='{"email" : "'.$logID.'"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "select pq_event_create.*,pq_company_info.`pci_comp_name` FROM pq_event_invite_detail
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
         WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         AND `pq_event_create`.`pec_event_status` = '1' 
         AND  `pq_event_create`.`pec_event_start_dt` <= NOW() AND `pq_event_create`.`pec_event_end_dt` >= NOW()
         GROUP BY pq_event_create.`pec_event_id`  order by `pq_event_create`.`pec_event_end_dt` ASC
         ";
       

        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function buyerLiveEvent($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
          AND `pq_event_create`.`pec_event_end_dt` >= NOW() 
         ";
     

        $eventData=DB::select( $sql);
        return $eventData;
    }

    public static function buyerCompletedEvent($logID)
	{
        $json_date='{"email" : "'.$logID.'","accept": "1"}';
        //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
        $sql = "
        SELECT 
                *,(SELECT COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item  
              FROM
                `pq_event_create` 
              WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
                AND `pq_event_create`.`pec_event_status` = '1' 
                AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
          AND `pq_event_create`.`pec_event_end_dt` <= NOW() 
         ";
     

        $eventData=DB::select( $sql);
        return $eventData;
    }

    
    public static function buyerCompletedEventMIS($logID)
	{
        $sql="select 
       * 
       FROM
         `pq_event_additem` 
         INNER JOIN `pq_master_cat` 
           ON `pq_master_cat`.`pmc_id` = `pq_event_additem`.`pea_event_cat` 
         LEFT JOIN `pq_event_invite_detail` 
           ON `pq_event_invite_detail`.`pei_event_additem_id` = `pq_event_additem`.`pea_id` 
         INNER JOIN `pq_master_subcat` 
           ON `pq_master_subcat`.`pms_id` = `pq_event_additem`.`pea_event_subcat` 
         INNER JOIN `pq_master_unit` 
           ON `pq_master_unit`.`pmu_id` = `pq_event_additem`.`pea_event_unit` 
           INNER JOIN `pq_event_create` 
           ON `pq_event_create`.`pec_event_id` = `pq_event_additem`.`pea_event_id`
             LEFT JOIN `pq_live_auction` 
           ON pq_live_auction.`pla_lotid`=pq_event_additem.`pea_id`
           LEFT JOIN `pq_company_info` 
    ON `pq_company_info`.`pci_comp_id` = `pq_live_auction`.`pla_seller_compid`   
           
       WHERE `pq_event_create`.`pec_event_loginid` = '".$logID."'
         AND `pq_event_create`.`pec_event_status` = '1' 
         AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
         AND `pq_event_create`.`pec_event_end_dt` <= NOW()";
     

        $eventData=DB::select( $sql);
        return $eventData;
    }
    public static function getSellerInviteItem($eventID,$logID){

        $accept_status='{"email" : "'.$logID.'" ,"accept": "1"}';
        $reject_status='{"email" : "'.$logID.'" ,"reject": "1"}';
        $json_date='{"email" : "'.$logID.'"}';
        $sql = " select 
                    * ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$accept_status."'
                    ) as accept
                    ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$reject_status."'
                    ) as reject
                    FROM
                    pq_event_invite_detail 
                    
                    JOIN `pq_event_additem` 
                        ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
                    JOIN `pq_event_create` 
                        ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
                    LEFT JOIN  `pq_live_auction` 
                        ON `pq_live_auction`.`pla_lotid` = pq_event_additem.`pea_id`  and pq_live_auction.pla_seller_emailid='".$logID."'
                    JOIN `pq_master_cat` 
                        ON `pq_master_cat`.`pmc_id` = pq_event_additem.`pea_event_cat` 
                    JOIN `pq_master_subcat` 
                        ON `pq_master_subcat`.`pms_id` = pq_event_additem.`pea_event_subcat` 
                    JOIN `pq_master_unit` 
                        ON `pq_master_unit`.`pmu_id` = pq_event_additem.`pea_event_unit` 
                    WHERE json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$json_date."'
                    ) AND pq_event_additem.`pea_event_id`='".$eventID."'";

   $eventData=DB::select( $sql);
   return $eventData;
    }

    public static function getSellerMisReport($logID){

        $accept_status='{"email" : "'.$logID.'" ,"accept": "1"}';
        $reject_status='{"email" : "'.$logID.'" ,"reject": "1"}';
        $json_date='{"email" : "'.$logID.'"}';
        $sql = " select 
                    * ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$accept_status."'
                    ) as accept
                    ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$reject_status."'
                    ) as reject
                    FROM
                    pq_event_invite_detail 
                    
                    JOIN `pq_event_additem` 
                        ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
                    JOIN `pq_event_create` 
                        ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
                    LEFT JOIN  `pq_live_auction` 
                        ON `pq_live_auction`.`pla_lotid` = pq_event_additem.`pea_id`  and pq_live_auction.pla_seller_emailid='".$logID."'
                    JOIN `pq_master_cat` 
                        ON `pq_master_cat`.`pmc_id` = pq_event_additem.`pea_event_cat` 
                    JOIN `pq_master_subcat` 
                        ON `pq_master_subcat`.`pms_id` = pq_event_additem.`pea_event_subcat` 
                    JOIN `pq_master_unit` 
                        ON `pq_master_unit`.`pmu_id` = pq_event_additem.`pea_event_unit` 
                    WHERE json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$json_date."'
                    ) ";

   $eventData=DB::select( $sql);
   return $eventData;
    }

    public static function getCurrentBitAmount($lotID){
        $sql='select MIN(pq_live_auction.`pla_cur_bid`) as cur_bid FROM `pq_live_auction`
    WHERE pq_live_auction.`pla_lotid`='.$lotID;
    $eventData=DB::select( $sql);
    return $eventData;
    }
    public function updatebidData(Request $request){

        $session_val = session()->all();
        $input=array();
        try
        { 
           // $input = $request->all();	
         if(isset($session_val['pli_loginId'])){
           $input['email']=$session_val['pli_loginId'];		  
         }else{
            $input['email']="";		  
         }
         

            $validator = Validator::make($input, [
              'email' => 'required|email' 
             
             
                        
          ]);



          if ($validator->fails()) {
          
              $result = array(
                  'status' => false,
                  'message' =>  $validator->errors(),						
          
                  );

             return response()->json($result);
          
          }else{
            $logID=$session_val['pli_loginId'];		  
            $json_date='{"email" : "'.$logID.'"}';
            //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
            $sql = "
                    SELECT 
                    pq_event_additem.`pea_id` as bid_id,(SELECT MIN(pla_cur_bid) AS cur_bid FROM `pq_live_auction` WHERE `pla_lotid`=pq_event_additem.`pea_id` GROUP BY `pla_lotid`) AS curent_amount
                    FROM
                    `pq_event_invite_detail`
                    JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
                    JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
                    JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
                
                    WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' ) AND  `pq_event_create`.`pec_event_start_dt` <= NOW() 
                    AND `pq_event_create`.`pec_event_end_dt` >= NOW()";
            
    
            $eventData=DB::select( $sql);


            $result= $eventData;
          }
            }catch(\Exception $e){	

               
                 $result = array(
                        'status' => false,
                        'message' =>  $e->getMessage(),
                        'c_code'  =>1		
          
                    );
             }

             return response()->json($result);
    }

    public function buyerupdatebidData(Request $request){

        $session_val = session()->all();
        $input=array();
        try
        { 
           // $input = $request->all();	
         if(isset($session_val['pli_loginId'])){
           $input['email']=$session_val['pli_loginId'];		  
         }else{
            $input['email']="";		  
         }
         

            $validator = Validator::make($input, [
              'email' => 'required|email' 
             
             
                        
          ]);



          if ($validator->fails()) {
          
              $result = array(
                  'status' => false,
                  'message' =>  $validator->errors(),						
          
                  );

             return response()->json($result);
          
          }else{
            $logID=$session_val['pli_loginId'];		  
            $json_date='{"email" : "'.$logID.'"}';
            //select COUNT(*) FROM `pq_event_additem` WHERE pq_event_additem.`pea_event_id`= pq_event_create.`pec_event_id`) AS no_item 
            $sql = "select 
            pq_live_auction.`pla_id` AS bid_id,
            pq_live_auction.`pla_cur_bid` AS curent_amount 
          FROM
            `pq_event_invite_detail` 
            JOIN `pq_event_additem` 
              ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
            JOIN `pq_event_create` 
              ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
            JOIN pq_live_auction 
              ON pq_event_invite_detail.`pei_event_additem_id` = `pq_live_auction`.`pla_lotid` 
          WHERE pq_event_invite_detail.`pei_invite_createby` = '".$logID."' 
            AND `pq_event_create`.`pec_event_start_dt` <= NOW() 
            AND `pq_event_create`.`pec_event_end_dt` >= NOW()";
            

            $eventData=DB::select( $sql);


            $result= $eventData;
          }
            }catch(\Exception $e){	

               
                 $result = array(
                        'status' => false,
                        'message' =>  $e->getMessage(),
                        'c_code'  =>1		
          
                    );
             }

             return response()->json($result);
    }
    public static function getSellerInviteItemForReverse($eventID,$logID){

        $accept_status='{"email" : "'.$logID.'" ,"accept": "1"}';
        $reject_status='{"email" : "'.$logID.'" ,"reject": "1"}';
        $json_date='{"email" : "'.$logID.'"}';
        $sql = " select 
                    * ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$accept_status."'
                    ) as accept
                    ,json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$reject_status."'
                    ) as reject,
                    (SELECT MIN(pq_live_auction.`pla_cur_bid`) FROM `pq_live_auction` WHERE pq_live_auction.`pla_lotid` = pq_event_additem.`pea_id`) AS current_bid
  
                    FROM
                    pq_event_invite_detail 
                    
                    JOIN `pq_event_additem` 
                        ON `pq_event_additem`.`pea_id` = pq_event_invite_detail.`pei_event_additem_id` 
                    JOIN `pq_event_create` 
                        ON `pq_event_create`.`pec_event_id` = pq_event_additem.`pea_event_id` 
                    LEFT JOIN  `pq_live_auction` 
                        ON `pq_live_auction`.`pla_lotid` = pq_event_additem.`pea_id`  and pq_live_auction.pla_seller_emailid='".$logID."'
                    JOIN `pq_master_cat` 
                        ON `pq_master_cat`.`pmc_id` = pq_event_additem.`pea_event_cat` 
                    JOIN `pq_master_subcat` 
                        ON `pq_master_subcat`.`pms_id` = pq_event_additem.`pea_event_subcat` 
                    JOIN `pq_master_unit` 
                        ON `pq_master_unit`.`pmu_id` = pq_event_additem.`pea_event_unit` 
                    WHERE json_contains (
                        `pq_event_invite_detail`.`pei_invite_details`,
                        '".$json_date."'
                    ) AND pq_event_additem.`pea_event_id`='".$eventID."'";
                    

   $eventData=DB::select( $sql);
   return $eventData;
    }
    public static function getEvent($eventID)
	{
        $eventData = DB::table('pq_event_create')
         ->select('*')
        ->where('pq_event_create.pec_event_id',$eventID)
        ->first();
      
        return $eventData;
    }
    public static function getMasterCat($cat_id)
    {
       if($cat_id!=3){
        $category = DB::table('pq_master_cat')
         ->select('*')
         ->where('pq_master_cat.pmc_cat_type',$cat_id)
        ->get();
        }
        else{
           $category = DB::table('pq_master_cat')
         ->select('*')
         ->whereIn('pq_master_cat.pmc_cat_type',array('1','2','3'))
        ->get();
        }
      
        return $category;

    }
    
    public static function getUnit()
    {
        $unit = DB::table('pq_master_unit')
         ->select('*')
        ->get();
      
        return $unit;

    } 

    
    
    public function deleteProuctItem(Request $request){


        $input = $request->all();



    
       
          $dt 	= new DateTime();
          $date	= $dt->format('Y-m-d H:i:s');

          try{
            
            $pmc_id=$input['pmc_id'];
         
   
           
               $eventInsert = DB::table('pq_event_additem')
               ->where('pea_id', $pmc_id)
               ->delete();
  
  
              if( $eventInsert){

                  $result = array(
                      'status' => true,
                      'message' => 'Event item updated'
  
                  );
              
               
  
              //print_r( $eventid);die;
            }else{
              $result = array(
                  'status' => false,
                  'message' => 'Something Went Wrong',	
                  'c_code'  =>2	 			
             
              );
            }
              
              
              //echo $str;
              //print_r(count($package));die;
                      
         
         // print_r($insertData);die;
         }catch(\Exception $e){	
                $result = array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'c_code'  =>2	 			
               
                );
           }
  
           return response()->json($result);
          
    }
    public function buyerAcceptBid(Request $request){
        $input = $request->all();
    
 
         $dt 	= new DateTime();
         $date	= $dt->format('Y-m-d H:i:s');
         $session_val = session()->all();
     	$datetime		= $dt->format('Y-m-d H:i:s');
		$date			= $dt->format('Y-m-d');
		$year			= $dt->format('Y');
         
     //	$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));
                 
      try{
           $auctionID=$input['lotID'];
           $sellerID=$input['sellerID'];
           $check_exist = DB::table('pq_live_auction')
           ->where('pla_id', '=',  $auctionID)
           ->select('*')
           ->first();
        
             
            if( $check_exist ){
                $EventCompletedData =	[            
                    'pla_auction_status'  =>  '1',
                
                        
                ];
          
            
                  
 
         $updateData= DB::table('pq_live_auction')
         ->where('pla_id', $auctionID)
         ->update($EventCompletedData);
         $lotItem_id=$check_exist->pla_lotid;

         
           //  print_r( $check_exist->pla_seller_emailid);die;
          
              
             if( $updateData){


                $EventCompletedRejectData =	[            
                    'pla_auction_status'  =>  '2',
                
                        
                ];
                
                $rejectData= DB::table('pq_live_auction')
                ->where('pla_lotid', $lotItem_id)
                ->where('pla_id','!=', $auctionID)
                ->update($EventCompletedRejectData);
            
                 $result = array(
                     'status' => true,
                     'message' => 'Event Completed',	
                     'c_code'  =>1	 			
                
                 );
            
                 $sql=  "select `pec_event_name`,`pmc_cat_name`,pec_event_start_dt,pec_event_end_dt,`pms_subcat_name`,`pli_con_name` AS seller_name,`pci_comp_name` AS seller_company,`pla_cur_bid` FROM `pq_event_create`

                 JOIN `pq_event_additem` ON pq_event_additem.`pea_event_id`=pq_event_create.`pec_event_id`
                 
                 JOIN  pq_live_auction ON `pq_live_auction`.`pla_lotid`=pq_event_additem.`pea_id`
                 JOIN pq_login_info ON `pq_login_info`.`pli_loginid` = pq_live_auction.`pla_seller_emailid`
                 JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id` = pq_login_info.`pli_comp_id`
                 JOIN `pq_master_cat` ON `pq_master_cat`.`pmc_id` = pq_event_additem.`pea_event_cat`
                 JOIN `pq_master_subcat` ON `pq_master_subcat`.`pms_id` = pq_event_additem.`pea_event_subcat`
                 WHERE pq_live_auction.`pla_lotid`=".$lotItem_id." and pq_login_info.`pli_loginid`='".$sellerID."' ";
      
                 $winner_data= DB::select( $sql);
      

                   $data=$winner_data[0];
                   $start= new DateTime($data->pec_event_start_dt);
                   $end= new DateTime($data->pec_event_end_dt);
                   $start_date=$start->format('d M');
                   $end_date=$end->format('d M');
      
                   $date_format=$start_date.' -'. $end_date.', '.$year;
                   $data 		= array('event_name'=>$data->pec_event_name,'date_format'=>$date_format,'start_date'=>$data->pec_event_start_dt,'end_date'=>$data->pec_event_end_dt,'email'=>$check_exist->pla_seller_emailid,'event_cat_name'=>$data->pmc_cat_name,'seller_name'=>$data->seller_name,'year'=>$year,'seller_company'=>$data->seller_company,'seller_bidamount'=>$data->pla_cur_bid,'date'=>$date);
                   $data_val		= array_merge($data,$session_val);
                
//winning mail send to seller
                 Mail::send('emails.event-winner',$data_val,function($message) use ($data_val)
                 {				 
                     $message->to($data_val['email'])->subject('Congratulation - Purchase Quick');
                     $message->from('support@purchasequick.in','Purchase Quick');
                 });
                
             //print_r( $eventid);die;
           }else{
             $result = array(
                 'status' => false,
                 'message' => 'Some went wrong',	
                 'c_code'  =>2	 			
            
             );
           }
        }else{
            $result = array(
                'status' => false,
                'message' => 'Some went wrong',	
                'c_code'  =>2	 			
           
            );
          }
             
             
             //echo $str;
             //print_r(count($package));die;
                     
        
        // print_r($insertData);die;
        }catch(\Exception $e){	
               $result = array(
                   'status' => false,
                   'message' => $e->getMessage(),
                   'c_code'  =>2	 			
              
               );
          }
 
          return response()->json($result);

    }


    public function needClarification(Request $request){
        $input = $request->all();
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');
        $year			= $dt->format('Y');
        try{
            $buyerID = Crypt::decryptString($input['buyerID']);	  
            $sellerID = Crypt::decryptString($input['sellerID']);
            $lotID = Crypt::decryptString($input['lotID']);
            $clarification=$input['clarification'];
            $user_os 		= GetOsDetails::getOS();
            $user_browser   = GetOsDetails::getBrowser();

            $clarificationInsertData=[

         
                'peic_event_additem_id'        => $lotID,
                'peic_email'                   => $sellerID,
                'peic_seller_remarks'          => $clarification,
                'peic_insert_dt'               => $date,
                'peic_buyer_remaksdt'          => $date,
                'peic_ipaddress'               => $_SERVER['REMOTE_ADDR'],
                'peic_browser'                 => $user_browser['name'],    
                'peic_browser_ver'             => $user_browser['version'],
                'peic_os'                      => $user_os
            ];

         	
   
          
        $eventInsert = DB::table('pq_event_invite_clarify')->insert($clarificationInsertData);
        if($eventInsert){
             
            $eventLotData=purchaseInvite::getEventAdditem($lotID);
            
           
          
            
            
            $data 		= array('year'=>$year,
            'selleremail'=>$sellerID,
            'buyeremail'=>$buyerID,
            'clarification'=>$clarification,
            'pec_event_name'=>$eventLotData->pec_event_name,
            'pmc_cat_name'=>$eventLotData->pmc_cat_name,
            'pms_subcat_name'=>$eventLotData->pms_subcat_name,
            'pea_event_spec'=>$eventLotData->pea_event_spec,
            'pea_event_unit_quantity'=>$eventLotData->pea_event_unit_quantity,
            'pmu_unit_name'=>$eventLotData->pmu_unit_name,
            'pec_event_start_dt'=>$eventLotData->pec_event_start_dt,
            'pec_event_end_dt'=>$eventLotData->pec_event_end_dt,
            'pea_event_location'=>$eventLotData->pea_event_location

        
        );
        Mail::send('emails.event-clarification',$data,function($message) use ($data)
        {	
        
            $message->to($data['buyeremail'])->subject('Clarification-Purchase Quick');						
            $message->from('support@purchasequick.in','Purchase Quick');
            
        });
                                    
            $result = array(
                'status'  => true,
                'message' => "Invite Send success"			

            );	

        }else{
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
           
            );
        }
         
           
        }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }
         return response()->json($result);

    }
    public function addBidLotItem(Request $request){
        $input = $request->all();

        $session_val = session()->all();

       
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try{
          
          $addItemID=$input['lotID'];
          $comp_id= $session_val['pli_comp_id'];
          $log_id= $session_val['pli_loginId'];
          $cur_bid=$input['bid_price'];

          $check_exist = DB::table('pq_live_auction')
            ->where('pla_lotid', '=',  $addItemID)
            ->where('pla_seller_emailid', '=',  $log_id)
            ->select('*')
			->first();
	
			if (is_null($check_exist)) 
			{
          $EventInsertData =	[
            'pla_lotid' => $addItemID,
            'pla_seller_compid'=> $comp_id,
            'pla_seller_emailid'=> $log_id,
            'pla_cur_bid'=> $cur_bid,
            'pla_cur_bid_datetime'=>  $date,
            'pla_auction_status'=> '0'
      
            
        ];

       // print_r( $EventInsertData);die;
     
         $eventInsert = DB::table('pq_live_auction')->insert($EventInsertData);

         if( $eventInsert){

                    $result = array(
                        'status' => true,
                        'message' => 'Bit item inserted'

                    );
                }else{
                    $result = array(
                        'status' => false,
                        'message' => 'Something Went Wrong',	
                        'c_code'  =>2	 			
                
                    );
                }
        }else{

            $update_array=[
            
                'pla_cur_bid'=> $cur_bid,
                'pla_cur_bid_datetime'=>  $date,
                
            ];
            $eventInsert = DB::table('pq_live_auction')
            ->where('pla_lotid', $addItemID)
            ->where('pla_seller_emailid', '=',  $log_id)
            ->update($update_array);


                
         if( $eventInsert){

                    $result = array(
                        'status' => true,
                        'message' => 'Bit item updated'

                    );
            }else{
                        $result = array(
                            'status' => false,
                            'message' => 'Something Went Wrong',	
                            'c_code'  =>2	 			
                    
                        );
                }
    }
        

            if( $eventInsert){

                $result = array(
                    'status' => true,
                    'message' => 'Bit item updated'

                );
            
             

            //print_r( $eventid);die;
          }else{
            $result = array(
                'status' => false,
                'message' => 'Something Went Wrong',	
                'c_code'  =>2	 			
           
            );
          }
            
            
            //echo $str;
            //print_r(count($package));die;
                    
       
       // print_r($insertData);die;
       }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);


    }
    public function acceptInviteItem(Request $request){


        $input = $request->all();
       
          $dt 	= new DateTime();
          $date	= $dt->format('Y-m-d H:i:s');

         try{
            $session_val = session()->all();
          
            $pmc_id=$input['inviteID'];
            $invite_flag=$input['invite_type'];
            $loginID=$session_val['pli_loginId'];

           
         
            $check_exist = DB::table('pq_event_invite_detail')
            ->where('pei_event_additem_id', '=',  $pmc_id)
            ->select('*')
            ->first();
            
         

            $json_array=json_decode($check_exist->pei_invite_details,true);
           
           
            foreach($json_array as $key => $value) {
                if (in_array($loginID, $value)) {
                    unset($json_array[$key]);
                }
            }
           
            
            $json_date =array();
            $json_date=array_values($json_array);
          if($invite_flag==1) { 
          $jsonformat=[
            'bid'=> "0",
            'email' => $loginID,
            'accept'=>  "1",
            'reject'=>  "0",
            

             ];
            }else{
                $jsonformat=[
                    'bid'=> "0",
                    'email' => $loginID,
                    'accept'=>  "0",
                    'reject'=>  "1",
                    
        
                     ];
            }

             array_push($json_date,$jsonformat);

             //encode json format 

             $format_json=json_encode($json_date);
             if($invite_flag==1) { 
            $accept_count=$check_exist->pei_invite_accept_count+1 ;
            
            
             $update_array= [
                'pei_invite_details' =>   $format_json   ,
                'pei_invite_accept_count' =>    $accept_count                              
            ];
             
        }else{
            $update_array= [
                'pei_invite_details' =>   $format_json   
                                   
            ];
        }

           
               $inviteUpdate = DB::table('pq_event_invite_detail')
               ->where('pei_event_additem_id', $pmc_id)
               ->update($update_array);
  
           
              if( $inviteUpdate){

                  $result = array(
                      'status' => true,
                      'message' => 'Event item updated'
  
                  );
              
               
  
              //print_r( $eventid);die;
            }else{
              $result = array(
                  'status' => false,
                  'message' => 'Something Went Wrong',	
                  'c_code'  =>2	 			
             
              );
            }
              
              
              //echo $str;
              //print_r(count($package));die;
                      
         
         // print_r($insertData);die;
         }catch(\Exception $e){	
                $result = array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'c_code'  =>2	 			
               
                );
           }
  
           return response()->json($result);

                 
    }

    
    public function productEditItem(Request $request){


        $input = $request->all();


    
       
          $dt 	= new DateTime();
          $date	= $dt->format('Y-m-d H:i:s');

          try{
            $prod_cat=$input['pmc_cat_name'];
            $prod_sub_cat=$input['pms_subcat_name'];
            $spec=$input['pea_event_spec'];
            $prod_unit=$input['pmu_unit_name'];
            $prod_st_price=$input['pea_event_start_price'];
            $prod_min_decrement=$input['pea_event_max_dec'];
            $location=$input['pea_event_location'];
            $quantity=$input['pea_event_unit_quantity'];
            $pmc_id=$input['pmc_id'];
         
            
             // print_r($Pkg_curCount);die;
  
      

  
              $EventItemData =	[
                
                  'pea_event_cat' 		 	    =>  $prod_cat,
                  'pea_event_subcat'  		    =>  $prod_sub_cat,
                  'pea_event_spec'  	        =>  $spec,
                  'pea_event_unit' 	            =>  $prod_unit,
                  'pea_event_unit_quantity'     =>  $quantity,
                  'pea_event_start_price'  	    =>  $prod_st_price,
                  'pea_event_max_dec'  		    =>  $prod_min_decrement,
                  'pea_event_location'  	    =>  $location,
                  'pea_event_additem_dt'        =>  $date
      
                  
              ];
             
              
            //  DB::enableQueryLog();
               $eventInsert = DB::table('pq_event_additem')
               ->where('pea_id', '=',  $pmc_id)
               ->update($EventItemData);

               //$query = DB::getQueryLog();

              if($eventInsert){

                  $result = array(
                      'status' => true,
                      'message' => 'Event item updated'
  
                  );
              
               
  
              //
            }else{
              $result = array(
                  'status' => false,
                  'message' => 'Something Went Wrong',	
                  'c_code'  =>2	 			
             
              );
            }
              
              
              //echo $str;
              //print_r(count($package));die;
                      
         
         // print_r($insertData);die;
         }catch(\Exception $e){	
                $result = array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'c_code'  =>2	 			
               
                );
           }
  
           return response()->json($result);
          
    }

    public function addEventProduct(Request $request){

        $input = $request->all();
        

          $dt 	= new DateTime();
          $date	= $dt->format('Y-m-d H:i:s');
         if($request->hasFile('file')){
          $validator = Validator::make($request->all(), [
            'file'   => 'mimes:doc,pdf,docx|nullable'
        ]);
        if ($validator->fails()) {
			
            $result = array(
                'status' => false,
                'message' => "please upload only doc,pdf,docx file",				
        
                );
                return response()->json($result);
          
        
        }
    }
    
    

      //	$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));
                  
        try{
            $prod_cat=$input['prod_cat'];
            $prod_sub_cat=$input['prod_sub_cat'];
            $spec=$input['spec'];
            $prod_unit=$input['prod_unit'];
            $prod_st_price=$input['prod_st_price'];
            $prod_min_decrement=$input['prod_min_decrement'];
            $location=$input['location'];
            $quantity=$input['prod_quant'];
            $loginid=$input['loginid'];
            $eventID=$input['eventID'];
            $start_date = $input['pec_event_start_dt'];
            $end_date = $input['pec_event_end_dt'];            
            if(isset($input['pea_event_reserve_price'])){
                $reserve_price=  $input['pea_event_reserve_price'];
            }else{
                $reserve_price=  0;
            }
         
            
             // print_r($Pkg_curCount);die;
            
      

  
              $EventItemData =	[
                  'pea_event_id'                =>  $eventID,
                  'pea_event_cat' 		 	    =>  $prod_cat,
                  'pea_event_subcat'  		    =>  $prod_sub_cat,
                  'pea_event_spec'  	        =>  $spec,
                  'pea_event_unit' 	            =>  $prod_unit,
                  'pea_event_unit_quantity'     =>  $quantity,
                  'pea_event_start_price'  	    =>  $prod_st_price,
                  'pea_event_max_dec'  		    =>  $prod_min_decrement,
                  'pea_event_location'  	    =>  $location,
                  'pea_event_additem_dt'  	    =>  $date,
                  'pea_event_additem_loginid'  	=>  $loginid,
                  'pea_event_reserve_price'     =>  $reserve_price,
                  'pea_event_start_dt'        =>  $start_date,
                  'pea_event_end_dt'          =>  $end_date                  
              ];
             
            //  print_r( $EventItemData);die;
           
               $eventInsert = DB::table('pq_event_additem')->insertGetId($EventItemData);
  
  
              if( $eventInsert){

                if($request->hasFile('file')){
                $extension = $request->file('file');
                $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
                $dir = 'pq_terms/'.date('Y').'/'.date('m').'/'.date('d').'/'. $eventID;
                $filename = $eventInsert.'.'.$extension;
                $request->file('file')->move($dir, $filename);
                $path=  $dir.'/'. $filename;
                $updateItemFileLocation =	[


                    'pea_event_additem_dt'  	    =>  $date,
                    'pea_event_terms_condition' =>   $path
                    
                ];

                $upadteFileLocation = DB::table('pq_event_additem')
               ->where('pea_id', $eventInsert)
               ->update($updateItemFileLocation);
            }


                  $result = array(
                      'status' => true,
                      'message' => 'Event item added'
  
                  );
              
               
  
              //print_r( $eventid);die;
            }else{
              $result = array(
                  'status' => false,
                  'message' => 'Something Went Wrong',	
                  'c_code'  =>2	 			
             
              );
            }
              
              
              //echo $str;
              //print_r(count($package));die;
                      
         
         // print_r($insertData);die;
         }catch(\Exception $e){	
                $result = array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'c_code'  =>2	 			
               
                );
           }
        
  
           return response()->json($result);
    }

    public function completedEvent(Request $request){
        $input = $request->all();
    
       //sssprint_r($input);die;
        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');
        
    
        
    //	$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));
                
      try{
          $eventID=$input['eventID'];
          $loginid=$input['loginid'];
          
           // print_r($Pkg_curCount);die;
           $EventCompletedData =	[            
            'pec_event_status' 		 	    =>  '1',
            'pec_event_create_dt' =>  $date
                 
        ];

        $updateData= DB::table('pq_event_create')
        ->where('pec_event_id', $eventID)
        ->update($EventCompletedData);
       
    
          //  print_r( $EventItemData);die;
         
             
            if( $updateData){

                $result = array(
                    'status' => true,
                    'message' => 'Event Completed',	
                    'c_code'  =>1	 			
               
                );
               
            //print_r( $eventid);die;
          }else{
            $result = array(
                'status' => false,
                'message' => 'Some went wrong',	
                'c_code'  =>2	 			
           
            );
          }
            
            
            //echo $str;
            //print_r(count($package));die;
                    
       
       // print_r($insertData);die;
       }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),
                  'c_code'  =>2	 			
             
              );
         }

         return response()->json($result);
    }

    public function getSubProduct(Request $request){

        $input = $request->all();
        $categories=$input['cat'];
        $cat_type=$input['cat_type'];
        $otherText='OTHERS';
      /*  $others=DB::table('pq_master_subcat as sub_other')
        ->select('*')
        ->where('sub_other.pms_subcat_name',$otherText)
        ->where('sub_other.pms_cat_type',$cat_type);

         $category = DB::table('pq_master_subcat')
         ->select('*')
         ->where('pq_master_subcat.pms_catid',$categories)

         ->union($others)
        ->get();*/
        $category = DB::table('pq_master_subcat')
         ->select('*')
         ->where('pq_master_subcat.pms_catid',$categories)
         ->get();


        return  $category;
    }

    public function lotInviteCheck(Request $request){

        $input = $request->all();

        $email=$input['email'];

        $lotID=$input['lotid'];
        $json_date='{"email" : "'.$email.'"}';
        $check_logintype=DB::table('pq_login_info')
        ->select('*')
        ->where('pli_loginid',$email)
        ->first();

       
if($check_logintype){
        if( $check_logintype->pli_login_status=='A' && $check_logintype->pli_login_type!='1'){
      
    
         $sql="select COUNT(pq_event_additem.`pea_id`) as invite_count FROM pq_event_invite_detail
            JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
            JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
            JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
            JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
            WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
             
             AND `pq_event_additem`.`pea_id`=".$lotID;
             $invite=DB::select( $sql);
             $invite_count=$invite[0];
             $result=  $invite_count->invite_count;
        }else{
            $result=2;
        }
    }else{
        

     $sql="select COUNT(pq_event_additem.`pea_id`) as invite_count FROM pq_event_invite_detail
        JOIN `pq_event_additem` ON `pq_event_additem`.`pea_id`=pq_event_invite_detail.`pei_event_additem_id`
        JOIN `pq_event_create` ON `pq_event_create`.`pec_event_id`=pq_event_additem.`pea_event_id`
        JOIN `pq_login_info` ON `pq_login_info`.`pli_loginid`= pq_event_additem.`pea_event_additem_loginid`
        JOIN `pq_company_info` ON `pq_company_info`.`pci_comp_id`=pq_login_info.`pli_comp_id`
        WHERE json_contains(`pq_event_invite_detail`.`pei_invite_details`,'".$json_date."' )
         
         AND `pq_event_additem`.`pea_id`=".$lotID;
         $invite=DB::select( $sql);
         $invite_count=$invite[0];
         
        $result=  $invite_count->invite_count;
    }
    return   $result;
    }

    
    public static function getAddedItems($eventID){
        $eventID=$eventID;
         $category = DB::table('pq_event_additem')
         ->Join('pq_master_cat', 'pq_master_cat.pmc_id' , '=', 'pq_event_additem.pea_event_cat')
         ->leftJoin('pq_event_invite_detail', 'pq_event_invite_detail.pei_event_additem_id' , '=', 'pq_event_additem.pea_id')
         ->Join('pq_master_subcat', 'pq_master_subcat.pms_id' , '=', 'pq_event_additem.pea_event_subcat')
         ->Join('pq_master_unit', 'pq_master_unit.pmu_id' , '=', 'pq_event_additem.pea_event_unit')
         ->select('pq_event_additem.pea_id',  
        
         'pq_master_cat.pmc_id',
         'pq_master_cat.pmc_cat_name',
         'pq_master_subcat.pms_id',
         'pq_master_subcat.pms_subcat_name',
         'pq_event_additem.pea_event_spec',
         'pq_master_unit.pmu_id',
         'pq_master_unit.pmu_unit_name',
         'pq_event_additem.pea_event_unit_quantity',
         'pq_event_additem.pea_event_start_price',
         'pq_event_additem.pea_event_max_dec',
         'pq_event_additem.pea_event_terms_condition',
         'pq_event_additem.pea_event_location',
         'pq_event_additem.pea_event_reserve_price',
         'pq_event_additem.pea_event_start_dt',
         'pq_event_additem.pea_event_end_dt',
         'pq_event_invite_detail.*')
         ->where('pq_event_additem.pea_event_id',$eventID)
        ->get();
        
       
        return  $category;
    }
    public static function sellerBidDetails($lotId){
        
        $category = DB::table('pq_live_auction')

        ->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_live_auction.pla_seller_compid')
        ->select('pq_live_auction.pla_cur_bid','pq_live_auction.pla_id',
        'pq_company_info.pci_comp_name', 'pq_live_auction.pla_auction_status',
        'pq_live_auction.pla_seller_emailid' 
        )
        ->where('pq_live_auction.pla_lotid',$lotId)
        ->get();

        return  $category;
    }

    
    public function getPqvendor(Request $request){
        
        $input = $request->all();

        $lotID= $input['lotid'];
        $catID= $input['category_id'];
        $location= $input['location'];
       
        $checkinvite_insert = DB::table('pq_event_invite_detail')
       ->select('*')    
       ->where('pei_event_additem_id', $lotID)
       ->first();
           
        if(!$checkinvite_insert){
            $pqvendor = DB::table('pq_master_vendors')
            ->select('*')    
            ->where('pmv_cat_id',$catID)
            ->where('pmv_comp_city', 'like', '%'.$location.'%')
            ->get();
        }else{
           $sql="select * FROM `pq_master_vendors` WHERE NOT FIND_IN_SET(pq_master_vendors.pmv_comp_email,(SELECT REPLACE(REPLACE(REPLACE(REPLACE(JSON_EXTRACT(`pei_invite_details`, '$[*].email'),'[',''),']',''),'\"',''),' ','') FROM pq_event_invite_detail WHERE pq_event_invite_detail.`pei_event_additem_id`='". $lotID."')) AND `pmv_cat_id`='".$catID."' and  `pmv_comp_city` LIKE '%".$location."%'";
           $invite=DB::select( $sql);

       
           $pqvendor= $invite;
        }
        

        return  $pqvendor;

    }
    public static function getLocation(){
        
        $location = DB::table('pq_master_cities')
        ->select('*')
        ->get();

        return  $location;

    }


    
    

}