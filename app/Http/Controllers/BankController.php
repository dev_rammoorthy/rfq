<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use DateTime;
use Crypt;
use App\GetOsDetails;
use View;
use GuzzleHttp\Exception\GuzzleException;
use App\Http\Controllers\PurchaseController;
use GuzzleHttp\Client;
use Hash;

class BankController extends Controller
{
    //Validates the bank details - If provided information valid then it will be updated in database
    public function addbankinfo(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try
        {  
            $user_os 		= PurchaseController::getOS();	
            $user_browser   = PurchaseController::getBrowser();

            $authenticate = $this->authenticate(); //authenticate payout
            $statusCode = $authenticate->subCode; //response code        

            if($statusCode == "200")
            {
                $token_data = $authenticate->data; //token data
                $token = $token_data->token; //access token

                $client  = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
            
                $url = 'https://payout-gamma.cashfree.com/payout/v1/validation/bankDetails?name='.$input['pbd_name'].'&phone='.$input['pbd_phone'].'&bankAccount='.$input['pbd_bank_account'].'&ifsc='.$input['pbd_ifsc'];

                $res = $client->get($url,['headers' => ['Authorization' => 'Bearer ' . $token]]);
                $result = $res->getBody()->getContents();		
                
                $respose = json_decode($result);

                $reponse_data = $respose->data;

                if($reponse_data->accountExists == "YES")
                {
                    $getWalletModel = DB::table('pq_wallet_current')
                            ->where('pwc_login_id', '=', $login_id)
                            ->first();

                    $getModel = DB::table('pq_beneficiary_details')
                                    ->where('pbd_loginid', '=', $login_id)
                                    ->first();

                    if(isset($getModel) && $getModel != null)
                    {
                        $updateValue = [
                            'pbd_name' => $input['pbd_name'],
                            'pbd_email' => $input['pbd_email'],
                            'pbd_phone' => $input['pbd_phone'],
                            'pbd_bank_account' => $input['pbd_bank_account'],
                            'pbd_ifsc' => $input['pbd_ifsc'],
                            'pbd_address1' => $input['pbd_address1'],
                            'pbd_address2' => $input['pbd_address2'],
                            'pbd_city' => $input['pbd_city'],
                            'pbd_state' => $input['pbd_state'],
                            'pbd_pincode' => $input['pbd_pin'],
                            'pbd_updated_at' => $date,
                            'pbd_is_account_verified' => '1',
                            'pbd_os' => $user_os,
                            'pbd_ipaddress' => $_SERVER['REMOTE_ADDR'],
                            'pbd_browser' => $user_browser['name'],
                            'pbd_browser_ver' => $user_browser['version']
                        ];

                        $updateModel = DB::table('pq_beneficiary_details')
                                            ->where('pbd_loginid', '=', $login_id)
                                            ->update($updateValue);

                        if($updateModel)
                        {
                            $result = array(
                                'status' => true,
                                'message' => 'Beneficiary updated successfully',	
                                'c_code'  =>1
                            );
                        }
                        else
                        {
                            $result = array(
                                'status' => false,
                                'message' => 'Something Went Wrong',	
                                'c_code'  => 2
                            );
                        }
                    }
                    else
                    {
                        $insertValue = [
                            'pbd_bene_id' => 'BENE'.uniqid(),
                            'pbd_wallet_id' => $getWalletModel->pwc_wallet_id,
                            'pbd_loginid' => $login_id,
                            'pbd_name' => $input['pbd_name'],
                            'pbd_email' => $input['pbd_email'],
                            'pbd_phone' => $input['pbd_phone'],
                            'pbd_bank_account' => $input['pbd_bank_account'],
                            'pbd_ifsc' => $input['pbd_ifsc'],
                            'pbd_address1' => $input['pbd_address1'],
                            'pbd_address2' => $input['pbd_address2'],
                            'pbd_city' => $input['pbd_city'],
                            'pbd_state' => $input['pbd_state'],
                            'pbd_pincode' => $input['pbd_pin'],
                            'pbd_created_at' => $date,
                            'pbd_is_account_verified' => '1',
                            'pbd_os' => $user_os,
                            'pbd_ipaddress' => $_SERVER['REMOTE_ADDR'],
                            'pbd_browser' => $user_browser['name'],
                            'pbd_browser_ver' => $user_browser['version']
                        ];

                        $insertModel = DB::table('pq_beneficiary_details')
                                            ->where('pbd_loginid', '=', $login_id)
                                            ->insert($insertValue);

                        if($insertModel)
                        {
                            $result = array(
                                'status' => true,
                                'message' => 'Beneficiary added successfully',	
                                'c_code'  =>1
                            );
                        }
                        else
                        {
                            $result = array(
                                'status' => false,
                                'message' => 'Something Went Wrong',	
                                'c_code'  => 2
                            );
                        }    
                    }
                }
                else
                {
                    $result = array(
                        'status' => false,
                        'message' => $respose->message,
                        'c_code'  => 2
                    );
                }
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => $authenticate->message,
                    'c_code'  => 2
                );
            }            

        }catch(\Exception $e){	

            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    //Add new account
    public function addNewBank(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $dt 	= new DateTime();
        $date	= $dt->format('Y-m-d H:i:s');

        try
        {  
            $user_os 		= PurchaseController::getOS();	
            $user_browser   = PurchaseController::getBrowser();

            $authenticate = $this->authenticate(); //authenticate payout
            $statusCode = $authenticate->subCode; //response code        

            if($statusCode == "200")
            {
                $token_data = $authenticate->data; //token data
                $token = $token_data->token; //access token

                $client  = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
            
                $url = 'https://payout-gamma.cashfree.com/payout/v1/validation/bankDetails?name='.$input['pbd_name'].'&phone='.$input['pbd_phone'].'&bankAccount='.$input['pbd_bank_account'].'&ifsc='.$input['pbd_ifsc'];

                $res = $client->get($url,['headers' => ['Authorization' => 'Bearer ' . $token]]);
                $result = $res->getBody()->getContents();		
                
                $respose = json_decode($result);

                $reponse_data = $respose->data;

                if($reponse_data->accountExists == "YES")
                {
                    $getWalletModel = DB::table('pq_wallet_current')
                            ->where('pwc_login_id', '=', $login_id)
                            ->first();

                    $getModel = DB::table('pq_beneficiary_details')
                                    ->where('pbd_loginid', '=', $login_id)
                                    ->where('pbd_bank_account', '=', $input['pbd_bank_account'])
                                    ->first();

                    if(isset($getModel) && $getModel != null)
                    {
                        $result = array(
                            'status' => false,
                            'message' => 'Account number already exist',	
                            'c_code'  => 2
                        );                       
                    }
                    else
                    {
                        $insertValue = [
                            'pbd_bene_id' => 'BENE'.uniqid(),
                            'pbd_wallet_id' => $getWalletModel->pwc_wallet_id,
                            'pbd_loginid' => $login_id,
                            'pbd_name' => $input['pbd_name'],
                            'pbd_email' => $input['pbd_email'],
                            'pbd_phone' => $input['pbd_phone'],
                            'pbd_bank_account' => $input['pbd_bank_account'],
                            'pbd_ifsc' => $input['pbd_ifsc'],
                            'pbd_address1' => $input['pbd_address1'],
                            'pbd_address2' => $input['pbd_address2'],
                            'pbd_city' => $input['pbd_city'],
                            'pbd_state' => $input['pbd_state'],
                            'pbd_pincode' => $input['pbd_pin'],
                            'pbd_created_at' => $date,
                            'pbd_is_account_verified' => '1',
                            'pbd_os' => $user_os,
                            'pbd_ipaddress' => $_SERVER['REMOTE_ADDR'],
                            'pbd_browser' => $user_browser['name'],
                            'pbd_browser_ver' => $user_browser['version']
                        ];

                        //add bene in payout
                        $insert_ben = $this->addBeneFunction($insertValue, $token);

                        if($insert_ben->subCode == "200")
                        {
                            $insertModel = DB::table('pq_beneficiary_details')
                                                ->where('pbd_loginid', '=', $login_id)
                                                ->insert($insertValue);

                            if($insertModel)
                            {
                                $result = array(
                                    'status' => true,
                                    'message' => 'Beneficiary added successfully',	
                                    'c_code'  =>1
                                );
                            }
                            else
                            {
                                $result = array(
                                    'status' => false,
                                    'message' => 'Something Went Wrong',	
                                    'c_code'  => 2
                                );
                            }    
                        }
                        else
                        {
                            $result = array(
                                'status' => false,
                                'message' => $insert_ben->message,
                                'c_code'  => 2
                            );
                        }
                    }
                }
                else
                {
                    $result = array(
                        'status' => false,
                        'message' => $respose->message,
                        'c_code'  => 2
                    );
                }
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => $authenticate->message,
                    'c_code'  => 2
                );
            }            

        }catch(\Exception $e){	

            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    //API call for add beneficiary
    public function addBeneFunction($data, $token)
    {
        $client    = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
            
        $url = 'https://payout-gamma.cashfree.com/payout/v1/addBeneficiary';

        $post_data = [
                'beneId'  	=> $data['pbd_bene_id'],
                'name' 		=> $data['pbd_name'],
                'email' 	=> $data['pbd_email'],
                'phone' =>  $data['pbd_phone'],
                'bankAccount' 	=>  $data['pbd_bank_account'],
                'ifsc' 	=>  $data['pbd_ifsc'],
                'address1' 	=> $data['pbd_address1'],
                'city' 	=> $data['pbd_city'],
                'state' 	=> $data['pbd_state'],
                'pincode' 	=> $data['pbd_pincode']
        ];

        $res = $client->post($url,['json' => $post_data, 'headers' => ['Content-type' => 'application/json', 'Authorization' => 'Bearer ' . $token]]);
        $result = $res->getBody()->getContents();		
        
        $respose = json_decode($result);

        return $respose;
    }

    public function deleteBank(Request $request)
    {
        $input = $request->all();

        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        try
        {
            $deleteId = $input['radioBeneId'];

            $authenticate = $this->authenticate(); //authenticate payout
            $statusCode = $authenticate->subCode; //response code        

            if($statusCode == "200")
            {
                $getModel = DB::table('pq_beneficiary_details')
                                ->where('pbd_loginid', '=', $login_id)
                                ->where('pbd_id', '=', $deleteId)
                                ->first();

                if(isset($getModel))
                {
                    $token_data = $authenticate->data; //token data
                    $token = $token_data->token; //access token

                    //validate bank
                    $client  = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
                
                    $url = 'https://payout-gamma.cashfree.com/payout/v1/validation/bankDetails?name='.$getModel->pbd_name.'&phone='.$getModel->pbd_phone.'&bankAccount='.$getModel->pbd_bank_account.'&ifsc='.$getModel->pbd_ifsc;

                    $res = $client->get($url,['headers' => ['Authorization' => 'Bearer ' . $token]]);
                    $result = $res->getBody()->getContents();		
                    
                    $respose = json_decode($result);

                    $reponse_data = $respose->data;

                    if($reponse_data->accountExists == "YES")
                    {
                        //remove beneficiary
                        $client  = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
                
                        $url = 'https://payout-gamma.cashfree.com/payout/v1/removeBeneficiary';

                        $post_data = [
                            'beneId'  	=> $getModel->pbd_bene_id
                        ];

                        $res = $client->post($url,['json' => $post_data, 'headers' => ['Authorization' => 'Bearer ' . $token]]);
                        $result = $res->getBody()->getContents();		
                        
                        $reponse_data1 = json_decode($result);

                        if($reponse_data1->subCode == "200")
                        {
                            $deletModel = DB::table('pq_beneficiary_details')
                                            ->where('pbd_id', '=', $deleteId)
                                            ->delete();

                            if($deletModel)
                            {
                                $result = array(
                                    'status' => true,
                                    'message' => $reponse_data1->message,
                                    'c_code'  =>1
                                );
                            }
                            else
                            {
                                $result = array(
                                    'status' => false,
                                    'message' => 'Something Went Wrong',
                                    'c_code'  =>2
                                );
                            }

                        }
                        else
                        {
                            $result = array(
                                'status' => false,
                                'message' => $reponse_data1->message,
                                'c_code'  =>2
                            );
                        }
                    }
                    else
                    {
                        $deletModel = DB::table('pq_beneficiary_details')
                                        ->where('pbd_id', '=', $deleteId)
                                        ->delete();

                        if($deletModel)
                        {
                            $result = array(
                                'status' => true,
                                'message' => 'Beneficiary Removed Successfully',
                                'c_code'  =>1
                            );
                        }
                        else
                        {
                            $result = array(
                                'status' => false,
                                'message' => 'Something Went Wrong',
                                'c_code'  =>2
                            );
                        }
                    }
                }
                else
                {
                    $result = array(
                        'status' => false,
                        'message' => 'Account not exist in DB',
                        'c_code'  =>2
                    );
                }
            }

        }catch(\Exception $e){	

            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    public static function getBankInfo()
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $getModel = DB::table('pq_beneficiary_details')
                            ->where('pbd_loginid', '=', $login_id)
                            ->first();

        return $getModel;
    }

    public static function listBanks()
    {
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        $getModel = DB::table('pq_beneficiary_details')
                            ->where('pbd_loginid', '=', $login_id)
                            ->get();

        return $getModel;
    }

    //Confirm bank - Creates the beneficiary account in payout
    public function confirmBank(Request $request)
    {
        $input = $request->all();
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        try
        {
            $bene_id = $input['radioBeneId'];

            $getBeneficiaryDetails = DB::table('pq_beneficiary_details')
                                    ->where('pbd_id', '=', $bene_id)
                                    ->first();

            if(isset($getBeneficiaryDetails))
            {
                $encryptedWalletId = Crypt::encryptString($getBeneficiaryDetails->pbd_wallet_id);
                $encryptedBeneId = Crypt::encryptString($getBeneficiaryDetails->pbd_bene_id);

                $result = array(
                    'status' => true,
                    'message' => 'Account exist',
                    'id' => $encryptedWalletId,
                    'bene_id' => $encryptedBeneId,
                    'c_code'  =>1
                );
            }
            else
            {
                $result = array(
                    'status' => false,
                    'message' => 'Account not exist',
                    'c_code'  =>2
                );
            }
               
        }
        catch(\Exception $e){	

            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2
            );
        }

        return response()->json($result);
    }

    //debit amount
    public function debitAmount(Request $request)
    {
        $input = $request->all();
        $session_val = session()->all();
        $login_id = $session_val['pli_loginId'];

        try
        {
            $encryptedWalletId = $input['walletid'];
            $encrypedBeneId = $input['BeneId'];
            $decryptedWalletId = Crypt::decryptString($encryptedWalletId);
            $decryptedBeneId = Crypt::decryptString($encrypedBeneId);
            $amount = $input['pbd_amount'];

            $buyer_logid	= $input['login_username'];
            $buyer_password	= $input['login_password'];

            $passwordDetails =  DB::table('pq_login_info')
                                ->select('pli_loginid','pli_password','pli_login_status','pli_sno','pli_comp_id','pci_comp_type_cat','pli_con_name','pli_login_type')
                                ->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_login_info.pli_comp_id')
                                ->where('pli_loginid', $login_id)
                                ->first();

            if(count($passwordDetails)==1) 
            {
                if($passwordDetails->pli_login_status == "A")
                {            
                    $hash= $passwordDetails->pli_password;
			  
                    if(Hash::check($buyer_password, $hash))
                    {
                        $authenticate = $this->authenticate(); //authenticate payout
                        $statusCode = $authenticate->subCode; //response code        

                        $dt 	= new DateTime();
                        $date	= $dt->format('Y-m-d H:i:s');

                        if($statusCode == "200")
                        {
                            $token_data = $authenticate->data; //token data
                            $token = $token_data->token; //access token
                            $transfer_id = 'TRANS'.uniqid();

                            //request transfer
                            $client  = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
                            
                            $url = 'https://payout-gamma.cashfree.com/payout/v1/requestTransfer';

                            $post_data = [
                                'beneId'  	=> $decryptedBeneId,
                                'amount'    => $amount,
                                'transferId' => $transfer_id,
                            ];

                            $res = $client->post($url,['json' => $post_data, 'headers' => ['Authorization' => 'Bearer ' . $token]]);
                            $result = $res->getBody()->getContents();		
                            
                            $reponse_data1 = json_decode($result);

                            if($reponse_data1->subCode == "200")
                            {
                                $user_os 		= PurchaseController::getOS();	
                                $user_browser   = PurchaseController::getBrowser();

                                $getWalletModel = DB::table('pq_wallet_current')
                                                    ->where('pwc_login_id', '=', $login_id)
                                                    ->where('pwc_wallet_id', '=', $decryptedWalletId)
                                                    ->first();

                                if(isset($getWalletModel))
                                {
                                    $lat_balance = $getWalletModel->pwc_current_balance - $amount;

                                    $updateLatWallet = [
                                        'pwc_current_balance' => $lat_balance,
                                        'pwc_ipaddress' 			=>  $_SERVER['REMOTE_ADDR'],
                                        'pwc_browser' 				=>  $user_browser['name'],                           
                                        'pwc_browser_ver' 			=>  $user_browser['version'],                           
                                        'pwc_os' 					=>  $user_os,
                                        'pwc_updated_at'    => $date
                                    ];

                                    $updateWalletModel = DB::table('pq_wallet_current')
                                                            ->where('pwc_login_id', '=', $login_id)
                                                            ->where('pwc_wallet_id', '=', $decryptedWalletId)
                                                            ->update($updateLatWallet);

                                    if($updateWalletModel)
                                    {
                                        $insertHistory = [
                                            'pwh_wallet_id' => $decryptedWalletId,
                                            'pwh_balance' => $amount,
                                            'pwh_do' => 2, //spend
                                            'pwh_for' => '8',
                                            'pwh_receiver_loginid' => $login_id,
                                            'pwh_receiver_wallet_id' => $decryptedWalletId,
                                            'pwh_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                            'pwh_browser' => $user_browser['name'],  
                                            'pwh_browser_ver' =>  $user_browser['version'],
                                            'pwh_os' => $user_os,
                                            'pwh_loginid' => $login_id,
                                        ];
                
                                        $historyInsert = DB::table('pq_wallet_history')->insert($insertHistory);

                                        if($historyInsert)
                                        {
                                            $result = array(
                                                'status' => true,
                                                'message' => 'Amount debited to your account',
                                                'c_code'  => 1
                                            );
                                        }
                                        else
                                        {
                                            $result = array(
                                                'status' => false,
                                                'message' => 'Something went wrong',
                                                'c_code'  => 2
                                            );
                                        }
                                    }
                                    else
                                    {
                                        $result = array(
                                            'status' => false,
                                            'message' => 'Wallet not updated',
                                            'c_code'  => 2
                                        );
                                    }

                                }
                                else
                                {
                                    $result = array(
                                        'status' => false,
                                        'message' => 'Wallet not found',
                                        'c_code'  => 2
                                    );
                                }                   
                            }
                            else
                            {
                                $result = array(
                                    'status' => false,
                                    'message' => 'Transfer failed',
                                    'c_code'  => 2
                                );
                            }

                            $insertTransactionHistory = [
                                'pwtt_bene_id' => $decryptedBeneId,
                                'pwtt_amount' => $amount,
                                'pwtt_transfer_id' => $transfer_id,
                                'pwtt_loginid' => $login_id,
                                'pwtt_status' => $reponse_data1->status,
                                'pwtt_subcode' => $reponse_data1->subCode,
                                'pwtt_data' => json_encode($reponse_data1->data),
                                'pwtt_ipaddress' => $_SERVER['REMOTE_ADDR'],
                                'pwtt_browser' => $user_browser['name'],  
                                'pwtt_browser_ver' =>  $user_browser['version'],
                                'pwtt_os' => $user_os,
                            ];

                            $historyTransactionInsert = DB::table('pq_wallet_transaction_track')->insert($insertTransactionHistory);
                        }
                        else
                        {
                            $result = array(
                                'status' => false,
                                'message' => $authenticate->message,
                                'c_code'  => 2
                            );
                        }
                    }
                    else
                    {
                        $result = array(
                            'status'  => false,
                            'message' => "Password incorrect",
                            'c_code'  =>2 		
              
                        );
                    }
                }
                else
                {
                    $result = array(
                        'status'  => false,
                        'message' => "Email Id Not Conform ",
                        'c_code'  =>2	 		
          
                    );
                }
            }
            else{
                $result = array(
                    'status'  => false,
                    'message' => "Invalid email address and password",
                    'c_code'  =>2	 		
      
                );
            } 

        }catch(\Exception $e){	

            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2
            );
        }

        return response()->json($result);

    }

    //Authenticate Payout
    public function authenticate()
    {
        $client    = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
		
        $url = 'https://payout-gamma.cashfree.com/payout/v1/authorize';

        $res = $client->post($url,['headers' => ['X-Client-Id' => 'CF4470FO960S9MCFYYEUA','X-Client-Secret' 	=> 'ebd18661cf4b4bfc459700c626a9a37e44b02ac8']]);
		$result = $res->getBody()->getContents();		
		
        $respose = json_decode($result);
        
        return $respose;
    }
}
