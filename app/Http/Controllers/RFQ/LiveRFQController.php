<?php

namespace App\Http\Controllers\RFQ;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RFQ\RFQ_Event_Create;
use Carbon\Carbon;
use App\Models\RFQ\RFQ_Support_Document;
use App\Models\RFQ\RFQ_Live_Commercial_Document;
use App\Models\RFQ\RFQ_Live_Technical_Document;
use Storage;
use App\Imports\RFQLiveCommercialImport;
use App\Imports\RFQLiveTechnicalImport;
use Maatwebsite\Excel\Facades\Excel;
use Crypt;
use App\Models\RFQ\RFQ_Live_Invite;
use App\Models\Wallet\PQ_Wallet_Current;
use App\Models\RFQ\RFQ_Technical_Document;
use App\Models\RFQ\RFQ_Commercial_Document;
use App\Exports\RFQ_TechincalExport;
use App\Exports\RFQ_CommercialExport;

class LiveRFQController extends Controller
{
    public function liverfq($id){
    	$get_invite=RFQ_Live_Invite::find(decrypt($id));
        $fetch=RFQ_Event_Create::where('id',$get_invite->rfq_create_id)->whereDate('start_date_time','<',Carbon::now()->format('Y-m-d H:i'))->get()->first();
        return view('RFQ.liverfq')->with(['fetch'=>$fetch,'get_invite'=>$get_invite]);
    }

    public function uploaddocument(Request $request){
    	
    	$support = $request->file('support_document');
        $filename_support = time().$support->getClientOriginalName();
        Storage::disk('local')->putFileAs(
            'live_support/',
            $support,
            $filename_support
        );

        $tech_document = $request->file('technical_document');
        $filename_tech_document = time().$tech_document->getClientOriginalName();
        Storage::disk('local')->putFileAs(
            'live_technical/',
            $tech_document,
            $filename_tech_document
        );

        $commercial_document = $request->file('commercial_document');
        $filename_commercial_document = time().$commercial_document->getClientOriginalName();
        Storage::disk('local')->putFileAs(
            'live_commercial/',
            $commercial_document,
            $filename_commercial_document
        );

        $sup=new RFQ_Support_Document;
        $sup->rfq_inivte_id=$request->input('id');
        $sup->support_document_path='app/live_support/';
        $sup->support_document_file=$filename_support;
        $sup->save();

        $tech = Excel::toArray(new RFQLiveTechnicalImport, $request->file('technical_document'));
        $tech_ary=array_splice($tech[0], 1);
        foreach ($tech_ary as $value) {
            $technical_insert=new RFQ_Live_Technical_Document;
            $technical_insert->rfq_inivte_id=$request->input('id');
            $technical_insert->technical_document_id=$value[0];
            $technical_insert->input=$value[3];
            $technical_insert->remarks=$value[4];
            $check=RFQ_Technical_Document::find($value[0]);
            if($value[3] == "yes"){
                $technical_insert->rating=$check->rate;
            }else{
                $technical_insert->rating=0;
            }
            
            $technical_insert->save();
        }

        $commercial=Excel::toArray(new RFQLiveCommercialImport, $request->file('commercial_document'));

        $commercial_ary=array_splice($commercial[0], 1);
        foreach ($commercial_ary as $value) {
            $commercial_insert=new RFQ_Live_Commercial_Document;
            $commercial_insert->rfq_inivte_id=$request->input('id');
            $commercial_insert->commercial_document_id=$value[0];
            $commercial_insert->input=$value[6];
            $commercial_insert->remarks=$value[7];
            $commercial_insert->save();
        }

         return redirect()->back()->with('msg', 'The Message');
    }

    public function paidlive(Request $request){

    	$session_val = session()->all();
    	$update=PQ_Wallet_Current::where('pwc_login_id',$session_val['pli_loginId'])->get()->first();
    	$final=$update->pwc_current_balance-$request->input('get_amount');
    	PQ_Wallet_Current::where('pwc_login_id',$session_val['pli_loginId'])->update(['pwc_current_balance'=>$final]);

    	RFQ_Live_Invite::where('id',$request->input('id'))->update(['amount_paid'=>'true']);
    	return  redirect()->route('liverfq',['id'=>encrypt($request->input('id'))]);
;
    }

    public function downloadtech($id){
        $get=RFQ_Live_Invite::find($id);
        return Excel::download(new RFQ_TechincalExport($get->rfq_create_id), 'techincal.xlsx');
    }

    public function downloadcomm($id){
        $get=RFQ_Live_Invite::find($id);
        return Excel::download(new RFQ_CommercialExport($get->rfq_create_id), 'commercial.xlsx');
    }
}
