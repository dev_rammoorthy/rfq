<?php

namespace App\Http\Controllers\RFQ;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\RFQ\RFQ_Event_Create;
use App\Models\RFQ\RFQ_Technical_Document;
use App\Models\RFQ\RFQ_Commercial_Document;
use App\Models\RFQ\RFQ_Live_Invite;
use Storage;
use URL;
use Carbon\Carbon;
use App\Imports\RFQTechExport;
use App\Imports\RFQCommercialExport;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use App\Mail\RFQ\InviteRFQ;
use App\Models\Users\PQ_Login_Info;
use Crypt;

class RFQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $session_val = session()->all();
        $fetch=RFQ_Event_Create::where('login_id',$session_val['pli_sno'])->whereDate('start_date_time','<',Carbon::now()->format('Y-m-d H:i'))->get()->toArray();
        return view('RFQ.rfq_index')->with('fetch', $fetch);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('RFQ/rfqcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        /*print_r($request->all());

        die();*/

        $catalogue = $request->file('catalogue');
        $filename_catalogue = time().$catalogue->getClientOriginalName();
        Storage::disk('local')->putFileAs(
            'catalogue/',
            $catalogue,
            $filename_catalogue
        );

        if($request->input('count_xls')){
            $tech_document = $request->file('tech_document');
            $filename_tech_document = time().$tech_document->getClientOriginalName();
            Storage::disk('local')->putFileAs(
                'technical/',
                $tech_document,
                $filename_tech_document
            ); 
        }

        if($request->input('comm_count_xls')){
            $commercial_document = $request->file('commercial_document');
            $filename_commercial_document = time().$commercial_document->getClientOriginalName();
            Storage::disk('local')->putFileAs(
                'commercial/',
                $commercial_document,
                $filename_commercial_document
            );
        }      

        

        $session_val = session()->all();

        $insert=new RFQ_Event_Create;
        $insert->login_id=$session_val['pli_sno'];
        $insert->event_name=$request->input('event_name');
        $insert->description=$request->input('description');
        $insert->catalogue_path='app/catalogue/';
        $insert->catalogue_file=$filename_catalogue;
        $insert->start_date_time=Carbon::parse($request->input('start_date_time'))->format('Y-m-d H:i');
        $insert->end_date_time=Carbon::parse($request->input('end_date_time'))->format('Y-m-d H:i');
        $insert->location=$request->input('location');
        $insert->emd=$request->input('emd');
        $insert->rfq_fees=$request->input('rfq_fees');
        
        if($request->input('count_xls')){
            $insert->technical_document_path='app/technical/';
            $insert->technical_document_file=$filename_tech_document; 
        }

        if($request->input('comm_count_xls')){
            $insert->commercial_document_path='app/commercial/';
            $insert->commercial_document_file=$filename_commercial_document;
        }
        
        $insert->save();

        if($request->input('count_xls')){
            $tech = Excel::toArray(new RFQTechExport, $request->file('tech_document'));
            $tech_ary=array_splice($tech[0], 1);
            foreach ($tech_ary as $value) {
                $technical_insert=new RFQ_Technical_Document;
                $technical_insert->rfq_create_id=$insert->id;
                $technical_insert->technical_quality_criteria=$value[0];
                $technical_insert->estimated_rate=$value[1];
                $technical_insert->rate=$value[2];
                $technical_insert->save();
            }
        }



        if($request->input('count_form')){
            for ($i=0; $i < $request->input('count_form'); $i++) {
                
                $set_var=$i+1;

                $technical_insert=new RFQ_Technical_Document;
                $technical_insert->rfq_create_id=$insert->id;
                $technical_insert->technical_quality_criteria=$request->input('criteria'.$set_var);
                $technical_insert->estimated_rate=$request->input('estimated'.$set_var);
                $technical_insert->rate=$request->input('rate'.$set_var);
                $technical_insert->save();
            } 
        } 

        if($request->input('comm_count_xls')){
            $commercial=Excel::toArray(new RFQCommercialExport, $request->file('commercial_document'));

            $commercial_ary=array_splice($commercial[0], 1);
            foreach ($commercial_ary as $values) {

                $commercial_insert=new RFQ_Commercial_Document;
                $commercial_insert->rfq_create_id=$insert->id;
                $commercial_insert->item_no=$values[0];
                $commercial_insert->item_description=$values[1];
                $commercial_insert->item_code=$values[2];
                $commercial_insert->quantity=$values[3];
                $commercial_insert->units=$values[4];
                $commercial_insert->estimate_rate=$values[5];
                $commercial_insert->save();
            }  
        }

        for ($i=0; $i < $request->input('comm_count_form'); $i++) {

            $comm_set_var=$i+1;

            $commercial_insert=new RFQ_Commercial_Document;
            $commercial_insert->rfq_create_id=$insert->id;
            $commercial_insert->item_no=$request->input('comm_item_num'.$comm_set_var);
            $commercial_insert->item_description=$request->input('comm_item_desc'.$comm_set_var);
            $commercial_insert->item_code=$request->input('comm_item_code'.$comm_set_var);
            $commercial_insert->quantity=$request->input('comm_item_quantity'.$comm_set_var);
            $commercial_insert->units=$request->input('comm_item_units'.$comm_set_var);
            $commercial_insert->estimate_rate=$request->input('comm_item_rate'.$comm_set_var);
            $commercial_insert->save();

        }

        return redirect('/rfq');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inviterfq(Request $request){
        $session_val = session()->all();

        $check_email=PQ_Login_Info::where('pli_loginid',$request->input('email'))->get()->first();

        if($check_email){
            $get_email=$check_email->pli_loginid;
        }else{
            $get_email=$request->input('email');
        }

        $email=new RFQ_Live_Invite;
        $email->rfq_create_id=$request->input('pass_id');
        $email->from_login_id=$session_val['pli_sno'];
        if($check_email){
            $email->to_login_id=$check_email->pli_sno;
        }else{
            $email->to_email=$request->input('email');
        }
        $email->save();

        $content=[
            'pass_id'=>encrypt($email->id),
        ];

        //Mail::to($request->input('email'))->send(new InviteRFQ($content));
        Mail::to('ramamoorthy@proctek.in')->send(new InviteRFQ($content));

        return redirect('/rfq');
    }
}
