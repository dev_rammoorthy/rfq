<?php

namespace App\Http\Controllers\RFQ;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\RFQ\RFQ_Event_Create;
use Carbon\Carbon;
use DB;
use App\Models\RFQ\RFQ_Technical_Document;
use App\Models\RFQ\RFQ_Commercial_Document;
use App\Models\RFQ\RFQ_Live_Invite;
use App\Models\RFQ\RFQ_Live_Technical_Document;
use App\Models\RFQ\RFQ_Live_Commercial_Document;
use App\Models\Users\PQ_Login_Info;

class CompletedRFQController extends Controller
{
    public function getcompleted(){
    	$session_val = session()->all();
    	$get_data=RFQ_Event_Create::where('end_date_time','<',Carbon::now()->format('Y-m-d H:i'))->get();
    	return view('RFQ.completedrfq')->with('get_data',$get_data);
    }

    public function detailrfq($id){
    	
    	$view_data=RFQ_Event_Create::find(decrypt($id));
        $get_invite=RFQ_Live_Invite::where('rfq_create_id',$view_data->id)->get();
        
        $upload_tech=RFQ_Technical_Document::where('rfq_create_id',$view_data->id)->get();
    	$get_tech=[];
        foreach ($get_invite as $value) {
            $get_log=PQ_Login_Info::where('pli_sno',$value->to_login_id)->get()->first();
            $user['username']=$get_log->pli_con_name;
            $user['tech']=RFQ_Live_Technical_Document::where('rfq_inivte_id',$value->id)->get();
            $user['rating']= RFQ_Live_Technical_Document::where('rfq_inivte_id',$value->id)->sum('rating');
           array_push($get_tech, $user);
        }

        $upload_comm=RFQ_Commercial_Document::where('rfq_create_id',$view_data->id)->get();
        $get_comm=[];
        foreach ($get_invite as $values) {
            $get_log=PQ_Login_Info::where('pli_sno',$values->to_login_id)->get()->first();
            $comm['username']=$get_log->pli_con_name;
            $comm['comm']=RFQ_Live_Commercial_Document::where('rfq_inivte_id',$values->id)->get();
            $comm['final']=RFQ_Live_Commercial_Document::where('rfq_inivte_id',$values->id)->sum('input');
           array_push($get_comm, $comm);
        }

        $ary_tech=[];

        foreach ($get_invite as $keyed) {
            $low_tech= RFQ_Live_Technical_Document::where('rfq_inivte_id',$keyed->rfq_create_id)->sum('rating');

            array_push($ary_tech, $low_tech);
        }

        $get_min_tech=min($ary_tech);


        $final_acc=[];
        foreach ($get_invite as $valued) {
            $get_log=PQ_Login_Info::where('pli_sno',$valued->to_login_id)->get()->first();
            $set_amt['username']=$get_log->pli_con_name;

            $set_amt['final']=RFQ_Live_Commercial_Document::where('rfq_inivte_id',$valued->id)->sum('input');

            $set_amt['rating']= RFQ_Live_Technical_Document::where('rfq_inivte_id',$valued->id)->sum('rating');

            $set_amt['tech_rat']= RFQ_Technical_Document::where('rfq_create_id',$valued->rfq_create_id)->sum('rate');

            $set_amt['comm_rat']= RFQ_Commercial_Document::where('rfq_create_id',$view_data->id)->sum('estimate_rate');

            $set_amt['id']= $valued->id;

            $set_amt['bidd_percent']=100*$get_min_tech/$set_amt['final'];

            $set_amt['tech_percent']=number_format((float)0.7*$set_amt['rating']+0.3*$set_amt['bidd_percent'], 2, '.', '');

            array_push($final_acc, $set_amt);
        }

        $recom=max(array_column($final_acc, 'tech_percent'));

    	return view('RFQ.rfqtable')->with(['get_tech'=>$get_tech,'upload_tech'=>$upload_tech,'upload_comm'=>$upload_comm,'get_comm'=>$get_comm,'final_acc'=>$final_acc,'recom'=>$recom]);
    }

    public function rfqaccept(Request $request){
       
       RFQ_Live_Invite::where('id',$request->input('accept'))->update(['accept_status'=>'accept']);
       return redirect('/rfq');

    }
}
