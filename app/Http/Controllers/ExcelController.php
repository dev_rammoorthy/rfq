<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Excel;
use DateTime;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use File;
use View;
use Exception;
use Validator;
use App\Imports\EventItemImport;
use App\Imports\ClosedEventItemImport;

class ExcelController extends Controller
{

	public static function downloadsamplelot()
    {
        $dir='sample_item_template/';

        $file = $dir.'Sample_RA.ods';

        if (file_exists($file))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
	}

	public static function downloadsampleclosed()
    {
        $dir='sample_item_template/';

        $file = $dir.'Sample_CA.ods';

        if (file_exists($file))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
	}
	
	public function excelitemupload(Request $request)
    {
		$eventId = $request['hfEventId'];

		$validator = Validator::make($request->all(), [
			'excel' => 'required',
			'excel'   => 'mimes:ods'
		]);

		if($request->hasFile('excel')){			

			if ($validator->fails()) {

				$result = array(
					'status' => false,
					'message' => "please upload only ods file",
					);					

				// $message = "please upload only xlsx,ods file";
				// $status = 'failure';
			}
			else
			{
				$data = Excel::import(new EventItemImport($eventId), request()->file('excel'));

				$result = array(
					'status' => true,
					'message' => "Item imported successfully"
					);

				// $message = "Item imported successfully!";
				// $status = 'success';				
			}
		}
		else
		{
			// $message = "File not found";
			// $status = 'failure';		
			
			$result = array(
				'status' => false,
				'message' => "File not found",
				);

		}

		//return back()->with($status,$message);
		return response()->json($result);
		
    }
	
	public function excelcloseditemupload(Request $request)
    {
		$eventId = $request['hfClosedEventId'];

		$validator = Validator::make($request->all(), [
			'excel' => 'required',
			'excel'   => 'mimes:ods'
		]);

		if($request->hasFile('excel')){			

			if ($validator->fails()) {

				$result = array(
					'status' => false,
					'message' => "please upload only ods file",
					);					

				// $message = "please upload only xlsx,ods file";
				// $status = 'failure';
			}
			else
			{
				$data = Excel::import(new ClosedEventItemImport($eventId), request()->file('excel'));

				$result = array(
					'status' => true,
					'message' => "Item imported successfully"
					);

				// $message = "Item imported successfully!";
				// $status = 'success';				
			}
		}
		else
		{
			// $message = "File not found";
			// $status = 'failure';		
			
			$result = array(
				'status' => false,
				'message' => "File not found",
				);

		}

		//return back()->with($status,$message);
		return response()->json($result);
		
    }	

}
