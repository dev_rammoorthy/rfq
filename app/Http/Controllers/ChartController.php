<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Charts;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    //
    public function index()
    {
        $session = session()->all();
		
        $UserId = $session['pli_loginId'];
        
        $basicEvents = DB::table('pq_event_create')
                        ->where('pec_event_loginid', $UserId)		
                        ->where('pec_event_pkg_id', 1)
                        ->count();
                        
        $standardEvents = DB::table('pq_event_create')
                        ->where('pec_event_loginid', $UserId)		
                        ->where('pec_event_pkg_id', 2)
                        ->count();     
                        
        $premiumEvents = DB::table('pq_event_create')
                        ->where('pec_event_loginid', $UserId)		
                        ->where('pec_event_pkg_id', 3)
                        ->count();
                    
        // $chart = Charts::database($events, 'bar', 'highcharts')
		// 	      ->title("Monthly new Register Users")
		// 	      ->elementLabel("Total Users")
		// 	      ->dimensions(1000, 500)
		// 	      ->responsive(false)
        //           ->groupByMonth(date('Y'), true);

        //events created by package type
        $chart = Charts::create('donut', 'highcharts')
                    ->title('Events created')
                    ->labels(['Basic', 'Standard', 'Premium'])
                    ->values([$basicEvents,$standardEvents,$premiumEvents])
                    ->dimensions(1000,500)
                    ->responsive(false);
        
        $pie  =	 Charts::create('pie', 'highcharts')
				    ->title('Sellers Invited')
				    ->labels(['First', 'Second', 'Third'])
				    ->values([5,10,20])
				    ->dimensions(1000,500)
                    ->responsive(false);
                    
        $line  =	 Charts::create('line', 'highcharts')
                        ->title('My nice chart')
                        ->elementLabel('My nice label')
                        ->labels(['First', 'Second', 'Third'])
                        ->values([5,10,20])
                        ->dimensions(1000,500)
                        ->responsive(false);

        $area = Charts::create('area', 'highcharts')
                    ->title('My nice chart')
                    ->elementLabel('My nice label')
                    ->labels(['First', 'Second', 'Third'])
                    ->values([5,10,20])
                    ->dimensions(1000,500)
                    ->responsive(false);

        $areaspline  = Charts::multi('areaspline', 'highcharts')
                        ->title('My nice chart')
                        ->colors(['#ff0000', '#ffffff'])
                        ->labels(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday', 'Sunday'])
                        ->dataset('John', [3, 4, 3, 5, 4, 10, 12])
                        ->dataset('Jane',  [1, 3, 4, 3, 3, 5, 4]);

        $geo  = Charts::create('geo', 'highcharts')
                            ->title('My nice chart')
                            ->elementLabel('My nice label')
                            ->labels(['ES', 'FR', 'RU'])
                            ->colors(['#C5CAE9', '#283593'])
                            ->values([5,10,20])
                            ->dimensions(1000,500)
                            ->responsive(false);

        $percentage   = Charts::create('percentage', 'justgage')
                        ->title('My nice chart')
                        ->elementLabel('My nice label')
                        ->values([65,0,100])
                        ->responsive(false)
                        ->height(300)
                        ->width(0);
                 
                 
        return view('chart',compact('chart', 'pie', 'line', 'area', 'areaspline', 'geo', 'percentage'));
    }
}
