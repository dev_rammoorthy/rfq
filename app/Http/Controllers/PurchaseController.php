<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Mail\reminder;
use validator;
use App\EmailChecker;
use Mail;  
use App\GetListuser;
use Illuminate\Support\Facades\Cache;
use DateTime;
use Exception;
use URL;
use Hash;
use Crypt;
use Session;
use App\purchaseInvite;
use OpenPayU;
use OpenPayU_Configuration;
use OpenPayU_Order;
use Tzsk\Payu\Facade\Payment;
use Charts;

class PurchaseController extends Controller
{
	
	public function testmail()
	{		
		Mail::send('emails.register-conform',['name','kwuy'],function($message){
			$message->to('support@purchasequick.in','To Kuwy')->subject('Test Email');
			$message->from('support@purchasequick.in','hari');
		});
		
		dd('Mail send successfully');
			
	} 
	
	public static function getGstData()
	{
		
		$client    = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
		$url 	   = 'https://commonapi.mastersindia.co/oauth/access_token';
		$post_data = [
							'username'  	=> "pravin@matexnet.com",
							'password' 		=> "praviN@123#",
							'client_id' 	=> "IvVNBppYkYtQpoDIop",
							'client_secret' => "HLX5fRyUY0dNWp9f6vbXSwQp",
							'grant_type' 	=> "password"
					];

	try{
		
		$res = $client->post($url, ['json' => $post_data,'headers' => ['Content-type' => 'application/json']]);
		$resposeParam = $res->getBody()->getContents();
		
		$results = json_decode($resposeParam);
		//print_r($respose->access_token);die;

		if(!isset($results->access_token)){
			
			$res = $client->post($url, ['json' => $post_data,'headers' => ['Content-type' => 'application/json']]);
			$resposeParam = $res->getBody()->getContents();
			$results = json_decode($resposeParam);
			//$results=json_decode($respose);
		}
	}catch(\Exception $e)
	{	
		$results = array(
			'status'  => false,
			'message' => $e->getMessage(),
			'c_code'  => 2	 			
		
		);
	}
		return $results;
	}
	public function payment(Request $request){

		$data   = $request->all(); 
		$session_val = session()->all(); 
		
		session(['package_data' => $data ]);
		//print_r(	$data  );die;
		$attributes = [
			'txnid' => strtoupper(str_random(8)), # Transaction ID.
			//'amount' => $data['pmp_pkg_amount'], # Amount to be charged.
			'amount' => 5,
			'productinfo' => $data['pmp_pkg_name'].' Plan' ,
			'firstname' => $session_val['pli_con_name'], # Payee Name.
			'email' =>$data['pli_loginId'], # Payee Email Address.
			'phone' => $data['pci_comp_phone'], # Payee Phone Number.
		];
		
		return Payment::make($attributes, function ($then) {
			$then->redirectTo('payment/status');
			
		});
	}
	public function status(Request $request){

		$payment = Payment::capture();

// Get the payment status.
		$payment->isCaptured(); # Returns boolean - true / false

		
		if($payment->isCaptured()){
			$session_val = session()->all();

			$this->create_event($session_val);
		}else{
				$encrypted1 = Crypt::encryptString("0");
				Redirect::to('/payment-status/fail/'.$encrypted1)->send();	
		}
		
		//$then->redirectTo('addevent');

	}
	
	public function getCompanyDetails(Request $request)
	{
		$data   = $request->all(); 
		$gst=$data['gst'];
		$token=$data['token'];
		$client = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
	
		$url 	   = 'https://commonapi.mastersindia.co/commonapis/searchgstin?gstin='.strtoupper($gst);
		

		//['headers' => ['Content-type' => 'application/json','client_id' 	=> 'IvVNBppYkYtQpoDIop','Authorization' => 'Bearer ' . $token]]
		
		try{
		$res = $client->get($url,['headers' => ['Content-type' => 'application/json','client_id' 	=> 'IvVNBppYkYtQpoDIop','Authorization' => 'Bearer ' . $token]]);
		$result = $res->getBody()->getContents();
		
		
		$respose = json_decode($result);
		
	
		if($respose->error =='invalid_grant'){

			$GData=$this->getGstData();
			$token=$GData->access_token;
			$client = new \GuzzleHttp\Client(['verify' => 'C:/wamp64/bin/php/php7.1.26/extras/ssl/cacert.pem']);
			$res = $client->get($url,['headers' => ['Content-type' => 'application/json','client_id' 	=> 'IvVNBppYkYtQpoDIop','Authorization' => 'Bearer ' . $token]]);
		    $result = $res->getBody()->getContents();
			
		}

		}catch(\Exception $e)
		{	
			$result = array(
				'status'  => false,
				'message' =>  $e->getMessage(),
				'c_code'  =>2	 			
			
			);
        }
		
		return $result;
		
		
	}

	public static function pageLogout($page)
    {	

		$dt = new DateTime();
		
		$datetime = $dt->format('Y-m-d H:i:s');
		
		$session = session()->all();
		
		$UserId = $session['pli_loginId'];

		$pageUpdate = DB::table('pq_page_tracker')
						->where('ppt_loginid', $UserId)			  			  
						->orderBy('ppt_sno', 'desc')->first();

		
		if($pageUpdate->ppt_pages_visited != null)
		{
			 $json_array = json_decode($pageUpdate->ppt_pages_visited,true);
	 
            $json_date = array();
			
            $json_date = array_values($json_array);	
			
		 	$jsonformat = [								
								'PageName'=> $page,
								'Date' =>$datetime
							]; 
		
			array_push($json_date,$jsonformat);


             $format_json = json_encode($json_date);

			$update_array = [
					'ppt_pages_visited' => $format_json ,					
					];
		   
		   					   
		
		$pagetrackUpdate = DB::table('pq_page_tracker')
				->where('ppt_loginid', $UserId)			  
				->where('ppt_sno',$pageUpdate->ppt_sno)			  
				->update($update_array);
		}		
	}
	
	public static function CheckSession($request)
    {
        $session_val = session()->all(); 
		
		if(isset($session_val['pli_login_type']))
		{
			if($session_val['pli_login_type'] == "1")
			{
				return redirect('event');
			}
			elseif($session_val['pli_login_type'] == "2" || $session_val['pli_login_type'] == "3")
			{
				return redirect('live-auction');
			}
		}
		else
		{		
			Redirect::to('/login')->send();		
		}
	
	}
	
	public function loginCheck(Request $request)
    {
        //$request->session()->flush();
		$session_val = session()->all(); 
		
		
		if(isset($session_val['pli_login_type']))
		{
			if($session_val['pli_login_type'] == "1")
			{
				return redirect('event');
			}
			elseif($session_val['pli_login_type'] == "2")
			{
				return redirect('live-auction');
			}
			elseif($session_val['pli_login_type'] == "3"){
			   return redirect('event');
			}
		}
		else
		{
			return view('welcome');
		}
	}

    public function regiterBuyer(Request $request)
    {
						
        $data				 = $request->all();         
		$dt 				 = new DateTime();
		$datetime			 = $dt->format('Y-m-d H:i:s');
		$date				 = $dt->format('Y-m-d');
		$year				 = $dt->format('Y');
        $buyer_companyName 	 = $data['pci_comp_name'];
        $buyer_companyGst 	 = $data['pci_comp_gst'];
        $buyer_companyPan	 = $data['pci_comp_pan'];
        $buyer_orgtype  	 = $data['pci_comp_type'];
        $buyer_phone 		 = $data['pci_comp_phone'];
        $buyer_name  		 = $data['pli_con_name'];
        $buyer_desig 		 = $data['pli_con_desig'];
        $buyer_email  		 = $data['pli_loginid'];
        $buyer_contactMob	 = $data['pli_con_mob'];        
        $buyer_website 		 = $data['pci_comp_website'];
        $comp_establish 	 = $data['pci_comp_establish_date'];
        $comp_type 	 		 = $data['pci_comp_type'];
        $comp_nature 	 	 = $data['pci_comp_nature_business'];
        $comp_type_cat 	 	 = $data['pci_comp_type_cat'];
		$comp_address 	 	 = $data['pci_comp_address'];
		if(isset($data['pci_comp_billing_address'])){
			$comp_comm_address 	 	 = $data['pci_comp_billing_address'];
		}else{
			$comp_comm_address 	 	 = $comp_address;
		}
        $comp_location 	 	 = $data['pci_comp_location'];
		
		//print_r($data);die;
		
		$data['pli_login_token']	 = sha1($buyer_contactMob);
   
         try
		 {
			$session_val = session()->all(); 
			$check_exist = DB::table('pq_login_info')
			->where('pli_loginid', '=',  $buyer_email)
			->first();
			
			if (is_null($check_exist)) 
			{
                    $company_data =	[
                        'pci_comp_name' 			=>  $buyer_companyName,
                        'pci_comp_gst' 				=>  $buyer_companyGst,
                        'pci_comp_pan' 				=>  $buyer_companyPan,
                        'pci_comp_type' 			=>  $buyer_orgtype,
                        'pci_comp_phone' 			=>  $buyer_phone,
                        'pci_comp_website' 			=>  $buyer_website,
                        'pci_comp_establish_date' 	=>  $comp_establish,
                        'pci_comp_type'				=>  $comp_type,
                        'pci_comp_nature_business'	=>  $comp_nature,
                        'pci_comp_type_cat'			=>  $comp_type_cat,
						'pci_comp_address'			=>  $comp_address,
						'pci_commu_address'			=>  $comp_comm_address,
                        'pci_comp_location'			=>  $comp_location,
                        'pci_date_time'				=>  $datetime                        
                    ];

				$companyId= DB::table('pq_company_info')->insertGetId($company_data);
				
                if($companyId)
				{

                    $login_data=	[
                        'pli_con_name' 		=>  $buyer_name,
                        'pli_con_desig'	 	=>  $buyer_desig,
                        'pli_loginid' 		=>  $buyer_email,
                        'pli_con_mob' 		=>  $buyer_contactMob,
                        'pli_login_type' 	=>  '1',
                        'pli_comp_id' 		=>  $companyId,
                        'pli_password' 		=>  Hash::make($buyer_email),
                        'pli_date_time' 	=>  $datetime,
                        'pli_login_status' 	=> 'I',
                        'pli_login_act_date'=>  $datetime,
                        'pli_login_role' 	=> 'U',
                        'pli_login_token' 	=> sha1($buyer_contactMob),
                    ];
        
                $loginId= DB::table('pq_login_info')->insertGetId($login_data);
											
					if($loginId)
					{
						// print_r($loginId);	die;
						 
						 
						 $encrypted = Crypt::encryptString($loginId);
						 $verifictionlink =  URL::to('/users/confirm-email/'.$encrypted.'/'.sha1($data['pli_loginid']));
						 
						 $html = '<div style="width:500px; background-color:#fff; margin:0px auto;">
									<div>
									<img src="http://www.purchasequick.in/img/logo.png">
									</div>
									<div style="background-color:#fff; width:100%; box-shadow:0px 0px 10px 0px hsla(0, 0%, 49%, 0.47); border-radius:4px;">
									<div style="background-color:#146da0; color:#fff; padding:50px 0px; text-align:center; border-radius:4px 4px 0px 0px;">
									<div style="font-size:20px;font-weight:800; margin:0px"><p style="width: 50px;
										height: 50px; margin: 0px auto; background-color: #155f8a; border-radius: 50%; line-height: 50px; text-align: center;">&#10004;</p></div>
									<p style="font-size:19px; font-weight:600; margin:10px 0px; letter-spacing:2px; text-transform:capitalize; font-family:sans-serif;">Activate your Account</p>
									<span style="font-size:13px; color:#add8f1; font-weight:500; margin:10px 0px; text-transform:capitalize; font-family:sans-serif;">"'.$date.'"</span>
									</div>
									<div style="padding:30px 20px; font-size:14px; font-family:sans-serif; line-height:28px; color:#333; font-weight:500; text-align:center;">
									<p style="margin:25px 0px;">Hey.. <span style="color:#146da0">"'.$data['pli_loginid'].'"</span>. You&rsquo;re almost ready to setup your new Registration Here. Simply click the below button to register your new Account.</p>
									<a  title="Register Now" style="display:inline-block; background-color:#f15d1a; padding:10px 20px; text-decoration:none; font-size:14px; font-weight:500; letter-spacing:1px; text-transform:uppercase; color:#fff; border-radius:4px;" href="'.$verifictionlink.'">Confirm Email</a>
									<p style="color:#a5a5a5; margin-top:30px; margin-bottom:0px; line-height:10px; font-size:12px; font-weight:500; font-family:sans-serif;">Email Sent by PurchaseQuik</p>
									<p style="color:#a5a5a5; font-size:12px; margin:0px; line-height:20px; font-weight:500; font-family:sans-serif;">Copyright &copy; "'.$year.'" PurchaseQuik. All rights reserved.</p>
									</div>
									</div>
									</div>';
						 
						 Mail::send([] , [ $data  ], function ($message) use($html,$data)
						{		
							$message->to($data['pli_loginid'],'To Kuwy')->subject('New User Registered');
							$message->from('support@purchasequick.in','Purchase Quick');
							$message->setBody($html, 'text/html');
						});
						
						$result = array(
							'status'  => true,
							'message' => "Register success"			
			  
						);
											
					}        
                }
            }
			elseif($check_exist->pli_login_type =='2')
			{
				 $company_data =	[
                        'pci_comp_name' 			=>  $buyer_companyName,
                        'pci_comp_gst' 				=>  $buyer_companyGst,
                        'pci_comp_pan' 				=>  $buyer_companyPan,
                        'pci_comp_type' 			=>  $buyer_orgtype,
                        'pci_comp_phone' 			=>  $buyer_phone,
                        'pci_comp_website' 			=>  $buyer_website,
                        'pci_comp_establish_date' 	=>  $comp_establish,
                        'pci_comp_type'				=>  $comp_type,
                        'pci_comp_nature_business'	=>  $comp_nature,
                        'pci_comp_type_cat'			=>  $comp_type_cat,
						'pci_comp_address'			=>  $comp_address,
						'pci_commu_address'			=>  $comp_comm_address,
                        'pci_date_time'				=>  $datetime                        
                    ];

			
				$companyId= DB::table('pq_company_info')
				->where('pci_comp_id', '=',  $check_exist->pli_comp_id)
                ->update($company_data);
                if($companyId)
				{

                    $login_data=	[
               
                        'pli_login_type' 	=>  '3',
						'pli_con_name' 		=>  $buyer_name,
                        'pli_con_desig'	 	=>  $buyer_desig,
                        'pli_con_mob' 		=>  $buyer_contactMob
                        
                    ];
        
                $loginId= DB::table('pq_login_info')
				->where('pli_loginid', '=',  $buyer_email)
                ->update($login_data);
										
					if($loginId)
					{
						if(isset($session_val['pli_login_type']))
						{		
				session(['dashboard_check' => '0',
				'pli_con_name' =>  $buyer_name,
				'pli_login_type' => '3']);
						}	
						/*
						// print_r($loginId);	die;
						 
						 
						 $encrypted = Crypt::encryptString($loginId);
						 $verifictionlink =  URL::to('/users/confirm-email/'.$encrypted.'/'.sha1($data['pli_loginid']));
						 
						 $html = '<div style="width:500px; background-color:#fff; margin:0px auto;">
									<div>
									<img src="http://www.purchasequick.in/img/logo.png">
									</div>
									<div style="background-color:#fff; width:100%; box-shadow:0px 0px 10px 0px hsla(0, 0%, 49%, 0.47); border-radius:4px;">
									<div style="background-color:#146da0; color:#fff; padding:50px 0px; text-align:center; border-radius:4px 4px 0px 0px;">
									<div style="font-size:20px;font-weight:800; margin:0px"><p style="width: 50px;
										height: 50px; margin: 0px auto; background-color: #155f8a; border-radius: 50%; line-height: 50px; text-align: center;">&#10004;</p></div>
									<p style="font-size:19px; font-weight:600; margin:10px 0px; letter-spacing:2px; text-transform:capitalize; font-family:sans-serif;">Activate your Account</p>
									<span style="font-size:13px; color:#add8f1; font-weight:500; margin:10px 0px; text-transform:capitalize; font-family:sans-serif;">"'.$date.'"</span>
									</div>
									<div style="padding:30px 20px; font-size:14px; font-family:sans-serif; line-height:28px; color:#333; font-weight:500; text-align:center;">
									<p style="margin:25px 0px;">Hey.. <span style="color:#146da0">"'.$data['pli_loginid'].'"</span>. You&rsquo;re almost ready to setup your new Registration Here. Simply click the below button to register your new Account.</p>
									<a  title="Register Now" style="display:inline-block; background-color:#f15d1a; padding:10px 20px; text-decoration:none; font-size:14px; font-weight:500; letter-spacing:1px; text-transform:uppercase; color:#fff; border-radius:4px;" href="'.$verifictionlink.'">Confirm Email</a>
									<p style="color:#a5a5a5; margin-top:30px; margin-bottom:0px; line-height:10px; font-size:12px; font-weight:500; font-family:sans-serif;">Email Sent by PurchaseQuik</p>
									<p style="color:#a5a5a5; font-size:12px; margin:0px; line-height:20px; font-weight:500; font-family:sans-serif;">Copyright &copy; "'.$year.'" PurchaseQuik. All rights reserved.</p>
									</div>
									</div>
									</div>';
						 
						 Mail::send([] , [ $data  ], function ($message) use($html,$data)
						{		
							$message->to($data['pli_loginid'],'To Kuwy')->subject('New User Registered');
							$message->from('support@purchasequick.in','Purchase Quick');
							$message->setBody($html, 'text/html');
						});
						 */ 
						$result = array(
							'status'  => true,
							'message' => "Register success"			
			  
						);
											
					}      
                }
				
			}
            else 
			{	  
						// It exists - remove from favorites button will show									
				$result = array(
					'status'  => false,
					'message' => "Already Register",
					'c_code'  =>1	 		
	  
				);
		 										
			}

         }
		catch(\Exception $e)
		{	
			$result = array(
				'status'  => false,
				'message' =>  $e->getMessage(),
				'c_code'  =>2	 			
			
			);
        }
    return response()->json($result);
	}
	
	public function checkEmailAvailability(Request $request)
	{
		$data  = $request->all();      
		$buyer_email = $data['email_id'];
		
		try
		{
			$session_val = session()->all(); 
			$check_exist = DB::table('pq_login_info')
							->where('pli_loginid', '=',  $buyer_email)
							->first();

			if (is_null($check_exist)) 
			{
				$result = array(
					'status'  => true,
					'message' => "Email address available",
					'c_code'  =>1	 		
	  
				);
			}
			elseif($check_exist->pli_login_type =='2')
			{
				$result = array(
					'status'  => true,
					'message' => "Email address available",
					'c_code'  =>1	 		
	  
				);
			}
			else 
			{	  
				// It exists - remove from favorites button will show									
				$result = array(
					'status'  => false,
					'message' => "Already Register",
					'c_code'  =>2	 		
	  
				);
		 										
			}
		}
		catch(\Exception $e)
		{	
			$result = array(
				'status'  => false,
				'message' =>  $e->getMessage(),
				'c_code'  =>2	 			
			
			);
        }
    	return response()->json($result);
	}
	
	 public function activateUser($token,$token2)
    {
		//print_r($token2);
		//print_r($token);die;
		
		 $activation = $this->getActivationByToken($token);
			
			//print_r(sha1('hari@gmail.com'));
			//print_r($token2);
												
			if(sha1($activation->pli_loginid) == $token2)
			{
				//print_r($activation);die;
						
				  DB::table('pq_login_info')
                            ->where('pli_sno', $activation->pli_sno)
                            ->update([
                                'pli_login_status' => "A"                               
                                ]);
				return	view('auth.regsucess');
			}
			else
			{
				return view('auth.regfail');
			}
					
			//print_r('a'); die;
			
        $activation = $this->activationRepo->getActivationByToken($token);

        if ($activation === null) 
		{
            return null;
        }

        $user = User::find($activation->user_id);

        $user->activated = true;

        $user->save();

        $this->activationRepo->deleteActivation($token);

        return $user;

    }
	
	public function getActivationByToken($token)
    {
		
		//print_r($token);
		//print_r(sha1('1'));
		//die;
		
		//$encrypted = Crypt::encryptString('1');

		$decrypted = Crypt::decryptString($token);
				
		//print_r($decrypted);
		
        return DB::table('pq_login_info')
				   ->where('pli_sno', $decrypted)->first();
	}
	
	public function pageTrack($page)
	{
		$a=array();
		array_push($a,$page);
		return $a;
	}
	
		
    public function buyerLogin(Request $request)
    {
        $data 			= $request->all(); 
        $buyer_logid	= $data['email'];
        $buyer_password	= $data['password'];
		$data			= $request->all();         
		$dt 			= new DateTime();
		$datetime		= $dt->format('Y-m-d H:i:s');
		$user_os 		= $this->getOS();	
		$user_browser   = $this->getBrowser();	
	
 
         try{

            $passwordDetails =  DB::table('pq_login_info')
            ->select('pli_loginid','pli_password','pli_login_status','pli_sno','pli_comp_id','pci_comp_type_cat','pli_con_name','pli_login_type')
			->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_login_info.pli_comp_id')
            ->where('pli_loginid', $buyer_logid)
            ->first();
            
			
           if(count($passwordDetails)==1) 
		   { 
	   
			if($passwordDetails->pli_login_status == "A")
			{
				
			 $track_data =	[
                        'plia_loginid' 				=>  $passwordDetails->pli_loginid,
                        'plia_comp_id' 				=>  $passwordDetails->pli_comp_id,
                        'plia_login_time' 			=>  $datetime,                 
                        'plia_ipaddress' 			=>  $_SERVER['REMOTE_ADDR'],
						'plia_browser' 				=>  $user_browser['name'],                           
                        'plia_browser_ver' 			=>  $user_browser['version'],                           
                        'plia_os' 					=>  $user_os           						
					];

				$PageHistoryjson = '{"Page":{"PageName":"Event","Date":"'.$datetime.'"}}';
				$pagearray = json_decode($PageHistoryjson, true);
			
				$pagejsondata = array();					
            	$pagejsondata = array_values($pagearray);			
            	$pageHistory = json_encode($pagejsondata);			
					
				$page_tracker =	[
					'ppt_loginid' 				=>  $passwordDetails->pli_loginid,
					'ppt_comp_id' 				=>  $passwordDetails->pli_comp_id,
					'ppt_login_date' 			=>  $datetime,                 
					'ppt_login_ip_address' 		=>  $_SERVER['REMOTE_ADDR'],
					'ppt_browser' 				=>  $user_browser['name'],                           
					'ppt_browser_ver' 			=>  $user_browser['version'],                           
					'ppt_os' 					=>  $user_os,
					'ppt_login_type'			=>  $passwordDetails->pli_login_type,
					'ppt_pages_visited'			=> $pageHistory					
				];

				$trackId= DB::table('pq_login_info_audit')->insertGetId($track_data);
						
			 	if($trackId)
				{
				
				$hash= $passwordDetails->pli_password;
			  
				if ( Hash::check($buyer_password, $hash))
				{
					$pageTrackId= DB::table('pq_page_tracker')->insertGetId($page_tracker);					

					// The passwords match...
					session(
						[
							'pli_loginId' =>  $passwordDetails->pli_loginid,                      
							'pli_sno' =>  $passwordDetails->pli_sno,                      
							'pli_comp_id' =>  $passwordDetails->pli_comp_id,  
							'pci_comp_type_cat'  =>  $passwordDetails->pci_comp_type_cat,                     
							'pli_con_name' =>  $passwordDetails->pli_con_name,                      
							'pli_login_type' =>  $passwordDetails->pli_login_type,                      
							'track_id' =>  $trackId,  
							//'page_track_id' => $pageTrackId,
							'dashboard_check'=>'0'                    
						]
					);

					//$array = $this->pageTrack('event');
					//return $array;

					$result = array(
						'status'  => true,
						'message' => "Login Success",		
						'login_type' => $passwordDetails->pli_login_type		
		
					);
				}else{
					$result = array(
						'status'  => false,
						'message' => "Invalid email address and password",
						'c_code'  =>1	 		
		  
					);
				}
			   }
			   else{
				 $result = array(
                    'status'  => false,
                    'message' => "Invalid Track Details",
                    'c_code'  =>2	 		
      
                );
			 }
			}			
			else{
				 $result = array(
                    'status'  => false,
                    'message' => "Email Id Not Conform ",
                    'c_code'  =>3	 		
      
                );
			}
                      
           }else{

					$result = array(
						'status'  => false,
						'message' => "Invalid email address and password",
						'c_code'  =>4	 		
		  
					);
			 
           }         

        }catch(\Exception $e){	
            $result = array(
                'status'  => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
            
            );
        }

        return response()->json($result);
       
	}
	

	public function forgotpassword(Request $request)
    {
		$data 			= $request->all(); 
		
        $buyer_logid	= $data['pq_email'];         
		$dt 			= new DateTime();
		$datetime		= $dt->format('Y-m-d H:i:s');
		$date			= $dt->format('Y-m-d');
		$year			= $dt->format('Y');
		
	
 
          try{

            $passwordDetails =  DB::table('pq_login_info')
            ->select('pli_loginid','pli_password','pli_login_status','pli_sno','pli_comp_id','pci_comp_type_cat','pli_con_name','pli_login_type')
			->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_login_info.pli_comp_id')
            ->where('pli_loginid', $buyer_logid)
			->first();
			
			$hash = $passwordDetails->pli_password;

			
           if(count($passwordDetails)==1) 
		   { 
	   
				if($passwordDetails->pli_login_status == "A")
				{
					
					$encrypted 		= array('enc'=>Crypt::encryptString($buyer_logid),'year'=>$year,'date'=>$date);		
					$data			= array_merge($data,$encrypted);
									
					Mail::send('emails.event-forget-password',$data,function($message) use ($data)
					{				 
						$message->to($data['pq_email'])->subject('Password Reset');
						$message->from('support@purchasequick.in','Purchase Quick');
					});
					
					$result = array(
						'status'  => true,
						'message' => "Reset Success"	
					
		
					);
			
				}			
				else{
					 $result = array(
						'status'  => false,
						'message' => "Email Id Not Conform ",
						'c_code'  =>3	 		
		  
					);
				}
			
                      
           }else{

					$result = array(
						'status'  => false,
						'message' => "Invalid email address and password",
						'c_code'  =>4	 		
		  
					);
			 
           }         

        }catch(\Exception $e){	
            $result = array(
                'status'  => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
            
            );
        }

        return response()->json($result);
       
    }
	
	public function resetpassword(Request $request)
	{
		$data = $request->all();
		if(isset($data['url'])){
			$email = Crypt::decryptString($data['url']);
		}else{
			$email = $data['email'];
		}
		
		$dt 			= new DateTime();
		$datetime		= $dt->format('Y-m-d H:i:s');
		$date			= $dt->format('Y-m-d');
		$year			= $dt->format('Y');
		$user_os 		= $this->getOS();
		$user_browser   = $this->getBrowser();	
		
		try{
			
			 $passwordDetails =  DB::table('pq_login_info')
            ->select('pli_loginid','pli_password','pli_login_status','pli_sno','pli_comp_id','pci_comp_type_cat','pli_con_name','pli_login_type')
			->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_login_info.pli_comp_id')
            ->where('pli_loginid', $email)
            ->first();
          
						
			if(count($passwordDetails) == 1)
				
			{
				
				 $track_data =	[
                        'plia_loginid' 				=>  $email,
                        'plia_comp_id' 				=>  $passwordDetails->pli_comp_id,
                        'plia_password_reset' 		=>  $datetime,                 
                        'plia_ipaddress' 			=>  $_SERVER['REMOTE_ADDR'],
						'plia_browser' 				=>  $user_browser['name'],                           
                        'plia_browser_ver' 			=>  $user_browser['version'],                           
                        'plia_os' 					=>  $user_os           						
                    ];

				$trackId= DB::table('pq_login_info_audit')->insertGetId($track_data);
								
				if($trackId)
				{
					
					$update_login =	[
			
									'pli_password' 			=>  Hash::make($data['password']),                    
									'pli_password_reset' 	=>  $datetime,      
									'pli_firsttime_login'   =>  '1',            
								];

				
					$loginId= DB::table('pq_login_info')
					->where('pli_loginid', '=',  $email)
					->update($update_login);
					
					if($loginId)
					{
						$data 		= array('email'=>$email,'year'=>$year,'date'=>$date);
									
						Mail::send('emails.password-changed',$data,function($message) use ($data)
						{				 
							$message->to($data['email'])->subject('Password Changed');
							$message->from('support@purchasequick.in','Purchase Quick');
						});
						
						$result = array(
							'status'  => true,
							'message' => "Password Reset Success"
						);
											
					}					
					else{
						
						$result = array(
							'status'  => false,
							'message' => "Invalid Password"			
						);
					
					}
				
				}	
				 else{
					 
				 $result = array(
                    'status'  => false,
                    'message' => "Invalid Track Details",
                    'c_code'  =>2	 		
      
                );
			 }
			}else{
						
						$result = array(
							'status'  => false,
							'message' => "Invalid Password"			
						);
					
					}
			 
			
        }catch(\Exception $e){	
            $result = array(
                'status'  => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
            
            );
        }
		
		return response()->json($result);
		
	}
	
	public function logout_user(Request $request)
    {
		$dt 			= new DateTime();
		$datetime		= $dt->format('Y-m-d H:i:s');
		$session 		= session()->all();
		
		$update_array	= [
							'plia_logout_time' =>   $datetime,                                         
						  ];
						  
		if(isset($session['track_id'])){
		$packageid = DB::table('pq_login_info_audit')
						->where('plia_sno', '=',  $session['track_id'])
						->update($update_array);
		}

		$UserId = $session['pli_loginId'];
		$pageUpdate = DB::table('pq_page_tracker')
						->where('ppt_loginid', $UserId)			  			  
						->orderBy('ppt_sno', 'desc')->first();
						
		$updatelogutarray	= [
							'ppt_logout_date' =>  $datetime,
							 'ppt_logout_ip_address' => $_SERVER['REMOTE_ADDR']
								//'ppt_browser' =>   $this->getBrowser()	
							
						  ];

						  
		$pagelogoutUpdate = DB::table('pq_page_tracker')
				->where('ppt_loginid', $UserId)			  
				->where('ppt_sno',$pageUpdate->ppt_sno)			  
				->update($updatelogutarray);
			
        $request->session()->flush();

        return redirect('/');
	}
	
	 /* get session */

    public static function getSession()
	{
        $data = Session::all();
        if(empty($data['pli_loginId'])){
            Redirect::to('/')->send();
        }else{
            return $data;
        }
        
	}
	public static function checkloginReset($loginID){
		$data=	DB::table('pq_login_info')
		->select('pli_firsttime_login')
		
		->where('pli_loginid', $loginID)
		->first();
		return   $data;
	}

	
	public  function skippopup( Request $request)
	{

		session(['skip' => '1']);	
		return  0;
	}
    public static function clearSession()
	{
        $data = Session::flush();
        return   $data;
    }
	
	public function event_manager(Request $request)
	{				
		//print_r($session_val);
		$session = session()->all();	

		$UserId = isset($session['pli_loginId']) ? $session['pli_loginId'] : null;

		if(isset($UserId))
		{
			$basicEvents = DB::table('pq_event_create')
							->where('pec_event_loginid', $UserId)		
							->where('pec_event_pkg_id', 1)
							->count();
			
			$standardEvents = DB::table('pq_event_create')
						->where('pec_event_loginid', $UserId)		
						->where('pec_event_pkg_id', 2)
						->count();     
						
			$premiumEvents = DB::table('pq_event_create')
						->where('pec_event_loginid', $UserId)		
						->where('pec_event_pkg_id', 3)
						->count();
						
			$chart = Charts::create('donut', 'highcharts')
						->title('Events Created')
						->labels(['Basic', 'Standard', 'Premium'])
						->values([$basicEvents,$standardEvents,$premiumEvents])
						//->dimensions(500,500)
						->responsive(false);

			if(isset($session_val['pli_login_type']))
			{
				if( $session_val['pli_login_type']== "1")
				{
					return view('event_manager.event-creation', compact('chart'));
				}
				elseif($session_val['pli_login_type'] == "2")
				{
					return redirect('live-auction');
				}
			}
			else
			{
				return view('event_manager.event-creation', compact('chart'));
			}
		}
		else
		{
			return redirect('login');
		}		
	}
	
    public static function getPackage($planID=null)
	{
       $query = DB::table('pq_master_package')
       ->select('*');
       if($planID!='')
       {
        $query->where('pq_master_package.pmp_id',$planID);
        $packagellist= $query->first();
       }else{
        $packagellist= $query->get();
       }
      
        return $packagellist;
    }
	
    public static function getBuyerPackage($seession)
	{
	
        $packagellist = DB::table('pq_login_info')
        ->Join('pq_company_info', 'pq_company_info.pci_comp_id' , '=', 'pq_login_info.pli_comp_id')
        ->Join('pq_buynow_package', 'pq_buynow_package.pbp_comp_id' , '=', 'pq_company_info.pci_comp_id')
        ->Join('pq_master_package', 'pq_master_package.pmp_id' , '=', 'pq_buynow_package.pbp_pkg_id')
        ->select('*')
        ->where('pq_login_info.pli_loginid',$seession)
        ->orderBy('pq_buynow_package.pbp_pkg_id')
        ->get();
      
        return $packagellist;
    }

    public static function getNotBuyPackage($session)
	{
        $packagellist = DB::table('pq_login_info')
        	->join('pq_buynow_package', 'pq_buynow_package.pbp_login_id', '=', 'pq_login_info.pli_loginid')
          
        ->Join('pq_master_package', 'pq_master_package.pmp_id' , '!=', 'pq_buynow_package.pbp_pkg_id')
        ->select('*')
        ->where('pq_login_info.pli_loginid',$session)
        ->orderBy('pq_buynow_package.pbp_pkg_id')
        ->get();
      
        return $packagellist;
    }

    public static function getPlanData($plan)
	{
        $planData = DB::table('pq_master_package')
        ->select('*')
        ->where('pmp_id',$plan)
        ->first();
       
        return $planData;
    }
	
	public function errorCode404()
    {
    	return view('errors.404');
    }
	
    public function errorCode405()
    {
    	return view('errors.404');
    }
	
	public function payment_conformation($token)
	{
		$data['pq_master_package'] = DB::table('pq_master_package')
        ->where('pmp_id', '=', $token)
        ->first();
		$session_val = session()->all();
		$pli_comp_id = $session_val['pli_comp_id'];
		$data['pq_company_info'] = DB::table('pq_company_info')
        ->where('pci_comp_id', '=', $pli_comp_id)
        ->first();
		return view('event_manager.confirm-payment',$data);
	}
	
	public function create_event($data)
	{

		$input = $data['package_data'];

	

	
	
	 
		$dt 	= new DateTime();
		$date	= $dt->format('Y-m-d H:i:s');
		
		$str = preg_replace('/\D/', '', $input['pmp_pkg_validity']);
		
		$enddate = date('Y-m-d H:i:s', strtotime($date. ' + '.$str.' days'));
				
		
		 try {
			$package = DB::table('pq_buynow_package')
						->where('pbp_comp_id', '=', $input['pli_comp_id'])
						->where('pbp_pkg_id', '=', $input['pmp_id'])
						
						->first();
						
			
			$packagellist = DB::table('pq_master_package')        
							->select('*')
							->where('pmp_id',$input['pmp_id'])
							->get();
			
			//echo $str;
			//print_r(count($package));die;
			if(isset($package->pbp_pkg_cur_count)){
				$pack_count=$package->pbp_pkg_cur_count;
			}else{
				$pack_count=0;
			}
				
        if ((count($package)==0 )|| ($pack_count==0 ) )
		{
            // It does not exist - add to favorites button will show
				
				$package_data =	[
                        'pbp_comp_id' 		 	=>  $input['pli_comp_id'],
						'pbp_login_id'  		=>  $input['pli_loginId'],
						'pbp_pkg_id'  			=>  $packagellist[0]->pmp_id,
						'pbp_pkg_eventcount' 	=>  $packagellist[0]->pmp_pkg_eventcount,
						'pbp_pkg_cur_count'  	=>  $packagellist[0]->pmp_pkg_eventcount,
						'pbp_pkg_start_dt'  	=>  $date,
						'pbp_pkg_end_dt'  		=>  $enddate,
						'pbp_pkg_status'  		=>  '1',
                        
                    ];
					if(($pack_count==0 )&&(count($package)==0 )){
					$packageid = DB::table('pq_buynow_package')->insertGetId($package_data);
					}
					else{
						
						$packageid = DB::table('pq_buynow_package')
						->where('pbp_id', '=', $package->pbp_id)
						->update($package_data);
						
						
					}
					
				
				
				 //print_r($packageid);
				 
				$encrypted = Crypt::encryptString($packageid);
				
				  //print_r($encrypted);
				  
                if($packageid)
				{		//print_r($encrypted);								
					Redirect::to('/payment-status/success/'.$encrypted.'/'.$input['pmp_id'])->send();		
					
					
                }
        } else {
            // It exists - remove from favorites button will show
			
				$encrypted1 = Crypt::encryptString("0");
				Redirect::to('/payment-status/fail/'.$encrypted1)->send();	
			
                
         
        }       
       // print_r($insertData);die;
          }catch(\Exception $e){	
              $result = array(
                  'status' => false,
                  'message' => $e->getMessage(),	
                  'c_code'  =>2	 			
             
              );
          }

       
	}
	
	public function capturePayment(){
		
		$payment = Payment::capture();

// Get the payment status.
		$status=$payment->isCaptured(); # Returns boolean - true / false

		print_r($status);die;
	}

	public static function getproductList()
	{
        $productList = DB::select('
        SELECT `pmc_id`,`pmc_cat_name` FROM `pq_master_cat` WHERE `pmc_cat_type`="1" ');

        return $productList;
    }
	
	public static function getserviceList() 
	{
        $serviceList = DB::select('
        SELECT `pmc_id`,`pmc_cat_name` FROM `pq_master_cat` WHERE `pmc_cat_type`="2" ');

        return $serviceList;

    }
	
	public function regiterSeller(request $request)
	{
		//print_r($request->all());die;
		
		
		$data				 = $request->all();         
		$dt 				 = new DateTime();
		$datetime			 = $dt->format('Y-m-d H:i:s');
		$date				 = $dt->format('Y-m-d');
		$year				 = $dt->format('Y');
        $seller_companyName  = $data['pci_comp_name'];
        $seller_companyGst 	 = $data['pci_comp_gst'];
        $seller_companyPan	 = $data['pci_comp_pan'];
        $seller_orgtype  	 = $data['pci_comp_type'];
        $seller_phone 		 = $data['pci_comp_phone'];
        $seller_name  		 = $data['pli_con_name'];
        $seller_desig 		 = $data['pli_con_desig'];
        $seller_email  		 = $data['pli_loginid'];
        $seller_contactMob	 = $data['pli_con_mob'];        
        $seller_website 	 = $data['pci_comp_website'];
        $comp_establish 	 = $data['pci_comp_establish_date'];
        $comp_nature 	 	 = $data['pci_comp_nature_business'];
        $comp_type_cat 	 	 = $data['pci_comp_type_cat'];
		$comp_address 	 	 = $data['pci_comp_address'];
		if(isset($data['pci_comp_billing_address'])){
			$comp_comm_address 	 	 = $data['pci_comp_billing_address'];
		}else{
			$comp_comm_address 	 	 = $comp_address;
		}
		
        $comp_autorized_type = $data['pci_comp_autorized_type'];
        $comp_ref_client 	 = $data['pci_comp_ref_client'];
        $comp_service_area 	 = implode(",",$data['pci_comp_service_area']);
        $comp_type_cat_desc  = implode(",",$data['pci_comp_type_cat_desc']);
        $iso_cert  			 = $data['pci_iso_cert'];
        $msim_cert  		 = $data['pci_msim_cert'];
        $other_branch  		 = $data['pci_other_branch'];
        $other_branch_detail = $data['pci_other_branch_detail'];
				
		
		$data['pli_login_token']	 = sha1($seller_contactMob);
   
         try
		  {
			$session_val = session()->all(); 
			$check_exist = DB::table('pq_login_info')
			->where('pli_loginid', '=',  $seller_email)
			->first();
				
			if (is_null($check_exist)) 
			{
				$company_data =	[
					'pci_comp_name' 			=>  $seller_companyName,
					'pci_comp_gst' 				=>  $seller_companyGst,
					'pci_comp_pan' 				=>  $seller_companyPan,
					'pci_comp_type' 			=>  $seller_orgtype,
					'pci_comp_phone' 			=>  $seller_phone,
					'pci_comp_website' 			=>  $seller_website,
					'pci_comp_establish_date' 	=>  $comp_establish,
					'pci_comp_nature_business'	=>  $comp_nature,
					'pci_comp_type_cat'			=>  $comp_type_cat,
					'pci_comp_address'			=>  $comp_address,
					'pci_commu_address'			=>  $comp_comm_address,
					'pci_comp_type_cat_desc'	=>  $comp_type_cat_desc,
					'pci_comp_autorized_type'	=>  $comp_autorized_type,
					'pci_comp_ref_client'		=>  $comp_ref_client,
					'pci_comp_service_area'		=>  $comp_service_area,
					'pci_iso_cert'				=>  $iso_cert,
					'pci_msim_cert'				=>  $msim_cert,
					'pci_other_branch'			=>  $other_branch,
					'pci_other_branch_detail'	=>  $other_branch_detail,
					'pci_date_time'				=>  $datetime
					];

				$companyId= DB::table('pq_company_info')->insertGetId($company_data);
				
                if($companyId)
				{

                    $login_data=	[
                        'pli_con_name' 		=>  $seller_name,
                        'pli_con_desig'	 	=>  $seller_desig,
                        'pli_loginid' 		=>  $seller_email,
                        'pli_con_mob' 		=>  $seller_contactMob,
                        'pli_login_type' 	=>  '2',
                        'pli_comp_id' 		=>  $companyId,
                        'pli_password' 		=>  Hash::make($seller_email),
                        'pli_date_time' 	=>  $datetime,
                        'pli_login_status' 	=> 'I',
                        'pli_login_act_date'=>  $datetime,
                        'pli_login_role' 	=> 'U',
                        'pli_login_token' 	=> sha1($seller_contactMob),
                    ];
        
                $loginId= DB::table('pq_login_info')->insertGetId($login_data);
											
					if($loginId)
					{
						// print_r($loginId);	die;
						 						 
						 $encrypted = Crypt::encryptString($loginId);
						 $verifictionlink =  URL::to('/users/confirm-email/'.$encrypted.'/'.sha1($data['pli_loginid']));
						 
						 $html = '<div style="width:500px; background-color:#fff; margin:0px auto;">
									<div>
									<img src="http://www.purchasequick.in/img/logo.png">
									</div>
									<div style="background-color:#fff; width:100%; box-shadow:0px 0px 10px 0px hsla(0, 0%, 49%, 0.47); border-radius:4px;">
									<div style="background-color:#146da0; color:#fff; padding:50px 0px; text-align:center; border-radius:4px 4px 0px 0px;">
									<div style="font-size:20px;font-weight:800; margin:0px"><p style="width: 50px;
										height: 50px; margin: 0px auto; background-color: #155f8a; border-radius: 50%; line-height: 50px; text-align: center;">&#10004;</p></div>
									<p style="font-size:19px; font-weight:600; margin:10px 0px; letter-spacing:2px; text-transform:capitalize; font-family:sans-serif;">Activate your Account</p>
									<span style="font-size:13px; color:#add8f1; font-weight:500; margin:10px 0px; text-transform:capitalize; font-family:sans-serif;">"'.$date.'"</span>
									</div>
									<div style="padding:30px 20px; font-size:14px; font-family:sans-serif; line-height:28px; color:#333; font-weight:500; text-align:center;">
									<p style="margin:25px 0px;">Hey.. <span style="color:#146da0">"'.$data['pli_loginid'].'"</span>. You&rsquo;re almost ready to setup your new Registration Here. Simply click the below button to register your new Account.
									
									</p>
									<p style="margin:25px 0px;">User Name: "'.$data['pli_loginid'].'"<br>
									password: "'.$data['pli_loginid'].'"<br>
									
									</p>
									<a  title="Register Now" style="display:inline-block; background-color:#f15d1a; padding:10px 20px; text-decoration:none; font-size:14px; font-weight:500; letter-spacing:1px; text-transform:uppercase; color:#fff; border-radius:4px;" href="'.$verifictionlink.'">Confirm Email</a>
									
									<p style="color:#a5a5a5; margin-top:30px; margin-bottom:0px; line-height:10px; font-size:12px; font-weight:500; font-family:sans-serif;">Email Sent by PurchaseQuik</p>
									<p style="color:#a5a5a5; font-size:12px; margin:0px; line-height:20px; font-weight:500; font-family:sans-serif;">Copyright &copy; "'.$year.'" PurchaseQuik. All rights reserved.</p>
									</div>
									</div>
									</div>';
						 
							 Mail::send([] , [ $data  ], function ($message) use($html,$data)
							{		
								$message->to($data['pli_loginid'],'To Kuwy')->subject('New User Registered');
								$message->from('support@purchasequick.in','Purchase Quick');
								$message->setBody($html, 'text/html');
							});
										
						$result = array(
							'status'  => true,
							'message' => "Register success"			
			  
						);
											
					}        
                }
            }
			elseif($check_exist->pli_login_type =='1')
			{
			//	print_r($check_exist);die;
				
				$company_data =	[
					'pci_comp_name' 			=>  $seller_companyName,
					'pci_comp_gst' 				=>  $seller_companyGst,
					'pci_comp_pan' 				=>  $seller_companyPan,
					'pci_comp_type' 			=>  $seller_orgtype,
					'pci_comp_phone' 			=>  $seller_phone,
					'pci_comp_website' 			=>  $seller_website,
					'pci_comp_establish_date' 	=>  $comp_establish,
					'pci_comp_nature_business'	=>  $comp_nature,
					'pci_comp_type_cat'			=>  $comp_type_cat,
					'pci_comp_address'			=>  $comp_address,
					'pci_commu_address'			=>  $comp_comm_address,
					'pci_comp_type_cat_desc'	=>  $comp_type_cat_desc,
					'pci_comp_autorized_type'	=>  $comp_autorized_type,
					'pci_comp_ref_client'		=>  $comp_ref_client,
					'pci_comp_service_area'		=>  $comp_service_area,
					'pci_iso_cert'				=>  $iso_cert,
					'pci_msim_cert'				=>  $msim_cert,
					'pci_other_branch'			=>  $other_branch,
					'pci_other_branch_detail'	=>  $other_branch_detail,
					'pci_date_time'				=>  $datetime
					];

				$companyId= DB::table('pq_company_info')
				->where('pci_comp_id', '=',  $check_exist->pli_comp_id)
                ->update($company_data);
                if($companyId)
				{

                    $login_data=	[
               
                        'pli_login_type' 	=>  '3',
                        'pli_con_name' 		=>  $seller_name,
                        'pli_con_desig'	 	=>  $seller_desig,
                        'pli_con_mob' 		=>  $seller_contactMob
                    ];
        
                $loginId= DB::table('pq_login_info')
				->where('pli_loginid', '=',  $seller_email)
				->update($login_data);
						
					if($loginId)
					{
						if(isset($session_val['pli_login_type']))
						{		
				session(['dashboard_check' => '0',
				'pli_con_name' =>  $seller_name,
				'pli_login_type' => '3']);
						}
						
						/*
						// print_r($loginId);	die;
						 						 
						 $encrypted = Crypt::encryptString($loginId);
						 $verifictionlink =  URL::to('/users/confirm-email/'.$encrypted.'/'.sha1($data['pli_loginid']));
						 
						 $html = '<div style="width:500px; background-color:#fff; margin:0px auto;">
									<div>
									<img src="http://www.purchasequick.in/img/logo.png">
									</div>
									<div style="background-color:#fff; width:100%; box-shadow:0px 0px 10px 0px hsla(0, 0%, 49%, 0.47); border-radius:4px;">
									<div style="background-color:#146da0; color:#fff; padding:50px 0px; text-align:center; border-radius:4px 4px 0px 0px;">
									<div style="font-size:20px;font-weight:800; margin:0px"><p style="width: 50px;
										height: 50px; margin: 0px auto; background-color: #155f8a; border-radius: 50%; line-height: 50px; text-align: center;">&#10004;</p></div>
									<p style="font-size:19px; font-weight:600; margin:10px 0px; letter-spacing:2px; text-transform:capitalize; font-family:sans-serif;">Activate your Account</p>
									<span style="font-size:13px; color:#add8f1; font-weight:500; margin:10px 0px; text-transform:capitalize; font-family:sans-serif;">"'.$date.'"</span>
									</div>
									<div style="padding:30px 20px; font-size:14px; font-family:sans-serif; line-height:28px; color:#333; font-weight:500; text-align:center;">
									<p style="margin:25px 0px;">Hey.. <span style="color:#146da0">"'.$data['pli_loginid'].'"</span>. You&rsquo;re almost ready to setup your new Registration Here. Simply click the below button to register your new Account.</p>
									<p style="margin:25px 0px;">User Name: "'.$data['pli_loginid'].'"<br>
									password: "'.$data['pli_loginid'].'"<br>
									
									</p>
									<a  title="Register Now" style="display:inline-block; background-color:#f15d1a; padding:10px 20px; text-decoration:none; font-size:14px; font-weight:500; letter-spacing:1px; text-transform:uppercase; color:#fff; border-radius:4px;" href="'.$verifictionlink.'">Confirm Email</a>
									<p style="color:#a5a5a5; margin-top:30px; margin-bottom:0px; line-height:10px; font-size:12px; font-weight:500; font-family:sans-serif;">Email Sent by PurchaseQuik</p>
									<p style="color:#a5a5a5; font-size:12px; margin:0px; line-height:20px; font-weight:500; font-family:sans-serif;">Copyright &copy; "'.$year.'" PurchaseQuik. All rights reserved.</p>
									</div>
									</div>
									</div>';
						 
							 Mail::send([] , [ $data  ], function ($message) use($html,$data)
							{		
								$message->to($data['pli_loginid'],'To Kuwy')->subject('New User Registered');
								$message->from('support@purchasequick.in','Purchase Quick');
								$message->setBody($html, 'text/html');
							});*/ 
										
						$result = array(
							'status'  => true,
							'message' => "Register success As Both"			
			  
						);
											
					}       
                }
				
			}
            else 
			{	  
						// It exists - remove from favorites button will show									
				$result = array(
					'status'  => false,
					'message' => "User Already Register",
					'c_code'  =>1	 		
	  
				);
		 										
			}

         }
		catch(\Exception $e)
		{	
			$result = array(
				'status'  => false,
				'message' => $e->getMessage(),
				'c_code'  =>2	 			
			
			);
        }
    return response()->json($result);
    }	
	
	public function inviteSupplier(request $request)
	{
		
		 $data1			= $request->all();

		 if(isset($data1['join'])){
			if($data1['join'] == "1")
			{
			   $pei_invite_details =  $data1['pei_invite_details1'];
			}
			else
			{
				$pei_invite_details =  substr($data1['pei_invite_details1'], 0, -1); 
			}
		 }
		 
		 else
		 {
			$pei_invite_details =  substr($data1['pei_invite_details1'], 0, -1); 
		 }
		
		$session_val 	= session()->all();
		$dt 			= new DateTime();
		$date			= $dt->format('Y-m-d');
		$year			= $dt->format('Y');
		$data_val		= array_merge($data1,$session_val);
		$invite			= explode(',',$pei_invite_details);
		
		if(!empty($pei_invite_details)) 
		{
			foreach($invite	as $email)
			{
				$encrypted 		= array('enc'=>Crypt::encryptString($email),'enc1'=>Crypt::encryptString($data_val['pmc_id']),'enc_buyer'=>Crypt::encryptString($data_val['pec_loginID']),'year'=>$year,'email'=>$email,'pei_invite_details'=>$pei_invite_details );		
				$data			= array_merge($data_val,$encrypted);
				
				//print_r($data);die;
				//print_r($data['pei_invite_details']);
				//print_r($invite);
				//print_r($encrypted);
				//die;	
				if($data1['pea_event_terms_condition']!=''){
					$pathToFile=$data1['pea_event_terms_condition'];
				}

				
				$check_exist = DB::table('pq_login_info')
					->where('pli_loginid', '=',  $email)
					->first();
				
				
				
				if (is_null($check_exist)) 
				{	
				
					$test = purchaseInvite::inviteSellerInsert($data);

					Mail::send('emails.event-register-invite',$data,function($message) use ($data)
						{				 
							$message->to($data['email'])->subject('Event Register');
							$message->from('support@purchasequick.in','Purchase Quick');
							if($data['pea_event_terms_condition'])
							$message->attach($data['pea_event_terms_condition']);
						});
							
					$result = array(
						'status'  => true,
						'message' => "Seller Registration Mail Send Success"			
		  
					);	
					//print_r($check_exist);	echo('a');
				}
				else
				{	
				
					$test = purchaseInvite::inviteSellerInsert($data);
					
					Mail::send('emails.event-invite',$data,function($message) use ($data)
						{	
						
							$message->to($data['email'])->subject('Event Register');						
							$message->from('support@purchasequick.in','Purchase Quick');
							if($data['pea_event_terms_condition'])
							$message->attach($data['pea_event_terms_condition']);
						});
															
					$result = array(
						'status'  => true,
						'message' => "Invite Send success"			
		  
					);	
					//Session::flash('success', 'Your email was sent.');
					//print_r($check_exist);	echo('b'); 
					
				}
			
			}
		}
		else
		{
			$result = array(
						'status'  => false,
						'message' => "Enter Any One Email to Invite"	
		  
					);	
		}
		return $result;
	
	}
	
		
	public function Accept_user_Event($token,$token1,$token2)
    {
				
		$decrypted = Crypt::decryptString($token);
		$decrypted1 = Crypt::decryptString($token2);
						

		  $dt 	= new DateTime();
          $date	= $dt->format('Y-m-d H:i:s');

         try{
						  
				$pmc_id = $decrypted;
				
				$loginID = $decrypted1;
         
				$check_exist = DB::table('pq_event_invite_detail')
				->where('pei_event_additem_id', '=',  $pmc_id)
				->first();
				$json_accept='{"email" : "'.$loginID.'","accept": "1"}';
				$json_reject='{"email" : "'.$loginID.'","reject": "1"}';
			
				if($token1==1){
				$sql = "select * from pq_event_invite_detail
			
				WHERE json_contains(pei_invite_details,'".$json_accept."' )
				and pei_event_additem_id=".$pmc_id;
				}else{
					$sql = "select * from pq_event_invite_detail
			
				WHERE json_contains(pei_invite_details,'".$json_reject."' )
				and pei_event_additem_id=".$pmc_id;
				}
			   $check_already_accept=DB::select( $sql);
		
			   if($check_already_accept!=0){

            $json_array = json_decode($check_exist->pei_invite_details,true);
           
           
            foreach($json_array as $key => $value) 
			{
                if (in_array($loginID, $value)) 
				{					
                    unset($json_array[$key]);
                }
            }           
            
            $json_date = array();
            $json_date = array_values($json_array);
			
            if($token1==1)
			{
				$jsonformat = [
				'bid'=> "0",
				'email' => $loginID,
				'accept'=> "1",
				'reject'=>  "0",
				 ];
			}
			else
			{
				$jsonformat = [
				'bid'=> "0",
				'email' => $loginID,
				'accept'=> "0",
				'reject'=>  "1",
				 ];
			}

            array_push($json_date,$jsonformat);

             //encode json format 

             $format_json = json_encode($json_date);
			
			 
			if($check_already_accept!=0)
            {
			 if($token1==1)
			 {
				$accept_count = $check_exist->pei_invite_accept_count+1 ;
				$update_array = [
				'pei_invite_details' => $format_json ,
				'pei_invite_accept_count' => $accept_count 
				];
			}else{
				$accept_count = $check_exist->pei_invite_accept_count-1 ;
				$update_array = [
					'pei_invite_details' => $format_json ,
					'pei_invite_accept_count' => $accept_count 
					];
			}
		
             
               $inviteUpdate = DB::table('pq_event_invite_detail')
               ->where('pei_event_additem_id', $pmc_id)
               ->update($update_array);
			
           
              if( $inviteUpdate){

                  $result = array(
                      'status' => true,
                      'message' => 'Event item updated'
  
                  );
				}
               
  
              //print_r( $eventid);die;
            }else{
              $result = array(
                  'status' => false,
                  'message' => 'Something Went Wrong',	
                  'c_code'  =>2	 			
             
              );
			}
		}else{
			$result = array(
				'status' => false,
				'message' => 'Already accept',	
				'c_code'  =>3	 			
		   
			);
		}
              
              
              //echo $str;
              //print_r(count($package));die;
                      
         
         // print_r($insertData);die;
         }catch(\Exception $e){	
                $result = array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'c_code'  =>2	 			
               
                );
           }
		
		
		 $activation = $this->getActivationByToken($token);
			 if($token1==1)
			 {	
			     return	view('auth.accept_event');		
			 }else{
				 return	view('auth.reject_event');
			 }			
			
    }
	
	public function get_Supplier(request $request)
	{
		print_r($request->all());
		$data = $request->all();
		$check_exist = DB::table('pq_company_info')			
			->get();
			print_r($check_exist);
		
	}
	
	public static function getOS($user_agent = null)
	{
		if(!isset($user_agent) && isset($_SERVER['HTTP_USER_AGENT'])) 
		{
			$user_agent = $_SERVER['HTTP_USER_AGENT'];
			
		}
		
		$os_array = [
			'windows nt 10'                              =>  'Windows 10',
			'windows nt 6.3'                             =>  'Windows 8.1',
			'windows nt 6.2'                             =>  'Windows 8',
			'windows nt 6.1|windows nt 7.0'              =>  'Windows 7',
			'windows nt 6.0'                             =>  'Windows Vista',
			'windows nt 5.2'                             =>  'Windows Server 2003/XP x64',
			'windows nt 5.1'                             =>  'Windows XP',
			'windows xp'                                 =>  'Windows XP',
			'windows nt 5.0|windows nt5.1|windows 2000'  =>  'Windows 2000',
			'windows me'                                 =>  'Windows ME',
			'windows nt 4.0|winnt4.0'                    =>  'Windows NT',
			'windows ce'                                 =>  'Windows CE',
			'windows 98|win98'                           =>  'Windows 98',
			'windows 95|win95'                           =>  'Windows 95',
			'win16'                                      =>  'Windows 3.11',
			'mac os x 10.1[^0-9]'                        =>  'Mac OS X Puma',
			'macintosh|mac os x'                         =>  'Mac OS X',
			'mac_powerpc'                                =>  'Mac OS 9',
			'linux'                                      =>  'Linux',
			'ubuntu'                                     =>  'Linux - Ubuntu',
			'iphone'                                     =>  'iPhone',
			'ipod'                                       =>  'iPod',
			'ipad'                                       =>  'iPad',
			'android'                                    =>  'Android',
			'blackberry'                                 =>  'BlackBerry',
			'webos'                                      =>  'Mobile',

			'(media center pc).([0-9]{1,2}\.[0-9]{1,2})'=>'Windows Media Center',
			'(win)([0-9]{1,2}\.[0-9x]{1,2})'=>'Windows',
			'(win)([0-9]{2})'=>'Windows',
			'(windows)([0-9x]{2})'=>'Windows',

			'Win 9x 4.90'=>'Windows ME',
			'(windows)([0-9]{1,2}\.[0-9]{1,2})'=>'Windows',
			'win32'=>'Windows',
			'(java)([0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2})'=>'Java',
			'(Solaris)([0-9]{1,2}\.[0-9x]{1,2}){0,1}'=>'Solaris',
			'dos x86'=>'DOS',
			'Mac OS X'=>'Mac OS X',
			'Mac_PowerPC'=>'Macintosh PowerPC',
			'(mac|Macintosh)'=>'Mac OS',
			'(sunos)([0-9]{1,2}\.[0-9]{1,2}){0,1}'=>'SunOS',
			'(beos)([0-9]{1,2}\.[0-9]{1,2}){0,1}'=>'BeOS',
			'(risc os)([0-9]{1,2}\.[0-9]{1,2})'=>'RISC OS',
			'unix'=>'Unix',
			'os/2'=>'OS/2',
			'freebsd'=>'FreeBSD',
			'openbsd'=>'OpenBSD',
			'netbsd'=>'NetBSD',
			'irix'=>'IRIX',
			'plan9'=>'Plan9',
			'osf'=>'OSF',
			'aix'=>'AIX',
			'GNU Hurd'=>'GNU Hurd',
			'(fedora)'=>'Linux - Fedora',
			'(kubuntu)'=>'Linux - Kubuntu',
			'(ubuntu)'=>'Linux - Ubuntu',
			'(debian)'=>'Linux - Debian',
			'(CentOS)'=>'Linux - CentOS',
			'(Mandriva).([0-9]{1,3}(\.[0-9]{1,3})?(\.[0-9]{1,3})?)'=>'Linux - Mandriva',
			'(SUSE).([0-9]{1,3}(\.[0-9]{1,3})?(\.[0-9]{1,3})?)'=>'Linux - SUSE',
			'(Dropline)'=>'Linux - Slackware (Dropline GNOME)',
			'(ASPLinux)'=>'Linux - ASPLinux',
			'(Red Hat)'=>'Linux - Red Hat',
			'(linux)'=>'Linux',
			'(amigaos)([0-9]{1,2}\.[0-9]{1,2})'=>'AmigaOS',
			'amiga-aweb'=>'AmigaOS',
			'amiga'=>'Amiga',
			'AvantGo'=>'PalmOS',
			'[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,3})'=>'Linux',
			'(webtv)/([0-9]{1,2}\.[0-9]{1,2})'=>'WebTV',
			'Dreamcast'=>'Dreamcast OS',
			'GetRight'=>'Windows',
			'go!zilla'=>'Windows',
			'gozilla'=>'Windows',
			'gulliver'=>'Windows',
			'ia archiver'=>'Windows',
			'NetPositive'=>'Windows',
			'mass downloader'=>'Windows',
			'microsoft'=>'Windows',
			'offline explorer'=>'Windows',
			'teleport'=>'Windows',
			'web downloader'=>'Windows',
			'webcapture'=>'Windows',
			'webcollage'=>'Windows',
			'webcopier'=>'Windows',
			'webstripper'=>'Windows',
			'webzip'=>'Windows',
			'wget'=>'Windows',
			'Java'=>'Unknown',
			'flashget'=>'Windows',
			'MS FrontPage'=>'Windows',
			'(msproxy)/([0-9]{1,2}.[0-9]{1,2})'=>'Windows',
			'(msie)([0-9]{1,2}.[0-9]{1,2})'=>'Windows',
			'libwww-perl'=>'Unix',
			'UP.Browser'=>'Windows CE',
			'NetAnts'=>'Windows',
		];

		$arch_regex = '/\b(x86_64|x86-64|Win64|WOW64|x64|ia64|amd64|ppc64|sparc64|IRIX64)\b/ix';
		$arch = preg_match($arch_regex, $user_agent) ? '64' : '32';

		foreach ($os_array as $regex => $value) {
			if (preg_match('{\b('.$regex.')\b}i', $user_agent)) {
				return $value.' x'.$arch;
			}
		}

		return 'Unknown';
	}
	
			
	public static function getBrowser() 
	{ 
		$u_agent = $_SERVER['HTTP_USER_AGENT']; 
		$bname = 'Unknown';
		$version= "";
		

		if(preg_match('/^Mozilla\/5\.0/',$u_agent)) 
		{ 
			$bname = 'Internet Explorer'; 
			$ub = "MSIE"; 
		} 
		elseif(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
		{ 
			$bname = 'Internet Explorer'; 
			$ub = "MSIE"; 
		} 
		elseif(preg_match('/Firefox/i',$u_agent)) 
		{ 
			$bname = 'Mozilla Firefox'; 
			$ub = "Firefox"; 
		} 
		elseif(preg_match('/Chrome/i',$u_agent)) 
		{ 
			$bname = 'Google Chrome'; 
			$ub = "Chrome"; 
		} 
		elseif(preg_match('/Safari/i',$u_agent)) 
		{ 
			$bname = 'Apple Safari'; 
			$ub = "Safari"; 
		} 
		elseif(preg_match('/Opera/i',$u_agent)) 
		{ 
			$bname = 'Opera'; 
			$ub = "Opera"; 
		} 
		elseif(preg_match('/Netscape/i',$u_agent)) 
		{ 
			$bname = 'Netscape'; 
			$ub = "Netscape"; 
		} 
		
		$known = array('Version', $ub, 'other');
		$pattern = '#(?<browser>' . join('|', $known) .
		')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';


		if (!preg_match_all($pattern, $u_agent, $matches)) {
		}
		
		$i = count($matches['browser']);
	
		if ($i != 1) {
			if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
				$version= $matches['version'][0];
			}
			else {
				if(isset($matches['version'][1])){
					$version= $matches['version'][1];
				}else{
					$version= "Unknown";
				}
				
			}
		}
		else {
			$version= $matches['version'][0];
		}
		
		// check if we have a number
		if ($version==null || $version=="") {$version="?";}
		
		return array(
			'name'      => $bname,
			'version'   => $version,
			'pattern'    => $pattern
		);
	} 

	public function gotoSellerDashboard(){
		$session = session()->all(); 
		$this->CheckSession(	$session );
		if($session['pli_loginId']){
		session(['dashboard_check' => '1']);
		return redirect('live-auction');
		}
	}
	public function gotoBuyerDashboard(){
		$session = session()->all(); 
		$this->CheckSession(	$session );
		if($session['pli_loginId']){
		session(['dashboard_check' => '0']);
		return redirect('/event');
		}
	}
	

}
