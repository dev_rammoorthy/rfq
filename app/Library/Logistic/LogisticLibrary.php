<?php
namespace App\Library\Logistic;
use Illuminate\Support\Facades\Request;
use App\Models\Ship\PQ_Shipping_Address;

class LogisticLibrary
{
    public static function picode($request){

    	$get_ship=PQ_Shipping_Address::where('psa_id',$request->input('delivery'))->get()->first();
    	$endpoint = "http://127.0.0.1:8081/get/pincode/".$get_ship->psa_zipcode;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		$result=curl_exec($ch);
		curl_close($ch);
		$data=(json_decode($result, true));
		$dimensional=0;		
		for ($x = 0; $x <= $request->input('count'); $x++) {
            $box_length=$request->input('box_length'.$x.'');
            $box_breadth=$request->input('box_breadth'.$x.'');
            $box_height=$request->input('box_height'.$x.'');

            $dimensional+=((($box_length*$box_breadth*$box_height)/27000)*6)*$request->input('no_box'.$x.'');
        }
		$actual_weight=$request->input('total_weight');
		$charge_weight=max($dimensional,$actual_weight,20);
		$invoice_value=$request->input('invoice_value');
		$base_freight=$charge_weight*$data['rates'];
		$fuel_surcharge=$base_freight*0.15;
		$insure_value=($invoice_value*0.001);
		
		$freight_value=max($insure_value,100);
		$awb_charges=100;
		if($charge_weight < 100){
			$add_charge=0;
		}else if($charge_weight < 250){
			$add_charge=2;
		}else if($charge_weight < 500){
			$add_charge=3;
		}else{
			$add_charge=4;
		}
		$additional_handling_charges=($charge_weight/7)*$add_charge;
		
		$total_freight=$base_freight+$fuel_surcharge+$freight_value+$awb_charges+$additional_handling_charges;

		$sum_gst=$total_freight*0.18;

		return number_format((float)$total_freight+$sum_gst, 2, '.', '');
    }

    public static function apichildnumber(){
    	$endpoint = "http://127.0.0.1:8081/get/childnumber";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		$result=curl_exec($ch);
		curl_close($ch);
		$data=(json_decode($result, true));
		return $data;
	}

	public static function apimasternumber(){
    	$endpoint = "http://127.0.0.1:8081/get/masternumber/";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		$result=curl_exec($ch);
		curl_close($ch);
		$data=(json_decode($result, true));
		return $data;
    }

    public static function updateapimasternumber($id){
    	$endpoint = "http://127.0.0.1:8081/updatemaster/".$id;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		$result=curl_exec($ch);
		curl_close($ch);
    }

    public static function updateapichildnumber($id){
    	$endpoint = "http://127.0.0.1:8081/updatechild/".$id;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		$result=curl_exec($ch);
		curl_close($ch);
	}
}