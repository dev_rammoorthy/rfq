<?php

namespace App\Mail\RFQ;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteRFQ extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('support@purchasequick.in', 'Purchase Quick')
                    ->subject('New Invite')
                    ->markdown('emails.RFQ.rfqinvite')
                    ->with('content',$this->content);
        //return $this->markdown('emails.RFQ.rfqinvite');
    }
}
