<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PQ_LIVE_AUCTION extends Model
{
    //

    protected $table = 'pq_bid_history';

    protected $primaryKey = 'pbh_id';
}
