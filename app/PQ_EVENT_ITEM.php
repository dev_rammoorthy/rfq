<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PQ_EVENT_ITEM extends Model
{
    //

    public $timestamps = false;

    protected $table = 'pq_event_additem';

    protected $primaryKey = 'pea_id';

    public $fillable = array('pea_event_id', 'pea_event_market', 'pea_event_cat', 'pea_event_subcat', 'pea_event_start_dt', 'pea_event_end_dt', 'pea_event_spec', 'pea_event_unit', 'pea_event_unit_quantity', 'pea_event_start_price', 'pea_event_max_dec', 'pea_event_reserve_price', 'pea_is_emd_required', 'pea_event_emd_value', 'pea_event_location', 'pea_event_additem_dt', 'pea_event_additem_loginid', 'pea_ipaddress', 'pea_browser', 'pea_browser_ver', 'pea_os', 'pea_time_extension', 'pea_no_of_extension', 'pea_extn_count');
}
