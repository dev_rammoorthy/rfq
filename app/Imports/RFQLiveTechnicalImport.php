<?php

namespace App\Imports;

use App\Models\RFQ\RFQ_Live_Technical_Document;
use Maatwebsite\Excel\Concerns\ToModel;

class RFQLiveTechnicalImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new RFQ_Live_Technical_Document([
            //
        ]);
    }
}
