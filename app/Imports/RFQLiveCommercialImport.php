<?php

namespace App\Imports;

use App\Models\RFQ\RFQ_Live_Commercial_Document;
use Maatwebsite\Excel\Concerns\ToModel;

class RFQLiveCommercialImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new RFQ_Live_Commercial_Document([
            //
        ]);
    }
}
