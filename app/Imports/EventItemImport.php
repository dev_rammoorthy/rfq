<?php

namespace App\Imports;

use App\PQ_EVENT_ITEM;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use DateTime;
use App\Http\Controllers\PurchaseController;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use DateInterval;

class EventItemImport implements ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */

    //public function model(array $row)
    // public function collection(Collection $rows)
    // {
    //     $dt 	= new DateTime();
    //     $date=$dt->format('Y-m-d H:i:s');

    //     $session_val = session()->all();
    //     $loginId = $session_val['pli_loginId'];	
        
    //     $user_os 		= PurchaseController::getOS();	
    //     $user_browser   = PurchaseController::getBrowser();	
        
    //     foreach ($rows as $row) 
    //     {
    //         PQ_EVENT_ITEM::create([
    //             'pea_event_id'     => $row['Event_Id'],
    //             'pea_event_cat'    => $row['Event_Cat'],
    //             'pea_event_subcat' => $row['Event_Subcat'],
    //             'pea_event_start_dt' => $row['Start_Date'],
    //             'pea_event_end_dt' => $row['End_Date'],
    //             'pea_event_spec' => $row['Spec'],
    //             'pea_event_unit' => $row['Unit'],
    //             'pea_event_unit_quantity' => $row['Unit_Quantity'],
    //             'pea_event_start_price' => $row['Start_Price'],
    //             'pea_event_max_dec' => $row['Max_Diff'],
    //             'pea_event_reserve_price' => $row['Reserve_Price'],
    //             'pea_event_location' => $row['Location'],
    //             'pea_event_additem_dt' => $dt,
    //             'pea_event_additem_loginid' => $loginId,
    //             'pea_ipaddress'         =>  $_SERVER['REMOTE_ADDR'],
    //             'pea_browser'           =>  $user_browser['name'],        
    //             'pea_browser_ver'       =>  $user_browser['version'],    
    //             'pea_os'                =>  $user_os  
    //         ]);
    //     }
    // }
    

    use Exportable;

    public function __construct(string $event_id)
    {
        $this->event_id = $event_id;
    }

    public function model(array $row)
    {
        $dt 	= new DateTime();
        $date=$dt->format('Y-m-d H:i:s');

        $session_val = session()->all();
        $loginId = $session_val['pli_loginId'];	
        
        $user_os 		= PurchaseController::getOS();	
        $user_browser   = PurchaseController::getBrowser();	

        try
        {
            $st=new DateTime($row['start_date']);
            $et=new DateTime($row['end_date']);
            $event_start=$st->format('Y-m-d H:i:s');
            $event_end=$et->format('Y-m-d H:i:s');

            /* $FROM_DATE = ($row['start_date'] - 25569) * 86400;
            $start = gmdate("Y-m-d H:i:s", $FROM_DATE);

            $TO_DATE = ($row['end_date'] - 25569) * 86400;
            $end = gmdate("Y-m-d H:i:s", $TO_DATE);

            $event_start = date('Y-m-d H:i:s',strtotime('+1 seconds',strtotime($start)));
            $event_end = date('Y-m-d H:i:s',strtotime('+1 seconds',strtotime($end))); */

            $event_start = date('Y-m-d H:i:s',strtotime('-1 hour -0 minutes',strtotime($event_start)));
            $event_end = date('Y-m-d H:i:s',strtotime('-1 hour -0 minutes',strtotime($event_end)));
            
            $marketData = DB::table('pq_master_market')
                            ->where('pmm_market_name', $row['event_market'])
                            ->first();

            if($marketData)
            {
                $market_id = $marketData->pmm_id;

                $categoryData = DB::table('pq_master_cat_new')
                            ->where('pmca_market_id', $market_id)
                            ->where('pmca_cat_name', $row['event_cat'])
                            ->first();

                if($categoryData)
                {
                    $category_id = $categoryData->pmca_id;

                    $subCategoryData = DB::table('pq_master_subcat_new')
                                    ->where('pms_catid', $category_id)
                                    ->where('pms_subcat_name', $row['event_subcat'])
                                    ->first();

                    if($subCategoryData)
                    {
                        $subcategory_id = $subCategoryData->pms_id;
                    }
                    else
                    {
                        $sub_category_id = 1597;
                    }
                }
                else
                {
                    $category_id = 213;
                    $sub_category_id = 1597;
                }

            }   
            else
            {
                $market_id = 22;
                $category_id = 213;
                $sub_category_id = 1597;
            }

            if($row['emd_required'] == 'Yes')
            {
                $is_emd_required = 1;
            }
            else
            {
                $is_emd_required = 0;
            }

            $updateUnitData = DB::table('pq_master_unit')
                            ->where('pmu_unit_name', 'like', '%'.$row['unit'].'%')
                            ->first();

            $unit_id = null;
            if($updateUnitData)
            {
                $unit_id = $updateUnitData->pmu_id;
            }

            //echo $event_start.'+'.$event_end; die;
            //echo $event_start.'+'.$date; die;

            // echo $this->event_id.'\n';
            // echo $market_id.'\n';
            // echo $category_id.'\n';
            // echo $subcategory_id.'\n';
            // echo $event_start.'\n';
            // echo $event_end.'\n';
            // echo $row['spec'].'\n';
            // echo $unit_id.'\n';
            // echo $row['unit_quantity'].'\n';
            // echo $row['start_price'].'\n';
            // echo $row['max_diff'].'\n';
            // echo $row['reserve_price'].'\n';
            // echo $is_emd_required.'\n';
            // echo $row['emd'].'\n';
            // echo $row['location'].'\n';
            // echo $dt.'\n';
            // echo $row['time_extension'].'\n';
            // echo $row['no_of_time_extension'].'\n';
            // die;


            if($event_start > $date && $event_end > $event_start)
            {
                return new PQ_EVENT_ITEM([
                    //'pea_event_id'     => $row['event_id'],
                    'pea_event_id' => $this->event_id,
                    'pea_event_market'    => $market_id,
                    'pea_event_cat'    => $category_id,
                    'pea_event_subcat' => $subcategory_id,
                    'pea_event_start_dt' => $event_start,
                    'pea_event_end_dt' => $event_end,
                    'pea_event_spec' => $row['spec'],
                    'pea_event_unit' => $unit_id,
                    'pea_event_unit_quantity' => $row['unit_quantity'],
                    'pea_event_start_price' => $row['start_price'],
                    'pea_event_max_dec' => $row['max_diff'],
                    'pea_event_reserve_price' => $row['reserve_price'],
                    'pea_is_emd_required'  => $is_emd_required,
                    'pea_event_emd_value'  => $row['emd'],
                    'pea_event_location' => $row['location'],
                    'pea_event_additem_dt' => $dt,
                    'pea_time_extension' => $row['time_extension'],
                    'pea_no_of_extension' => $row['no_of_time_extension'],
                    'pea_extn_count' => 0,
                    'pea_event_additem_loginid' => $loginId,
                    'pea_ipaddress'         =>  $_SERVER['REMOTE_ADDR'],
                    'pea_browser'           =>  $user_browser['name'],        
                    'pea_browser_ver'       =>  $user_browser['version'],    
                    'pea_os'                =>  $user_os
                ]);
            }           
        }
        catch(\Exception $e){	
            $result = array(
                'status' => false,
                'message' => $e->getMessage(),
                'c_code'  =>2	 			
           
            );
        }
    }
}
