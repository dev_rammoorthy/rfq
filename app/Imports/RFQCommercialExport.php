<?php

namespace App\Imports;

use App\Models\RFQ\RFQ_Commercial_Document;
use Maatwebsite\Excel\Concerns\ToModel;

class RFQCommercialExport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new RFQ_Commercial_Document([
            //
        ]);
    }
}
