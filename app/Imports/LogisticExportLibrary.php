<?php

namespace App\Imports;
use Excel;
use App\Models\Logistic\PQ_Logistic_Delivery_Details;

class LogisticExportLibrary
{
	
	public static function exportdata($id){

		$data = PQ_Logistic_Delivery_Details::get()->toArray();
		return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download('xlsx');

	}
}