<?php

namespace App\Imports;

use App\Models\RFQ\RFQ_Technical_Document;
use Maatwebsite\Excel\Concerns\ToModel;

class RFQTechExport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new RFQ_Technical_Document([
            //
        ]);
    }
}
