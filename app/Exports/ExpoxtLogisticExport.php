<?php

namespace App\Exports;

use App\Models\Logistic\PQ_Logistic_Delivery_Details;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class ExpoxtLogisticExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($f_data){

        $this->id = $f_data;

    }
    public function collection()
    {
        return collect($this->id);
    }

    

    use Exportable;

    public function headings(): array
    {
        return [
            'Seller Name',
            'Seller Contact Number',
            'Seller Address 1',
            'Seller Address 2',
            'Seller State',
            'Seller City',
            'Seller Zipcode',
            'Seller Country',
            'Buyer Name',
            'Buyer Contact Number',
            'Buyer Address 1',
            'Buyer Address 2',
            'Buyer State',
            'Buyer City',
            'Buyer Zipcode',
            'Buyer Country',
            'Total Boxes',
            'Total weight',
            'Pickup Appointment Date',
            'Pickup Appointment Time',
            'Pickup Descriptio',
            'Ewaybill Number',
            'Invoice Number',
            'Invoice Id',
            'Invoice Value',
            'Price',            
            'Master Air Way Number',
            'Child Air Way Number',
            'Box Details',
        ];
    }

}
