<?php

namespace App\Exports;

use App\PQ_EVENT_ITEM;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EventItemExport implements FromQuery, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    public function __construct(string $event_id, int $market_flag, int $category_flag, int $sub_flag, int $unit_flag, int $quantity_flag, int $startp_flag, int $diff_flag, int $floor_flag, int $location_flag)
    {
        $this->event_id = $event_id;

        $this->market_flag = $market_flag;
        $this->category_flag = $category_flag;
        $this->sub_flag = $sub_flag;
        $this->unit_flag = $unit_flag;
        $this->quantity_flag = $quantity_flag;
        $this->startp_flag = $startp_flag;
        $this->diff_flag = $diff_flag;
        $this->floor_flag = $floor_flag;
        $this->location_flag = $location_flag;
    }

    public function headings(): array
    {
        $return_array = array('Event Id');

        if($this->market_flag == 1)
        {
            array_push($return_array,'Market Name');
        }

        if($this->category_flag == 1)
        {
            array_push($return_array,'Category Name');
        }

        if($this->sub_flag == 1)
        {
            array_push($return_array,'Sub Category Name');
        }

        array_push($return_array,'Start Date','End Date','Spec');

        if($this->unit_flag == 1)
        {
            array_push($return_array,'Unit');
        }

        if($this->quantity_flag == 1)
        {
            array_push($return_array,'Item Quantity');
        }

        if($this->startp_flag == 1)
        {
            array_push($return_array,'Start Price');
        }

        if($this->diff_flag == 1)
        {
            array_push($return_array,'Min Decrement');
        }

        if($this->floor_flag == 1)
        {
            array_push($return_array,'Floor Price');
        }

        if($this->location_flag == 1)
        {
            array_push($return_array,'Location');
        }

        return $return_array;
    }

    public function query()
    {

        $select_array = array('pea_event_id');

        if($this->market_flag == 1)
        {
            array_push($select_array,'pq_master_market.pmm_market_name');
        }

        if($this->category_flag == 1)
        {
            array_push($select_array,'pq_master_cat_new.pmca_cat_name');
        }

        if($this->sub_flag == 1)
        {
            array_push($select_array,'pq_master_subcat_new.pms_subcat_name');
        }

        array_push($select_array,'pea_event_start_dt','pea_event_end_dt','pea_event_spec');

        if($this->unit_flag == 1)
        {
            array_push($select_array,'pq_master_unit.pmu_unit_name');
        }

        if($this->quantity_flag == 1)
        {
            array_push($select_array,'pea_event_unit_quantity');
        }

        if($this->startp_flag == 1)
        {
            array_push($select_array,'pea_event_start_price');
        }

        if($this->diff_flag == 1)
        {
            array_push($select_array,'pea_event_max_dec');
        }

        if($this->floor_flag == 1)
        {
            array_push($select_array,'pea_event_reserve_price');
        }

        if($this->location_flag == 1)
        {
            array_push($select_array,'pea_event_location');
        }


        return PQ_EVENT_ITEM::query()->select($select_array)
        ->Join('pq_master_cat_new', 'pq_event_additem.pea_event_cat' , '=', 'pq_master_cat_new.pmca_id')
        ->Join('pq_master_subcat_new', 'pq_event_additem.pea_event_subcat' , '=', 'pq_master_subcat_new.pms_id')
        ->Join('pq_master_market', 'pq_event_additem.pea_event_market' , '=', 'pq_master_market.pmm_id')
        ->Join('pq_master_unit', 'pq_event_additem.pea_event_unit' , '=', 'pq_master_unit.pmu_id')
        ->where('pea_event_id', $this->event_id);
    }
    
}
