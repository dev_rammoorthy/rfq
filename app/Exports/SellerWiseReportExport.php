<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;
use App\PQ_LIVE_AUCTION;

class SellerWiseReportExport implements FromQuery, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    
    use Exportable;

    public function __construct(string $lot_id, string $seller_id)
    {
        $this->lot_id = $lot_id;
        $this->seller_id = $seller_id;
    }

    public function headings(): array
    {
        return [
            'Item Id',
            'Seller Company Name',
            'Bid Amount',
            'Bid Date',
            'Auction Status',
        ];
    }

    public function query()
    {
        return PQ_LIVE_AUCTION::query()
        ->select(array(
            'pbh_lotid', 'pq_company_info.pci_comp_name', 'pbh_cur_bid', 'pbh_cur_bid_datetime', 
            DB::raw('(CASE WHEN pbh_auction_status= "1" THEN "Win" WHEN pbh_auction_status= "2" THEN "Lose" WHEN pbh_auction_status= "3" THEN "Bidding" ELSE "Pending" END) AS is_user')
        ))
        ->Join('pq_company_info', 'pq_bid_history.pbh_seller_compid' , '=', 'pq_company_info.pci_comp_id')
        ->where('pbh_lotid', $this->lot_id)
        ->where('pbh_seller_compid', $this->seller_id)
        ->orderBy('pq_bid_history.pbh_id', 'desc');
    }

}
