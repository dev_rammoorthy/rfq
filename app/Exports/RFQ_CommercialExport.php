<?php

namespace App\Exports;

use App\Models\RFQ\RFQ_Commercial_Document;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RFQ_CommercialExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return RFQ_Commercial_Document::select('id','item_description','item_code','quantity','units','estimate_rate')->where('rfq_create_id',$this->data)->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'Item Description',
            'Item Code',
            'Quantity',
            'Units UOM',
            'Estimate Rate',
            'Input',
            'Remarks',
        ];
    }
}
