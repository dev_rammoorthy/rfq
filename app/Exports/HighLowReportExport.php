<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;
use App\PQ_LIVE_AUCTION;

class HighLowReportExport implements FromQuery, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    public function __construct(string $lot_id, int $address_flag, int $phone_flag, int $date_flag, int $status_flag)
    {
        $this->lot_id = $lot_id;
        
        $this->address_flag = $address_flag;
        $this->phone_flag = $phone_flag;
        $this->date_flag = $date_flag;
        $this->status_flag = $status_flag;
    }

    public function headings(): array
    {
        $return_array = array('Item Id','Seller Company Name');

        if($this->address_flag == 1)
        {
            array_push($return_array,'Company Address');
        }

        if($this->phone_flag == 1)
        {
            array_push($return_array,'Phone Number');
        }

        array_push($return_array, 'Bid Amount');

        if($this->date_flag == 1)
        {
            array_push($return_array,'Bid Date');
        }

        if($this->status_flag == 1)
        {
            array_push($return_array, 'Auction Status');
        }

        return $return_array;
    }

    public function query()
    {
        $validateData = DB::table('pq_bid_history')
                            ->where('pbh_lotid', $this->lot_id)
                            ->orderBy('pbh_id', 'desc')
                            ->first();


        $select_array = array('pbh_lotid','pq_company_info.pci_comp_name');

        if($this->address_flag == 1)
        {
            array_push($select_array,'pq_company_info.pci_comp_address');
        }

        if($this->phone_flag == 1)
        {
            array_push($select_array,'pq_company_info.pci_comp_phone');
        }

        array_push($select_array,'pbh_cur_bid');

        if($this->date_flag == 1)
        {
            array_push($select_array,'pbh_cur_bid_datetime');
        }

        if($this->status_flag == 1)
        {
            array_push($select_array, DB::raw('(CASE WHEN pbh_auction_status= "1" THEN "Win" WHEN pbh_auction_status= "2" THEN "Lose" WHEN pbh_auction_status= "3" THEN "Bidding" ELSE "Pending" END) AS is_user'));
        }

        return PQ_LIVE_AUCTION::query()
        ->select($select_array)
        ->Join('pq_company_info', 'pq_bid_history.pbh_seller_compid' , '=', 'pq_company_info.pci_comp_id')
        ->where('pbh_lotid', $this->lot_id)
        ->where('pbh_id', $validateData->pbh_id);
    }
}
