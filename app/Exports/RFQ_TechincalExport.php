<?php

namespace App\Exports;

use App\Models\RFQ\RFQ_Technical_Document;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RFQ_TechincalExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return RFQ_Technical_Document::select('id','technical_quality_criteria','estimated_rate')->where('rfq_create_id',$this->data)->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'Technical Quality Criteria',
            'Rstimated Rate',
            'Your Input (Please Enter yes / no)',
            'Your Remarks',
        ];
    }
}
