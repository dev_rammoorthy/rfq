<?php

namespace App\Exports;

use App\PQ_LIVE_AUCTION;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class LotBidExport implements FromQuery, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    public function __construct(string $lot_id, int $date_flag, int $auction_status_flag)
    {
        $this->lot_id = $lot_id;
        $this->date_flag = $date_flag;
        $this->auction_status_flag = $auction_status_flag;
    }

    public function headings(): array
    {
        $return_array = array('Item Id', 'Seller Company Name', 'Bid Amount');

        if($this->date_flag == 1)
        {
            array_push($return_array,'Bid Date');
        }

        if($this->auction_status_flag == 1)
        {
            array_push($return_array,'Auction Status');
        }

        return $return_array;
    }

    public function query()
    {
        $select_array = array('pbh_lotid', 'pq_company_info.pci_comp_name', 'pbh_cur_bid');

        if($this->date_flag == 1)
        {
            array_push($select_array,'pbh_cur_bid_datetime');
        }

        if($this->auction_status_flag == 1)
        {
            array_push($select_array, DB::raw('(CASE WHEN pbh_auction_status= "1" THEN "Win" WHEN pbh_auction_status= "2" THEN "Lose" WHEN pbh_auction_status= "3" THEN "Bidding" ELSE "Pending" END) AS is_user'));
        }

        return PQ_LIVE_AUCTION::query()
        ->select($select_array)
        ->Join('pq_company_info', 'pq_bid_history.pbh_seller_compid' , '=', 'pq_company_info.pci_comp_id')
        ->where('pbh_lotid', $this->lot_id)
        ->orderBy('pq_bid_history.pbh_id', 'desc');
    }
}
