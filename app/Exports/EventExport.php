<?php

namespace App\Exports;

use App\PQ_EVENT_CREATE;
use Maatwebsite\Excel\Concerns\FromCollection;

class EventExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PQ_EVENT_CREATE::all();
    }
}
