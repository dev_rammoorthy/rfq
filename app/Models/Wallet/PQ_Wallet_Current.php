<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Model;

class PQ_Wallet_Current extends Model
{
    protected $table="pq_wallet_current";

    public $timestamps = false;
}
