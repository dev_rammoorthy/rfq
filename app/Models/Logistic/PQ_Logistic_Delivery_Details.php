<?php

namespace App\Models\Logistic;

use Illuminate\Database\Eloquent\Model;

class PQ_Logistic_Delivery_Details extends Model
{
    protected $table='pq_logistic_delivery_details';
}
