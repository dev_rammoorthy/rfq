<?php

namespace App\Models\Logistic;

use Illuminate\Database\Eloquent\Model;

class PQ_Logistic_Box_Details extends Model
{
    protected $table='pq_logistic_box_details';
}
