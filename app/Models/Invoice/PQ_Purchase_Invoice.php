<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Model;

class PQ_Purchase_Invoice extends Model
{
    protected $table='pq_purchase_invoice';
}
