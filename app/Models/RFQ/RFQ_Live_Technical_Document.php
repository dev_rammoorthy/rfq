<?php

namespace App\Models\RFQ;

use Illuminate\Database\Eloquent\Model;

class RFQ_Live_Technical_Document extends Model
{
    protected $table="rfq_live_technical_document";
}
