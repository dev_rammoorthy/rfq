<?php

namespace App\Models\RFQ;

use Illuminate\Database\Eloquent\Model;

class RFQ_Commercial_Document extends Model
{
    protected $table='rfq_commercial_document';
}
