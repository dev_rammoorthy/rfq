<?php

namespace App\Models\RFQ;

use Illuminate\Database\Eloquent\Model;

class RFQ_Live_Commercial_Document extends Model
{
    protected $table="rfq_live_commercial_document";
}
