<?php

namespace App\Models\RFQ;

use Illuminate\Database\Eloquent\Model;

class RFQ_Support_Document extends Model
{
    protected $table="rfq_live_support_document";
}
