<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use DateTime;
use Exception;
use URL;
use Hash;
use Crypt;
use Session;
class purchaseInvite extends Model
{
    //
	
	
	public static function inviteSellerInsert($data){

		$dt 				 = new DateTime();
		$date			 = $dt->format('Y-m-d H:i:s');
		
		
		
		 try{
				$sellerID  = $data['pei_invite_details'];
				$additemID = $data['pmc_id'];
				$createdBy = $data['pli_loginId'];
				
				$sellerID = explode(',',$data['pei_invite_details']);
						
			//print_r($sellerID);
			foreach($sellerID as $key=>$email) 
			{
			
					$json_date[$key]=[
						'email' => $email,
						'accept'=>  "0",
						'reject'=>  "0",
						'bid'=> "0"

				];
				
			}
			
				$company_data=['pei_event_additem_id'=> $additemID,
				'pei_invite_details'=>  json_encode($json_date),
				'pei_invite_accept_count'=>  "0",
				'pei_invite_bidcount'=>  "0",
				'pei_invite_createby'=>  $createdBy,
				'pei_invite_send_date'=>  $date,
				'pei_invite_modify_login'=>  $data['pli_loginId'],
				'pei_invite_modify_date'=>  $date

			];

			$check_exist = DB::table('pq_event_invite_detail')
					->where('pei_event_additem_id', '=',  $additemID)
					->select('*')
					->first();
		
			
			if (is_null($check_exist)) 
			{
				
				 //print_r($company_data);	
	 
				$companyId = DB::table('pq_event_invite_detail')->insertgetid($company_data);
				
				//print_r($companyId);	
				//die;
				
				$result = array(
                    'status' => true,
                    'message' => "invite Insert"
                );

				
			}elseif(count($check_exist)!=0){
				
			$invite_email=$data['pei_invite_details'];
			$json_array=json_decode($check_exist->pei_invite_details,true);
			$json_date =array();
			$jsonformat[] =array();
			//print_r($sellerID);die;
			$i=0;
			foreach($sellerID as $key=>$email) 
			{
				$where_json='{"email" : "'.$email.'"}';
				$sql="select * FROM pq_event_invite_detail
								
				WHERE json_contains(pei_invite_details,'".$where_json."' )

				AND `pei_event_additem_id`=".$additemID."";
				$eventInviteData=DB::select( $sql);
				
				if(!$eventInviteData){
					
				
				
			  	$jsonformat[$i]=[
								'bid'=> "0",
								'email' => $email,
								'accept'=>  "0",
								'reject'=>  "0"
								
					
								];
								array_push($json_array,$jsonformat[$i]);
				
				$i++;
				
				}
				
           
       
	
				 //encode json format 
	
				
			}
		
			$format_json=json_encode($json_array);
			
			


				$invaite_update=['pei_event_additem_id'=> $additemID,
				'pei_invite_details'=> $format_json,
				'pei_invite_modify_login'=>  $createdBy,
				'pei_invite_modify_date'=>  $date

			];


				$companyId= DB::table('pq_event_invite_detail')
				->where('pei_event_additem_id', '=',  $additemID)
				->update($invaite_update);
				

				$result = array(
                    'status' => true,
                    'message' => "invite Insert"
                );

			}
			else {
                // It exists - remove from favorites button will show

                $result = array(
                    'status' => "false",
                    'message' => "You have already subscribed"
                );
             }
		 }
		 catch(\Exception $e){	
			 $result = array(
				 'status' => false,
				 'message' => $e->getMessage()			
			
			 );
			}


	 return $result;
	}


	public static function getEventAdditem($lotID){
				$sql="select `pec_event_name`,`pea_event_unit_quantity`,`pmu_unit_name`,`pea_event_spec`,`pea_event_unit`,`pea_event_location`,`pmc_cat_name`,pec_event_start_dt,pec_event_end_dt,`pms_subcat_name` FROM `pq_event_create`
				JOIN `pq_event_additem` ON pq_event_additem.`pea_event_id`=pq_event_create.`pec_event_id`    
				JOIN `pq_master_unit` ON pq_master_unit.`pmu_id`=pq_event_additem.`pea_event_unit`
				JOIN `pq_master_cat` ON `pq_master_cat`.`pmc_id` = pq_event_additem.`pea_event_cat`
                 JOIN `pq_master_subcat` ON `pq_master_subcat`.`pms_id` = pq_event_additem.`pea_event_subcat`
				 WHERE pq_event_additem.`pea_id`=".$lotID;
				 $data=DB::select( $sql);
				 return  $data[0];
	}
}
