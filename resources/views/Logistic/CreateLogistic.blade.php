@extends('event_manager/event-header')

@section('content')

<style type="text/css">
	.sub{
		text-transform: capitalize;
	}

	.movie_item_toolbar {
	    clear: both;
	    vertical-align: bottom;
	    width: 100%;
	    height: 66px;
	}

	.sub_btn {
	    padding: 8px 27px;
	    font-size: 16px;
	    font-weight: bold;
	}
</style>
<script type="text/javascript">

	function loadDoc() {
	  	var xhttp = new XMLHttpRequest();
		xhttp.open("POST", "/json-handler");
		xhttp.setRequestHeader("Content-Type", "application/json");
		var data = new FormData(document.querySelector('form'));
	  	xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		      	document.getElementById("show_data").innerHTML = this.responseText;
		    }
	  	};
	  	xhttp.open("POST", "api/pincode", true);
	  	xhttp.send(data);
	}

	var clicks = 0;

	function textBoxCreate(){
		var i = 1;

		clicks += 1;

		var length1 = document.createElement("INPUT");
		length1.setAttribute("type", "text");
		length1.setAttribute("Placeholder", "length");
		length1.setAttribute("Name", "box_length" + clicks);
		length1.setAttribute("id","length_put");
		length1.setAttribute('style','margin-bottom: 15px');
		document.getElementById("box_length1").appendChild(length1).classList.add("form-control");

		var height1 = document.createElement("INPUT");
		height1.setAttribute("type", "text");
		height1.setAttribute("Placeholder", "height");
		height1.setAttribute("Name", "box_height" + clicks);
		height1.setAttribute("id","height_put");
		height1.setAttribute('style','margin-bottom: 15px');
		document.getElementById("box_height1").appendChild(height1).classList.add("form-control");

		var breadth1 = document.createElement("INPUT");
		breadth1.setAttribute("type", "text");
		breadth1.setAttribute("Placeholder", "breadth");
		breadth1.setAttribute("Name", "box_breadth" + clicks);
		breadth1.setAttribute("id","breadth_put");
		breadth1.setAttribute('style','margin-bottom: 15px');
		document.getElementById("box_breadth1").appendChild(breadth1).classList.add("form-control");

		var box_cnt = document.createElement("INPUT");
		box_cnt.setAttribute("type", "text");
		box_cnt.setAttribute("Placeholder", "Number of Box");
		box_cnt.setAttribute("Name", "no_box" + clicks);
		box_cnt.setAttribute("id","box_cnt_put");
		box_cnt.setAttribute('style','margin-bottom: 15px');
		document.getElementById("no_box1").appendChild(box_cnt).classList.add("form-control");

		document.getElementById("count").value = clicks;

		i++;

	}


</script>

<div class="content-wrapper">

	<div class="inner-page-img">
	<div class="container">
	<h3 class="page-title">Logistic Details</h3>
	</div>
	</div>


	<div class="box-container">
		<div class="row">
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Add Logistic Details</h3>
							<p class="sub">You can add logistic details</p>
						</div>
						<form class="event-add-items" id="event-add-items" enctype="multipart/form-data" method="post" action='/logistic'>

							<input type="hidden" class="form-control" name="trans_id" value="{{$view_data['trans_id']}}">

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>Total Boxes</label>
								<input type="text" class="form-control" id="total_boxes" name="total_boxes" placeholder="Total Boxes">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>Total Weight ( In Kg's) </label>
								<input type="text" class="form-control" name="total_weight" id ="total_weight" placeholder="Total Weight ( In Kg's)">
							</div>

							<div class="event-full-details movie_item_toolbar">
								<h3>Shipment Details</h3>
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>Appointment Date and Time</label>
								<input type="text" class="form-control datetimes1" name="appointment_date_time" placeholder="Appointment Date">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>Description</label>
								<input type="text" class="form-control" name="pickup_description" placeholder="Description">
							</div>

							<div class="event-full-details movie_item_toolbar">
								<h3>Invoice Details</h3>
							</div>


							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>Invoice Value *</label>
								<input type="text" class="form-control" name="invoice_value" value="{{$view_data['in_voc_val']}}" readonly="true">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<input type="hidden" class="form-control" name="delivery" value="{{$view_data['buser_delivery_address']}}" readonly="true">
							</div>

							<div class="event-full-details movie_item_toolbar">
								<h3>Box Details</h3>
							</div>



							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>Length</label>
								<input type="text" class="form-control" name="box_length0" id="box_length" placeholder="length">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label> Height</label>
								<input type="text" class="form-control" name="box_height0" id="box_height" placeholder="Height">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label> Breadth</label>
								<input type="text" class="form-control" name="box_breadth0" id="box_breadth" placeholder="Breadth">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label> No of Boxes</label>
								<input type="text" class="form-control" name="no_box0" id="no_box0" placeholder="No of Boxes">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group" id="box_length1">
							</div>
							<div class="col-xs-3 col-md-3 col-sm-3 form-group" id="box_height1">
							</div>
							<div class="col-xs-3 col-md-3 col-sm-3 form-group" id="box_breadth1">
							</div>
							<div class="col-xs-3 col-md-3 col-sm-3 form-group" id="no_box1">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<input type="hidden" class="form-control" name="count" id="count">
							</div>

							<div align="right">
								<input type="button" class="btn btn-purple sub_btn valid" onclick="textBoxCreate()" value="Add Box Details">
							</div>

							<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
								<input type="submit" class="btn btn-purple sub_btn" id="signupform">
							</div>

							<button type="button" onclick="loadDoc()" class="btn btn-purple sub_btn" style="margin: 0 auto;display: block;margin-top: 29px;">Calculate Amount</button>
						<p id="show_data" style="text-align: center;font-size: 30px;font-weight: bold;"></p>
						</form>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
