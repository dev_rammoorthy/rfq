@extends('header')

@section('content')

	 <div class="col-xl-12 d-lg-flex align-items-center">
			<!--login form-->
			<div class="buyer-register-form">
				<div id="status"></div>
				<div class="login-form-brand">
					<img src="img/logo.png" alt="PurchaseQick">
				</div>
				<h4 class="text-uppercase text-purple text-center mb-4">Create Account</h4>
				<form name="buyer_register" id="buyer_register">
					<div class="col-xs-12 col-md-6 col-sm-6 register-divided">
						<h5>Company Information</h5>
						<div class="form-group">
							<input type="text" class="form-control" name="pci_comp_name" placeholder="Name of Company *" >
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="pci_comp_gst" placeholder="GSTIN *" >
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="pci_comp_pan" placeholder="PAN Number *" >
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="pci_comp_type" placeholder="Type of Organization *" >
						</div>
						<div class="form-group">
							<input type="number" class="form-control" name="pci_comp_phone" placeholder="Company Phone *" >
						</div>					
					</div>
					
					<div class="col-xs-12 col-md-6 col-sm-6 register-divided">
						<h5>Contact Info</h5>
							<div class="form-group">
								<input type="text" class="form-control" name="pli_con_name" placeholder="Person Name *" >
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="pli_con_desig" placeholder="Designation *" >
							</div>
							<div class="form-group">
								<input type="number" class="form-control" name="pli_con_mob" placeholder="Phone *" >
							</div>
							<div class="form-group">
								<input type="email" class="form-control" name="pli_loginid" placeholder="Email *" >
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="pci_comp_website" placeholder="Website" >
							</div>
					</div>
					
				</form>
			</div>
			<!--/login form-->

	</div>
		
@endsection
