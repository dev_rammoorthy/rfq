<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	
	</head>

<body>
<div style="width:500px; background-color:#fff; margin:0px auto;">
<div>
<img src="{{ asset('img/logo.png')}}">
</div>
<div style="background-color:#fff; width:100%; box-shadow:0px 0px 10px 0px hsla(0, 0%, 49%, 0.47); border-radius:4px;">
<div style="background-color:#146da0; color:#fff; padding:50px 0px; text-align:center; border-radius:4px 4px 0px 0px;">
<div style="font-size:20px;font-weight:800; margin:0px"><p style="width: 50px;
    height: 50px; margin: 0px auto; background-color: #155f8a; border-radius: 50%; line-height: 50px; text-align: center;">&#10004;</p></div>
<p style="font-size:19px; font-weight:600; margin:10px 0px; letter-spacing:2px; text-transform:capitalize; font-family:sans-serif;">your Account Activate Successfully</p>
</div>
<div style="padding:30px 20px; font-size:14px; font-family:sans-serif; line-height:28px; color:#333; font-weight:500; text-align:center;">
<a href="{{ URL::TO('/')}}" title="Register Now" style="display:inline-block; background-color:#f15d1a; padding:10px 20px; text-decoration:none; font-size:14px; font-weight:500; letter-spacing:1px; text-transform:uppercase; color:#fff; border-radius:4px;">Login</a>
<p style="color:#a5a5a5; margin-top:30px; margin-bottom:0px; line-height:10px; font-size:12px; font-weight:500; font-family:sans-serif;">Email Sent by PurchaseQuik</p>
<p style="color:#a5a5a5; font-size:12px; margin:0px; line-height:20px; font-weight:500; font-family:sans-serif;">Copyright &copy; 2018 PurchaseQuik. All rights reserved.</p>
</div>
</div>
</div>
</body>
</html>