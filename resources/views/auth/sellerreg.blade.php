<?php 
	  use \App\Http\Controllers\PurchaseController;
	  use \App\Http\Controllers\EventController;
	  $productList = PurchaseController::getproductList(); 
	  $serviceList = PurchaseController::getserviceList(); 
	  $GData = PurchaseController::getGstData();
	  $location = EventController::getLocation();
	  //Request::url();
	  //Request::path();
	  $routeName = Request::segment(2);
	  if(isset($routeName))
	  {
	  $decrypted = Crypt::decryptString($routeName);
	  } 
	 else{
	  $decrypted = "";
	  }	  		
	  ?>
	
@extends('header')

@section('title', 'Registration Form')

@section('content')
		<div class="login-reg">
			<div class="container">
				<div class="row">
					<div class="col-xl-12 d-lg-flex align-items-center">
						<!--login form-->
						<div class="seller-register-forms">
								<div id="status"></div>
						<a href="{{ URL::TO('/')}}">
							<div class="login-form-brand">
								<img src="{{ asset('new-img/logo.png') }}" alt="PurchaseQick">
							</div>
						</a>
							<h4 class="text-uppercase text-purple text-center mb-4">Create Account</h4>
							<form name="seller_register" id="seller_register" class="create-accountForm">
								<div class="col-xs-12 col-md-12 col-sm-12">	
									 <div class="purchasequick-tab" id="seller-register-first">
										<h5>Company Information</h5>
										<div class="form-group">
												<input type="text" class="form-control gst_no" name="pci_comp_gst" placeholder="GSTIN *" >
												<input type="hidden" class="form-control" id="gst_access" name="comp_gst" value="{{$GData->access_token}}" >
											
												 <span class="error gst_error" style="display:none">Invalid GSTIN</span>
											</div>
										<div class="form-group">
											<input type="text" class="form-control" name="pci_comp_name" id="pci_comp_name" placeholder="Name of Company *">
										</div>
										
										<div class="form-group">
											<input type="text" class="form-control" name="pci_comp_pan" placeholder="PAN Number *">
										</div>
										<div class="form-group">
												<input type="text" class="form-control" name="pci_comp_type" placeholder="Type of Organization *" >
											<!--<select class="form-control" name="pci_comp_type">
											   <option value="">-- Type of Organization --</option>
											   <option value="PVT Ltd">PVT Ltd</option>
											   <option value="Proprietorship">Proprietorship</option>
											   <option value="Partnership">Partnership</option>
											<select>-->
										</div>
										<div class="form-group">
											<input type="number" class="form-control" name="pci_comp_phone" placeholder="Company Phone *">
										</div>							
										<div class="form-group">
											<input type="text" class="form-control" name="pci_comp_establish_date" placeholder="Established Date *" data-toggle="datepicker" autocomplete="off" readonly>
										</div>							
										<div class="form-group">
											
										</div>							
										<div class="form-group">
												<input type="text" class="form-control" name="pci_comp_nature_business" placeholder="Nature of Business *" >
						
										</div>							
										<div class="form-group">
											<span class="check-btn-label">Authorized Type</span>
											<div class="middle">
												<label>
													<input type="radio" name="pci_comp_autorized_type" value="1"/>
													<div class="back-end box">
														<span>Agent</span>
													</div>
												</label>
												<label>
													<input type="radio" name="pci_comp_autorized_type" value="2"/>
														<div class="back-end box">
															<span>Trader</span>
														</div>
												</label>
											</div>
										</div>	
										<div style="overflow:auto;">
											<div style="float:right;">
										
											<button type="button" id="nextBtn" class="btn btn-purple" onclick="nextPrev(1)">Next</button>
											</div>
											<div class="text-center mt-1 f12"> 
											Already have and account ? <a href="{{ url('login')}}" class="btn-link text-capitalize">Login</a> 
											</div> 
										</div>						
									</div>
								<div class="purchasequick-tab" id="seller-register-second">
									<h5>Contact Info</h5>
									<div class="form-group">
										<input type="text" class="form-control" name="pli_con_name" placeholder="Person Name *">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" name="pli_con_desig" placeholder="Designation *">
									</div>
									<div class="form-group">
										<input type="number" class="form-control" name="pli_con_mob" placeholder="Phone *">
									</div>
									<div class="form-group">
										<input type="email" class="form-control" id="pli_loginid" name="pli_loginid" value="{{$decrypted}}" placeholder="Email *">
									</div>
									
									<div class="form-group">
										<textarea class="form-control" name="pci_comp_address" id="pci_comp_address" placeholder="Company Address *"></textarea>
										<span class="input-popup-link">
												 <a href="#" data-toggle="modal" id="branch_name" data-target="#gst_data_receive" style="display: none;"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Select your Branch Address</a>
										</span>

									</div>
									<div class="form-group">
											<div class="custom-control custom-checkbox agree-form position-relative mt-11"> 
											   <span> 
												<input type="checkbox" class="custom-control-input" name="same_address" id="communication_checkbox">
												   <label class="custom-control-label" for="communication_checkbox">Same Communication Address </label>
											   </span> 
										   </div>

										   </div>

									<div class="form-group">
											<textarea class="form-control" name="pci_comp_billing_address" id="pci_comp_billing_address" placeholder="Communication  Address "></textarea>
										
										
												
										</div>
									<div class="form-group">	
											<input type="text" class="form-control" name="pci_comp_location" placeholder="Location *" >
									</div>
									<div class="form-group">
										<input type="text" class="form-control" name="pci_comp_website" placeholder="Website">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" name="pci_comp_ref_client" placeholder="Refer any 5 Clients">
									</div>
									<h5>Company Product Info</h5>
									<div class="form-group">
										<span class="check-btn-label">Geographic Services Area</span>
										<ul class="chec-radio">
											<li class="pz">
												<label class="radio-inline">
													<input type="checkbox" id="pro-chx-residential" name="pci_comp_service_area[]" class="pro-chx" value="Local" checked>
													<div class="clab">Local</div>
												</label>
											</li>
											<li class="pz">
												<label class="radio-inline">
													<input type="checkbox" id="pro-chx-residential" name="pci_comp_service_area[]" class="pro-chx" value="Regional">
													<div class="clab">Regional</div>
												</label>
											</li>
											<li class="pz">
												<label class="radio-inline">
													<input type="checkbox" id="pro-chx-residential" name="pci_comp_service_area[]" class="pro-chx" value="Domestic">
													<div class="clab">Domestic</div>
												</label>
											</li>
												<li class="pz">
												<label class="radio-inline">
													<input type="checkbox" id="pro-chx-residential" name="pci_comp_service_area[]" class="pro-chx" value="International">
													<div class="clab">International</div>
												</label>
											</li>
										</ul>
									</div>
						
									<div class="form-group">
										<span class="check-btn-label">Company Category</span>
										<div class="middle">
											<label>
												<input type="radio" name="pci_comp_type_cat" value="1" />
												<div class="back-end box">
													<span>Product</span>
												</div>
											</label>
											<label>
												<input type="radio" name="pci_comp_type_cat" value="2" />
												<div class="back-end box">
													<span>Service</span>
												</div>
											</label>
										</div>
									</div>
					
									<div class="form-group 1 com-category cat-product">
										<select class="form-control select2-multiple pci_comp_type_cat_desc" id="multiple"  name="pci_comp_type_cat_desc[]" multiple="multiple" >
											@foreach ($productList as $key =>$product)
											<option value="{{ $product->pmc_id }}">{{ $product->pmc_cat_name }}</option>
											@endforeach
										</select>
										
									</div>
					
									<div class="form-group 2 com-category cat-service">
										<select class="form-control select2-multiple pci_comp_type_cat_desc1" id="multiple"  name="pci_comp_type_cat_desc[]" multiple="multiple">
											@foreach ($serviceList as $key =>$service)
											<option value="{{ $service->pmc_id }}">{{ $service->pmc_cat_name }}</option>
											@endforeach
										</select>
									</div>
									<span class="error category_error" style="display:none">please select category </span>
									<h5>Company Certificate Info</h5>
									<div class="form-group">
										<span class="radio-btn-label">ISO Certificate</span>
										<div class="form-check-inline">
											<label class="customradio"><span class="radiotextsty">Yes</span>
												<input type="radio" id="isoyes" name="isocert">
												<span class="checkmark"></span>
											</label>        
											<label class="customradio"><span class="radiotextsty">No</span>
												<input type="radio" id="isono" name="isocert" checked>
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
									
									<div class="form-group hidden-input" id="iso-certificate">
										<input type="text" class="form-control" name="pci_iso_cert" placeholder="Enter Details..">
									</div>
									
									<div class="form-group">
										<span class="radio-btn-label">MSME Certificate</span>
											<div class="form-check-inline">
											<label class="customradio"><span class="radiotextsty">Yes</span>
												<input type="radio" id="msimyes" name="msimcert">
												<span class="checkmark"></span>
											</label>        
											<label class="customradio"><span class="radiotextsty">No</span>
												<input type="radio" id="msimno" name="msimcert" checked>
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
									
									<div class="form-group hidden-input" id="msim-certificate">
										<input type="text" class="form-control" name="pci_msim_cert" placeholder="Enter Details..">
									</div>
									
									<div class="form-group">
										<span class="radio-btn-label">Others</span>
										<div class="form-check-inline">
											<label class="customradio"><span class="radiotextsty">Yes</span>
												  <input type="radio" id="othersyes" name="pci_other_branch" value="1">
												  <span class="checkmark"></span>
											</label>        
											<label class="customradio"><span class="radiotextsty" >No</span>
												  <input type="radio" id="othersno" name="pci_other_branch" value="2" checked>
												  <span class="checkmark"></span>
											</label>
										</div>
									</div>
									
									<div class="form-group hidden-input" id="others-certificate">
										<input type="text" class="form-control" name="pci_other_branch_detail" placeholder="Enter Details..">
									</div>
									
									<div class="col-xs-12 col-md-12 col-sm-12 register-form-submit">
										<div class="custom-control custom-checkbox agree-form position-relative"> 
											 <span> 
												<input type="checkbox" class="custom-control-input" name="accept_checkbox" id="customControlInline">
											   <label class="custom-control-label" for="customControlInline">I have read and agree with the <a href="#" data-toggle="modal" data-target="#terms_conditions">Terms &amp; Conditions</a></label>
										   </span> 
										 </div> 
									</div>
									
									<div style="overflow:auto;">
										<div style="float:right;">
										<button type="button" id="prevBtn" class="btn btn-purple"  onclick="nextPrev(-1)">Previous</button>
										<button type="submit" id="nextBtn" class="btn btn-purple">Submit</button>
										</div>
										<div class="text-center mt-1 f12"> 
										Already have and account ? <a href="{{ url('login')}}" class="btn-link text-capitalize">Login</a> 
										</div> 
									</div>
								</div>
								
								
								
																									
									<!-- steps of the form: -->
									<div style="text-align:center;margin-top:0px;">
										<span class="step"></span>
										<span class="step"></span>
									</div>
								
								</div>
											
							</form>
						</div>
						<!--/login form-->
					</div>
				</div>
			</div>	
		</div>	

		<div class="modal fade" id="gst_data_receive" role="dialog">
				<div class="modal-dialog vendor-invite-popup">
				  <!-- Modal content-->
				  <div class="modal-content invite-popup-content mail-invite-popup">
					<div class="modal-header">
					  <h4 class="modal-title">Choose Company Address</h4>
					</div>
					<div class="modal-body">
					  
	
				</div>
				   
				  </div>
				  
				</div>
			  </div>
			  <div class="modal fade" id="terms_conditions" role="dialog">
				<div class="modal-dialog terms-popup-content">
				
				  <!-- Modal content-->
				  <div class="modal-content invite-popup-content">
					<div class="modal-header">
					  <h4 class="modal-title">TERMS AND CONDITIONS</h4>	 
					</div>
					<div class="modal-body">
					  <span class="h6">ALL INFORMATION PROVIDED HEREIN WILL BE SHARED AND PUBLIC.
			MEMBERS AGREE NOT TO HOLD SIMPLIFIED PROCUREMENT SOLUTIONS
			(OPC) PRIVATE LIMITED, RESPONSIBLE FOR SHARING THE INFORMATION
			PROVIDED HEREIN.<span>
			
				<div class="scrollbar" id="style-3">
				  <div class="force-overflow"></div>
			
					<ul class="terms-point">
					 <li>Simplified Procurement Solutions (OPC) Private Limited, shall strive to
			provide information on potential enquiries in good faith and intention.</li>
			<li>Members, when they are selected as vendors by Companies through
			Simplified Procurement Solutions (OPC) Private Limited, referrals, must
			necessarily comply with and strictly adhere to the terms &amp; conditions as
			may be prescribed by the Companies from time to time.
			</li>
			<li>
			This membership does not guarantee any business. It only provides a
			platform for the members an opportunity to increase their business.
			</li>
			<li>
			Simplified Procurement Solutions (OPC) Private Limited, takes no
			responsibility of the quality or quantity of the business enquiries that may
			be generated through its referrals. In no event shall Simplified
			Procurement Solutions (OPC) Private Limited, be liable for any loss for the
			members by business, revenues, profit, costs direct and incidental,
			consequential or punitive damages of any claim. Members agree to have
			discussed all the related matter regarding the supply of services with the
			Companies who call for RFQ and have understood in full that Simplified
			Procurement Solutions (OPC) Private Limited, has provided a source of
			business and has nothing to do any further especially with regards to
			delivery schedules, payments, rejections, transportation, legal laws and
			regulations to be followed from time to time etc. Simplified Procurement
			Solutions (OPC) Private Limited, is only an e-commerce service provider
			and is not and cannot be a party to or control in any manner any
			transactions between the Member and the Companies. Simplified
			Procurement Solutions (OPC) Private Limited, shall neither be responsible
			nor liable to mediate or resolve any disputes or disagreements or breach of
			
			contract between the Member and the Companies with whom the member
			enters into an agreement to provide services.
			</li>
			<li>
			Member explicitly consents to receive communication from Simplified
			Procurement Solutions (OPC) Private Limited, by e-mail, SMS on their
			mobile phones, by phone calls and any notice issued to members in
			respect of any matter by Simplified Procurement Solutions (OPC) Private
			Limited, or Companies to the email address or physical address as
			provided herein and received by any officers or staffs / employees shall be
			treated as adequate notice and duly served. Members fully understand that
			this consent shall override their registration on the NDNC, if any.
				</li>
			  </ul>
			</div>
			
				
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
					</div>
				  </div>
				  
				</div>
			  </div>

			  
	<div class="messagealert" id="toast_message_success">	
			<div class="alert alert-success ">
			   <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>.
			  </div>
		  </div>

		  <div class="messagealert" id="toast_message_fail">	
				<div class="alert alert-danger ">
				   <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
				  </div>
			  </div>
 @endsection