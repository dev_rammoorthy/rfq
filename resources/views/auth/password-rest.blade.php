@extends('header')

@section('content')
     <div class="forgot-page">
		<div class="forgot-form">
            
			<h4 class="text-uppercase text-purple text-center mb-2">Forgot Password</h4>
								<a href="#" class="float-left forgot-link my-2">Please Enter Your New Password </a>
			<form name="password_reset" id="password_reset" novalidate="novalidate">

				<div class="form-group mb-4">
					<input type="password" name="password" class="form-control" id="exampleInputPassword" placeholder="New Password">
				</div>
				<div class="form-group mb-4">
					<input type="password" name="confirm_password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">
				</div>

				<div class="form-group clearfix">
					<input type="submit" class="btn btn-purple float-right">				
				</div>

			</form>
		</div>
        <!--/login form-->
</div>

	<!--<div class="messagealert" id="aap">	
      <div class="alert alert-success">
	     <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>Successfully.
		</div>
    </div>
    basic scripts-->
 @endsection


