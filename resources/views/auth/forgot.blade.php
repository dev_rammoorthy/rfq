@extends('header')

@section('title', 'Forget Password')

@section('content')

<?php 
	use \App\Http\Controllers\EventController;
	use \App\Http\Controllers\PurchaseController;
		  	
	  ?>
		<div class="login-reg">
			<div class="container">
				<div class="row">
					 <div class="col-xl-12 d-lg-flex align-items-center">
							<!--forgot form-->
					<div class="buyer-register-form">
								<div id="status"></div>
								<a href="{{ URL::TO('/')}}">
									<div class="login-form-brand">
										<img src="{{ asset('img/logo.png') }}" alt="PurchaseQick">
									</div>
								</a>
								<h4 class="text-uppercase text-purple text-center mb-4">Forgot Password</h4>
								<form name="forgot_password" id="forgot_password">
						
										<div class="form-group">
												<input type="email" class="form-control" name="pq_email" id="pq_email" placeholder="Enter your email *">
											</div>
											
											
											<div class="form-group">
													<input type="submit"  class="btn btn-purple"> 
											</div>
											
					</form>
						
				</div>
				<!--/forgot form-->
					</div>
				</div>
			</div>
		</div>	
		
		
		
@endsection
