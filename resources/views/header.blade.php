<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="">

		<!--favicon icon-->
		<link rel="icon" type="image/png" href="{{asset ('img/purch-icon.png') }}">

		<title>Purchasequick - @yield('title')</title>

		 <!--web fonts-->
		<link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
		<link href="{{asset ('css/bootstrap.min.css') }}?{{ time() }}" rel="stylesheet">
		<!--icon font-->
		<link href="{{asset ('css/font-awesome.min.css') }}?{{ time() }}" rel="stylesheet">
		<!--custom styles-->
		<link href="{{asset ('css/main.css') }}?{{ time() }}" rel="stylesheet">	
		<!-- Select2 min css -->
		<link href="{{asset ('css/select2.css') }}?{{ time() }}" rel="stylesheet">
		<!-- iziToast css -->
		<link href="{{asset ('css/plugins/izitoast/iziToast.min.css') }}?{{ time() }}" rel="stylesheet">
		<link href="{{asset ('css/datepicker.css') }}?{{ time() }}" rel="stylesheet">

	</head>

	<body class="login-bgm">
		<div class="preloader" >
			<div class="loading" >
				<div class="lds-ripple"><div></div><div></div></div>
			</div>
		</div>
	
		@yield('content')		

		<!--basic scripts-->
		<script src="{{ asset ('js/jquery.min.js') }}"></script>
		<script src="{{ asset ('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset ('js/jquery.validate.min.js') }}"></script>
		<script src="{{ asset('js/pages/purchase-auth.js') }}"></script> 
		<script src="{{ asset('js/plugins/izitoast/iziToast.min.js') }}"></script> 
		<script src="{{ asset('js/select2.full.js') }}"></script>
		<script src="{{ asset('js/datepicker.js') }}"></script>
		
		<script type='text/javascript'>
			$(function() {			 
				$('[data-toggle="datepicker"]').datepicker({
					autoHide: true,
					zIndex: 2048,
					format:'yyyy-mm-dd',
					endDate:'0',
				});		  				  
			});
		</script>
		<script>
		var currentTab = 0; // Current tab is set to be the first tab (0)
			showTab(currentTab); // Display the current tab

			function showTab(n) {
			  // This function will display the specified tab of the form ...
			  var x = document.getElementsByClassName("purchasequick-tab");
			  x[n].style.display = "block";
			  // ... and fix the Previous/Next buttons:
			  if (n == 0) {
				document.getElementById("prevBtn").style.display = "none";
			  } else {
				document.getElementById("prevBtn").style.display = "inline";
			  }
			  if (n == (x.length - 1)) {
				document.getElementById("nextBtn").innerHTML = "Submit";
		
			
			  } else {
				document.getElementById("nextBtn").innerHTML = "Next";
			  }
			  // ... and run a function that displays the correct step indicator:
			  fixStepIndicator(n)
			}

			function nextPrev(n) {
			  // This function will figure out which tab to display
			  var x = document.getElementsByClassName("purchasequick-tab");
			  // Exit the function if any field in the current tab is invalid:
			  if (n == 1 && !validateForm()) return false;
			  // Hide the current tab:
			  x[currentTab].style.display = "none";
			  // Increase or decrease the current tab by 1:
			  currentTab = currentTab + n;
			  // if you have reached the end of the form... :
			  if (currentTab >= x.length) {
				//...the form gets submitted:
			//	document.getElementByClassName("create-accountForm").submit();
				return false;
			  }
			  // Otherwise, display the correct tab:
			  showTab(currentTab);
			}

			function validateForm() {
			  // This function deals with validation of the form fields
			  var x, y, i, valid = true;
			/*  x = document.getElementsByClassName("purchasequick-tab");
			  y = x[currentTab].getElementsByTagName("input");
			  // A loop that checks every input field in the current tab:
			  for (i = 0; i < y.length; i++) {
				// If a field is empty...
				if (y[i].value == "") {
				  // add an "invalid" class to the field:
				  y[i].className += " invalid";
				  // and set the current valid status to false:
				  valid = false;
				}
				}*/
				valid=	$('.create-accountForm').valid();
			  // If the valid status is true, mark the step as finished and valid:
			  if (valid) {
				document.getElementsByClassName("step")[currentTab].className += " finish";
			  }
			  return valid; // return the valid status
			}

			function fixStepIndicator(n) {
			  // This function removes the "active" class of all steps...
			  var i, x = document.getElementsByClassName("step");
			  for (i = 0; i < x.length; i++) {
				x[i].className = x[i].className.replace(" active", "");
			  }
			  //... and adds the "active" class to the current step:
			  x[n].className += " active";
			}
					
		</script>
	</body>
</html>