<?php
  use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 

 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');
$planId= Request::route('planId');
$session = session()->all(); 
$loginid = $session['pli_loginId'];

$event_data=EventController::getEvent($eventID);
if($event_data->pec_event_status==1){
    Redirect::to('event')->send();
}

$market=EventController::getMarket($session['pci_comp_type_cat']);
$unit=EventController::getUnit();
$event_added_items=EventController::getAddedItems1($eventID);
$location=EventController::getLocation();
/*echo '<pre>';
print_r($event_added_items);die;*/

$page="Add Event Items";
$pagetrack=PurchaseController::pageLogout($page);

?>

@extends('event_manager/event-header')

@section('content')
<!--main content wrapper-->
    <div class="content-wrapper" oncontextmenu="return false">
<!--creative states-->
	<div class="inner-page-img">
	<div class="container">
	<h3 class="page-title">Item Details</h3>
	</div>
	</div>
	

<div class="box-container">
<div class="row">              
<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
<div class="card card-shadow mb-4">
<div class="card-body">
<div class="event-full-details">
<h3>Product List</h3>
<p>You can add products for event</p>
</div>




<div class="row"> 
	<div class="col-md-3">
	<div class="event-auc-name">
	<h4>{{$event_data->pec_event_name}}</h4>
	<p>Event Name</p>
	</div>
	<div class="border-rig"></div>
	</div>
	
	<div class="col-md-3">
	<div class="event-auc-name">
	<h4>{{$event_data->pec_event_start_dt}}</h4>
	<p>Event Date</p>
	</div> 
	<div class="border-rig"></div>
	</div>
	
	
	<div class="col-md-3">
	<div class="event-auc-name">
	<h4>{{$event_data->pec_event_end_dt}}</h4>
	<p>Event End </p>
	</div>
	<div class="border-rig"></div>
	</div> 
	
	<div class="col-md-3"> 
	<div class="event-auc-name">
	<h4>Event ID</h4>
	<p>{{$event_data->pec_event_id}}</p>
	</div>
	</div> 
	
	
	</div>


</div>
</div>
</div>
</div>
</div>

<div class="box-container">
<div class="row">
<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12">
		<!-- @if ($message = Session::get('success'))
			<input type="hidden" id="hfExcelUploadSuccess" value="{{ $message }}" /> -->
			<!-- <div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>	
					<strong>{{ $message }}</strong>
			</div> 
		@endif
		@if ($message = Session::get('failure')) -->
		<!-- <input type="hidden" id="hfExcelUploadFailure" value="{{ $message }}" />
		<div class="alert alert-danger alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
						<strong>{{ $message }}</strong>
		</div> 
		@endif -->
		
<div class="card card-body card-shadow">
<div class="add-items-form">
<h3>Add Items
	<a data-id="{{$event_data->pec_event_id}}" data-event-type="{{$event_data->pec_event_type}}" title="Bulk excel upload" style="float:right" href="#" class="btn btn-purple" id="bulk-upload">
		<i class="fa fa-cloud-upload" aria-hidden="true"></i> Bulk Upload 
	</a>
</h3>

<form class="event-add-items" id="event-add-items" enctype="multipart/form-data">


	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Market </label>
		<select class="form-control"  name="prod_market" id="prod_market">
			<option value="">-- Choose Your Market --</option>

			@foreach ($market as $item)
			<option value="{{$item->pmm_id}}">{{$item->pmm_market_name}}</option>

			@endforeach

		</select>
	</div>

	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Category </label>
		<select class="form-control"  name="prod_cat" id="prod_cat">
			<option value="">-- Choose Your Categories --</option>
		</select>
	</div>

	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Sub Category </label>
		<select class="form-control"  name="prod_sub_cat" id="prod_sub_cat">
			<option value="">-- Choose Your Products --</option>
		</select>
	</div>
	<div class="col-xs-3 col-md-3 col-sm-3 form-group date">
		<label> Start Date </label>
		<input type="text" class="form-control datetimes1" name="pec_event_start_dt" id="pec_event_start_dt" placeholder="Auction Start Date &amp; Time">
	</div>
	<div class="col-xs-3 col-md-3 col-sm-3 form-group date">
		<label> End Date </label>
		<input type="text" class="form-control datetimes2" name="pec_event_end_dt" id="pec_event_end_dt" placeholder="Auction Start Date &amp; Time">
	</div>
			
	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Specification </label>
		<input type="text" class="form-control" name="spec" placeholder="Specification">
	</div>
	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Unit </label>
		<select class="form-control" name="prod_unit">
			<option value="">-- Choose Your Units --</option>
			@foreach ($unit as $item)
			<option value="{{$item->pmu_id}}">{{$item->pmu_unit_name}}</option>

			@endforeach
		</select>
	</div>

	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Quantity </label>
		<input type="number" class="form-control" name="prod_quant" placeholder="Quantity">
	</div>
	@if ($event_data->pec_event_type==2)		
	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Reserve Price </label>
		<input type="number" class="form-control" name="prod_st_price" placeholder="Reserve Price">
	</div>
	@endif


@if ($event_data->pec_event_type==2)		
	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Minimum Decrement </label>
		<input type="number" class="form-control" name="prod_min_decrement" placeholder="Min Decrement">
	</div>
	@endif

	@if ($event_data->pec_event_type==2)		
	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Floor Price (Optional) </label>
		<input type="number" class="form-control" name="pea_event_reserve_price" placeholder="Floor Price" required>
	</div>
	@endif

	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Location </label>
		<select class="form-control select2" name="location">
			<option value="">-- Choose Your Location --</option>
			@foreach ($location as $item)
			<option value="{{$item->pmci_city_name}}">{{$item->pmci_city_name}}</option>			
			@endforeach
		</select>
	</div>	
	
	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label for="term_upload" class="text-purple"> Upload Catalogue </label>
		<input  type="file" placeholder="upload terms " id="term_upload" name="term_upload" /> 		
	</div>	
	
</div>
	
<br>
<h4>EMD Details</h4>

<div class="col-md-12 col-xs-12 col-sm-12 row">
	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> EMD Required ? </label> <br>
		<div>
			<input type="radio" id="yes" name="pea_emd_check" value="1" checked>
			<label for="yes">Yes</label> 
		</div>
		<div>
			<input type="radio" id="no" name="pea_emd_check" value="0"> 
			<label for="no">No</label> 
		</div>
	</div>
	<div class="col-xs-3 col-md-3 col-sm-3 form-group emd-div">
		<label> EMD Value </label>
		<input type="number" class="form-control" id="pea_event_emd_value" name="pea_event_emd_value" placeholder="EMD Value" required>
		<label for="pea_event_emd_value"> (Secure Deposit Amount) </label>
	</div>	
</div>

@if ($event_data->pec_event_type==2)	
<br>
<h4>Additional Details</h4>

<div class="col-md-12 col-xs-12 col-sm-12 row additional-detail">
	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> Time Extension </label>
		<input type="number" class="form-control" id="time_extension" name="time_extension" placeholder="Time Extension">
		<label> (Max 60 minutes) </label>
	</div>
	<div class="col-xs-3 col-md-3 col-sm-3 form-group">
		<label> No Of Time Extension </label>
		<input type="number" class="form-control" id="no_of_time_extension" name="no_of_time_extension" placeholder="No Of Time Extension">
		<label> (0 Unlimited) </label>
	</div>	
</div>
@endif

<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
<input type="hidden" id="eventID" name="eventID" value="{{$event_data->pec_event_id}}">
<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
<input type="hidden" id="com_cat_type" name="com_cat_type" value="{{$session['pci_comp_type_cat']}}">
<a href="{{ URL::to('/create-event/'.$planId)}}" class="btn btn-default">Go Back</a>
<input type="submit" class="btn btn-purple" id="add-event" value="ADD ITEM">
<!-- <a href="#" class="btn btn-purple" id="bulk-upload"> Bulk Upload </a> -->
</div>
</form>
</div>
</div>
</div>
</div>
</div>


<div class="box-container">
<div class="row">              
<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
<div class="card card-shadow mb-4">
        @if (count($event_added_items)!=0)
        <form name="even-completed" id="even-completed">      
<div class="card-body">
<div class="table-responsive">
   
       
   
	<table id="makeEditable" class="table table-striped table-bordered" cellspacing="0">
		<thead>
		<tr>
			<th>Market</th>
			<th>Category</th>
			<th>Product Name</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Spec</th>
			<th>Units</th>
			<th>Qty</th>			
			@if ($event_data->pec_event_type==2)
			<th>Reserve Price</th>
			<th>Min Dec.,</th>  
			<th>Floor Price</th>              
			@endif    
			<th>Location</th>			
			<th>Actions</th>                                        
		</tr>
		</thead>
		<tbody>
			@foreach ($event_added_items as $item)  
			<tr id="{{$item->pea_id}}">
				<td>{{$item->pmm_market_name}}</td>
				<td>{{$item->pmca_cat_name}}</td>
				<td>{{$item->pms_subcat_name}}</td>
				<td>{{$item->pea_event_start_dt}}</td>
				<td>{{$item->pea_event_end_dt}}</td>
				<td>{{$item->pea_event_spec}}</td>
				<td>{{$item->pmu_unit_name}}</td>
				<td>{{$item->pea_event_unit_quantity}}</td>			
				@if ($event_data->pec_event_type==2)	
				<td>{{$item->pea_event_start_price}}</td>			        
				<td>{{$item->pea_event_max_dec}}</td>   
				<td>{{$item->pea_event_reserve_price}}</td>  
				@endif 
				<td>{{$item->pea_event_location}}</td>
				<td name="buttons">
					<div class="btn-group table-action-btn">
													<button id="bEdit" type="button" class="btn btn-sm btn-edit-row" data-market-id="{{$item->pmm_id}}" data-event-type="{{$event_data->pec_event_type}}" data-cat-id="{{$item->pmca_id}}"  data-unit-id="{{$item->pmu_id}}" data-subcat-id="{{$item->pms_id}}" data-id="{{$item->pea_id}}" data-toggle="modal" data-target="#edit_product"><span class="fa fa-pencil" > </span></button>
													<button id="bElim" type="button" class="btn btn-sm btn-delete-row removeItem" data-trash-id="{{$item->pea_id}}"><span class="fa fa-trash" > </span></button>								
					<!--	<button type="button" class="btn btn-sm btn-invite-row dropdown">
							<a href="javascript:" class="dropbtn"><span class="fa fa-envelope"></span></a>
							<div class="dropdown-content">
								<a href="#" class="own_vender" title="Own Vendor" data-toggle="modal" data-target="#invite_ownvendor" data-id="$item->pea_id}}">Own Vendor</a>
								<a href="#" class="pq_vender" title="PQ Vendor" data-toggle="modal" data-target="#invite_PQvendor">PQ Vendor</a></div></button>-->
						</div>
				</td>   
			</tr>
		@endforeach
		</tbody>
	</table>
	<input type="hidden" id="eventID" name="eventID" value="{{$eventID}}">
	<input type="hidden" id="hfEventType" name="hfEventType" value="{{$event_data->pec_event_type}}">
	<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">

	<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
		<input type="submit" class="btn btn-purple" id="add-event" name="SUBMIT">
	</div>  


</div>
</div>
@endif
</form>

</div>
</div>
</div>
</div>


	<div class="modal fade" id="edit_product" role="dialog">
		<div class="modal-dialog vendor-invite-popup">		
		  <!-- Modal content-->
		  <div class="modal-content invite-popup-content">
			<div class="modal-header">
			  <h4 class="modal-title">Edit Product Item</h4>
			</div>
			<div class="modal-body">
			<form id="invite-vendorform" class="invite-vendorform">
		
				<div class="form-group">
					<select class="form-control"  name="pmc_market_name" id="pmc_market_name">
						<option value="">-- Choose Your Market --</option>
						
						@foreach ($market as $item)
						<option value="{{$item->pmm_id}}">{{$item->pmm_market_name}}</option>
						
						@endforeach
						
					</select>
				</div>
				<div class="form-group">
					<select class="form-control changeCategory"  name="pmc_cat_name" id="pmc_cat_name">
						<option value="">-- Choose Your Category --</option>
					</select>
				</div>
				<div class="form-group">
					<select class="form-control"  name="pms_subcat_name" id="pms_subcat_name">
						<option value="">-- Choose Your Products --</option>
						</select>
					</div>
					<div class="form-group date">
						<input type="text" class="form-control datetimes1" name="pea_event_start_dt" id="pea_event_start_dt" placeholder="Lot Start Date &amp; Time">
					</div>
					<div class="form-group date">
						<input type="text" class="form-control datetimes2" name="pea_event_end_dt" id="pea_event_end_dt" placeholder="Lot Start Date &amp; Time">
					</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pea_event_spec" name="pea_event_spec" placeholder="Name of Spec *">
				</div>
				<div class="form-group">
				<select class="form-control" name="pmu_unit_name" id="pmu_unit_name">
					<option value="">-- Choose Your Units --</option>
					@foreach ($unit as $item)
					<option value="{{$item->pmu_id}}">{{$item->pmu_unit_name}}</option>
					
					@endforeach
					</select>
					
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pea_event_unit_quantity" name="pea_event_unit_quantity" placeholder="Name of Qty *">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pea_event_start_price" name="pea_event_start_price" placeholder="Reserve Price">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pea_event_max_dec" name="pea_event_max_dec" placeholder="Min Dec *">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pea_event_reserve_price" name="pea_event_reserve_price" placeholder="Floor Price *">
				</div>
				<div class="form-group">
					<!--<input type="text" class="form-control" id="pea_event_location" name="" placeholder="Name of Location*">-->
					<select class="form-control select2" id="pea_event_location" name="pea_event_location">
						<option value="">-- Choose Your Location --</option>
						@foreach ($location as $item)
						<option value="{{$item->pmci_city_name}}">{{$item->pmci_city_name}}</option>
						
						@endforeach
					</select>
				</div>
				<input type="hidden" value="" name="pmc_id" id="pmc_id">
				<div class="ownvendor_mailinput">
					<input type="submit"  class="btn btn-purple" value="Submit">
				</div>
			</form>
			
			<div class="invited-maillist">
			<p>dilipvinoth@gmail.com</p>
			<p>b.dilip@gmail.com</p>
			</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
			</div>
		  </div>
		  
		</div>
	</div>



<div class="modal fade" id="invite_ownvendor" role="dialog">
    <div class="modal-dialog vendor-invite-popup">    
      <!-- Modal content-->
		<div class="modal-content invite-popup-content">
			<div class="modal-header">
				<h4 class="modal-title">Invite Your Suppliers</h4>
			</div>
			<div class="modal-body">
				<form action="#" id="inviteSupp" class="invite-vendorform">
					<div class="ownvendor_mailinput">
					
						<div class="add_invite_email" style="position:relative; top:1em; margin-bottom:1em;"> 
			
						</div>
						
						<input type="hidden" class="form-control" id="cat_name" name="pmc_cat_name">
						<input type="hidden" class="form-control" id="subcat_name" name="pms_subcat_name">
						<input type="hidden" class="form-control" id="event_spec" name="pea_event_spec">
						<input type="hidden" class="form-control" id="unit_name" name="pmu_unit_name">
						<input type="hidden" class="form-control" id="event_unit_quantity" name="pea_event_unit_quantity">
						<input type="hidden" class="form-control" id="event_start_price" name="pea_event_start_price">
						<input type="hidden" class="form-control" id="event_max_dec" name="pea_event_max_dec">
						<input type="hidden" class="form-control" id="event_location" name="pea_event_location">
						<input type="hidden" class="form-control" id="pmc1_id" name="pmc_id">
						<input type="hidden" class="form-control" name="pec_event_name" value="{{$event_data->pec_event_name}}">
						<input type="hidden" class="form-control" name="pec_event_start_dt" value="{{$event_data->pec_event_start_dt}}">
						<input type="hidden" class="form-control" name="pec_event_end_dt" value="{{$event_data->pec_event_end_dt}}">
						<input type="text" class="form-control" name="pei_invite_details" placeholder="Enter Your vendor mail..">
						<div class="btn-danger inline-addmail-btn" id="add_mail">Add Mail</div>						
					</div>
				</form>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
			</div>
		</div>      
    </div>
</div>
  
  <div class="modal fade" id="invite_PQvendor" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content invite-popup-content">
        <div class="modal-header">
			<h4 class="modal-title">Invite Your New Suppliers</h4>
        </div>
        <div class="modal-body">
			<form action="#" class="invite-vendorform">
				<input type="hidden" class="form-control" id="cat_name1" name="pmc_cat_name">
				<input type="hidden" class="form-control" id="event_spec1" name="pea_event_spec">
				<input type="hidden" class="form-control" id="unit_name1" name="pmu_unit_name">
				<input type="hidden" class="form-control" id="event_unit_quantity1" name="pea_event_unit_quantity">
				<input type="hidden" class="form-control" id="event_start_price1" name="pea_event_start_price">
				<input type="hidden" class="form-control" id="event_max_dec1" name="pea_event_max_dec">
				<input type="hidden" class="form-control" id="pmc1_id1" name="pmc_id">
				<input type="hidden" class="form-control" name="pec_event_name" value="{{$event_data->pec_event_name}}">
				<input type="hidden" class="form-control" name="pec_event_start_dt" value="{{$event_data->pec_event_start_dt}}">
				<input type="hidden" class="form-control" name="pec_event_end_dt" value="{{$event_data->pec_event_end_dt}}">
				<div class="pqvendor_mailinput form-group col-sm-6 col-xs-12 col-md-6">
					<label for="">Location</label>
					<input type="text" class="form-control" id="event_location1" name="pea_event_location" readonly >
				</div>
				<div class="pqvendor_mailinput form-group col-sm-6 col-xs-12 col-md-6">
					<label for="">Product</label>
					<input type="text" class="form-control" id="subcat_name1" name="pms_subcat_name" readonly >
				</div>
			</form>
        </div>
		
		<div class="table-responsive invited-newlist" id="pq_invite_list">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Select</th>
						<th>Company</th>
						<th>Email</th>
						<th>Rating</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<label class="check-vendor">
								<input type="checkbox" name="is_name">
								<span class="check-checkmark"></span>
							</label>
						</td>
						<td>Samsung</td>
						<td>sales@samsung.com</td>
						<td class="vendor-star-rate">
							<i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
						</td>
					</tr>
					<tr>
						<td>
							<label class="check-vendor">
								<input type="checkbox" name="is_name">
								<span class="check-checkmark"></span>
							</label>
						</td>
						<td>Microsoft</td>
						<td>purchase@microsoft.com</td>
						<td class="vendor-star-rate">
							<i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
<input type="submit" class="btn btn-purple" id="add-event" name="SUBMIT">
</div>  
		
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

	<div class="modal fade headShake" id="uploadForm-Reverse" role="dialog">
			<div class="modal-dialog vendor-invite-popup">
			
				<!-- Modal content-->
				<div class="modal-content modal-content-amount invite-popup-content">
						<div class="modal-header modal-header-amount" style="background: #ff9800;">
								<h4 class="modal-title-amount">Excel Import - Reverse</h4>
						</div>
								
								<div class="modal-body modal-body-bg" style="height:auto">
										<!-- <form action="{{action('ExcelController@excelitemupload')}}" method="POST" enctype="multipart/form-data" class="event-detail-form" id="report-form" name="add_event"> -->
										<form enctype="multipart/form-data" class="event-detail-form" id="report-form" name="add_event">
												<div style="padding:5%; border: 1px lightgrey solid;">
													<div class="col-md-12">
														<label for="excel"> Upload Excel File </label>
														<input type="file" placeholder="upload terms" id="excel" name="excel" /> 
														<input type="hidden" id="hfEventId" name="hfEventId" />
													</div>															
												</div>
												<div class="col-md-12" style="text-align:center">
													<br><a href="{{action('ExcelController@downloadsamplelot')}}" style="color: #ff9800;text-decoration:underline"> Download Item Template </a>
												</div>
												<br>
												<div class="amount-btn">
														<div style="margin-top:1em" class="float-left col-6">
																<button type="submit" id="upload-model" class="btn-amount btn-primary text-center"> Upload </button>
														</div>
														<div style="margin-top:1.6em" class="float-left col-6">
																<a  id="upload-model-cancel" class="btn-cancel btn-light text-center"> Cancel </a>
														</div>
												</div>                    
										</form>      
								</div>                              
						</div>
				</div>
			
			</div>
	</div>

	<div class="modal fade headShake" id="uploadForm-Closed" role="dialog">
			<div class="modal-dialog vendor-invite-popup">
			
				<!-- Modal content-->
				<div class="modal-content modal-content-amount invite-popup-content">
						<div class="modal-header modal-header-amount" style="background: #ff9800;">
								<h4 class="modal-title-amount">Excel Import - Closed</h4>
						</div>
								
								<div class="modal-body modal-body-bg" style="height:auto">
										<!-- <form action="{{action('ExcelController@excelitemupload')}}" method="POST" enctype="multipart/form-data" class="event-detail-form" id="report-form" name="add_event"> -->
										<form enctype="multipart/form-data" class="event-detail-form" id="upload-form-closed" name="add_event">
												<div style="padding:5%; border: 1px lightgrey solid;">
													<div class="col-md-12">
														<label for="excel"> Upload Excel File </label>
														<input type="file" placeholder="upload terms" id="excel" name="excel" /> 
														<input type="hidden" id="hfClosedEventId" name="hfClosedEventId" />
													</div>															
												</div>
												<div class="col-md-12" style="text-align:center">
													<br><a href="{{action('ExcelController@downloadsampleclosed')}}" style="color: #ff9800;text-decoration:underline"> Download Item Template </a>
												</div>
												<br>
												<div class="amount-btn">
														<div style="margin-top:1em" class="float-left col-6">
																<button type="submit" id="upload-model" class="btn-amount btn-primary text-center"> Upload </button>
														</div>
														<div style="margin-top:1.6em" class="float-left col-6">
																<a  id="upload-closed-model-cancel" class="btn-cancel btn-light text-center"> Cancel </a>
														</div>
												</div>                    
										</form>      
								</div>                              
						</div>
				</div>
			
			</div>
	</div>


    <!--/main content wrapper-->
</div>
    <!--footer-->

@endsection
