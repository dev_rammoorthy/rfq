<?php
  use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 

 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');
$planId= Request::route('planId');
$session = session()->all(); 
$loginid = $session['pli_loginId'];

$event_data=EventController::getEvent($eventID);
if($event_data->pec_event_status==1){
    Redirect::to('event')->send();
}

$catagoires=EventController::getMasterCat($session['pci_comp_type_cat']);
$unit=EventController::getUnit();
$event_added_items=EventController::getAddedItems($eventID);
$location=EventController::getLocation();
/*echo '<pre>';
print_r($event_added_items);die;*/

?>

@extends('event_manager/event-header')

@section('content')
<!--main content wrapper-->
    <div class="content-wrapper">
<!--creative states-->
	<div class="inner-page-img">
	<div class="container">
	<h3 class="page-title">Item Details</h3>
	</div>
	</div>
	

<div class="box-container">
<div class="row">              
<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
<div class="card card-shadow mb-4">
<div class="card-body">
<div class="event-full-details">
<h3>Product List</h3>
<p>You can add products for event</p>
</div>




<div class="row"> 
	<div class="col-md-3">
	<div class="event-auc-name">
	<h4>{{$event_data->pec_event_name}}</h4>
	<p>Event Name</p>
	</div>
	<div class="border-rig"></div>
	</div>
	
	<div class="col-md-3">
	<div class="event-auc-name">
	<h4>{{$event_data->pec_event_start_dt}}</h4>
	<p>Event Date</p>
	</div> 
	<div class="border-rig"></div>
	</div>
	
	
	<div class="col-md-3">
	<div class="event-auc-name">
	<h4>{{$event_data->pec_event_end_dt}}</h4>
	<p>Event End </p>
	</div>
	<div class="border-rig"></div>
	</div> 
	
	<div class="col-md-3"> 
	<div class="event-auc-name">
	<h4>Event ID</h4>
	<p>{{$event_data->pec_event_id}}</p>
	</div>
	</div> 
	
	
	</div>


</div>
</div>
</div>
</div>
</div>

<div class="box-container">
<div class="row">
<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12">
<div class="card card-body card-shadow">
<div class="add-items-form">
<h3>Add Items</h3>
<form class="event-add-items" id="event-add-items" enctype="multipart/form-data">
<div class="col-xs-12 col-md-3 col-sm-3 form-group">
<select class="form-control"  name="prod_cat" id="prod_cat">
<option value="">-- Choose Your Category --</option>

@foreach ($catagoires as $item)
<option value="{{$item->pmc_id}}">{{$item->pmc_cat_name}}</option>

@endforeach

</select>
</div>
<div class="col-xs-12 col-md-3 col-sm-3 form-group">
<select class="form-control"  name="prod_sub_cat" id="prod_sub_cat">
<option value="">-- Choose Your Products --</option>
</select>
</div>

<div class="col-xs-12 col-md-3 col-sm-3 form-group date">
			<input type="text" class="form-control datetimes1" name="pec_event_start_dt" id="pec_event_start_dt" placeholder="Auction Start Date &amp; Time">
		</div>
		<div class="col-xs-12 col-md-3 col-sm-3 form-group date">
			<input type="text" class="form-control datetimes2" name="pec_event_end_dt" id="pec_event_end_dt" placeholder="Auction Start Date &amp; Time">
		</div>
		
<div class="col-xs-12 col-md-3 col-sm-3 form-group">
<input type="text" class="form-control" name="spec" placeholder="Specification">
</div>
<div class="col-xs-12 col-md-3 col-sm-3 form-group">
<select class="form-control" name="prod_unit">
<option value="">-- Choose Your Units --</option>
@foreach ($unit as $item)
<option value="{{$item->pmu_id}}">{{$item->pmu_unit_name}}</option>

@endforeach
</select>
</div>

<div class="col-xs-12 col-md-3 col-sm-3 form-group">
<input type="number" class="form-control" name="prod_quant" placeholder="Quantity">
</div>
<div class="col-xs-12 col-md-3 col-sm-3 form-group">
<input type="number" class="form-control" name="prod_st_price" placeholder="Expected Price">
</div>

<div class="col-xs-12 col-md-3 col-sm-3 form-group">
<input type="number" class="form-control" name="prod_min_decrement" placeholder="Min Decrement">
</div>
@if ($event_data->pec_event_type==2)
	
<div class="col-xs-12 col-md-3 col-sm-3 form-group">
	<input type="number" class="form-control" name="pea_event_reserve_price" placeholder="Reserve Price" required>
	</div>
@endif
<div class="col-xs-12 col-md-3 col-sm-3 form-group">

<select class="form-control select2" name="location">
		<option value="">-- Choose Your Location --</option>
		@foreach ($location as $item)
		<option value="{{$item->pmci_city_name}}">{{$item->pmci_city_name}}</option>
		
		@endforeach
		</select>
</div>
<div class="col-xs-12 col-md-3 col-sm-3 form-group">
	<input  type="file" placeholder="upload terms " id="term_upload" name="term_upload" /> 
		</div>


<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
<input type="hidden" id="eventID" name="eventID" value="{{$event_data->pec_event_id}}">
<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
<input type="hidden" id="com_cat_type" name="com_cat_type" value="{{$session['pci_comp_type_cat']}}">
<a href="{{ URL::to('/create-event/'.$planId)}}" class="btn btn-default">Go Back</a>
<input type="submit" class="btn btn-purple" id="add-event" value="ADD ITEM">
</div>
</form>
</div>
</div>
</div>
</div>
</div>


<div class="box-container">
<div class="row">              
<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
<div class="card card-shadow mb-4">
        @if (count($event_added_items)!=0)
        <form name="even-completed" id="even-completed">      
<div class="card-body">
<div class="table-responsive">
   
       
   
	<table id="makeEditable" class="table table-striped table-bordered" cellspacing="0">
		<thead>
		<tr>
		
		 
			<th>Category</th>
			<th>Product Name</th>
			<th>Spec</th>
			<th>Units</th>
			<th>Qty</th>
			<th>Location</th>
			<th>Expected Price</th>
			<th>Min Dec.,</th>  
			@if ($event_data->pec_event_type==2)
			<th>Reserve Price</th>              
			@endif                                     
			<th>Actions</th>                                        
		</tr>
		</thead>
		<tbody>
			@foreach ($event_added_items as $item)  
			<tr id="{{$item->pea_id}}">
				<td>{{$item->pmc_cat_name}}</td>
				<td>{{$item->pms_subcat_name}}</td>
				<td>{{$item->pea_event_spec}}</td>
				<td>{{$item->pmu_unit_name}}</td>
				<td>{{$item->pea_event_unit_quantity}}</td>
				<td>{{$item->pea_event_location}}</td>
				<td>{{$item->pea_event_start_price}}</td>
			        
				<td>{{$item->pea_event_max_dec}}</td>   
				@if ($event_data->pec_event_type==2)
				<td>{{$item->pea_event_reserve_price}}</td>  
				@endif 
				<td name="buttons">
					<div class="btn-group table-action-btn">
													<button id="bEdit" type="button" class="btn btn-sm btn-edit-row" data-cat-id="{{$item->pmc_id}}"  data-unit-id="{{$item->pmu_id}}" data-subcat-id="{{$item->pms_id}}" data-id="{{$item->pea_id}}" data-toggle="modal" data-target="#edit_product"><span class="fa fa-pencil" > </span></button>
													<button id="bElim" type="button" class="btn btn-sm btn-delete-row removeItem" data-trash-id="{{$item->pea_id}}"><span class="fa fa-trash" > </span></button>								
					<!--	<button type="button" class="btn btn-sm btn-invite-row dropdown">
							<a href="javascript:" class="dropbtn"><span class="fa fa-envelope"></span></a>
							<div class="dropdown-content">
								<a href="#" class="own_vender" title="Own Vendor" data-toggle="modal" data-target="#invite_ownvendor" data-id="$item->pea_id}}">Own Vendor</a>
								<a href="#" class="pq_vender" title="PQ Vendor" data-toggle="modal" data-target="#invite_PQvendor">PQ Vendor</a></div></button>-->
						</div>
				</td>   
			</tr>
		@endforeach
		</tbody>
	</table>
	<input type="hidden" id="eventID" name="eventID" value="{{$eventID}}">
	<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">

	<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
		<input type="submit" class="btn btn-purple" id="add-event" name="SUBMIT">
	</div>  

</div>
</div>
@endif
</form>

</div>
</div>
</div>
</div>


	<div class="modal fade" id="edit_product" role="dialog">
		<div class="modal-dialog vendor-invite-popup">		
		  <!-- Modal content-->
		  <div class="modal-content invite-popup-content">
			<div class="modal-header">
			  <h4 class="modal-title">Edit Product Item</h4>
			</div>
			<div class="modal-body">
			<form id="invite-vendorform" class="invite-vendorform">
		
				<div class="form-group">
				<select class="form-control"  name="pmc_cat_name" id="pmc_cat_name">
					<option value="">-- Choose Your Category --</option>
					
					@foreach ($catagoires as $item)
					<option value="{{$item->pmc_id}}">{{$item->pmc_cat_name}}</option>
					
					@endforeach
					
					</select>
				</div>
				<div class="form-group">
					<select class="form-control"  name="pms_subcat_name" id="pms_subcat_name">
						<option value="">-- Choose Your Products --</option>
						</select>
					</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pea_event_spec" name="pea_event_spec" placeholder="Name of Spec *">
				</div>
				<div class="form-group">
				<select class="form-control" name="pmu_unit_name" id="pmu_unit_name">
					<option value="">-- Choose Your Units --</option>
					@foreach ($unit as $item)
					<option value="{{$item->pmu_id}}">{{$item->pmu_unit_name}}</option>
					
					@endforeach
					</select>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pea_event_unit_quantity" name="pea_event_unit_quantity" placeholder="Name of Qty *">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pea_event_start_price" name="pea_event_start_price" placeholder="Expected Price">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pea_event_max_dec" name="pea_event_max_dec" placeholder="Min Dec *">
				</div>
				<div class="form-group">
					<!--<input type="text" class="form-control" id="pea_event_location" name="" placeholder="Name of Location*">-->
					<select class="form-control select2"  id="pea_event_location" name="pea_event_location">
						<option value="">-- Choose Your Location --</option>
						@foreach ($location as $item)
						<option value="{{$item->pmci_city_name}}">{{$item->pmci_city_name}}</option>
						
						@endforeach
					</select>
				</div>
				<input type="hidden" value="" name="pmc_id" id="pmc_id">
				<div class="ownvendor_mailinput">
					<input type="submit"  class="btn btn-purple" value="Submit">
				</div>
			</form>
			
			<div class="invited-maillist">
			<p>dilipvinoth@gmail.com</p>
			<p>b.dilip@gmail.com</p>
			</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
			</div>
		  </div>
		  
		</div>
	</div>



<div class="modal fade" id="invite_ownvendor" role="dialog">
    <div class="modal-dialog vendor-invite-popup">    
      <!-- Modal content-->
		<div class="modal-content invite-popup-content">
			<div class="modal-header">
				<h4 class="modal-title">Invite Your Suppliers</h4>
			</div>
			<div class="modal-body">
				<form action="#" id="inviteSupp" class="invite-vendorform">
					<div class="ownvendor_mailinput">
					
						<div class="add_invite_email" style="position:relative; top:1em; margin-bottom:1em;"> 
			
						</div>
						
						<input type="hidden" class="form-control" id="cat_name" name="pmc_cat_name">
						<input type="hidden" class="form-control" id="subcat_name" name="pms_subcat_name">
						<input type="hidden" class="form-control" id="event_spec" name="pea_event_spec">
						<input type="hidden" class="form-control" id="unit_name" name="pmu_unit_name">
						<input type="hidden" class="form-control" id="event_unit_quantity" name="pea_event_unit_quantity">
						<input type="hidden" class="form-control" id="event_start_price" name="pea_event_start_price">
						<input type="hidden" class="form-control" id="event_max_dec" name="pea_event_max_dec">
						<input type="hidden" class="form-control" id="event_location" name="pea_event_location">
						<input type="hidden" class="form-control" id="pmc1_id" name="pmc_id">
						<input type="hidden" class="form-control" name="pec_event_name" value="{{$event_data->pec_event_name}}">
						<input type="hidden" class="form-control" name="pec_event_start_dt" value="{{$event_data->pec_event_start_dt}}">
						<input type="hidden" class="form-control" name="pec_event_end_dt" value="{{$event_data->pec_event_end_dt}}">
						<input type="text" class="form-control" name="pei_invite_details" placeholder="Enter Your vendor mail..">
						<div class="btn-danger inline-addmail-btn" id="add_mail">Add Mail</div>						
					</div>
				</form>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
			</div>
		</div>      
    </div>
</div>
  
  <div class="modal fade" id="invite_PQvendor" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content invite-popup-content">
        <div class="modal-header">
			<h4 class="modal-title">Invite Your New Suppliers</h4>
        </div>
        <div class="modal-body">
			<form action="#" class="invite-vendorform">
				<input type="hidden" class="form-control" id="cat_name1" name="pmc_cat_name">
				<input type="hidden" class="form-control" id="event_spec1" name="pea_event_spec">
				<input type="hidden" class="form-control" id="unit_name1" name="pmu_unit_name">
				<input type="hidden" class="form-control" id="event_unit_quantity1" name="pea_event_unit_quantity">
				<input type="hidden" class="form-control" id="event_start_price1" name="pea_event_start_price">
				<input type="hidden" class="form-control" id="event_max_dec1" name="pea_event_max_dec">
				<input type="hidden" class="form-control" id="pmc1_id1" name="pmc_id">
				<input type="hidden" class="form-control" name="pec_event_name" value="{{$event_data->pec_event_name}}">
				<input type="hidden" class="form-control" name="pec_event_start_dt" value="{{$event_data->pec_event_start_dt}}">
				<input type="hidden" class="form-control" name="pec_event_end_dt" value="{{$event_data->pec_event_end_dt}}">
				<div class="pqvendor_mailinput form-group col-sm-6 col-xs-12 col-md-6">
					<label for="">Location</label>
					<input type="text" class="form-control" id="event_location1" name="pea_event_location" readonly >
				</div>
				<div class="pqvendor_mailinput form-group col-sm-6 col-xs-12 col-md-6">
					<label for="">Product</label>
					<input type="text" class="form-control" id="subcat_name1" name="pms_subcat_name" readonly >
				</div>
			</form>
        </div>
		
		<div class="table-responsive invited-newlist" id="pq_invite_list">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Select</th>
						<th>Company</th>
						<th>Email</th>
						<th>Rating</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<label class="check-vendor">
								<input type="checkbox" name="is_name">
								<span class="check-checkmark"></span>
							</label>
						</td>
						<td>Samsung</td>
						<td>sales@samsung.com</td>
						<td class="vendor-star-rate">
							<i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
						</td>
					</tr>
					<tr>
						<td>
							<label class="check-vendor">
								<input type="checkbox" name="is_name">
								<span class="check-checkmark"></span>
							</label>
						</td>
						<td>Microsoft</td>
						<td>purchase@microsoft.com</td>
						<td class="vendor-star-rate">
							<i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
<input type="submit" class="btn btn-purple" id="add-event" name="SUBMIT">
</div>  
		
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>



    <!--/main content wrapper-->
</div>
    <!--footer-->

@endsection
