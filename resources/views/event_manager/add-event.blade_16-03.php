<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
$planID = Request::route('id');
$packageData=PurchaseController::getPackage($planID);
$session = session()->all(); 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];
$Pkg_curCount=EventController::eventPackageBalance($planID,$loginid);

$plan=$packageData->pmp_pkg_name;
$plan_id=$packageData->pmp_id;
$event_data=EventController::getEventData($loginid);
$choosePackage=PurchaseController::getBuyerPackage($loginid);


?>
@extends('event_manager/event-header')

@section('content')

<!--main content wrapper-->
    <div class="content-wrapper" id="event-content">
<!--creative states-->
	<div class="inner-page-img">
	<div class="container">
	<h3 class="page-title">Event Details</h3>
	</div>
	</div>
	
	<div class="box-container">
		<div class="row">              
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Event Details</h3>
							<p>Check your product details &amp; can edit your products</p>
						</div>
						@if($Pkg_curCount->pbp_pkg_cur_count!=0 )
						<form  class="event-detail-form" name="add_event" id="event-detail-form">
							<div class="col-md-4 col-xs-12 col-sm-4 form-group">
								<input type="text" class="form-control" name="pec_event_name" placeholder="Event Name">
							</div>

							<div class="col-md-4 col-xs-12 col-sm-4 form-group">
								<input type="text" class="form-control" name="pec_plan_name" value="{{strtoupper($plan)}}" readonly>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-4 form-group">
								<select class="form-control" name="pec_event_type">
									<option value="0">Event Type</option>
									<option value="1">Closed Bid</option>
									<option value="2">Reverse Auction</option>
									<option value="3">Both</option>
								</select>
							</div>
							
							 <div class="col-md-2 col-xs-12 col-sm-3 form-group date">
								<input type="hidden" class="form-control datetimes1" name="pec_event_start_dt" id="pec_event_start_dt" placeholder="Auction Start Date &amp; Time">
							</div>
							<div class="col-md-2 col-xs-12 col-sm-3 form-group date">
								<input type="hidden" class="form-control datetimes2" name="pec_event_end_dt" id="pec_event_end_dt" placeholder="Auction Start Date &amp; Time">
							</div>

						<!--	<div class="col-md-4 col-xs-12 col-sm-3 form-group date">
								<input type="text" class="form-control datetimes" name="pec_event_dt" placeholder="Auction Start Date &amp; Time">
							</div>-->
						<input type="hidden" id="package_id" name="package_id" value="{{$plan_id}}">
						<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
						
							<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
								<input type="submit" class="btn btn-purple" id="add-event" name="SUBMIT">
							</div>    
						</form>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="added-events">
		<div class="box-container">
			<div class="row"> 

				@foreach ($event_data as $item)
					
			
				<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
					<div class="card card-shadow mb-4">
						<div class="card-body">
							<div class="event-fulldetails">
								<div class="packname">
									<p>Package Name</p>
									<span>{{$item->pmp_pkg_name}}</span>
								</div>
								<div class="remain-no">
									<p>Remaining Event</p>
									<span>{{$item->pbp_pkg_cur_count}}</span>
								</div>
								<div class="liveevent-name">
									<p>Event Name</p>
									<span>{{$item->pec_event_name}}</span>
								</div>
								<div class="auctionid">
									<p>Event ID</p>
									<span>{{$item->pec_event_id}}</span>
								</div>
								<div class="auctionstart">
									<p>Event Starts</p>
									<span>{{$item->pec_event_start_dt}}</span>
								</div>
								<div class="auctionend">
									<p>Event Ends</p>
									<span>{{$item->pec_event_end_dt}}</span>
								</div>
								<div class="add-product-btn">
									<a href="/event-invite/{{$planID}}/{{$item->pec_event_id}}" class="btn btn-purple">Add Item</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				@endforeach
			</div>
		</div>
	</div>


    <!--/main content wrapper-->
</div>

@endsection



