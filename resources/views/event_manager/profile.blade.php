<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');

$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];

$event_data = EventController::upcomingEvent($loginid);


?>


@extends('event_manager/event-header')

@section('content')

<!--main content wrapper-->
<div class="content-wrapper">
		<!--creative states-->
			<div class="buyer-admin-cover">
			<img src="img/profile-banner.jpg" alt="">
			<div class="edit-profile-pic"><a href="#" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a></div>
			</div>
			
		<div class="bg-white">
		<div class="container-fluid">
		<div class="col-md-8 ml-auto">
		<div class="profile-nav-links">
			<ul class="nav f12">
				<li class="nav-item">
					<a class="nav-link active" href="#">MY DETAILS</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">My TRANSACTION</a>
				</li>
			</ul>
		</div>
		
		</div>
		
		</div>
		</div>
		
		 <div class="box-container">
		<div class="row">              
		<div class="col-md-3 col-xs-6 col-lg-3 col-sm-3 profile-info-position">
			<div class="card card-shadow">
				<div class="card-body">
					<div class="text-center">
						<div class="mt-4 mb-3">
							<img class="rounded-circle" src="img/profile-pic.png" width="85" alt="">
						</div>
						<h5 class="text-uppercase mb-0">B. Dilipkumar</h5>
						<p class="text-muted mb-0">Marketing Manager</p>
						
						<div class="profile-social-link mb-4">
							<p class="profile-ph"><i class="fa fa-phone" aria-hidden="true"></i> +91 9876543210</p>
							<p class="profile-em"><i class="fa fa-envelope" aria-hidden="true"></i> b.dilip@kuwy.in</p>
						</div>
					</div>
		
				</div>
			</div>
		</div>
		<div class="col-xl-9 profile-info-view">
		<div class="card card-shadow mb-4">
		<div class="card-header border-0">
		<div class="custom-title-wrap bar-primary">
		<div class="custom-title">My Profile</div>
		</div>
		</div>
		<div class="card-body">
		<form class=""  action="myprofile2.php" novalidate="" method='post'>
		<div class="form-row">
		<div class="col-md-4 mb-3">
				<label for="validationCustom02">Person Name * </label>
				<input type="text" class="form-control" value="Dilip" disabled  placeholder="Name" required="">
			</div>
			<div class="col-md-4 mb-3">
				<label for="validationCustom01">Phone No *</label>
				<input type="text" class="form-control" value="1234567890"  disabled placeholder="Phone" required="">
			</div>
			<div class="col-md-4 mb-3">
				<label for="validationCustom02">Email *</label>
				<input type="text" class="form-control" id="email" value="b.dilip@kuwy.in" disabled  placeholder="Email" required="">
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-4 mb-3">
				<label for="validationCustom03">GSTIN *</label>
				<input type="text" class="form-control" value="Enter GSTIN" disabled placeholder="Gstn" required="">
			</div>
				<div class="col-md-4 mb-3">
				<label for="validationCustom03">Name of Company *</label>
				<input type="text" class="form-control" value="MatexNet" disabled placeholder="Company Name" required="">
			</div>
			<div class="col-md-4 mb-3">
				<label for="validationCustom04">Pan Number *</label>
				<input type="text" class="form-control" value="WE345689" disabled placeholder="Pan Number" required="">
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-4 mb-3">
				<label for="validationCustom03">Type of Organization *</label>
				<input type="text" class="form-control" id="organi" value="Organ" disabled placeholder="Type of Organization" required="">
			</div>
				<div class="col-md-4 mb-3">
				<label for="validationCustom03">Company Phone *</label>
				<input type="text" class="form-control"  value="0123 4567 8901" disabled placeholder="Company Phone" required="">
			</div>
			<div class="col-md-4 mb-3">
				<label for="validationCustom04">Established Date *</label>
				<input type="text" class="form-control" name="pci_comp_establish_date"  placeholder="Established Date" data-toggle="datepicker" autocomplete="off" readonly="">
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-4 mb-3">
				<label for="validationCustom03">Nature of Business *</label>
				<input type="text" class="form-control" value="Products" disabled placeholder="Nature of Business" required="">
			</div>
				<div class="col-md-4 mb-3">
				<label for="validationCustom03">Location *</label>
				<input type="text" class="form-control"value="Chennai" disabled placeholder="Location" required="">
			</div>
			<div class="col-md-4 mb-3">
				<label for="validationCustom04">WebSite</label>
				<input type="text" class="form-control"  value="www.matexnet.com" disabled placeholder="Established Date" required="">
			</div>
		</div>
		
		<div class="form-row">
			
			<div class="col-md-6 mb-3">
				<label for="validationCustom05">Company Address *</label>
				<textarea class="form-control" placeholder="Address" disabled cols="60" rows="5"> MatexNet Pvt. Ltd 
		124, 2nd Floor,
		Karpagam Avenue, 4th Street,
		Opp to Mayor Ramanathan Hall, R.A Puram,
		Chennai - 600028. </textarea>
			</div>
			<div class="col-md-6 mb-3">
				<label for="validationCustom05">Communication  Address *</label>
				<textarea class="form-control"  placeholder="Address" disabled cols="60" rows="5"> Dilip 
		124, 1nd Floor,
		Karpagam Avenue, 4th Street,
		Opp to Mayor Ramanathan Hall, R.A Puram,
		Chennai - 600028. </textarea>
			</div>
			
		</div>
		<div class="form-group">
		  <span class="check-btn-label">Company Category</span>
			<div class="category ml-0">
			  <label>
			   <input type="radio" name="pci_comp_type_cat" value="1">
				<div class="back-end box1">
				  <span><i class="fa fa-cubes" aria-hidden="true"></i>  &nbsp; Product</span>
				</div>
			 </label>
			<label>
			 <input type="radio" name="pci_comp_type_cat" value="2">
				<div class="back-end box1"> 
				<span><i class="fa fa-cogs" aria-hidden="true"></i> &nbsp; Service</span>
				</div>
			  </label>
			<label>
				<input type="radio" name="pci_comp_type_cat" value="3">
				 <div class="back-end box1">
					<span> <i class="fa fa-group" aria-hidden="true"></i> &nbsp;  Both</span>
				 </div>
				</label>
			 </div>
		  </div>
		
		<button class="btn btn-primary" type="submit" id="myButton">Edit Profile</button>
		</form>
		
		</div>
		</div>
		</div>
						</div>
				</div>
			<!--/main content wrapper-->
		</div>
    <!--footer-->
@endsection