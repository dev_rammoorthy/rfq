<?php 
		$payment_status = Request::route('status');
		$session_val = session()->all();
		$packID = $session_val['package_data']['pmp_id'];
		$price = $session_val['package_data']['pmp_pkg_amount'];

	$payment_details=$session_val['cash_data'];

?>
@extends('header')

@section('content')


	<div class="container">
		<div class="row">
			<div class="col-xl-12 d-lg-flex align-items-center">
			<!--login form-->
			@if($payment_details['txStatus']=='SUCCESS')
				<div class="payment-page">
					<span class="payment-status-icon text-success"><i class="fa fa-check" aria-hidden="true"></i></span>
					<h4 class="text-uppercase text-success text-center mb-2">Payment Successfully</h4>
					<div class="payment-status-info">
						<p>Thank you! Your payment of Rs. {{$payment_details['orderAmount']}} has been received.</p> 

					<span class="payment-successid"><b>Transaction Purpose :</b> Buying an event </span>
					<span class="payment-successid"><b>Transaction Amount :</b> INR {{$payment_details['orderAmount']}}</span>
					<span class="payment-successid"><b>Transaction Status :</b> {{$payment_details['txMsg']}}</span>						
					<span class="payment-successid"><b>Transaction ID :</b>{{$payment_details['orderId']}}</span>
						<p class="btn-uppertxt">Kindly click below button and create your event.</p>
						<a href="{{ URL::to('create-event') }}/{{$packID}}" class="btn btn-purple" title="Create Event">Create Event</a>
						<a class="skip-page-link" href="{{ URL::to('event') }}" title="Back to page">Back to Page</a>
					</div>
				</div>
			@else
				<div class="payment-page">
					<span class="payment-status-icon danger text-danger"><i class="fa fa-close" aria-hidden="true"></i></span>
					<h4 class="text-uppercase text-danger text-center mb-2">Payment Failed</h4>
					<div class="payment-status-info">
							<span class="payment-successid"><b>Transaction Purpose :</b> Buying an event </span>
							<span class="payment-successid"><b>Transaction Amount :</b> INR {{$payment_details['orderAmount']}}</span>
							<span class="payment-successid"><b>Transaction Status :</b> {{$payment_details['txMsg']}}</span>
							<span class="payment-successid"><b>Transaction ID :</b> {{$payment_details['orderId']}}</span>
					
						<a class="skip-page-link" href="{{ URL::to('event') }}" title="Back to page">Back to Page</a>
					</div>
				</div>
			@endif
			</div>
		</div>
    </div>
@endsection

