<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \App\Http\Controllers\BankController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');

$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];

$bank_info = BankController::getBankInfo();

$page="My Wallet";
$pagetrack=PurchaseController::pageLogout($page);

?>


@extends('event_manager/event-header')

@section('content')

<div class="content-wrapper">
	<!--creative states-->
	<div class="inner-page-img">
		<div class="container">
            <h3 class="page-title">Confirm Bank Information</h3>
		</div>
	</div>

	<div class="box-container">
		<div class="row">              
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Confirm Bank Info</h3>
                            <div class="text-right">                               
                                <h6> * Confirm bank details to withdraw the money </h6>
                            </div>
						</div>
						
						<form class="event-detail-form" name="bank-confirm" id="bank-confirm">
                            <br>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> Name *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_name" name="pbd_name" value="{{ ($bank_info->pbd_name != null) ? $bank_info->pbd_name : null }}" placeholder="Enter Name" readonly>
								</div>

                            </div>   
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> Email *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="email" class="form-control" id="pbd_email" name="pbd_email" value="{{ ($bank_info->pbd_email != null) ? $bank_info->pbd_email : null }}" placeholder="Enter Email" readonly>
								</div>

                            </div> 
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> Phone Number *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="number" class="form-control" id="pbd_phone" name="pbd_phone" value="{{ ($bank_info->pbd_phone != null) ? $bank_info->pbd_phone : null }}" placeholder="Enter Phone Number" readonly>
								</div>

                            </div> 
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> Bank Account *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="number" class="form-control" id="pbd_bank_account" name="pbd_bank_account" value="{{ ($bank_info->pbd_bank_account != null) ? $bank_info->pbd_bank_account : null }}" placeholder="Enter Bank Account" readonly>
								</div>

                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> IFSC Code *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_ifsc" name="pbd_ifsc" value="{{ ($bank_info->pbd_ifsc != null) ? $bank_info->pbd_ifsc : null }}" placeholder="Enter IFSC Code" readonly>
								</div>

                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> Address 1 *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_address1" name="pbd_address1" value="{{ ($bank_info->pbd_address1 != null) ? $bank_info->pbd_address1 : null }}" placeholder="Enter Address 1" readonly>
								</div>

                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> Address 2 </h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_address2" name="pbd_address2" value="{{ ($bank_info->pbd_address2 != null) ? $bank_info->pbd_address2 : null }}" placeholder="Enter Address 2" readonly>
								</div>

                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> City *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_city" name="pbd_city" value="{{ ($bank_info->pbd_city != null) ? $bank_info->pbd_city : null }}" placeholder="Enter City" readonly>
								</div>

                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> State *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_state" name="pbd_state" value="{{ ($bank_info->pbd_state != null) ? $bank_info->pbd_state : null }}" placeholder="Enter State" readonly>
								</div>

                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group text-right">
                                    <h5 style="padding:7px"> Pin Code *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="number" class="form-control" id="pbd_pin" name="pbd_pin" value="{{ ($bank_info->pbd_pincode != null) ? $bank_info->pbd_pincode : null }}" placeholder="Enter Pin Code" readonly>
								</div>

                            </div>

							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            
								<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
						
								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                </div>
								<div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <?php if(isset($bank_info) && $bank_info != null) : ?>
                                        <input type="submit" class="btn btn-purple" id="wallet-submit" name="SUBMIT" value="Confirm Bank">
                                        <a href="{{ URL::to('wallet') }}" class="btn btn-danger">Goto Wallet</a>
                                        <a href="{{ URL::to('/') }}" class="btn btn-info" >Go Home</a>
                                    <?php else: ?>
                                        <a href="{{ URL::to('bank-info') }}" class="btn btn-danger">Update Bank Info</a>
                                        <a href="{{ URL::to('/') }}" class="btn btn-info" >Go Home</a>
                                    <?php endif; ?>

									
								</div>
                            </div>                  
                            
						</form>
						
					</div>
				</div>
			</div>
					
		</div>
	</div>
</div>

@endsection