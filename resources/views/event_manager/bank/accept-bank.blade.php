<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \App\Http\Controllers\BankController;
 use \App\Http\Controllers\WalletController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];

$walletId = Request::route('id');
$BeneId = Request::route('beneid');
$wallet_data = WalletController::showmoney();

$page="Accept Bank";
$pagetrack=PurchaseController::pageLogout($page);

?>


@extends('event_manager/event-header')

@section('content')

<div class="content-wrapper">
	<!--creative states-->
	<div class="inner-page-img">
		<div class="container">
            <h3 class="page-title">Withdraw money from wallet</h3>
		</div>
	</div>

	<div class="box-container">
		<div class="row">              
			<div class="col-xl-8 col-sm-8 col-md-8 col-lg-8 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Withdraw Money</h3>
                            <div class="text-right">                               
                                <h6> * Fill up the amount that you want to transfer into your bank account </h6>
                            </div>
						</div>
						
						<form class="event-detail-form" name="debit-amount" id="debit-amount">
                            <br>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Amount *</h5>
                                </div>

                                <div class="col-md-9 col-xs-9 col-sm-9 form-group">
                                    <input type="number" class="form-control" id="pbd_amount" name="pbd_amount" placeholder="Enter Amount in INR">
								</div>
                            </div>   
                            <br>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">      
                                <div class="alert alert-warning text-center" role="alert">
                                    Note : Confirm your credentials to transfer the money into your bank account
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">                                  
                                    <h5 style="padding:7px"> Email *</h5>
                                </div>
                                <div class="col-md-9 col-xs-9 col-sm-9 form-group">                                    
                                    <input type="email" class="form-control" name="login_username" id="login_username" value="{{$loginid}}" placeholder="Enter Your Login Email" readonly>
                                </div>

                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">                                  
                                    <h5 style="padding:7px"> Password *</h5>
                                </div>
                                <div class="col-md-9 col-xs-9 col-sm-9 form-group">
                                    <input type="password" class="form-control" name="login_password" id="login_password" placeholder="Enter Your Login Password">
                                </div>
                            </div>

							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">                            
								<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
                                <input type="hidden" id="walletid" name="walletid" value="{{$walletId}}">
                                <input type="hidden" id="BeneId" name="BeneId" value="{{$BeneId}}">
                                <input type="hidden" id="balance" name="balance" value="<?php echo (isset($wallet_data)) ? $wallet_data->pwc_current_balance : 0 ?>">
                                
								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                </div>
								<div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <!-- <a id="transfer-submit" class="btn btn-purple">Transfer</a> -->
									<input type="submit" class="btn btn-purple" id="transfer-submit" name="SUBMIT" value="Confirm">
									<a href="{{ URL::to('confirm-bank') }}" class="btn btn-danger">Back</a>
									<a href="{{ URL::to('/') }}" class="btn btn-info" >Go Home</a>
                                </div>                               
                                
                            </div> 
                            
						</form>						
					</div>
				</div>
			</div>

            <div class="col-xl-4 col-sm-4 col-md-4 col-lg-4 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Balance</h3>
						</div>						
						<br>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
							<div class="col-md-12 col-xs-12 col-sm-12 form-group">
								<h3 style="padding:7px"> <?php echo (isset($wallet_data)) ? $wallet_data->pwc_current_balance : 0 ?> INR</h3>
							</div>
						</div>					
					</div>
				</div>

			</div>		
					
		</div>
	</div>
</div>

    <div class="modal fade headShake" id="confirm-payment-form" role="dialog">
            <div class="modal-dialog vendor-invite-popup">
            
            <!-- Modal content-->
            <div class="modal-content modal-content-amount invite-popup-content">
                <div class="modal-header modal-header-amount" style="background: #ff9800;">
                    <h4 class="modal-title-amount">Confirm your credentials</h4>
                </div>
                    
                    <div class="modal-body modal-body-bg" style="height:auto">
                        <form class="event-detail-form" name="add_event" id="transaction-login-form">
                            <div class="col-md-12 row">
                                <div class="col-md-12 col-xs-12 col-sm-12 form-group date">
                                    <label> Email </label>
                                    <input type="text" class="form-control" name="login_username" id="login_username" placeholder="Login Email Id">
                                </div>
                                <div class="col-md-12 col-xs-12 col-sm-12 form-group date">
                                    <label> Password </label>
                                    <input type="password" class="form-control" name="login_password" id="login_password" placeholder="Login Password">
                                </div>
                            </div>

                            <div class="amount-btn">
                                <div style="margin-top:1em" class="float-left mb-4">
                                    <a id="transfer-model" class="btn-amount btn-primary text-center"> Confirm </a>
                                </div>
                                <div style="margin-top:1em" class="float-left ml-4 mb-4">
                                    <a  id="transfer-model-cancel" class="btn-cancel btn-light text-center"> Cancel </a>
                                </div>
                            </div>                    
                        </form>      
                    </div>                              
                </div>
            </div>
            
            </div>
        </div>

@endsection