<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \App\Http\Controllers\BankController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');

$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];

$bank_info = BankController::getBankInfo();

$bank_details = BankController::listBanks();

$page="List Bank";
$pagetrack=PurchaseController::pageLogout($page);

?>


@extends('event_manager/event-header')

@section('content')

<div class="content-wrapper">
	<!--creative states-->
	<div class="inner-page-img">
		<div class="container">
            <h3 class="page-title">Bank Accounts</h3>
		</div>
	</div>

	<div class="box-container">
		<div class="row">              
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>List Bank</h3>
                            <div class="text-right" style="margin-top: -35px;">
                                <a href="{{ URL::to('add-bank') }}" class="btn btn-purple">Add New Account</a>
                            </div>
                            <br>
                            <div class="text-right">                               
                                <h6> * Bank accounts you added </h6>
                            </div>
						</div>

                        <?php if(@count($bank_details) > 0) : ?>
						
                            <form class="event-detail-form" name="delete-bank" id="delete-bank">
                                <br>
                                <?php $i=1; ?>
                                <?php foreach($bank_details as $bankValue) { ?>
                                    <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                        <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                            <h5 style="padding:7px">Bank Account {{$i}}</h5>
                                        </div>

                                        <div class="col-md-9 col-xs-9 col-sm-9 form-group row" style="border: 1px #c5cbca solid;padding: 10px;">
                                            <div class="col-md-1 col-xs-1 col-sm-1 form-group">
                                                <input type="radio" name="radioBeneId" id="radioBeneId" value="{{ $bankValue->pbd_id }}" />
                                            </div>
                                            <div class="col-md-8 col-xs-8 col-sm-8 form-group row">
                                                    
                                                <div class="col-md-6">
                                                    <label> Account Holder Name  </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $bankValue->pbd_name ?> <br>
                                                </div>
                                                <div class="col-md-6">
                                                    <label> Bank Account Number </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $bankValue->pbd_bank_account ?> <br>
                                                </div>
                                                <div class="col-md-6">
                                                    <label> Bank IFSC </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $bankValue->pbd_ifsc ?> <br>
                                                </div>
                                                <div class="col-md-6">
                                                    <label> Address 1 </label> 
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $bankValue->pbd_address1 ?> <br>
                                                </div>
                                                <div class="col-md-6">
                                                    <label> Address 2 </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $bankValue->pbd_address2 ?> <br>
                                                </div>
                                                <div class="col-md-6">
                                                    <label> City </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $bankValue->pbd_city ?> <br>
                                                </div>
                                                <div class="col-md-6">
                                                    <label> State </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $bankValue->pbd_state ?> <br>
                                                </div>
                                                <div class="col-md-6">
                                                    <label> Pin Code </label> 
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $bankValue->pbd_pincode ?> 
                                                </div>                                                
                                                
                                            </div>
                                        </div>                                    
                                    </div> 
                                    <?php $i++; ?>  
                                <?php } ?>
                                
                                <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                
                                    <input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
                            
                                    <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    </div>
                                    <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                        <input type="submit" class="btn btn-purple" id="wallet-submit" name="SUBMIT" value="Delete">                                    
                                        <a href="{{ URL::to('wallet') }}" class="btn btn-danger">Goto Wallet</a>
                                        <a href="{{ URL::to('/') }}" class="btn btn-info" >Go Home</a>
                                    </div>
                                </div>                  
                                
                            </form>

                        <?php else: ?>

                            <!--inner-page-img End-->
							<div class="box-container">
							  <div class="row">
								  <div class="col-xl-12 text-center">
									 <img class="pr-3 img-fluid" src="{{asset('img/no-record.png')}}" alt="Purchase"/>
									<!-- <p> <a href="" class="btn btn-primary">Back</a> </p>-->
								  </div>
							   </div>
							 <!--row End-->
							 
							</div>
						<!--box-container End-->

                        <?php endif; ?>
						
					</div>
				</div>
			</div>
					
		</div>
	</div>
</div>

@endsection