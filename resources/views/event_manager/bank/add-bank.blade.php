<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \App\Http\Controllers\BankController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');

$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];

$page="Add Bank";
$pagetrack=PurchaseController::pageLogout($page);

?>


@extends('event_manager/event-header')

@section('content')

<div class="content-wrapper">
	<!--creative states-->
	<div class="inner-page-img">
		<div class="container">
            <h3 class="page-title">Add New Bank</h3>
		</div>
	</div>

	<div class="box-container">
		<div class="row">              
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Add Bank</h3>
                            <div class="text-right">                               
                                <h6> * Fill up your bank details to withdraw the money </h6>
                            </div>
						</div>
						
						<form class="event-detail-form" name="add-bank-info-form" id="add-bank-info-form">
                            <br>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Name *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_name" name="pbd_name" placeholder="Enter Name">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <label style="padding:7px"> Bank Account Holder Name </label>
                                </div>
                            </div>   
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Email *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="email" class="form-control" id="pbd_email" name="pbd_email" placeholder="Enter Email">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <label style="padding:7px"> Bank Linked Email Address </label>
                                </div>
                            </div> 
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Phone Number *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="number" class="form-control" id="pbd_phone" name="pbd_phone" placeholder="Enter Phone Number">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <label style="padding:7px"> Bank Linked Phone Number </label>
                                </div>
                            </div> 
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Bank Account *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="number" class="form-control" id="pbd_bank_account" name="pbd_bank_account" placeholder="Enter Bank Account">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <label style="padding:7px"> Bank Account Number </label>
                                </div>
                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> IFSC Code *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_ifsc" name="pbd_ifsc" placeholder="Enter IFSC Code">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <label style="padding:7px"> Bank IFSC Code </label>
                                </div>
                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Address 1 *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_address1" name="pbd_address1" placeholder="Enter Address 1">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">                                   
                                </div>
                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Address 2 (Optional) </h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_address2" name="pbd_address2" placeholder="Enter Address 2">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">                                   
                                </div>
                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> City *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_city" name="pbd_city" placeholder="Enter City">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">                                   
                                </div>
                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> State *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_state" name="pbd_state" placeholder="Enter State">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">                                   
                                </div>
                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Pin Code *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="number" class="form-control" id="pbd_pin" name="pbd_pin" placeholder="Enter Pin Code">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">                                   
                                </div>
                            </div>

							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            
								<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
						
								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                </div>
								<div class="col-md-6 col-xs-6 col-sm-6 form-group">
									<input type="submit" class="btn btn-purple" id="wallet-submit" name="SUBMIT" value="Save">
									<a href="{{ URL::to('list-bank') }}" class="btn btn-danger">Back</a>
									<a href="{{ URL::to('/') }}" class="btn btn-info" >Go Home</a>
								</div>
                            </div>                  
                            
						</form>
						
					</div>
				</div>
			</div>
					
		</div>
	</div>
</div>

@endsection