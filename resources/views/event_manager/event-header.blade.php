<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <!--favicon icon-->
    <link rel="icon" type="image/png" href="{{ asset ('new-img/purch-icon.png') }}">

    <title>MatexNet Event - Event Pricing</title>
    <!--web fonts-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,500,600,700,800" rel="stylesheet">
    <!--bootstrap styles-->
    <link href="{{ asset ('css/bootstrap.min.css') }}?{{ time() }}" rel="stylesheet">
    <!--icon font-->
    <link href="{{ asset ('css/font-awesome.min.css') }}?{{ time() }}" rel="stylesheet">
    <!--custom styles-->
    <link href="{{ asset ('css/main.css') }}?{{ time() }}" rel="stylesheet">
	<!-- matex css -->
    <link href="{{ asset ('css/custom.css') }}?{{ time() }}" rel="stylesheet">
	<!-- Bootstrap Pricing css -->
	<link href="{{ asset ('css/pricing-table.css') }}?{{ time() }}" rel="stylesheet">
	<!-- Bootstrap datetimepicker css -->
	<link href="{{ asset ('css/datetimepicker.css') }}?{{ time() }}" rel="stylesheet">
	<!-- Select2 css -->
	<link href="{{asset ('css/select2.css') }}?{{ time() }}" rel="stylesheet">
	<!-- iziToast css -->
	<link href="{{asset ('css/plugins/izitoast/iziToast.min.css') }}?{{ time() }}" rel="stylesheet">
	<!-- datatables css -->
	<link href="{{ asset ('css/datatables.css') }}?{{ time() }}" rel="stylesheet">
	@yield('assets')
</head>

 <?php 
  use \App\Http\Controllers\PurchaseController;
  use \App\Http\Controllers\WalletController;
 $session_val = session()->all();

 $choosePackage=PurchaseController::getBuyerPackage($session_val['pli_loginId']);
 $check_resetlogin=PurchaseController::checkloginReset($session_val['pli_loginId']);
 $checkWallet=WalletController::showmoney();
 
 $encrypted = Crypt::encryptString($session_val['pli_loginId']);
 

 ?>
  

<body class="fixed-nav top-nav">
	<div class="preloader" >
		<div class="loading" >
		 <div class="lds-ripple"><div></div><div></div></div>
		</div>
	</div>
	
    <!--header-->
    <nav class="navbar navbar-expand-lg fixed-top navbar-light" id="mainNav">
        <div class="box-container">
           <!--brand name for responsive-->
           <a class="navbar-brand navbar-brand-responsive" href="#">
               <img class="pr-3 float-left" src="{{ asset ('new-img/logo.png') }}" srcset="{{ asset ('new-img/logo.png') }}"  alt=""/>
           </a>
           <!--/brand name for responsive-->

            <!--responsive navigation list toggle-->
            <button class="navbar-toggler navigation-list-toggler" type="button" data-toggle="collapse" data-target="#navbarListResponsive" aria-controls="navbarListResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!--/responsive navigation list toggle-->

            <!--responsive nav notification toggle-->
            <button class="navbar-toggler nav-notification-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span> <i class="fa fa-user-o fa-5" aria-hidden="true"></i></span>
            </button>
            <!--/responsive nav notification toggle-->

            <div class="collapse navbar-collapse" id="navbarResponsive">

                <!--brand name-->
                <a class="navbar-brand navbar-hide-responsive" href="{{ URL::to('/') }}">
                    <img class="pr-3 float-left" src="{{ asset ('new-img/logo.png') }}"  alt=""/>
                </a>
                <!--/brand name-->

                <!--header rightside links-->
                <ul class="navbar-nav header-links ml-auto hide-arrow">
                 <!--   <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle mr-lg-3" id="alertsDropdown" href="#" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-bell-o" aria-hidden="true"></i>
                            <span class="d-lg-none">Notification
                               
                            </span>
                            <div class="notification-alarm">
                                <span class="wave wave-warning"></span>
                                <span class="dot bg-warning"></span>
                            </div>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right header-right-dropdown-width pb-0" aria-labelledby="alertsDropdown">
                            <h6 class="dropdown-header weight500">Notification</h6>

                            <div class="dropdown-divider mb-0"></div>
                            <a class="dropdown-item border-bottom" href="#">
                                <span class="text-primary">
                                <span class="weight500">
                                    Weekly Update</span>
                                </span>
                                <span class="small float-right text-muted">03:14 AM</span>

                                <div class="dropdown-message f12">
                                    This week project update report generated. All team members are requested to check the updates
                                </div>
                            </a>

                            <a class="dropdown-item border-bottom" href="#">
                                <span class="text-danger">
                                <span class="weight500">
                                    Server Error</span>
                                </span>
                                <span class="small float-right text-muted">10:34 AM</span>

                                <div class="dropdown-message f12">
                                    Unexpectedly server response stop. Responsible members are requested to fix it soon
                                </div>
                            </a>

                            <a class="dropdown-item border-bottom" href="#">
                                <span class="text-success">
                                <span class="weight500">
                                    Monthly Meeting</span>
                                </span>
                                <span class="small float-right text-muted">12:30 AM</span>

                                <div class="dropdown-message f12">
                                    Our monthly meeting will be held on tomorrow sharp 12:30. All members are requested to attend this meeting.
                                </div>
                            </a>

                            <a class="dropdown-item small" href="#">View all notification</a>
                        </div>
                    </li>-->
                    <li class="nav-item dropdown" style="border: 1px #ff9800 solid;background: azure;">
                        <a class="nav-link dropdown-toggle mr-lg-3" id="userNav" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-thumb">
                                <div class="signed-user">
									<p class="signed-username">Wallet Balance</p>
									<p class="signed-usertype"> <i class="fa fa-inr"> </i> <?php if($checkWallet) { echo $checkWallet->pwc_current_balance; } else { echo "0"; } ?></p>
								</div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle mr-lg-3" id="userNav" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-thumb">
                                <img class="rounded-circle" src="{{asset ('new-img/profile-pic.png') }}" alt=""/>
								<div class="signed-user">
									<p class="signed-username"><?php echo ucfirst($session_val['pli_con_name']); ?></p>
									<p class="signed-usertype"><?php if($session_val['pli_login_type'] == "1"){ echo ucfirst("Buyer"); }elseif($session_val['pli_login_type'] == "2"){ echo ucfirst("Seller"); } else{ 
                                        if($session_val['dashboard_check'] == "1")
                                                echo ucfirst("Seller"); 
                                        else
                                        echo ucfirst("Buyer"); 
                                            }
                                        
                                        
                                        ?></p>
								</div>
                            </div>
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userNav">
                           <!--     <a class="dropdown-item" href="myprofile.html" title="My Profile">My Profile</a>
                            <a class="dropdown-item" href="#">Account Settings</a>
                            <div class="dropdown-divider"></div>-->
                            <a class="dropdown-item" href="{{ URL::to('list-address') }}">Manage Shipping Address</a>
                            <a class="dropdown-item" href="{{ URL::to('wallet') }}">My Wallet</a>
                            <a class="dropdown-item" href="{{ URL::to('list-bank') }}">My Bank Accounts</a>
                            <a class="dropdown-item" href="{{ URL::to('confirm-bank') }}">Withdraw Money</a>
                            <a class="dropdown-item" href="{{ URL::to('logout') }}">Sign Out</a>
                        </div>
                    </li>
                </ul>
                <!--/header rightside links-->
            </div>
        </div>
    </nav>

    <nav class="navbar navbar-expand-lg navbar-dark"> 
        <div class="box-container">
            <div class="collapse navbar-collapse" id="navbarListResponsive">
                <!--header nav links-->
                <ul class="navbar-nav header-links">
                        <li class="nav-item active">
                        <a class="nav-link mr-lg-3" href="index.html">
                        
                        </a>
                        </li>
                        @if(($session_val['pli_login_type'] =="1"  && $session_val['dashboard_check']=="0") || ($session_val['pli_login_type'] =="3"  && $session_val['dashboard_check']=="0"))
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle mr-lg-3" id="actionNav" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        My RFQ
                        </a>
                        <div class="dropdown-menu" aria-labelledby="actionNav">
                        <a class="dropdown-item" href="{{ URL::to('event')}}" title="Event RFQ">RFQ Creation</a>
                        @if ( count($choosePackage)!=0)
                            
                      
                        <a class="dropdown-item" href="{{ URL::to('create-event')}}/{{$choosePackage[0]->pmp_id}}" title="Event Details">Event RFQ</a>

                        @endif
                        <!--<a class="dropdown-item" href="event-invite.php">Event Invites</a>-->
                        </div>
                        </li>
                        @endif
                       
                        @if(($session_val['pli_login_type'] =="1"  && $session_val['dashboard_check']=="0") || ($session_val['pli_login_type'] =="3"  && $session_val['dashboard_check']=="0"))
                        <li class="nav-item">
                        <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('upcoming-event')}}">Upcoming</a>
                        </li>
                        <li class="nav-item">
                                <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('buyer-live-auction')}}">Live RFQ</a>
                                </li>
                        <li class="nav-item">
                                <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('buyer-complete')}}">Completed RFQ</a>
                                </li>
						<!-- <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('buyer-complete-mis')}}">MIS Completed Events</a>
                          </li> -->

                          <!-- <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('reports')}}">Reports</a>
                          </li> -->
                        <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('accepted-event')}}">Orders</a>
                        </li>  
                        <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('documents')}}">Documents</a>
                        </li>  
                        <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('my-invoice')}}">Invoice</a>
                        </li>     
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle mr-lg-3" id="actionNav" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Reports
                            </a>
                            <div class="dropdown-menu" aria-labelledby="actionNav">
                                <a class="dropdown-item" id="reportNav" href="{{ URL::to('reports')}}" title="Reports">Summary Report</a>
                                <a class="dropdown-item" href="{{ URL::to('buyer-complete-mis')}}" title="Event Details">MIS Report</a>
                                <!-- <a class="dropdown-item" href="{{ URL::to('sellerreport') }}" title="Event Details">Sellerwise Report</a> -->
                                <a class="dropdown-item" href="{{ URL::to('reversereport')}}" title="Event Details">Reverse Bid Report</a>
                                <a class="dropdown-item" href="{{ URL::to('closereport')}}" title="Event Details">Closed Bid Report</a>
                                <!-- <a class="dropdown-item" href="#" title="Event Details">Sellers Participated</a> -->
                                <a class="dropdown-item" href="{{ URL::to('highlowreport') }}" title="Event Details">Highest/Lowest Bid Report</a> 
                                <a class="dropdown-item" href="{{ URL::to('bidwisereport') }}" title="Event Details">Bidwise Report</a> 
                                <a class="dropdown-item" href="{{ URL::to('sellerwisereport') }}" title="Event Details">Seller-wise Report</a>
                                <a class="dropdown-item" href="{{ URL::to('chartsReprts') }}" title="Event Details">Chart Reports</a> 
                            </div>
                        </li>
                               
                        @elseif($session_val['pli_login_type'] =="2"  || $session_val['dashboard_check']=="1")
                        <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('seller-upcoming')}}">Upcoming</a>
                            </li>
                            <li class="nav-item">
                                    <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('live-auction')}}">Live RFQ</a>
                                    </li>
                          <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('complete-auction')}}">Completed RFQ</a>
                          </li>
						  <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('complete-auction-mis')}}">MIS Completed RFQ</a>
                          </li>

                          <li class="nav-item">
                                <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('accepted-event')}}">Orders</a>
                        </li>  
                        <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('documents')}}">Documents</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('my-invoice')}}">Invoice</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('accepted-invoice')}}">Logistics</a>
                        </li> 

                        @endif
                        
                      
                       <!-- <li class="nav-item">
                        <a class="nav-link mr-lg-3" id="reportNav" href="#">Reports</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link mr-lg-3" id="reportNav" href="#">Master</a>
                        </li>-->
                        </ul>
                        <!--/header nav links-->
                        <!--header search-->
                        <ul class="navbar-nav header-links ml-auto">
                        @if($session_val['pli_login_type'] == "1")
                        <li class="nav-item">
                        <a class="become-acc-type" target="_blank" href="{{ URL::to('sregister/') }}/{{$encrypted}}">Become Seller</a>
                        </li>
                        @elseif($session_val['pli_login_type'] == "2")	
                        <li class="nav-item">
                        <a class="become-acc-type" target="_blank" href="{{ URL::to('register') }}/{{$encrypted}}">Become Buyer</a>
                        </li>	
                        @else
                        
                        @if($session_val['dashboard_check']=="0")
                        <li class="nav-item">
                        <a class="become-acc-type" href="{{ URL::to('gotoSeller') }}">Go Seller</a>
                        </li>
                        @elseif($session_val['dashboard_check']=="1")
                        <li class="nav-item">
                            <a class="become-acc-type" href="{{ URL::to('gotoBuyer') }}">Go Buyer</a>
                            </li>
                            @endif
                        @endif
                        </ul>
                <!--/header search-->
            </div>
        </div>
    </nav>
    <!--/header-->
	
	@yield('content')

 <!--footer-->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>&copy; 2018 MatexNet Pvt. Ltd.</small>
            </div>
        </div>
    </footer>
    <!--/footer-->

    <!--right sidebar-->

    <!--/right sidebar-->

<!--basic scripts-->
<script src="{{ asset ('js/jquery.min.js') }}?{{ time() }}"></script>
<script src="{{ asset ('js/bootstrap.min.js') }}?{{ time() }}"></script>
@yield('scripts')	
<!--basic scripts initialization-->
<script src="{{ asset ('js/scripts.js') }}?{{ time() }}"></script>
<script src="{{ asset ('js/moment.js') }}?{{ time() }}"></script>
<!--Datepicker bootstrap js-->
<script src="{{ asset ('js/datetimepicker.js') }}?{{ time() }}"></script>

<script src="{{ asset ('js/jquery.validate.min.js') }}?{{ time() }}"></script>
<script src="{{ asset ('js/page.js') }}?{{ time() }}"></script>
<script src="{{ asset('js/plugins/izitoast/iziToast.min.js') }}?{{ time() }}"></script>
<script src="{{ asset('js/select2.full.js') }}?{{ time() }}"></script>
<script src="{{ asset ('js/bootstable.min.js') }}?{{ time() }}"></script>

@yield('inline-scripts')

<div class="modal fade" id="force_reset_password" role="dialog">
    <div class="modal-dialog vendor-invite-popup">
    
      <!-- Modal content-->
      <div class="modal-content invite-popup-content">
        <div class="modal-header">
         <h4 class="text-uppercase text-purple text-center mb-2 mt-1">Change Password
                <button type="button" class="btn btn-close-mail float-right skippopup" data-dismiss="modal">X</button>
             </h4> 

        </div>
        <div class="modal-body">
		 <a href="#" class="float-left forgot-link my-2">Please Enter Your New Password </a>
			      <form name="force_reset" id="force_reset" novalidate="novalidate">
				 <div class="form-group mb-4">
					<input type="password" name="new_password" class="form-control" id="new_password" placeholder="New Password">
				</div>
				<div class="form-group mb-4">
					<input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="Confirm Password">
                </div>
            <input type="hidden" name="email" value="{{$session_val['pli_loginId']}}" class="form-control" id="email" >
				
				<div class="form-group clearfix">
					<button type="submit" ID="btnSuccess" runat="server"  class="btn btn-purple float-center">Submit</button>	
                  <span class="float-right btn-link text-capitalize cursor skippopup">Skip</span>					
				</div>
			</form>	

      </div>
      
    </div>
  </div>
</div>
</body>
</html>
<?php if($check_resetlogin->pli_firsttime_login==0){
    if(!isset($session_val['skip'])){
    ?>
<script type="text/javascript">

    $(document).ready(function(){
        $("#aap").hide();
      $('#force_reset_password').modal('show');
    });
    
    </script>
    <?php }} ?>

    