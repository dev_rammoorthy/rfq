<?php 

 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;

$session = session()->all(); 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];

$event_data=EventController::buyerCompletedEvent1($loginid);

?>



@extends('event_manager/event-header')

@section('content')

<!--main content wrapper-->
<div class="content-wrapper">
        <!--creative states-->
            <div class="inner-page-img">
            <div class="container">
            <h3 class="page-title">Completed Event</h3>
            </div>
            </div>
            
            
        
        <div class="box-container">
        <div class="row">            
        
 
         @if ($event_data)
             
        
        <div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
        <div class="auction-report-title">
        <div class="auction-collapsed-info">
        <span>Event ID</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event Name</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event start Date </span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event end Date </span>
        </div>
        <div class="auction-collapsed-info">
        <span>No.of Items</span>
        </div>
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach ($event_data as $item)
                        <div class="panel panel-default">
                            <div class="panel-heading auction-accordion" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$item->pec_event_id}}" aria-expanded="false" aria-controls="collapseOne">
                                            <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_id}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_name}}</p>
                                                    </div>
                                                    
                                                
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_start_dt}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_end_dt}}</p>
                                                    </div>
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->no_item}}</p>
                                                            </div>
                                    </a>
                                </h4>
                            </div>
                            
                            <div id="{{$item->pec_event_id}}"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="auction-inviter-fullinfo">
                                    <div class="table-responsive">
                                    <table class="table">
            <thead>
                <tr>
                <th>Category</th>
                <th>Product Name</th>
                <th>Spec</th>
                <th>Units</th>
                <th>Qty</th>
                <th>Location</th>
                <th>Start Price</th>
                <th>Min Inc.</th>
                <th>No.of Bidder</th>
                </tr>
            </thead>
            <tbody>

                
                    <?php $eventItem = EventController::getAddedItems2($item->pec_event_id);
										
                    ?>

                    <?php if(@count($eventItem) > 0) : ?>

                        @foreach ($eventItem as $add_item)
                <tr class="clickable" data-toggle="collapse" data-target="#group-of-rows-{{$add_item->pea_id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                        <?php                                            
                                      
                        $email_count= json_decode($add_item->pei_invite_details);
                        $invite_count=count( $email_count);
              
                             ?>  
                    
                        <td>{{$add_item->pmc_cat_name}}</td>
                        <td>{{$add_item->pms_subcat_name}}</td>
                        <td>{{$add_item->pea_event_spec}}</td>
                        <td>{{$add_item->pmu_unit_name}}</td>
                        <td>{{$add_item->pea_event_unit_quantity}}</td>
                        <td>{{$add_item->pea_event_location}}</td>
                        <td>{{$add_item->pea_event_start_price}}</td>
                        <td>{{$add_item->pea_event_max_dec}}</td>
                    <td>
                            <?php $sellerbid = EventController::sellerBidDetails($add_item->pea_id);
										
                            ?>
                    <span class="lot-mail-req">No.of Acceptance : {{$add_item->pei_invite_accept_count}}</span>
                    <a href="javascript:" id="hover-mail-list"><span class="mail-invite-list">{{count($sellerbid)}}</span></a>
                    </td>
                </tr>
             
            </tbody>
                    
            <tbody id="group-of-rows-{{$add_item->pea_id}}" class="collapse bidder-expand-info">
                    @foreach ($sellerbid as $bid_item)
                <tr>
                <td colspan="9">
                   <div class="final-bidder-details">
                   <div class="live-bid-info col-md-2 col-sm-3 col-xs-12">
                   <p>{{$bid_item->pci_comp_name}}</p>
                   <span>Company Name</span>
                   </div>
                   
                   <div class="live-bid-info col-md-3 col-sm-3 col-xs-12">
                   <p>{{$bid_item->pla_seller_emailid}}</p>
                   <span>Vendor Email</span>
                   </div>
                   
                   <div class="live-bid-info bid-highlight-price col-md-2 col-sm-3 col-xs-12">
                   <p>{{$bid_item->pla_cur_bid}}</p>
                   <span>Bid Price</span>
                   </div>
                   <div class="live-bid-info col-md-2 col-sm-3 col-xs-12">
                        <p>Location</p>
                        <span>Chennai</span>
                        </div>
                        <div class="live-bid-info col-md-3 col-sm-3 col-xs-12">
                @if ($bid_item->pla_auction_status=='1')
			    <span class="event-accepted"><i class="fa fa-check" aria-hidden="true"></i> Accepted</span>
                <span class="send-invoice"><a href="javascript:" title="Accept" class="btn-purple btn font-zize12">Send Invoice</a></span>

                @elseif ($bid_item->pla_auction_status=='2')
                <span class="bid-reject-status">Rejected <i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span>
                        </div>
                @else
      
             

                   <div class="live-bid-info col-md-2 col-sm-3 col-xs-12"> 
                   <button  id="{{$bid_item->pla_id}}" data-mail="{{$bid_item->pla_seller_emailid}}" type="button" class="btn btn-sm btn-edit-row btn-purple mt-1 finalizedBid" onclick="rowEdit(this);">
                            <span class="text-white" >Accept</span></button> 
                       </div>

                       @endif
                   </div>
                   </td>
                </tr>
                
                
               
                @endforeach     
                
            </tbody>
           
            @endforeach    

            <?php else: ?>
                <tr>
                    <td colspan="12">
                        No completed items found for this event
                    </td>
                </tr>
            <?php endif; ?> 
        
        </table>
        </div>
        
        
                                    </div>
                                </div>
                            </div>
                        </div>
       
                        @endforeach
                    </div>
        
        </div>
        @else
			<!--inner-page-img End-->
			<div class="box-container">
			  <div class="row">
				  <div class="col-xl-12 text-center">
					 <img class="pr-3 img-fluid" src="{{asset('img/norecord.png')}}" alt="Purchase"/>
					 <!--<p> <a href="" class="btn btn-primary">Back</a> </p>-->
				  </div>
			   </div>
			 <!--row End-->
			 
			 </div>
			 <!--box-container End-->
        @endif
        </div>
        </div>
            <!--/main content wrapper-->
        </div>
        
@endsection