<?php 
		$payment_status = Request::route('status');
		$session_val = session()->all();

?>
@extends('header')

@section('content')


	<div class="container">
		<div class="row">
			<div class="col-xl-12 d-lg-flex align-items-center">
			<!--login form-->
				
				<div class="payment-page">
					<span class="payment-status-icon danger text-danger"><i class="fa fa-close" aria-hidden="true"></i></span>
					<h4 class="text-uppercase text-danger text-center mb-2">Transaction Failed</h4>
					<div class="payment-status-info">						
						<span class="payment-successid"><b> Your wallet recharge failed. Try again </b> </span>
						<a class="skip-page-link" href="{{ URL::to('wallet') }}" title="Back to page">Back to Wallet</a>
					</div>
				</div>
		
			</div>
		</div>
    </div>
@endsection

