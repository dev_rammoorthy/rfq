<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \App\Http\Controllers\WalletController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');

$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];

$event_data = EventController::upcomingEvent1($loginid);
$wallet_data = WalletController::showmoney();

$page="My Wallet";
$pagetrack=PurchaseController::pageLogout($page);

?>


@extends('event_manager/event-header')

@section('content')

<div class="content-wrapper">
	<!--creative states-->
	<div class="inner-page-img">
		<div class="container">
            <h3 class="page-title">My Wallet</h3>
		</div>
	</div>

	<div class="box-container">
		<div class="row">              
			<div class="col-xl-8 col-sm-8 col-md-8 col-lg-8 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Add Money</h3>
                            <div class="text-right">                               
                                <h6> * Add money and use it in auction</h6>
                            </div>
						</div>
						
						<form action="{{ action('WalletController@addmoney') }}" method="POST"  class="event-detail-form" id="wallet-form">
                            <br>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                    <h5 style="padding:7px"> Amount </h5>
                                </div>

                                <div class="col-md-8 col-xs-8 col-sm-8 form-group">
                                    <input type="number" class="form-control" id="wallet_amount" name="wallet_amount" placeholder="Enter Amount">
								</div>
                            </div>        

							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                    <h5 style="padding:7px"> Payment Method </h5>
                                </div>

								<div class="col-md-8 col-xs-8 col-sm-8 form-group">
									<div class="middle">
										<!-- <label>
											<input type="radio" name="payment_method" value="1"/>
											<div class="back-end box" style="width:100px">
												<span>Instamojo</span>
											</div>
										</label> -->
										<label>
											<input type="radio" name="payment_method" value="2"/>
											<div class="back-end box" style="width:100px">
												<span>Cashfree</span>
											</div>
										</label>
									</div>

									<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
						   
									<br>	
									<input type="submit" class="btn btn-purple" id="wallet-submit" name="SUBMIT">
						
								</div>
                               
                            </div>                  
                            
						</form>
						
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-sm-4 col-md-4 col-lg-4 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Balance</h3>
						</div>						
						<br>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
							<div class="col-md-12 col-xs-12 col-sm-12 form-group">
								<h3 style="padding:7px"> <?php echo (isset($wallet_data)) ? $wallet_data->pwc_current_balance : 0 ?> INR</h3>
							</div>
						</div>					
					</div>
				</div>
				
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>History</h3>
						</div>						 
						<br>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
							<div class="col-md-12 col-xs-12 col-sm-12 form-group">
								* <a class="" href="{{ URL::to('wallethistory') }}" >Wallet History</a> <br>
								* <a href="{{ URL::to('emdhistory') }}" >EMD History</a>								
							</div>
						</div>					
					</div>
				</div>

			</div>			
		</div>
	</div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">                

                <div class="panel-body">
                    {!! $line->html() !!}
				</div>
					
            </div>
        </div>
    </div>
</div>

{!! Charts::scripts() !!}
{!! $line->script() !!}

@endsection