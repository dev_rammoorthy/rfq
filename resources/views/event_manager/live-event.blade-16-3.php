<?php 

 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;

$session = session()->all(); 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];

$event_data=EventController::buyerLiveEvent($loginid);

?>



@extends('event_manager/event-header')

@section('content')

<!--main content wrapper-->
<div class="content-wrapper">
        <!--creative states-->
            <div class="inner-page-img">
            <div class="container">
            <h3 class="page-title">Live Events</h3>
            </div>
            </div>
            
            
        
        <div class="box-container">
        <div class="row">            
        

@if ($event_data)
    


        <div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
        <div class="auction-report-title">
        <div class="auction-collapsed-info">
        <span>Event ID</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event Name</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event start Date </span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event end Date </span>
        </div>
        <div class="auction-collapsed-info">
        <span>No.of Items</span>
        </div>
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach ($event_data as $item)
                        <div class="panel panel-default">
                            <div class="panel-heading auction-accordion" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$item->pec_event_id}}" aria-expanded="false" aria-controls="collapseOne">
                                            <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_id}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_name}}</p>
                                                    </div>
                                                    
                                                
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_start_dt}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_end_dt}}</p>
                                                    </div>
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->no_item}}</p>
                                                            </div>
                                    </a>
                                </h4>
                            </div>
                            
                            <div id="{{$item->pec_event_id}}"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="auction-inviter-fullinfo">
                                    <div class="table-responsive">
                                    <table class="table">
            <thead>
                <tr>
                <th>Category</th>
                <th>Product Name</th>
                <th>Spec</th>
                <th>Units</th>
                <th>Qty</th>
                <th>Location</th>
                <th>Start Price</th>
                <th>Min Dec.</th>
                <th>No.of Bidder</th>
                <th>Invite</th>
                </tr>
            </thead>
            <tbody>

                
                    <?php $eventItem = EventController::getAddedItems($item->pec_event_id);
										
                    ?>
                        @foreach ($eventItem as $add_item)
                <tr class="clickable" data-toggle="collapse" id="{{$add_item->pea_id}}"  date-catid="{{$add_item->pmc_id}}" data-target="#group-of-rows-{{$add_item->pea_id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                        <?php                                            
                                      
                        $email_count= json_decode($add_item->pei_invite_details);
                        $invite_count=count( $email_count);
              
                             ?>  
                    
                        <td>{{$add_item->pmc_cat_name}}</td>
                        <td>{{$add_item->pms_subcat_name}}</td>
                        <td>{{$add_item->pea_event_spec}}</td>
                        <td>{{$add_item->pmu_unit_name}}</td>
                        <td>{{$add_item->pea_event_unit_quantity}}</td>
                        <td>{{$add_item->pea_event_location}}</td>
                        <td>{{$add_item->pea_event_start_price}}</td>
                        <td>{{$add_item->pea_event_max_dec}}</td>
                    <td>
                            <?php $sellerbid = EventController::sellerBidDetails($add_item->pea_id);
										
                            ?>
                    <span class="lot-mail-req">No.of Acceptance : {{$add_item->pei_invite_accept_count}}</span>
                    <a href="javascript:" id="hover-mail-list"><span class="mail-invite-list">{{count($sellerbid)}}</span></a>
                    </td>
                    <td name="buttons">
                        <div class="btn-group table-action-btn">						
                            <button type="button" class="btn btn-sm btn-invite-row dropdown">												
                                <a href="javascript:" class="dropbtn"><span class="fa fa-envelope"></span><span class="mail-counts">{{ $invite_count}}</span></a>
                                <div class="dropdown-content">														 
                                    <a href="#" class="own_vender" title="Own Vendor" data-toggle="modal" data-target="#invite_ownvendor" data-seller="{{$loginid}}"  data-event_name="{{$item->pec_event_name}}"  data-term_condition="{{$add_item->pea_event_terms_condition}}" data-event_start_dt="{{$item->pec_event_start_dt}}" data-event_end_dt="{{$item->pec_event_end_dt}}" data-id="{{$add_item->pea_id}}" data-event="{{$item->pec_event_type}}">Own Vendor</a>
                                   <a href="#" class="pq_vender" title="PQ Vendor" data-toggle="modal" data-target="#invite_PQvendor">PQ Vendor</a>
                                </div>
                            </button>
                        </div>
                        
                    </td>
                </tr>

             
            </tbody>
          
          
          
            <tbody id="group-of-rows-{{$add_item->pea_id}}" class="collapse bidder-expand-info">
                
                    @foreach ($sellerbid as $bid_item)
                <tr>
                <td colspan="9">
                   <div class="final-bidder-details">
                   <div class="live-bid-info col-md-3 col-sm-3 col-xs-12">
                   <p>{{$bid_item->pci_comp_name}}</p>
                   <span>Company Name</span>
                   </div>
                   
                   <div class="live-bid-info col-md-3 col-sm-3 col-xs-12">
                   <p>{{$bid_item->pla_seller_emailid}}</p>
                   <span>Vendor Email</span>
                   </div>
                   
                   <div class="live-bid-info bid-highlight-price col-md-3 col-sm-3 col-xs-12">
                   <p id="buyer_bid_{{$bid_item->pla_id}}">{{$bid_item->pla_cur_bid}}</p>
                   <span>Bid Price</span>
                   </div>
                   
                   <div class="live-bid-info col-md-3 col-sm-3 col-xs-12">
                   <p>Location</p>
                   <span>Chennai</span>
                   </div>
                   </div>
                   </td>
                </tr>
                
                
                 @endforeach     
                
                
            </tbody>
          
            @endforeach     
        
        </table>
        </div>
        
        
                                    </div>
                                </div>
                            </div>
                        </div>
       
                        @endforeach
                    </div>
        
        </div>

        @else
			
       <!--inner-page-img End-->
			<div class="box-container">
			  <div class="row">
				  <div class="col-xl-12 text-center">
					 <img class="pr-3 img-fluid" src="{{asset('img/norecord.png')}}" alt="Purchase"/>
					<!-- <p> <a href="" class="btn btn-primary">Back</a> </p>-->
				  </div>
			   </div>
			 <!--row End-->
			 
			</div>
        <!--box-container End-->
        

@endif
        </div>
        </div>
            <!--/main content wrapper-->
        </div>

        @if ($event_data)
        <div class="modal fade" id="invite_ownvendor" role="dialog">
            <div class="modal-dialog mail-invite-invite-popup">    
              <!-- Modal content-->
                <div class="modal-content invite-popup-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Invite Your Suppliers </h4>  
                    <button type="button" class="btn btn-close-mail float-right" data-dismiss="modal">X</button></h4>
                       
        
                    </div>
                    <div class="modal-body inviteSu">
                        
                        <form action="#" id="inviteSupp" class="invite-vendorform">
                            <div class="ownvendor_mailinput">
                          
                            <div class="add_invite_email" style="position:relative; top:1em; margin-bottom:1em;"> 
                                
                            </div>	 
                   
                             <div class="addmail-input">
                                <input type="hidden" class="form-control" id="cat_name" name="pmc_cat_name">
                                <input type="hidden" class="form-control" id="subcat_name" name="pms_subcat_name">
                                <input type="hidden" class="form-control" id="event_spec" name="pea_event_spec">
                                <input type="hidden" class="form-control" id="unit_name" name="pmu_unit_name">
                                <input type="hidden" class="form-control" id="event_unit_quantity" name="pea_event_unit_quantity">
                                <input type="hidden" class="form-control" id="event_start_price" name="pea_event_start_price">
                                <input type="hidden" class="form-control" id="event_max_dec" name="pea_event_max_dec">
                                <input type="hidden" class="form-control" id="event_location" name="pea_event_location">
                                <input type="hidden" class="form-control" id="pmc1_id" name="pmc_id">
                                <input type="hidden" class="form-control" id="pec_event_name"  name="pec_event_name" >
                                <input type="hidden" class="form-control" id="pec_event_start_dt" name="pec_event_start_dt">
                                <input type="hidden" class="form-control" id="pec_event_end_dt" name="pec_event_end_dt">
                                <input type="hidden" class="form-control" id="pec_loginID" name="pec_loginID">
                                <input type="hidden" class="form-control" id="event_type" name="event_type">
                                <input type="hidden" class="form-control" id="pea_event_terms_condition" name="pea_event_terms_condition">
                                <input type="email" class="form-control invite_email" value="" id="pei_invite_details" name="pei_invite_details" placeholder="Enter Your vendor mail..">
                                <input type="hidden" class="form-control invite_emails" id="pei_invite_details1" name="pei_invite_details1" placeholder="Enter Your vendor mail..">
                                <div class="btn-danger inline-addmail-btn" id="add_mail">Add Mail</div>
                                <div class="mail-submit-but">
                                <input type="submit" class="btn btn-primary invite_submit_mail text-center" value="Submit"> </div>
                                </div>
                            </div>			
                          
        
                        </form>
                        
                    </div>
                    <div class="modal-body success_val">					
                        <div id="status">
                        </div>
                    </div>
        
                </div>      
            </div>
        </div>
        
        <div class="modal fade" id="invite_PQvendor" role="dialog">
			<div class="modal-dialog">
			
			  <!-- Modal content-->
			  <div class="modal-content invite-popup-content">
				<div class="modal-header">
					<h4 class="modal-title">Invite Your New Suppliers</h4>
				</div>
				<div class="modal-body">
					<form action="#" class="invite-vendorform">
                        <input type="hidden" class="form-control" id="cat_name1" name="pmc_cat_name">
                        <input type="hidden" class="form-control" id="cat_id1" name="cat_id1">
						<input type="hidden" class="form-control" id="event_spec1" name="pea_event_spec">
						<input type="hidden" class="form-control" id="unit_name1" name="pmu_unit_name">
						<input type="hidden" class="form-control" id="event_unit_quantity1" name="pea_event_unit_quantity">
						<input type="hidden" class="form-control" id="event_start_price1" name="pea_event_start_price">
						<input type="hidden" class="form-control" id="event_max_dec1" name="pea_event_max_dec">
						<input type="hidden" class="form-control" id="pmc1_id1" name="pmc_id">
						<input type="hidden" class="form-control" name="pec_event_name1" value="{{$item->pec_event_name}}">
						<input type="hidden" class="form-control" name="pec_event_start_dt1" value="{{$item->pec_event_start_dt}}">
						<input type="hidden" class="form-control" name="pec_event_end_dt1" value="{{$item->pec_event_end_dt}}">
						<div class="pqvendor_mailinput form-group col-sm-6 col-xs-12 col-md-6">
							<label for="">Location</label>
							<input type="text" class="form-control" id="event_location1" name="pea_event_location" readonly >
						</div>
						<div class="pqvendor_mailinput form-group col-sm-6 col-xs-12 col-md-6">
							<label for="">Product</label>
							<input type="text" class="form-control" id="subcat_name1" name="pms_subcat_name" readonly >
						</div>
					</form>
				</div>
				
				<div class="table-responsive invited-newlist"  id="pq_invite_list">
				
				<input type="hidden" class="form-control invite_pqemails" id="invite_pqemails" name="pei_invite_details1">
					
				</div>
				
		 
				
				<div class="modal-footer">
				  <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
				</div>
			  </div>
			  
			</div>
		  </div>
        
        
            
        @endif
@endsection