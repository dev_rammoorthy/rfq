<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\AddressController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');

$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];

$address_details = AddressController::listAddress();

$page="List Bank";
$pagetrack=PurchaseController::pageLogout($page);

?>

<style>

.addresslabel
{
    background: #f4f6f9f7;
}

.addressvalue
{
    background: #47686d2b;
}

</style>

@extends('event_manager/event-header')

@section('content')

<div class="content-wrapper">
	<!--creative states-->
	<div class="inner-page-img">
		<div class="container">
            <h3 class="page-title">Shipping Address</h3>
		</div>
	</div>

	<div class="box-container">
		<div class="row">              
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>List Address</h3>
                            <div class="text-right" style="margin-top: -35px;">
                                <a href="{{ URL::to('add-address') }}" class="btn btn-purple">Add New Address</a>
                                <a href="{{ URL::to('/') }}" class="btn btn-info" >Go Home</a>
                            </div>
                            <br>
                            <div class="text-right">                               
                                <h6> * List of shipping addresses that you added </h6>
                            </div>
						</div>

                        <?php if(@count($address_details) > 0) : ?>
						
                            <form class="event-detail-form" name="delete-address" id="delete-address">
                                <br>
                                <?php $i=1; ?>
                                <?php foreach($address_details as $addressValue) { ?>
                                    <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                        <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                            <h5 style="padding:7px">Shipping Address {{$i}}</h5>
                                        </div>

                                        <div class="col-md-9 col-xs-9 col-sm-9 form-group row" >
                                            <!-- <div class="col-md-1 col-xs-1 col-sm-1 form-group">
                                                <input type="radio" name="radioAddressId" id="radioAddressId" value="{{ $addressValue->psa_id }}" />
                                            </div> -->
                                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                                    
                                                <div class="col-md-4 addresslabel">
                                                    <label> Address Line 1  </label>
                                                </div>
                                                <div class="col-md-8 addressvalue">
                                                    <?php echo $addressValue->psa_address_line1 ?> <br>
                                                </div>
                                                <div class="col-md-4 addresslabel" >
                                                    <label> Address Line 2 </label>
                                                </div>
                                                <div class="col-md-8 addressvalue">
                                                    <?php echo $addressValue->psa_address_line2 ?> <br>
                                                </div>
                                                <div class="col-md-4 addresslabel">
                                                    <label> State </label>
                                                </div>
                                                <div class="col-md-8 addressvalue">
                                                    <?php echo $addressValue->pmst_state_name ?> <br>
                                                </div>
                                                <div class="col-md-4 addresslabel">
                                                    <label> City </label> 
                                                </div>
                                                <div class="col-md-8 addressvalue">
                                                    <?php echo $addressValue->pmci_city_name ?> <br>
                                                </div>
                                                <div class="col-md-4 addresslabel">
                                                    <label> Country </label>
                                                </div>
                                                <div class="col-md-8 addressvalue">
                                                    <?php echo $addressValue->psa_country ?> <br>
                                                </div>
                                                <div class="col-md-4 addresslabel">
                                                    <label> Zip Code </label>
                                                </div>
                                                <div class="col-md-8 addressvalue">
                                                    <?php echo $addressValue->psa_zipcode ?> <br>
                                                </div>
                                                
                                                <div class="col-md-6">
                                                <br>
                                                    <a data-id="{{$addressValue->psa_id}}" class="btn btn-purple delete_address">Delete</a>
                                                    <a href="{{ URL::to('update-address')}}/{{$addressValue->psa_id}}" class="btn btn-danger update_address">Update</a>
                                                </div>
                                            </div>
                                        </div>                                    
                                    </div> 
                                    <?php $i++; ?>  
                                    
                                <?php } ?>
                                
                                <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                
                                    <input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
                            
                                    <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    </div>
                                    <div class="col-md-6 col-xs-6 col-sm-6 form-group">                                        
                                        
                                    </div>
                                </div>                  
                            </form>
                            

                        <?php else: ?>

                            <!--inner-page-img End-->
							<div class="box-container">
							  <div class="row">
								  <div class="col-xl-12 text-center">
									 <img class="pr-3 img-fluid" src="{{asset('img/no-record.png')}}" alt="Purchase"/>
									<!-- <p> <a href="" class="btn btn-primary">Back</a> </p>-->
								  </div>
							   </div>
							 <!--row End-->
							 
							</div>
						<!--box-container End-->

                        <?php endif; ?>
						
					</div>
				</div>
			</div>
					
		</div>
	</div>
</div>

@endsection