<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\AddressController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');

$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];

$states = AddressController::getStateNames();

$page="Add Bank";
$pagetrack=PurchaseController::pageLogout($page);

?>


@extends('event_manager/event-header')

@section('content')

<div class="content-wrapper">
	<!--creative states-->
	<div class="inner-page-img">
		<div class="container">
            <h3 class="page-title">Add New Address</h3>
		</div>
	</div>

	<div class="box-container">
		<div class="row">              
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Add Address</h3>
                            <div class="text-right">                               
                                <h6> * Fill up your address details to ship the product </h6>
                            </div>
						</div>
						
						<form class="event-detail-form" name="add-address-info-form" id="add-address-info-form">
                            <br>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Address Line 1 *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                   
                                    <input type="text" class="form-control" id="psa_address_line1" name="psa_address_line1" style="height: 100px;" placeholder="Enter Your Address Line 1">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <label style="padding:7px">Eg : Street address, P.O. Box, Company Name, C/O </label>
                                </div>
                            </div>   
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Address Line 2 (Optional)</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="psa_address_line2" name="psa_address_line2"  style="height: 100px;" placeholder="Enter Your Address Line 2">
                                    
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <label style="padding:7px"> Eg : Apartment, Suite, Unit, Building, Floor, etc. </label>
                                </div>
                            </div> 
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> State *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <select class="form-control select2" name="psa_state" id="psa_state">
                                        <option value="">-- Choose Your State --</option>
                                        @foreach ($states as $item)
                                        <option value="{{$item->pmst_state_id}}">{{$item->pmst_state_name}}</option>			
                                        @endforeach
                                    </select>
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">                                   
                                </div>
                            </div>
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> City *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <select class="form-control select2" name="psa_city" id="psa_city">
                                        <option value="">-- Choose Your Cities --</option>                                        
                                    </select>
                                    <!-- <input type="text" class="form-control" id="pbd_city" name="pbd_city" placeholder="Enter City"> -->
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">                                   
                                </div>
                            </div>							
							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Pin Code *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="number" class="form-control" id="psa_pincode" name="psa_pincode" placeholder="Enter Pin Code">
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">                                   
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                    <h5 style="padding:7px"> Country *</h5>
                                </div>

                                <div class="col-md-6 col-xs-6 col-sm-6 form-group">
                                    <input type="text" class="form-control" id="pbd_country" name="pbd_country" value="INDIA" readonly>
								</div>

								<div class="col-md-3 col-xs-3 col-sm-3 form-group">                                   
                                </div>
                            </div>

							<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            
								<input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
						
								<div class="col-md-3 col-xs-3 col-sm-3 form-group">
                                </div>
								<div class="col-md-6 col-xs-6 col-sm-6 form-group">
									<input type="submit" class="btn btn-purple" id="wallet-submit" name="SUBMIT" value="Save">
									<a href="{{ URL::to('list-address') }}" class="btn btn-danger">Back</a>
									<a href="{{ URL::to('/') }}" class="btn btn-info" >Go Home</a>
								</div>
                            </div>                  
                            
						</form>
						
					</div>
				</div>
			</div>
					
		</div>
	</div>
</div>

@endsection