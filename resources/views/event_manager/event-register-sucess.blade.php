@extends('header')

@section('content')

<div class="container">
<div class="row">
<div class="col-xl-12 d-lg-flex align-items-center">
<!--login form-->
<div class="payment-page">
<span class="payment-status-icon text-success"><i class="fa fa-check" aria-hidden="true"></i></span>
<h4 class="text-uppercase text-success text-center mb-2">Payment Successfuly</h4>
<div class="payment-status-info">
<p>Thank you! Your payment of Rs. 3000 has been received.</p>
<span class="payment-successid"><b>Transaction ID :</b> MTX1235805SP</span>
<p class="btn-uppertxt">Kindly click below button and create your event.</p>
<a href="{{ URL::to('add-event') }}" class="btn btn-purple" title="Create Event">Create Event</a>
<a class="skip-page-link" href="{{ URL::to('event') }}" title="Back to page">Back to Page</a>
</div>
</div>
                <!--/login form-->

             

            </div>
        </div>
    </div>
@endsection

