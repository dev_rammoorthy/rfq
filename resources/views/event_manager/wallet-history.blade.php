<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \App\Http\Controllers\WalletController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');

$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];
$wallet_data = $session_val['wallet_data'];
$wallet_id = isset($wallet_data) ? $wallet_data->pwc_wallet_id : null;

if(isset($wallet_history) && $wallet_history != null)
{

}
else
{
	$wallet_history = WalletController::wallethistorydetail();
}

$page="Wallet History";
$pagetrack=PurchaseController::pageLogout($page);

?>

<style>
.auction-collapsed-info
{
	width: 15% !important;
}

</style>


@extends('event_manager/event-header')

@section('content')

<div class="content-wrapper">
	<!--creative states-->
	<div class="inner-page-img">
		<div class="container">
            <h3 class="page-title">Wallet History</h3>
		</div>
	</div>

	<div class="box-container">
		<div class="row">     
			@if($wallet_data)         
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Wallet History</h3>
							<div class="col-md-12">
								<div class="text-right">                               
									<label> Your Wallet Id : </label> <h4> {{ $wallet_id }} </h4>
								</div>								
							</div>

							<div class="col-md-12 row">
								<div class="text-left col-md-9">     
									<form action="{{ action('WalletController@wallethistory') }}" method="POST"  class="event-detail-form" name="report" id="report-form">
										<label> Filter By Return Status : </label>      
										<br>                    
										<input type="hidden" id="hfWalletId" name="hfWalletId" value="{{$wallet_id}}" >
										<input type="submit" class="btn btn-purple" id="report-submit" name="SUBMIT" value="Sent">
										<input type="submit" class="btn btn-purple" id="report-submit" name="SUBMIT" value="Received">
										<input type="submit" class="btn btn-purple" id="report-submit" name="SUBMIT" value="Added">
									</form>
								</div>
								<div class="col-md-3">   
									<br>                            
									<h6> * Wallet credit and debit history</h6>
								</div>
							</div>	
                           
						</div>						
						<br>
						<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
							<div class="auction-report-title">
								
								<div class="auction-collapsed-info">
									<span>Sender Wallet ID</span>
								</div>							
								<div class="auction-collapsed-info">
									<span> Transaction Amount</span>
								</div>
								<div class="auction-collapsed-info">
									<span>Spend Reason</span>
								</div> 
								<div class="auction-collapsed-info">
									<span> Transaction Date </span>
								</div>
								<div class="auction-collapsed-info">
									<span> Transaction Status </span>
								</div>     
								<!-- <div class="auction-collapsed-info">
									<span> Receiver Login Id </span>
								</div>    -->
								<div class="auction-collapsed-info">
									<span> Receiver Wallet Id </span>
								</div>      
							</div>
					
							<?php $sno = 1; ?>

							@foreach ($wallet_history as $item)

								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">                                 
							
									<div class="panel panel-default">
										<div class="panel-heading auction-accordion" role="tab" id="headingOne">
																				
											<!-- <div class="auction-collapsed-info">
												<p>{{$sno}}</p>
											</div> -->

											<div class="auction-collapsed-info">
												<p>{{$item->pwh_wallet_id}}</p>
											</div>
											
											<div class="auction-collapsed-info">
												<p>{{$item->pwh_balance}}</p>
											</div>

											<div class="auction-collapsed-info">
												<p> 
													<?php if($item->pwh_for == 1)
													{
														echo 'Pay EMD to buyer';
													}
													else if($item->pwh_for == 2)
													{
														echo 'Return EMD to seller';
													}
													else if($item->pwh_for == 3)
													{
														echo 'Payment to seller';
													}
													else if($item->pwh_for == 4)
													{
														echo 'Buying an event';
													}
													else if($item->pwh_for == 5)
													{
														echo 'Added to wallet';
													}
													else if($item->pwh_for == 6)
													{
														echo 'Getting EMD from seller';
													}
													else if($item->pwh_for == 7)
													{
														echo 'Getting payment from seller';
													}
													else if($item->pwh_for == 8)
													{
														echo 'Transfer to bank account';
													}
													else
													{
														echo 'Not Defined';
													}
													?>
												</p>
											</div>																							
											
											<div class="auction-collapsed-info">
												<p>{{$item->pwh_created_at}}</p>
											</div>
											
											<div class="auction-collapsed-info">
												<p>{{ ($item->pwh_do == 1 ? 'Added' : 'Spend') }}</p>
											</div>   

											<!-- <div class="auction-collapsed-info">
												<p>{{ ($item->pwh_receiver_loginid != null ? $item->pwh_receiver_loginid : 'Not Defined') }}</p>
											</div>                                                                        -->

											<div class="auction-collapsed-info">
												<p>{{ ($item->pwh_receiver_wallet_id != null ? $item->pwh_receiver_wallet_id : 'Not Defined') }}</p>
											</div>                                                             
											
										</div>
												
									</div>
									
								</div>

								<?php $sno ++; ?>

							@endforeach
					
						</div>                            
					</div>
				</div>
			</div>
			@else
				<div class="box-container">
				<div class="row">
					<div class="col-xl-12 text-center">
						<img class="pr-3 img-fluid" src="{{asset('img/no-record.png')}}" alt="Purchase"/>
						<!--<p> <a href="" class="btn btn-primary">Back</a> </p>-->
					</div>
				</div>
				<!--row End-->
				
				</div>
			@endif
			
		</div>

			<!-- <div class="col-xl-4 col-sm-4 col-md-4 col-lg-4 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Balance</h3>
						</div>						
						<br>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
							<div class="col-md-12 col-xs-12 col-sm-12 form-group">
								
							</div>
						</div>					
					</div>
				</div>
			</div> -->
		</div>
	</div>
</div>

@endsection