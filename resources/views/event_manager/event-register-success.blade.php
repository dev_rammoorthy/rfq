<?php 
		$payment_status = Request::route('status');
		$packID = Request::route('packid');
		$session_val = session()->all();

	$payment_details=$session_val['tzsk_payu_data']['payment']['data'];
	$txnid=$session_val['tzsk_payu_data']['payment']['txnid'];
	$amount=$session_val['tzsk_payu_data']['payment']['amount'];
	$transaction_data=json_decode($payment_details);

?>
@extends('header')

@section('content')


	<div class="container">
		<div class="row">
			<div class="col-xl-12 d-lg-flex align-items-center">
			<!--login form-->
			@if($transaction_data->status=='success')
				<div class="payment-page">
					<span class="payment-status-icon text-success"><i class="fa fa-check" aria-hidden="true"></i></span>
					<h4 class="text-uppercase text-success text-center mb-2">Payment Successfully</h4>
					<div class="payment-status-info">
						<p>Thank you! Your payment of Rs. {{$amount}} has been received.</p>
					<span class="payment-successid"><b>Transaction ID :</b> {{$txnid}}</span>
						<p class="btn-uppertxt">Kindly click below button and create your event.</p>
						<a href="{{ URL::to('create-event') }}/{{$packID}}" class="btn btn-purple" title="Create Event">Create Event</a>
						<a class="skip-page-link" href="{{ URL::to('event') }}" title="Back to page">Back to Page</a>
					</div>
				</div>
			@else
				<div class="payment-page">
					<span class="payment-status-icon danger text-danger"><i class="fa fa-close" aria-hidden="true"></i></span>
					<h4 class="text-uppercase text-danger text-center mb-2">Payment Failed</h4>
					<div class="payment-status-info">
							<span class="payment-successid"><b>Transaction ID :</b> {{$txnid}}</span>
					
						<a class="skip-page-link" href="{{ URL::to('event') }}" title="Back to page">Back to Page</a>
					</div>
				</div>
			@endif
			</div>
		</div>
    </div>
@endsection

