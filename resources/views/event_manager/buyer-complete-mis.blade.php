<?php 

 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;

$session = session()->all(); 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];

$event_data=EventController::buyerCompletedEvent($loginid);

?>



@extends('event_manager/event-header')

@section('assets')
<link rel="stylesheet" type="text/css" href="{{asset('css/plugins/data-table/dataTables.bootstrap.min.css')}}?{{ time() }}">
<link rel="stylesheet" type="text/css" href="{{asset('css/page/tables.css')}}?{{ time() }}">
@endsection

@section('content')

<!--main content wrapper-->
<div class="content-wrapper">
        <!--creative states-->
            <div class="inner-page-img">
            <div class="container">
            <h3 class="page-title">Completed Event</h3>
            </div>
            </div>
            
            
        
        <div class="box-container">
        <div class="row">            
        
 
         @if ($event_data)
             
        
        <div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
       
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">




                <table id="mis_download" class="table table-striped table-bordered" cellspacing="0">
                        <thead>
                        <tr>
                                <th>  Event ID</th>
                                <th>  Event Name</th>
                              
                                <th>   Event start Date</th>
                                <th>  Event end Date</th>
                                <th> Event Type</th>
                                <th> Company Name</th>
                                <th> Vendor Email</th>
                                <th> Bid Price</th>
                                <th> Start Price</th>
                                <th> Min Dec</th>
                                
                            <th>Category</th>
                            <th>Product Name</th>
                            <th>Spec</th>
                            <th>Units</th>
                            <th>Qty</th>
                            <th>Location</th>
                            <!--<th>Expected Price</th>-->
                           
                            <th>Bid Status</th>
                            </tr>
                        </thead>
                        <tbody>

                                <?php $eventAddItem=EventController::buyerCompletedEventMIS($loginid);


                                ?>
                                    @foreach ($eventAddItem as $add_item)

                        <tr>
                                <td>{{$add_item->pea_event_id}}</td>
                                <td>{{$add_item->pec_event_name}}</td>
                                <td>{{$add_item->pec_event_start_dt}}</td>
                                <td>{{$add_item->pec_event_end_dt}}</td>
                                @if ($add_item->pec_event_type==1)
                                <td>Closed Bid</td>
                                @endif
                                @if ($add_item->pec_event_type==2)
                                <td>Reverse Bid</td>
                                @endif
                                <td>{{($add_item->pci_comp_name!=NULL)?$add_item->pci_comp_name:"NA"}}</td>
                                <td>{{($add_item->pla_seller_emailid!=NULL)?$add_item->pla_seller_emailid:"NA"}}</td>
                                <td>{{($add_item->pla_cur_bid!=NULL)?$add_item->pla_cur_bid:"NA"}}</td>
                                <td>{{$add_item->pea_event_start_price}}</td>
                                <td>{{$add_item->pea_event_max_dec}}</td>

                                <td>{{$add_item->pmc_cat_name}}</td>
                                <td>{{$add_item->pms_subcat_name}}</td>
                                <td>{{$add_item->pea_event_spec}}</td>
                                <td>{{$add_item->pmu_unit_name}}</td>
                                <td>{{$add_item->pea_event_unit_quantity}}</td>
                                <td>{{$add_item->pea_event_location}}</td>
                                <!--<td>$add_item->pea_event_start_price}}</td>-->
                                
                            <td>
                            {{$add_item->pla_cur_bid}} 
                            @if ($add_item->pla_auction_status=='1')
                            <span>WON</span> 
                            @elseif($add_item->pla_auction_status=='2')
                            <span>LOST</span>
                            @else
                            
                            
                            <span>PENDING</span>
                            @endif
                            </td>
                            
                        </tr>
                        @endforeach     
                        
                        
                        </tbody>
                    </table>
        @else
			<!--inner-page-img End-->
			<div class="box-container">
			  <div class="row">
				  <div class="col-xl-12 text-center">
					 <img class="pr-3 img-fluid" src="{{asset('img/no-record.png')}}" alt="Purchase"/>
					 <!--<p> <a href="" class="btn btn-primary">Back</a> </p>-->
				  </div>
			   </div>
			 <!--row End-->
			 
			 </div>
			 <!--box-container End-->
        @endif
        </div>
        </div>
            <!--/main content wrapper-->
        </div>
        </div>
        </div>
        
@endsection


@section('scripts')
<script type="text/javascript" src="{{asset('js/plugins/datatables/datatables.min.js')}}?{{ time() }}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/datatables/dataTables.buttons.min.js')}}?{{ time() }}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/datatables/jszip.min.js')}}?{{ time() }}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/datatables/buttons.html5.min.js')}}?{{ time() }}"></script>
@endsection
@section('inline-scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#mis_download').DataTable( {
			responsive: true,
			scrollX: true,
			ordering: false,
			searching: false,
			info: false,
			dom: 'Bfrtip',
			buttons: [
				'excel'
			]
		} );
	} );
</script>
@endsection 