<?php 

 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \App\Http\Controllers\WalletController;

$session = session()->all(); 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];

$event_data=EventController::buyerCompletedEvent1($loginid);

$page="Completed Events";
$pagetrack=PurchaseController::pageLogout($page);

?>
<style>
.auction-collapsed-info
{
  width: 15% !important;
}
</style>

@extends('event_manager/event-header')

@section('content')

<!--main content wrapper-->
<div class="content-wrapper">
        <!--creative states-->
            <div class="inner-page-img">
            <div class="container">
            <h3 class="page-title">Completed Event</h3>
            </div>
            </div>
            
            
        
        <div class="box-container">
        <div class="row">            
        
 
         @if ($event_data)
             
        
        <div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
        <div class="auction-report-title">
        <div class="auction-collapsed-info">
        <span>Event ID</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event Name</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event start Date </span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event end Date </span>
        </div>        
        <div class="auction-collapsed-info">
        <span>Event Type</span>
        </div>
        <div class="auction-collapsed-info">
        <span>No.of Items</span>
        </div>
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach ($event_data as $item)
                <?php $eventItem = EventController::getAddedItems2($item->pec_event_id); 
                if(@count($eventItem) > 0) :
                ?>

                        <div class="panel panel-default">
                            <div class="panel-heading auction-accordion" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$item->pec_event_id}}" aria-expanded="false" aria-controls="collapseOne">
                                            <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_id}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_name}}</p>
                                                    </div>
                                                    
                                                
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_start_dt}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_end_dt}}</p>
                                                    </div>
                                                    <div class="auction-collapsed-info">
                                                        <p><?php echo (($item->pec_event_type == 1) ? 'Closed Bid' : (($item->pec_event_type == 2) ? 'Reverse Bid' : 'Both Closed and Reverse Bid'));  ?></p>
                                                    </div>
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->no_item}}</p>
                                                            </div>
                                                   
                                    </a>
                                </h4>
                            </div>
                            
                            <div id="{{$item->pec_event_id}}"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="auction-inviter-fullinfo">
                                    <div class="table-responsive">
                                    <table class="table">
            <thead>
                <tr>
                <th>Market</th>
                <th>Category</th>
                <th>Product Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Spec</th>
                <th>Units</th>
                <th>Qty</th>
                <th>Location</th>
                @if($item->pec_event_type ==2)
                <th>Start Price</th>
                <th>Min Dec.</th>
                @endif
                <th>No.of Bidder</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>

                    <?php if(@count($eventItem) > 0) : ?>

                        @foreach ($eventItem as $add_item)
                <tr class="clickable" data-toggle="collapse" data-target="#group-of-rows-{{$add_item->pea_id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                        <?php                                            
                                      
                        $email_count= json_decode($add_item->pei_invite_details);
                        $invite_count=count( $email_count);
              
                             ?>  
                    
                    <td>{{$add_item->pmm_market_name}}</td>
                        <td>{{$add_item->pmca_cat_name}}</td>
                        <td>{{$add_item->pms_subcat_name}}</td>
                        <td>{{$add_item->pea_event_start_dt}}</td>
                        <td>{{$add_item->pea_event_end_dt}}</td>
                        <td>{{$add_item->pea_event_spec}}</td>
                        <td>{{$add_item->pmu_unit_name}}</td>
                        <td>{{$add_item->pea_event_unit_quantity}}</td>
                        <td>{{$add_item->pea_event_location}}</td>
                        @if($item->pec_event_type ==2)
                        <td>{{$add_item->pea_event_start_price}}</td>
                        <td>{{$add_item->pea_event_max_dec}}</td>
                        @endif
                    <td>
                            <?php $sellerbid = EventController::sellerBidDetails($add_item->pea_id);
                                    $validateRejectAllData = EventController::validateRejectAllData($add_item->pea_id);
									$validateAcceptData = EventController::validateAcceptData($add_item->pea_id);
										
                            ?>
                    <span class="lot-mail-req">No.of Acceptance : {{$add_item->pei_invite_accept_count}}</span>
                    <a href="javascript:" id="hover-mail-list"><span class="mail-invite-list">{{count($sellerbid)}}</span></a>
                    </td>
                    <td>
                        <?php if(@count($sellerbid) > 0 && $sellerbid != null): ?>   
                            <?php if(@count($validateAcceptData) == 0) : ?> 
                                <?php if(@count($sellerbid) != @count($validateRejectAllData)) : ?>
                                    <button data-mail="{{$add_item->pea_id}}" type="button" class="btn btn-sm btn-edit-row btn-purple mt-1 declineAllBid" onclick="rowEdit(this);">
                                        <span class="text-white" >Reject All</span>
                                    </button>
                                <?php else: ?> 
                                    <button data-mail="{{$add_item->pea_id}}" data-event="{{$add_item->pea_event_id}}" type="button" class="btn btn-sm btn-purple mt-1 rescheduleLot" onclick="rowEdit(this);">
                                        <span class="text-white" >Reschedule</span>
                                    </button>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>                
             
            </tbody>
                    
            <tbody id="group-of-rows-{{$add_item->pea_id}}" class="collapse bidder-expand-info">
                <?php $i=0; ?>
                @foreach ($sellerbid as $bid_item)
                   
                <tr>
                <td colspan="12">
                   <div class="final-bidder-details">
                   <div class="live-bid-info col-md-2 col-sm-3 col-xs-12">
                   <!-- <p>{{$bid_item->pci_comp_name}}</p> -->
                   <p>XXXX</p>
                   <span>Company Name</span>
                   </div>
                   
                   <div class="live-bid-info col-md-3 col-sm-3 col-xs-12">
                   <p>{{$bid_item->pla_seller_emailid}}</p>
                   <span>Vendor Email</span>
                   </div>
                   
                   <div class="live-bid-info bid-highlight-price col-md-2 col-sm-3 col-xs-12">
                   <p>{{$bid_item->pla_cur_bid}}</p>
                   <span>Bid Price</span>
                   </div>
                   <div class="live-bid-info col-md-2 col-sm-3 col-xs-12">
                        <p>Location</p>
                        <span>Chennai</span>
                        </div>
                        <div class="live-bid-info col-md-3 col-sm-3 col-xs-12">
                @if ($bid_item->pla_auction_status=='1')
			    <span class="event-accepted"><i class="fa fa-check" aria-hidden="true"></i> Accepted</span>
                <br>
                <!--<span class="send-invoice"><a href="javascript:" title="Accept" class="btn-purple btn font-zize12">Send Invoice</a></span> -->
                                
                <?php $paid_validation = WalletController::paidValidation($bid_item->pla_id); $payment_status = $paid_validation->pla_payment_status; ?>     
               
                <div class="col-md-12 row">
                    <div class="col-md-6">
                        <?php if($payment_status == 1) : ?>
                            <span style="color: #74af50;" class="event-accepted"><i class="fa fa-check" aria-hidden="true"></i> Paid</span>
                        <?php else: ?>
                            <button id="{{$bid_item->pla_id}}" data-lot-id="{{$bid_item->pla_lotid}}" data-bidid="{{$bid_item->pla_cur_bid}}" type="button" class="btn btn-sm btn-edit-row btn-purple mt-1 pay-seller-money">
                                <span class="text-white" >Pay Money</span>
                            </button> 
                        <?php endif; ?>
                    </div>
                    <?php if($payment_status == 1) : ?>
                        <div class="col-md-6">
                            <button data-sellerid="{{$bid_item->pla_seller_emailid}}" id="{{$bid_item->pla_id}}" data-lot-id="{{$bid_item->pla_lotid}}" data-bidid="{{$bid_item->pla_cur_bid}}" type="button" class="btn btn-sm btn-edit-row btn-purple mt-1 invoice-seller">
                                <span class="text-white" >Send Invoice</span>
                            </button> 
                        </div>
                    <?php endif; ?>
                </div>

                @elseif ($bid_item->pla_auction_status=='2')
                <span class="bid-reject-status">Rejected <i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span>
                        </div>
                @else 
                    <div class="live-bid-info col-md-2 col-sm-3 col-xs-12"> 
                        <button  id="{{$bid_item->pla_id}}" data-mail="{{$bid_item->pla_seller_emailid}}" type="button" class="btn btn-sm btn-edit-row btn-purple mt-1 finalizedBid" onclick="rowEdit(this);">
                            <span class="text-white" >Accept</span>
                        </button> 
                        <button  id="{{$bid_item->pla_id}}" data-mail="{{$bid_item->pla_seller_emailid}}" type="button" class="btn btn-sm btn-edit-row btn-purple mt-1 declinedBid" onclick="rowEdit(this);">
                            <span class="text-white" >Reject</span>
                        </button> 
                    </div>

                       @endif
                   </div>
                   </td>
                </tr>
                
                
               
                @endforeach     
                
            </tbody>
           
            @endforeach    

            <?php else: ?>
                <tr>
                    <td colspan="12">
                        No completed items found for this event
                    </td>
                </tr>
            <?php endif; ?> 
        
        </table>
        </div>
        
        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        @endforeach
                    </div>
        
        </div>
        @else
			<!--inner-page-img End-->
			<div class="box-container">
			  <div class="row">
				  <div class="col-xl-12 text-center">
					 <img class="pr-3 img-fluid" src="{{asset('img/no-record.png')}}" alt="Purchase"/>
					 <!--<p> <a href="" class="btn btn-primary">Back</a> </p>-->
				  </div>
			   </div>
			 <!--row End-->
			 
			 </div>
			 <!--box-container End-->
        @endif
        </div>
        </div>

        <div class="modal fade headShake" id="rescheduleForm" role="dialog">
            <div class="modal-dialog vendor-invite-popup">
            
            <!-- Modal content-->
            <div class="modal-content modal-content-amount invite-popup-content">
                <div class="modal-header modal-header-amount" style="background: #ff9800;">
                    <h4 class="modal-title-amount">Reschedule Form</h4>
                </div>
                    
                    <div class="modal-body modal-body-bg" style="height:auto">
                        <form class="event-detail-form" name="add_event" id="event-detail-form">
                            <div class="col-md-12 row">
                                <div class="col-md-6 col-xs-12 col-sm-6 form-group date">
                                    <label> Start Date </label>
                                    <input type="text" class="form-control datetimes1" name="pec_event_start_dt" id="event_start_dt" placeholder="Auction Start Date &amp; Time">
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-6 form-group date">
                                    <label> End Date </label>
                                    <input type="text" class="form-control datetimes2" name="pec_event_end_dt" id="event_end_dt" placeholder="Auction Start Date &amp; Time">
                                </div>
                            </div>

                            <div class="amount-btn">
                                <div style="margin-top:1em" class="float-left mb-4">
                                    <a id="reschedule-model" class="btn-amount btn-primary text-center"> Reschedule </a>
                                </div>
                                <div style="margin-top:1em" class="float-left ml-4 mb-4">
                                    <a  id="reschedule-model-cancel" class="btn-cancel btn-light text-center"> Cancel </a>
                                </div>
                            </div>                    
                        </form>      
                    </div>                              
                </div>
            </div>
            
            </div>
        </div>

        <div class="modal fade headShake" id="wallet_popup" role="dialog">
            <div class="modal-dialog vendor-invite-popup">
            
            <!-- Modal content-->
            <div class="modal-content modal-content-amount invite-popup-content">
                <div class="modal-header modal-header-amount amount_bg">
                <h4 class="modal-title-amount">Pay to seller from Wallet</h4>
                </div>
                <div class="modal-body modal-body-bg" style="height:auto">
            
                <div class="ownvendor_mailinput"> 
                    <div class="form-group mt-3">           
                    <div class="Pricing_Amount_bg">
                        <span class="event-full-rupee"> &#8377;</span>
                        <p class="event-full-amount" id="emd-text"> </p>
                    </div>
                    
                    
                    <p class="event-full-rupe" id="wallet-text" style="color: seagreen;"> </p>
                    <p class="text-center" style="font-size: 13px; color: #7c7a79; font-weight: bold; line-height: 20px;" id="emd-return"> </p>
                    <p class="text-center" style="font-size: 13px; color: #7c7a79; font-weight: bold; line-height: 20px;" id="total-pay"> </p>
                    <p class="text-center" style="font-size:13px; color:red"> Note : The accepted amount will be deducted from your wallet balance. </p>
                    </div>
                    <div class="amount-btn">
                    <div style="margin-top:1em" class="float-left mb-4">
                        <a id="confirm-model" class="btn-amount btn-primary text-center"> Pay </a>           
                    </div>
                    <div style="margin-top:1em" class="float-left ml-4 mb-4">
                        <a id="confirm-model-wallet-cancel" class="btn-cancel btn-light text-center"> Cancel </a>
                       
                    </div>
                    <div style="margin-top:1em" class="float-right mb-4">
                        <a href="{{ URL::to('wallet') }}" class="btn-amount btn-danger text-center"> Recharge </a>
                    </div>
                   
                    </div>  

                </div>
                </div>
            </div>
            
            </div>
        </div>


            <!--/main content wrapper-->
        </div>
        
@endsection