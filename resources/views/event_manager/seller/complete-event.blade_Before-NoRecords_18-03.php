<?php 

 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;

$session = session()->all(); 
 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];

$complete_event_data=EventController::seller_Completed_Aution1($loginid);

?>



@extends('event_manager/event-header')

@section('content')

<!--main content wrapper-->
    <div class="content-wrapper">
<!--creative states-->
	<div class="inner-page-img">
	<div class="container">
	<h3 class="page-title">Completed Events</h3>
	</div>
	</div>
	

<div class="box-container">
<div class="row">      

 
 @if ($complete_event_data)   
<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
<div class="auction-report-title">
<div class="auction-collapsed-info">
<span>Event ID</span>
</div>
<div class="auction-collapsed-info">
<span>Event Name</span>
</div>
<div class="auction-collapsed-info">
<span>Company Name</span>
</div>
<div class="auction-collapsed-info">
<span>Event start Date</span>
</div>
<div class="auction-collapsed-info">
<span>Event end Date</span>
</div>
</div>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach ($complete_event_data as $item)                    
    <div class="panel panel-default">
                    <div class="panel-heading auction-accordion" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$item->pec_event_id}}" aria-expanded="false" aria-controls="collapseOne">
                                    <div class="auction-collapsed-info">
                                            <p>{{$item->pec_event_id}}</p>
                                            </div>
                                            
                                            <div class="auction-collapsed-info">
                                            <p>{{$item->pec_event_name}}</p>
                                            </div>
                                            
                                            <div class="auction-collapsed-info">
                                            <p>{{$item->pci_comp_name}}</p>
                                            <span class="auc-company-location">Chennai</span>
                                            </div>
                                            
                                            <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_start_dt}}</p>
                                            </div>
                                            
                                            <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_end_dt}}</p>
                                            </div>
							
                            </a>
                        </h4>
                    </div>
					
                    <div id="{{$item->pec_event_id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="auction-inviter-fullinfo">
							<div class="table-responsive">
 <table id="makeEditable" class="table table-striped table-bordered" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Product Name</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Spec</th>
                                        <th>Units</th>
                                        <th>Qty</th>
										<th>Location</th>
                                        <!--<th>Expected Price</th>-->
                                       
										<th>Bid Status</th>
										</tr>
                                    </thead>
                                    <tbody>

                                            <?php $eventAddItem=EventController::getSellerInviteItem2($item->pec_event_id,$loginid);


                                            ?>

                                            <?php if(@count($eventAddItem) > 0) : ?>

                                                @foreach ($eventAddItem as $add_item)

                                    <tr>
                                            <td>{{$add_item->pmc_cat_name}}</td>
                                            <td>{{$add_item->pms_subcat_name}}</td>
                                            <td>{{$add_item->pea_event_start_dt}}</td>
                                            <td>{{$add_item->pea_event_end_dt}}</td>
                                            <td>{{$add_item->pea_event_spec}}</td>
                                            <td>{{$add_item->pmu_unit_name}}</td>
                                            <td>{{$add_item->pea_event_unit_quantity}}</td>
                                            <td>{{$add_item->pea_event_location}}</td>
                                            <!--<td>$add_item->pea_event_start_price}}</td>-->
                                            
										<td>
                                        {{$add_item->pla_cur_bid}} 
                                        @if ($add_item->pla_auction_status=='1')
                                        <span class="bid-status bid-won">WON</span> 
                                        @elseif($add_item->pla_auction_status=='2')
                                        <span class="bid-status bid-lost">LOST</span>
                                        @else
                                        
                                        
                                        <span class="bid-status bid-na">PENDING</span>
                                        @endif
                                        @if($add_item->pea_event_terms_condition)
                                   
                                        <a href="{{URL::to('/')}}/{{$add_item->pea_event_terms_condition}}" title="term" class="btn float-right">View T&amp;C</a>
                                        @endif
										</td>
										
									</tr>
                                    @endforeach     

                                    <?php else: ?>

                                        <tr>
                                            <td colspan="12">
                                                No completed items found for this event
                                            </td>
                                        </tr>

                                    <?php endif; ?>
									
									
                                    </tbody>
                                </table>
</div>

							</div>
                        </div>
                    </div>
                </div>
				
                @endforeach
            </div>
            @else
				<!--inner-page-img End-->
					<div class="box-container">
					  <div class="row">
						  <div class="col-xl-12 text-center">
							 <img class="pr-3 img-fluid" src="{{asset('img/norecord.png')}}" alt="Purchase"/>
							<!-- <p> <a href="" class="btn btn-primary">Back</a> </p>-->
						  </div>
					   </div>
					 <!--row End-->
					 
					</div>
				<!--box-container End-->
            @endif
</div>
</div>
</div>


    <!--/main content wrapper-->
</div>
    <!--footer-->
        
@endsection