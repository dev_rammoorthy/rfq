<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\WalletController;
 use \App\Http\Controllers\EventController;

$session = session()->all(); 
 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];

$event_data=EventController::sellerLiveEvent($loginid);
?>
@extends('event_manager/event-header')

<style>
.auction-collapsed-info
{
  width: 15% !important;
}
</style>

@section('content')
<!--main content wrapper-->
    <div class="content-wrapper">
<!--creative states-->
	<div class="inner-page-img">
	<div class="container">
	<h3 class="page-title">Live Events</h3>
	</div>
	</div>
	

<div class="box-container">
<div class="row">      

<script language="javascript">
  var localTime = new Date();

  var xmlHttp;
  function srvTime(){
    try {
        //FF, Opera, Safari, Chrome
        xmlHttp = new XMLHttpRequest();
    }
    catch (err1) {
        //IE
        try {
            xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
        }
        catch (err2) {
            try {
                xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch (eerr3) {
                //AJAX not supported, use CPU time.
                alert("AJAX not supported");
            }
        }
    }
    xmlHttp.open('HEAD',window.location.href.toString(),false);
    xmlHttp.setRequestHeader("Content-Type", "text/html");
    xmlHttp.send('');
    return xmlHttp.getResponseHeader("Date");
  }

  function display_c(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_ct()',refresh)
  }

  function display_ct() {
    var x = new Date()
    document.getElementById('ct').innerHTML = x;
    display_c();
  }

  var st = srvTime();
  var date = new Date(st);

  //document.write("Local machine time is: " + localTime + "<br>");
  //document.write("Server time is: " + date);
</script>
  
 @if ($event_data)    

<body onload=display_ct();>
    <div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view row" style="background: #ff9800; margin-left:0;color:white; font-weight: 600">
      <div class="col-md-6">
        Server : <label style="color:white"> Purchase Quick </label>
      </div>
      <div class="col-md-6">
        Server Time : <label id='ct' style="color:white"> </label>
      </div>
    </div>
  </body>

<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
  
<div class="auction-report-title">
<div class="auction-collapsed-info">
<span>Event ID</span>
</div>
<div class="auction-collapsed-info">
<span>Event Name</span>
</div>
<div class="auction-collapsed-info">
<span>Company Name</span>
</div>
<div class="auction-collapsed-info">
<span>Event start Date </span>
</div>
<div class="auction-collapsed-info">
<span>Event end Date </span>
</div>
<div class="auction-collapsed-info">
  <span>Event Type </span>
</div>
</div>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach ($event_data as $item)          
    <div class="panel panel-default">
                    <div class="panel-heading auction-accordion" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$item->pec_event_id}}" aria-expanded="false" aria-controls="collapseOne">
                                    <div class="auction-collapsed-info">
                                            <p>{{$item->pec_event_id}}</p>
                                            </div>
                                            
                                            <div class="auction-collapsed-info">
                                            <p>{{$item->pec_event_name}}</p>
                                            </div>
                                            
                                            <div class="auction-collapsed-info">
                                            <p>{{$item->pci_comp_name}}</p>
                                            <span class="auc-company-location">Chennai</span>
                                            </div>
                                            
                                            <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_start_dt}}</p>
                                            </div>
                                            
                                            <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_end_dt}}</p>
                                            </div>		
                                            <div class="auction-collapsed-info">
                                                <p><?php echo (($item->pec_event_type == 1) ? 'Closed Bid' : (($item->pec_event_type == 2) ? 'Reverse Bid' : 'Both Closed and Reverse Bid'));  ?></p>
                                            </div>						
							
                            </a>
                        </h4>
                    </div>
					
                    <div id="{{$item->pec_event_id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="auction-inviter-fullinfo">
							<div class="table-responsive">
 <table id="makeEditable" class="table table-striped table-bordered" cellspacing="0">
                                    <thead>
                                    <tr>
                                      <th>Market</th>
                                        <th>Category</th>
                                        <th>Product Name</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Spec</th>
                                        <th>Units</th>
                                        <th>Qty</th>
                                        <th>Location</th>
                                       @if ($item->pec_event_type==2)
                                       <th>Starting Price</th>
                                       <th>Min Dec.</th>
                                       <th>Current Bid</th>
                                       @endif
                                       <th>EMD</th>
                                    
                                      
										<th>Enter Quote</th>
										</tr>
                                    </thead>
                                    <tbody>
                                            <?php 
                                            if ($item->pec_event_type==1)
                                            $eventAddItem=EventController::getSellerInviteItem($item->pec_event_id,$loginid);
                                            if ($item->pec_event_type==2)
                                            $eventAddItem=EventController::getSellerInviteItemForReverse($item->pec_event_id,$loginid);

                                            ?>
                                            <?php if(isset($eventAddItem)) : ?>
                                                @foreach ($eventAddItem as $add_item)

                                              
                                 @if($add_item->reject!=1) 
                                    <tr>
                                    <td>{{$add_item->pmm_market_name}}</td>
                                            <td>{{$add_item->pmca_cat_name}}</td>
                                            <td>{{$add_item->pms_subcat_name}}</td>
                                            <td>{{$add_item->pea_event_start_dt}}</td>
                                            <td>{{$add_item->pea_event_end_dt}}</td>
                                            <!--<td class="spec_desc">{{$add_item->pea_event_spec}}</td> -->
                                            <td class="spec_desc">{{$add_item->pea_event_spec}}</td>
                                            <td>{{$add_item->pmu_unit_name}}</td>
                                            <td>{{$add_item->pea_event_unit_quantity}}</td>
                                            <td>{{$add_item->pea_event_location}}</td>
                                            @if ($item->pec_event_type==2)
                                            <?php  $curent_bit=EventController::getCurrentBitAmount($add_item->pea_id); $curent_bid_amount= $curent_bit[0]?>
                                            <td>{{$add_item->pea_event_start_price}}</td>
                                             <td>{{$add_item->pea_event_max_dec}}</td>
                                            <td id="current_{{$add_item->pea_id}}">{{($curent_bid_amount->cur_bid)?$curent_bid_amount->cur_bid : '0'}}</td>
                                        
                                         @endif

                                         <td><?php echo ($add_item->pea_event_emd_value > 0) ? $add_item->pea_event_emd_value : 'NA'; ?></td>

									                  	<td>
                                        @if($add_item->accept==0 || $add_item->reject==0 || $add_item->accept==1) 
                                        <form  class="seller-price-enter" data-id="{{$add_item->pea_id}}" id="bid_form_{{$add_item->pea_id}}">
                                            @if ($item->pec_event_type==2)
                                           
                                            
                                            <input type="hidden" id="reserve_bid" class="reserve_bid" name="reserve_bid" value="{{$add_item->pea_event_reserve_price}}">
                                            <input type="hidden" id="current_hidden_{{$add_item->pea_id}}" class="current_hidden_{{$add_item->pea_id}}" name="current_bid" value="{{($curent_bid_amount->cur_bid)?$curent_bid_amount->cur_bid : 0}}">
                                            <input type="hidden" id="max_dec" class="max_dec" name="max_dec" value="{{$add_item->pea_event_max_dec}}">
                                            <input type="hidden" id="start_price" class="start_price" name="start_price" value="{{$add_item->pea_event_start_price}}">  
                                            @endif
                                            <?php if($add_item->pla_cur_bid){
                                            $astyle='display: none;';
                                            $estyle='display: inline-block;';
                                            $readonly='readonly="readonly"';
                                            }else{
                                                $astyle='';
                                                $estyle='';
                                                $readonly="";
                                            }
                                              ?>
                                               <input type="hidden"  class="event_type" name="event_type" value="{{$item->pec_event_type}}">
                                           
                                              @if($add_item->accept==1)
                                                    <input type="hidden" class="lotID" name="lotID" value="{{$add_item->pea_id}}">
                                                    <?php $check_emd = WalletController::checkemdvalue($add_item->pea_id); ?>
                                                   

                                                    @if ($item->pec_event_type==2)
                                                   <?php 
                                                   
                                                   if(isset($curent_bid_amount->cur_bid)){
                                                    $current_bid=$curent_bid_amount->cur_bid;
                                                   }else{
                                                    $current_bid=$add_item->pea_event_start_price;
                                                   }
                                                   ?>
                                                    @if($current_bid>$add_item->pea_event_reserve_price )
                                                    <?php if($add_item->pea_is_emd_required == 1) : ?>
                                                      <?php if(isset($check_emd['emdData']) && @count($check_emd['emdData']) > 0) : ?>
                                                        <input type="number" name="bid_price" id="bid_price" data-bid='current_{{$add_item->pea_id}}' value="{{$curent_bid_amount->cur_bid}}" class="seller-quote-val form-control" placeholder="Enter your Price"  {{$readonly}}>
                                                      
                                                        <button class="btn-purple addattr" name="addattr" type="submit"  style="{{$astyle}}"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                        <button class="btn-purple editattr" name="editattr" type="button" style="{{$estyle}}"><i class="fa fa-pencil" aria-hidden="true" ></i></button> 
                                                      <?php else: ?>
                                                        <a class="btn btn-purple" onClick="payEMDFunction('{{$add_item->pea_id}}')" id="pay-emd-submit" name="SUBMIT"> Pay EMD </a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                        <input type="number" name="bid_price" id="bid_price" data-bid='current_{{$add_item->pea_id}}' value="{{$curent_bid_amount->cur_bid}}" class="seller-quote-val form-control" placeholder="Enter your Price"  {{$readonly}}>
                                                      
                                                        <button class="btn-purple addattr" name="addattr" type="submit"  style="{{$astyle}}"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                        <button class="btn-purple editattr" name="editattr" type="button" style="{{$estyle}}"><i class="fa fa-pencil" aria-hidden="true" ></i></button> 
                                                    <?php endif; ?>

                                                    @else

                                                    <span class="reserved_reached">bid reached reserved amount</span>
                                                    
                                                    @endif

                                                    @else
                                                    
                                                    <?php if($add_item->pea_is_emd_required == 1) : ?>
                                                      <?php if(isset($check_emd['myBideData']) && @count($check_emd['myBideData']) > 0) : ?>
                                                        <span class="bid_closed">Bid Closed</span>
                                                      <?php else: ?>
                                                        <?php if(isset($check_emd['emdData']) && @count($check_emd['emdData']) > 0) : ?>
                                                          <input type="number" name="bid_price" id="bid_price" value="{{$add_item->pla_cur_bid}}" class="seller-quote-val form-control" placeholder="Enter your Price"  {{$readonly}}>
                                                                                                              
                                                          <button class="btn-purple addattr" name="addattr" type="submit"  style="{{$astyle}}"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                          <button class="btn-purple editattr" name="editattr" type="button" style="{{$estyle}}"><i class="fa fa-pencil" aria-hidden="true" ></i></button>
                                                        <?php else: ?>
                                                          <a class="btn btn-purple" onClick="payEMDFunction('{{$add_item->pea_id}}')" id="pay-emd-submit" name="SUBMIT"> Pay EMD </a>
                                                        <?php endif; ?>  
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <input type="number" name="bid_price" id="bid_price" value="{{$add_item->pla_cur_bid}}" class="seller-quote-val form-control" placeholder="Enter your Price"  {{$readonly}}>
                                                                                                              
                                                      <button class="btn-purple addattr" name="addattr" type="submit"  style="{{$astyle}}"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                      <button class="btn-purple editattr" name="editattr" type="button" style="{{$estyle}}"><i class="fa fa-pencil" aria-hidden="true" ></i></button>
                                                    <?php endif; ?>                                                        

                                                    @endif

                                                    
                                                        @if ($add_item->pla_cur_bid)
                                                        <input type="hidden" class="bid_amount" name="bid_amount" value="{{$add_item->pla_cur_bid}}">
                                                        @else
                                                        <input type="hidden" class="bid_amount" name="bid_amount" value="{{$add_item->pea_event_start_price}}">
                                                        @endif
                                                                @if($add_item->pea_event_terms_condition)
                                                        
                                                                <a href="{{URL::to('/')}}/{{$add_item->pea_event_terms_condition}}" title="term" class="btn float-right">View Catalog</a>
                                                                @endif
                                          @else
                                          <a href="javascript:" title="Accept" id="{{$add_item->pei_event_additem_id}}" data-type="1" class="btn-purple btn pq_seller_accept">Accept</a>
                                          <a href="javascript:" title="Reject" id="{{$add_item->pei_event_additem_id}}" data-type="2" class="btn-purple btn pq_seller_accept">Reject</a>
                                      

                                          @endif

                                        
                                        </form>
                                        @endif
                                      </td>
                                    
                                </tr>
                                @endif
                                @endforeach     
                            
                                                  <?php endif; ?>
                                </tbody>
                            </table>
</div>

                        </div>
                    </div>
                </div>
            </div>
            
            @endforeach
            </div>
            
        </div>

        @else
			<!--inner-page-img End-->
				<div class="box-container">
				  <div class="row">
					  <div class="col-xl-12 text-center">
						 <img class="pr-3 img-fluid" src="{{asset('img/no-record.png')}}" alt="Purchase"/>
						 <!--<p> <a href="" class="btn btn-primary">Back</a> </p>-->
					  </div>
				   </div>
				 <!--row End-->
				 
				</div>
			<!--box-container End-->
        @endif

</div>
</div>
</div> 
<div class="modal fade headShake" id="invite_Pricing_Amount" role="dialog">
    <div class="modal-dialog vendor-invite-popup">
    
      <!-- Modal content-->
      <div class="modal-content modal-content-amount invite-popup-content">
        <div class="modal-header modal-header-amount amount_bg">
          <h4 class="modal-title-amount">Pricing Form</h4>
        </div>
        <div class="modal-body modal-body-bg">
	
          <div class="ownvendor_mailinput"> 
           <div class="form-group mt-3">
           
            <div class="Pricing_Amount_bg">
              <span class="event-full-rupee"> &#8377;</span>
	           <p class="event-full-amount"> 3000</p>
           </div>
              <p class="event-full-rupe" id="amnt-text"> Three Thousand Only</p>
           </div>
		 <div class="amount-btn">
		  <div style="margin-top:1em" class="float-left mb-4">
		  <a id="confirm-model" class="btn-amount btn-primary text-center"> confirm </a>
           
          </div>
          <div style="margin-top:1em" class="float-left ml-4 mb-4">
           <a  id="confirm-model-cancel" class="btn-cancel btn-light text-center"> Cancel </a>
          </div>
		 </div>
          
          </div>
	
		
		
        </div>
      </div>
      
    </div>
  </div>

  <div class="modal fade headShake" id="wallet_popup" role="dialog">
    <div class="modal-dialog vendor-invite-popup">
    
      <!-- Modal content-->
      <div class="modal-content modal-content-amount invite-popup-content">
        <div class="modal-header modal-header-amount amount_bg">
          <h4 class="modal-title-amount">Pay EMD from Wallet</h4>
        </div>
        <div class="modal-body modal-body-bg">
	
          <div class="ownvendor_mailinput"> 
            <div class="form-group mt-3">           
              <div class="Pricing_Amount_bg">
                <span class="event-full-rupee"> &#8377;</span>
                <p class="event-full-amount" id="emd-text"> </p>
              </div>
              <p class="event-full-rupe" id="wallet-text"> </p>
              <p class="text-center" style="font-size:13px; color:red"> Note : The EMD amount will be deducted from your wallet balance. </p>
            </div>
            <div class="amount-btn">
              <div style="margin-top:1em" class="float-left mb-4">
                <a id="confirm-model" class="btn-amount btn-primary text-center"> Pay EMD </a>           
              </div>
              <div style="margin-top:1em" class="float-left ml-4 mb-4">
                <a id="confirm-model-wallet-cancel" class="btn-cancel btn-light text-center"> Cancel </a>
              </div>
            </div>  

          </div>
        </div>
      </div>
      
    </div>
  </div>
            

<!--/main content wrapper-->
</div>
<!--footer-->
@endsection
