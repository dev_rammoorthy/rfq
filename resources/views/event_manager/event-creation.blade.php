<?php
 use \App\Http\Controllers\PurchaseController;
  use \App\Http\Controllers\EventController;
  use \Illuminate\Support\Facades\Redirect;
  $session = session()->all(); 

$session_val = PurchaseController::CheckSession($session);

// $sessionData=PurchaseController::getSession();
 $packageData=PurchaseController::getPackage();
 $choosePackage=PurchaseController::getBuyerPackage($session['pli_loginId']);
 $remainPackage=PurchaseController::getNotBuyPackage($session['pli_loginId']);

  ?>
@extends('event_manager/event-header')

@section('content')

<!--main content wrapper-->
    <div class="content-wrapper">
<!--creative states-->
 @if(count($choosePackage)!=0)
	<div class="inner-page-img">
		<div class="container">
		<h3 class="page-title">RFQ Creation</h3>
		</div>
	</div>
 @else
	 <div class="inner-page-img">
		<div class="container">
		<h3 class="page-title">Purchase RFQ</h3>
		</div>
	</div>
@endif


 <section class="section section-grad">
 
      <div class="container">
        <div class="row">
		@if(count($choosePackage)!=0)
		<div class="event-type-option">
		
		</div>
		@else
		<div class="event-type-option">
		
		</div> 
		@endif
 <?php $session = session()->all(); 
	//	print_r($session); 
 ?>
  
 
<div class="container">
<div class="row">
<div class="purchased-pack-list">

  @foreach( $packageData as $key=>$package)
   
<?php  

  $Pkg_curCount=EventController::eventPackageBalance($package->pmp_id,$session['pli_loginId']);		 

?>

  <div class="package-list">
            <!-- BEGIN TABLE -->
            <div class="table-default table3">
              <!-- BEGIN TABLE HEADER -->
              <div class="table__header">
                  <h2 class="table__header--title"> {{$package->pmp_pkg_name}}</h2>               
              </div>
              <!-- END TABLE HEADER -->
              <div class="table__content">
                <!-- BEGIN TABLE LIST -->
                <ul class="table__content--list">
                  <li> Package Name : {{$package->pmp_pkg_name}}</li>
                  @if ($package->pmp_id==3)
                  
                    @if ($Pkg_curCount)
                      <li>Used Events : Unlimited</li>
                      <li>Remaining Events : Unlimited</li>
                    @else
                      <li>Purchase Events : Unlimited</li>
                      <li>Purchase Validity : Unlimited</li>
                      <li>Purchase Amount : <i class="fa fa-inr" aria-hidden="true"></i> {{$package->pmp_pkg_amount}}</li>
                    @endif
                  @else
                    @if ($Pkg_curCount)
                      <li>Used Events : {{$package->pmp_pkg_eventcount}}</li>
                      <li>Remaining Events : {{$Pkg_curCount->pbp_pkg_cur_count}}</li>
                    @else
                      <li>Purchase Events : {{$package->pmp_pkg_eventcount}}</li>
                      <li>Purchase Validity : {{$package->pmp_pkg_validity}}</li>
                      <li>Purchase Amount : <i class="fa fa-inr" aria-hidden="true"></i> {{$package->pmp_pkg_amount}}</li>
                    @endif
                  @endif
  							
                  @if ($Pkg_curCount)
                    <?php		
                      $enddate = date('Y-m-d', strtotime($Pkg_curCount->pbp_pkg_end_dt));
                    ?>
                    <li>Events Expire On : {{$enddate}}</li>
                  @endif
  						  </ul>
                <!-- END TABLE LIST -->
              </div>
              <!-- BEGIN TABLE FOOTER -->
              
              <div class="table__footer">
                @if ($Pkg_curCount)
                  <a href="/create-event/{{strtolower($package->pmp_id)}}" class="btn btn-purple" title="Add Event">Add Event</a>
                @else
                  <a href="{{ URL::to('/confirm-payment/'.$package->pmp_id) }}" style="background: green !important" class="btn btn-purple" title="Buy Event">Buy Event</a>
                
                @endif
              </div>
              <!-- END TABLE FOOTER -->
            </div>
            <!-- END TABLE -->
    </div>
    
    @endforeach
	  
	   </div>
	   
</div>
</div>



		</div>
      </div>
    </section>
    <!--/main content wrapper-->
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                

                <div class="panel-body">
                    {!! $chart->html() !!}
				        </div>
					
            </div>
        </div>
    </div>
</div>
{!! Charts::scripts() !!}
{!! $chart->script() !!}


@endsection


