<?php 

 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \App\Http\Controllers\AddressController;
 use \App\Http\Controllers\OrderController;

$session = session()->all(); 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];

if($session['pli_login_type'] == "1"){
    $event_data=EventController::buyerReceivedPO($loginid);
}
elseif($session['pli_login_type'] == "2"){
    $event_data=EventController::sellerAcceptedPO($loginid);
}
else
{
    if($session['dashboard_check'] == "1")
    {
        $event_data=EventController::sellerAcceptedPO($loginid);
    }
    else
    {
        $event_data=EventController::buyerReceivedPO($loginid);
    }     
}
$address_details = AddressController::listAddress();
$delivery_details = OrderController::getDeliveryDays();

$page="Completed Events";
$pagetrack=PurchaseController::pageLogout($page);

?>
<style>
.auction-collapsed-info
{
  width: 15% !important;
}

/* @media (min-width: 576px) {
    .modal-dialog {
        max-width: none !important;
        padding: 20px;
    }
} */
</style>

@extends('event_manager/event-header')

@section('content')

<!--main content wrapper-->
<div class="content-wrapper">
        <!--creative states-->
            <div class="inner-page-img">
            <div class="container">
            <h3 class="page-title">Invoice</h3>
            </div>
            </div>
            
            
        
        <div class="box-container">
        <div class="row">            
        
 
         @if ($event_data)
             
        
        <div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
        <div class="auction-report-title">
        <div class="auction-collapsed-info">
        <span>Event ID</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event Name</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event start Date </span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event end Date </span>
        </div>        
        <div class="auction-collapsed-info">
        <span>Event Type</span>
        </div>
        <div class="auction-collapsed-info">
        <span>No.of Items</span>
        </div>
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach ($event_data as $item)
                <?php $eventItem = EventController::getAddedItems2($item->pec_event_id); 
                if(@count($eventItem) > 0) :
                ?>

                        <div class="panel panel-default">
                            <div class="panel-heading auction-accordion" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$item->pec_event_id}}" aria-expanded="false" aria-controls="collapseOne">
                                            <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_id}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_name}}</p>
                                                    </div>
                                                    
                                                
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_start_dt}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_end_dt}}</p>
                                                    </div>
                                                    <div class="auction-collapsed-info">
                                                        <p><?php echo (($item->pec_event_type == 1) ? 'Closed Bid' : (($item->pec_event_type == 2) ? 'Reverse Bid' : 'Both Closed and Reverse Bid'));  ?></p>
                                                    </div>
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->no_item}}</p>
                                                            </div>
                                                   
                                    </a>
                                </h4>
                            </div>
                            
                            <div id="{{$item->pec_event_id}}"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="auction-inviter-fullinfo">
                                    <div class="table-responsive">
                                    <table class="table">
            <thead>
                <tr>
                <th>Market</th>
                <th>Category</th>
                <th>Product Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Spec</th>
                <th>Units</th>
                <th>Qty</th>
                <th>Location</th>
                @if($item->pec_event_type ==2)
                <th>Start Price</th>
                <th>Min Inc.</th>
                @endif
                <th>No.of Bidder</th>
                </tr>
            </thead>
            <tbody>

                    <?php if(@count($eventItem) > 0) : ?>

                        @foreach ($eventItem as $add_item)
                <tr class="clickable" data-toggle="collapse" data-target="#group-of-rows-{{$add_item->pea_id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                        <?php                                            
                                      
                        $email_count= json_decode($add_item->pei_invite_details);
                        $invite_count=count( $email_count);
              
                             ?>  
                    
                    <td>{{$add_item->pmm_market_name}}</td>
                        <td>{{$add_item->pmca_cat_name}}</td>
                        <td>{{$add_item->pms_subcat_name}}</td>
                        <td>{{$add_item->pea_event_start_dt}}</td>
                        <td>{{$add_item->pea_event_end_dt}}</td>
                        <td>{{$add_item->pea_event_spec}}</td>
                        <td>{{$add_item->pmu_unit_name}}</td>
                        <td>{{$add_item->pea_event_unit_quantity}}</td>
                        <td>{{$add_item->pea_event_location}}</td>
                        @if($item->pec_event_type ==2)
                        <td>{{$add_item->pea_event_start_price}}</td>
                        <td>{{$add_item->pea_event_max_dec}}</td>
                        @endif
                    <td>
                            <?php $sellerbid = EventController::sellerWonBidDetails($add_item->pea_id);
                                   
										
                            ?>
                    <span class="lot-mail-req">No.of Acceptance : {{$add_item->pei_invite_accept_count}}</span>
                    <a href="javascript:" id="hover-mail-list"><span class="mail-invite-list">{{count($sellerbid)}}</span></a>
                    </td>
                    
                </tr>                
             
            </tbody>
                    
            <tbody id="group-of-rows-{{$add_item->pea_id}}" class="collapse bidder-expand-info">
                <?php $i=0; ?>
                @foreach ($sellerbid as $bid_item)
                   
                <tr>
                <td colspan="12">
                   <div class="final-bidder-details">
                   <div class="live-bid-info col-md-2 col-sm-3 col-xs-12">
                   <!-- <p>{{$bid_item->pci_comp_name}}</p> -->
                   <p>XXXX</p>
                   <span>Company Name</span>
                   </div>
                   
                   <div class="live-bid-info col-md-3 col-sm-3 col-xs-12">
                   <p>{{$bid_item->pla_seller_emailid}}</p>
                   <span>Vendor Email</span>
                   </div>
                   
                   <div class="live-bid-info bid-highlight-price col-md-2 col-sm-3 col-xs-12">
                   <p>{{$bid_item->pla_cur_bid}}</p>
                   <span>Bid Price</span>
                   </div>
                   <div class="live-bid-info col-md-2 col-sm-3 col-xs-12">
                        <p>Location</p>
                        <span>Chennai</span>
                        </div>
                        <div class="live-bid-info col-md-3 col-sm-3 col-xs-12">
                
                        @if ($bid_item->pla_auction_status=='1')
                            @if(($session['pli_login_type'] =="1"  && $session['dashboard_check']=="0") || ($session['pli_login_type'] =="3"  && $session['dashboard_check']=="0"))
                        
                                @if($bid_item->ppi_id != null)                                    
                                     
                                        @if($bid_item->ppi_is_invoice_sent == '1')     

                                            <a href="{{ URL::to('view-invoice')}}/{{$bid_item->ppi_id}}" id="view-model" class="btn btn-sm btn-purple mt-1 viewPO"> View Invoice </a>                                                
                                             
                                            @if($bid_item->ppi_is_buyer_confirmed == '0')
                                                <button data-po-id="{{$bid_item->ppi_id}}" id="{{$bid_item->pla_id}}" data-lot-id="{{$bid_item->pla_lotid}}" data-bidid="{{$bid_item->pla_cur_bid}}" type="button" class="btn btn-sm btn-purple mt-1 acceptInvoice">
                                                    <span class="text-white" >Accept Invoice</span>
                                                </button>
                                            @else
                                                <span class="event-accepted" >Invoice Accepted</span>
                                            @endif
                                                                                                                 
                                        @else                                            
                                            <span class="event-accepted" >Waiting for invoice</span>
                                        @endif
                                    
                                @else
                                    <span class="event-accepted" >Waiting for invoice</span>
                                @endif
                            @elseif($session['pli_login_type'] =="2"  || $session['dashboard_check']=="1")
                                @if($bid_item->ppi_id != null)
                                    
                                        <a href="{{ URL::to('view-invoice')}}/{{$bid_item->ppi_id}}" id="view-model" class="btn btn-sm btn-purple mt-1 viewPO"> View Invoice </a>                                                
                                            
                                        @if($bid_item->ppi_is_invoice_sent == '1')                                            
                                            
                                            @if($bid_item->ppi_is_buyer_confirmed == '0')
                                                <span class="event-accepted" >Invoice Sent/Waiting for Buyer Approval</span>
                                            @else
                                                <span class="event-accepted" >Invoice Accepted by Buyer</span>
                                            @endif
                                           
                                        @else
                                            <button data-po-id="{{$bid_item->ppi_id}}" id="{{$bid_item->pla_id}}" data-lot-id="{{$bid_item->pla_lotid}}" data-bidid="{{$bid_item->pla_cur_bid}}" type="button" class="btn btn-sm btn-purple mt-1 sendInvoice">
                                                <span class="text-white" >Send Invoice</span>
                                            </button> 
                                        @endif

                                @else
                                    <button id="{{$bid_item->pla_id}}" data-lot-id="{{$bid_item->pla_lotid}}" data-bidid="{{$bid_item->pla_cur_bid}}" type="button" class="btn btn-sm btn-purple mt-1 generateInvoice">
                                        <span class="text-white" >Generate Invoice</span>
                                    </button> 
                                @endif                            
                            @endif
                        @endif
                   </div>
                   </td>
                </tr>
               
                @endforeach     
                
            </tbody>
           
            @endforeach    

            <?php else: ?>
                <tr>
                    <td colspan="12">
                        No completed items found for this event
                    </td>
                </tr>
            <?php endif; ?> 
        
        </table>
        </div>
        
        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        @endforeach
                    </div>
        
        </div>
        @else
			<!--inner-page-img End-->
			<div class="box-container">
			  <div class="row">
				  <div class="col-xl-12 text-center">
					 <img class="pr-3 img-fluid" src="{{asset('img/no-record.png')}}" alt="Purchase"/>
					 <!--<p> <a href="" class="btn btn-primary">Back</a> </p>-->
				  </div>
			   </div>
			 <!--row End-->
			 
			 </div>
			 <!--box-container End-->
        @endif
        </div>
        </div>

        <div class="modal fade headShake" id="invoiceForm" role="dialog">
            <div class="modal-dialog vendor-invite-popup" style="width:auto;">
            
            <!-- Modal content-->
                <div class="modal-content modal-content-amount invite-popup-content">
                    <div class="modal-header modal-header-amount amount_bg" style="height:auto">
                    <h4 class="modal-title-amount">Generate Invoice</h4>
                    </div>
                    <div class="modal-body modal-body-bg" style="height:auto">
                        <form class="event-detail-form" name="report" id="generate_invoice_form">
                            <div class="ownvendor_mailinput"> 
                                
                                    <div class="col-md-12" style="background:white">
                                   
                                        <div class="col-md-12 form-group" style="line-height: normal; ">
                                            
                                                <p class="text-center" style="font-weight: 700;"> Informations Required to Generate Invoice </p>

                                                <label>1. Pickup Address </label> 
                                                <select class="form-control select2" name="deliver_address" id="deliver_address">
                                                    <option value="">-- Choose Your Pickup Location --</option>

                                                    @foreach ($address_details as $addressValue)
                                                    <option value="{{$addressValue->psa_id}}">

                                                        <?php echo $addressValue->psa_address_line1 ?> ,
                                                        
                                                        <?php echo $addressValue->psa_address_line2 ?> ,
                                                       
                                                        <?php echo $addressValue->pmst_state_name ?> ,
                                                       
                                                        <?php echo $addressValue->pmci_city_name ?> ,                                                    
                                                        
                                                        <?php echo $addressValue->psa_country ?> ,
                                                      
                                                        <?php echo $addressValue->psa_zipcode ?> ,
                                                       
                                                    </option>			
                                                    @endforeach
                                                </select>
                                                
                                            
                                        </div>   

                                        <div class="col-md-12 form-group">
                                            <label for="logo_upload" class="text-center">2. Upload Company Logo </label>
                                            <input  type="file" onchange="readPOImage(this);" placeholder="upload logo" id="logo_upload" name="logo_upload" /> 		
                                            
                                        </div> 

                                        <div class="col-md-12 form-group">
                                            <img id="blah" src="#" alt="Your Logo Image"/>
                                        </div>


                                        <div class="col-md-12">
                                            <label> Note: <br>
                                            Pickup Address: The product is going to pickup from this address <br>
                                            Company Logo: The logo is going to be printed in the Purchase Order <br>
                                        </div>
                                        <div class="amount-btn">
                                            <div style="margin-top:1em" class="float-left mb-4">
                                                <!-- <a id="confirm-pdf-model" class="btn-amount btn-primary text-center"> PDF </a>           
                                                <a id="confirm-excel-model" class="btn-amount btn-primary text-center"> EXCEL </a>  -->
                                                
                                                <input type="hidden" name="hfPOLotId" id="hfPOLotId" />
                                                <input type="hidden" name="hfCurrentBid" id="hfCurrentBid" />

                                                <input type="submit" class="btn-cancel btn-light btn-purple" name="submitButton">
                                                                                                
                                                <!-- <a id="address-model" class="btn-amount btn-primary text-center"> Confirm </a> -->
                                                <a id="invoice-cancel-model" class="btn-cancel btn-light text-center"> Cancel </a>
                                            </div>                                
                                        </div>                                      
                                    </div>
                                    
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

            <!--/main content wrapper-->
        </div>
        
@endsection