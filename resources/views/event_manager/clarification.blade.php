@extends('header')

@section('content')

<?php 
	use \App\Http\Controllers\EventController;
	use \App\Http\Controllers\PurchaseController;


	$crypt_buyerEmail=Request::route('buyer_email');
	
	$crypt_sellerEmail=Request::route('seller_email');
	$crypt_lotID=Request::route('lotid');
	


	  ?>
		  <div class="forgot-page">
		<div class="forgot-form">            
			<h4 class="text-uppercase text-purple text-center mb-2">Need Clarification</h4>
			 <span hclass="float-left forgot-link my-2">Please give your clarification here:</span>
			    <form name="clarification_form" id="clarification_form" >
				 <div class="form-group mb-4">
					<textarea class="form-control" name="clarification" id="clarification" rows="4" placeholder="Enter Here.."></textarea>
				</div>
				<input type="hidden" id="lotID" name="lotID" value="{{$crypt_lotID}}">
<input type="hidden" id="sellerID" name="sellerID" value="{{$crypt_sellerEmail}}">
<input type="hidden" id="buyerID" name="buyerID" value="{{$crypt_buyerEmail}}">				
				<div class="form-group clearfix">
				  <button type="submit"  class="btn btn-purple float-right">Sent</button>					
              </div>
			</form>
		</div>
       <!--/login form-->
	</div>
		
	

					<div class="messagealert" id="toast_message_success">		
							<div id="mail-alert-box">
							<div id="success-box">
								 <div class="dot two">
								<a href="#" class="close alert alert-sus" data-dismiss="alert" aria-label="close">×</a>
							 </div>
								 <div class="face">
									 <div class="eye"></div>
									 <div class="eye right-box"></div>
									 <div class="mouth happy"></div>
								 </div>
								 <div class="shadow scale"></div>
								 <div class="message">
							 <p class="h5-font1">Success!</p>
							 <p class="h6-font1" id="appendtext"></p>
							 </div> 
							  
							</div>  
						</div>
					</div>
					<div class="messagealert" id="toast_message_fail">		
							<div id="mail-alert-box">
									<div id="error-box">
											<div class="dot two">
										<a href="#" class="close alert alert-sus" data-dismiss="alert" aria-label="close">×</a>
										</div>
											<div class="face2">
												<div class="eye"></div>
												<div class="eye right-box"></div>
												<div class="mouth sad"></div>
											</div>
											<div class="shadow2 move"></div>
											<div class="message">
										<p class="h5-font2">Error!</p>
										<p class="h6-font2" id="appendtext"></p>
										</div>
										</div>
						  
								</div>  
						</div>
		
@endsection
