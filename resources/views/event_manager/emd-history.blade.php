<?php 
 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;
 use \App\Http\Controllers\WalletController;
 use \Illuminate\Support\Facades\Redirect;
 
 $session = session()->all(); 
 
 $session_val = PurchaseController::CheckSession($session);
 
$eventID = Request::route('id');

$session_val = session()->all(); 

$loginid = $session_val['pli_loginId'];
$wallet_data = $session_val['wallet_data'];
$wallet_id = isset($wallet_data) ? $wallet_data->pwc_wallet_id : null;

if(isset($emd_history) && $emd_history != null)
{

}
else
{
	$emd_history = WalletController::emdhistorydetail();
}

$page="Wallet History";
$pagetrack=PurchaseController::pageLogout($page);

?>

@extends('event_manager/event-header')

@section('content')

<div class="content-wrapper">
	<!--creative states-->
	<div class="inner-page-img">
		<div class="container">
            <h3 class="page-title">EMD History</h3>
		</div>
	</div>

	<div class="box-container">
		<div class="row">              
			@if($wallet_data)       
				<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
					<div class="card card-shadow mb-4">
						<div class="card-body">
							<div class="event-full-details">
								<h3>EMD History</h3>
								
								<div class="col-md-12">
									<div class="text-right">                               
										<label> Your Wallet Id : </label> <h4> {{ $wallet_id }} </h4>
									</div>								
								</div>
								<div class="col-md-12 row">
									<div class="text-left col-md-9">     
										<form action="{{ action('WalletController@emdhistory') }}" method="POST"  class="event-detail-form" name="report" id="report-form">
											<label> Filter By Transaction Status : </label>      
											<br>                    
											<input type="submit" class="btn btn-purple" id="report-submit" name="SUBMIT" value="Returned">
											<input type="submit" class="btn btn-purple" id="report-submit" name="SUBMIT" value="Pending">
										</form>
									</div>
									<div class="col-md-3">   
										<br>                            
										<h6> * EMD credited and returned history</h6>
									</div>
								</div>							
								
							</div>						
							<br>
							<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
								<div class="auction-report-title">
									
									<div class="auction-collapsed-info">
										<span>Wallet ID</span>
									</div>							
									<div class="auction-collapsed-info">
										<span> EMD Amount</span>
									</div>
									<div class="auction-collapsed-info">
										<span>EMD return status</span>
									</div> 
									<div class="auction-collapsed-info">
										<span> EMD Paid Date </span>
									</div>								
									<!-- <div class="auction-collapsed-info">
										<span> Receiver Login Id </span>
									</div>    -->
									<div class="auction-collapsed-info">
										<span> Receiver Wallet Id </span>
									</div>      
								</div>
						
								<?php $sno = 1; ?>

								@foreach ($emd_history as $item)

									<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">                                 
								
										<div class="panel panel-default">
											<div class="panel-heading auction-accordion" role="tab" id="headingOne">
																					
												<!-- <div class="auction-collapsed-info">
													<p>{{$sno}}</p>
												</div> -->

												<div class="auction-collapsed-info">
													<p>{{$item->peph_wallet_id}}</p>
												</div>
												
												<div class="auction-collapsed-info">
													<p>{{$item->peph_emd_value}}</p>
												</div>

												
												<div class="auction-collapsed-info">
													<p>{{ ($item->peph_is_emd_returned == 1 ? 'Returned to seller' : 'Pending') }}</p>
												</div>   

												<div class="auction-collapsed-info">
													<p>{{$item->peph_paid_date}}</p>
												</div>
												
												<div class="auction-collapsed-info">
													<p>{{ $item->peph_buyer_wallet_id }}</p>
												</div>                                                             
												
											</div>
													
										</div>
										
									</div>

									<?php $sno ++; ?>

								@endforeach
						
							</div>                            
						</div>
					</div>
				</div>
			@else
				<div class="box-container">
				<div class="row">
					<div class="col-xl-12 text-center">
						<img class="pr-3 img-fluid" src="{{asset('img/no-record.png')}}" alt="Purchase"/>
						<!--<p> <a href="" class="btn btn-primary">Back</a> </p>-->
					</div>
				</div>
				<!--row End-->
				
				</div>
			@endif
		</div>

			<!-- <div class="col-xl-4 col-sm-4 col-md-4 col-lg-4 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Balance</h3>
						</div>						
						<br>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
							<div class="col-md-12 col-xs-12 col-sm-12 form-group">
								
							</div>
						</div>					
					</div>
				</div>
			</div> -->
		</div>
	</div>
</div>

@endsection