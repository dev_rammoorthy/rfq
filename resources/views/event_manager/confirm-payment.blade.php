<?php $session_val = session()->all(); ?>



@extends('event_manager/event-header')

@section('content')

<!--main content wrapper-->
    <div class="content-wrapper">
<!--creative states-->
	<div class="inner-page-img">
	<div class="container">
	<h3 class="page-title">Event Confirmation</h3>
	</div>
	</div>
	
 <section class="section section-grad">
 
      <div class="container">
        <div class="row">
		<div class="event-type-option">

 </div> 
		<div class="container">
			<div id="status"></div>
			<form action="{{ action('CashfreeController@createRequest') }}" name="package_conformation" method="POST">
					{{ csrf_field() }}
                    <div class="col-xs-12 col-md-6 col-sm-6">
						<h5>Company Information</h5>
						<div class="form-group">
							<input type="text" class="form-control" value="{{$pq_company_info->pci_comp_name}}" name="pci_comp_name" placeholder="Name of Company *" readonly>
						</div>
						<div class="form-group">
							<input type="number" class="form-control" value="{{$pq_company_info->pci_comp_phone}}" name="pci_comp_phone" placeholder="Company Phone *" readonly>
						</div>			
											
							<div class="form-group">
								<input type="text" class="form-control" value="{{$pq_master_package->pmp_pkg_name}}" name="pmp_pkg_name" placeholder="package Name *" readonly>
							</div>	
							<div class="form-group">
								<input type="number" class="form-control" value="{{$pq_master_package->pmp_pkg_amount}}" name="pmp_pkg_amount" placeholder="Package Price *" readonly>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" value="{{$pq_master_package->pmp_pkg_eventcount}}" name="pmp_pkg_eventcount" placeholder="Event Count *" readonly>
							</div>	
							<div class="form-group">
								<input type="text" class="form-control" value="{{$pq_master_package->pmp_pkg_validity}}" name="pmp_pkg_validity" placeholder="Package Validity *" readonly>
							</div>
							<input type="hidden" class="form-control" value="{{$pq_company_info->pci_comp_id}}" name="pli_comp_id" readonly>
							<input type="hidden" class="form-control" value="{{$session_val['pli_sno']}}" name="pli_sno" readonly>
							<input type="hidden" class="form-control" value="{{$pq_master_package->pmp_id}}" name="pmp_id" readonly>
							<input type="hidden" class="form-control" value="{{$session_val['pli_loginId']}}" name="pli_loginId" readonly>
							
						
						 <input type="submit"  class="btn btn-purple"> 						
						
					</div>
				</form>
			
		</div>
      </div>
    </section>
    <!--/main content wrapper-->
</div>

@endsection
