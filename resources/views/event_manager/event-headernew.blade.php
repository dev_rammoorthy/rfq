<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <!--favicon icon-->
    <link rel="icon" type="image/png" href="{{ asset ('new-img/purch-icon.png') }}">

    <title>MatexNet Event - Event Pricing</title>
    <!--web fonts-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,500,600,700,800" rel="stylesheet">
    <!--bootstrap styles-->
    <link href="{{ asset ('css/bootstrap.min.css') }}?{{ time() }}" rel="stylesheet">
    <!--icon font-->
    <link href="{{ asset ('css/font-awesome.min.css') }}?{{ time() }}" rel="stylesheet">
    <!--custom styles-->
    <link href="{{ asset ('css/main.css') }}?{{ time() }}" rel="stylesheet">
	<!-- matex css -->
    <link href="{{ asset ('css/custom.css') }}?{{ time() }}" rel="stylesheet">
	<!-- Bootstrap Pricing css -->
	<link href="{{ asset ('css/pricing-table.css') }}?{{ time() }}" rel="stylesheet">
	<!-- Bootstrap datetimepicker css -->
	<link href="{{ asset ('css/datetimepicker.css') }}?{{ time() }}" rel="stylesheet">
	<!-- Select2 css -->
	<link href="{{asset ('css/select2.css') }}?{{ time() }}" rel="stylesheet">
	<!-- iziToast css -->
	<link href="{{asset ('css/plugins/izitoast/iziToast.min.css') }}?{{ time() }}" rel="stylesheet">
	<!-- datatables css -->
	<link href="{{ asset ('css/datatables.css') }}?{{ time() }}" rel="stylesheet">
	@yield('assets')
</head>

 <?php 
  use \App\Http\Controllers\PurchaseController;
  use \App\Http\Controllers\WalletController;
 $session_val = session()->all();

 $choosePackage=PurchaseController::getBuyerPackage($session_val['pli_loginId']);
 $check_resetlogin=PurchaseController::checkloginReset($session_val['pli_loginId']);
 $checkWallet=WalletController::showmoney();
 
 $encrypted = Crypt::encryptString($session_val['pli_loginId']);
 

 ?>
  

<body class="fixed-nav top-nav">
	<div class="preloader" >
		<div class="loading" >
		 <div class="lds-ripple"><div></div><div></div></div>
		</div>
	</div>
	
    <!--header-->
    <nav class="navbar navbar-expand-lg fixed-top navbar-light" id="mainNav">
        <div class="box-container">
           <!--brand name for responsive-->
           <a class="navbar-brand navbar-brand-responsive" href="#">
               <img class="pr-3 float-left" src="{{ asset ('new-img/logo.png') }}" srcset="{{ asset ('new-img/logo.png') }}"  alt=""/>
           </a>
           <!--/brand name for responsive-->

            <!--responsive navigation list toggle-->
            <button class="navbar-toggler navigation-list-toggler" type="button" data-toggle="collapse" data-target="#navbarListResponsive" aria-controls="navbarListResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!--/responsive navigation list toggle-->

            <!--responsive nav notification toggle-->
            <button class="navbar-toggler nav-notification-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span> <i class="fa fa-user-o fa-5" aria-hidden="true"></i></span>
            </button>
            <!--/responsive nav notification toggle-->

            <div class="collapse navbar-collapse" id="navbarResponsive">

                <!--brand name-->
                <a class="navbar-brand navbar-hide-responsive" href="{{ URL::to('/') }}">
                    <img class="pr-3 float-left" src="{{ asset ('new-img/logo.png') }}"  alt=""/>
                </a>
                <!--/brand name-->

                <!--header rightside links-->
                <ul class="navbar-nav header-links ml-auto hide-arrow">
                
                    <li class="nav-item dropdown" style="border: 1px #5596e6 solid;">
                        <a class="nav-link dropdown-toggle mr-lg-3" id="userNav" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-thumb">
                                <div class="signed-user">
									<p class="signed-username">Wallet Balance</p>
									<p class="signed-usertype"> <i class="fa fa-inr"> </i> <?php if($checkWallet) { echo $checkWallet->pwc_current_balance; } else { echo "0"; } ?></p>
								</div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle mr-lg-3" id="userNav" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-thumb">
                                <img class="rounded-circle" src="{{asset ('new-img/profile-pic.png') }}" alt=""/>
								<div class="signed-user">
									<p class="signed-username"><?php echo ucfirst($session_val['pli_con_name']); ?></p>
									<p class="signed-usertype"><?php if($session_val['pli_login_type'] == "1"){ echo ucfirst("Buyer"); }elseif($session_val['pli_login_type'] == "2"){ echo ucfirst("Seller"); } else{ 
                                        if($session_val['dashboard_check'] == "1")
                                                echo ucfirst("Seller"); 
                                        else
                                        echo ucfirst("Buyer"); 
                                            }
                                        
                                        
                                        ?></p>
								</div>
                            </div>
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userNav">
                           <!--     <a class="dropdown-item" href="myprofile.html" title="My Profile">My Profile</a>
                            <a class="dropdown-item" href="#">Account Settings</a>
                            <div class="dropdown-divider"></div>-->
                            <a class="dropdown-item" href="{{ URL::to('list-address') }}">Manage Shipping Address</a>
                            <a class="dropdown-item" href="{{ URL::to('wallet') }}">My Wallet</a>
                            <a class="dropdown-item" href="{{ URL::to('list-bank') }}">My Bank Accounts</a>
                            <a class="dropdown-item" href="{{ URL::to('confirm-bank') }}">Withdraw Money</a>
                            <a class="dropdown-item" href="{{ URL::to('logout') }}">Sign Out</a>
                        </div>
                    </li>
                </ul>
                <!--/header rightside links-->
            </div>
        </div>
    </nav>

    <nav class="navbar navbar-expand-lg navbar-dark"> 
        <div class="box-container">
            <div class="collapse navbar-collapse" id="navbarListResponsive">
                <!--header nav links-->
                <ul class="navbar-nav header-links">
                	<li class="nav-item">
                    	<a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('rfq')}}">RFQ</a>
                    </li>
                	@if($session_val['pli_login_type'] == 1 || $session_val['pli_login_type'] == 3)
                    	<li class="nav-item">
	                    	<a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('rfq/create')}}">Create RFQ</a>
	                    </li>
                    @endif

                    @if($session_val['pli_login_type'] == 2 || $session_val['pli_login_type'] == 3)
                    	<li class="nav-item">
                    		<a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('liverfq/')}}">Live RFQ</a>
                    	</li>
                    @endif

                   	

                    <li class="nav-item">
                    	<a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('completedrfq')}}">Completed RFQ</a>
                    </li>

                    <li class="nav-item">
                    	<a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('#')}}">Report RFQ</a>
                    </li>

                    <li class="nav-item">
                    	<a class="nav-link mr-lg-3" id="reportNav" href="{{ URL::to('#')}}">Logistic RFQ</a>
                    </li>

                </ul>
                <!--/header search-->
                <!--/header search-->
            </div>
        </div>
    </nav>
    <!--/header-->
	
	@yield('content')

 <!--footer-->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>&copy; 2018 MatexNet Pvt. Ltd.</small>
            </div>
        </div>
    </footer>
    <!--/footer-->

    <!--right sidebar-->

    <!--/right sidebar-->

<!--basic scripts-->
<script src="{{ asset ('js/jquery.min.js') }}?{{ time() }}"></script>
<script src="{{ asset ('js/bootstrap.min.js') }}?{{ time() }}"></script>
@yield('scripts')	
<!--basic scripts initialization-->
<script src="{{ asset ('js/scripts.js') }}?{{ time() }}"></script>
<script src="{{ asset ('js/moment.js') }}?{{ time() }}"></script>
<!--Datepicker bootstrap js-->
<script src="{{ asset ('js/datetimepicker.js') }}?{{ time() }}"></script>

<script src="{{ asset ('js/jquery.validate.min.js') }}?{{ time() }}"></script>
<script src="{{ asset ('js/page.js') }}?{{ time() }}"></script>
<script src="{{ asset('js/plugins/izitoast/iziToast.min.js') }}?{{ time() }}"></script>
<script src="{{ asset('js/select2.full.js') }}?{{ time() }}"></script>
<script src="{{ asset ('js/bootstable.min.js') }}?{{ time() }}"></script>

@yield('inline-scripts')

<div class="modal fade" id="force_reset_password" role="dialog">
    <div class="modal-dialog vendor-invite-popup">
    
      <!-- Modal content-->
      <div class="modal-content invite-popup-content">
        <div class="modal-header">
         <h4 class="text-uppercase text-purple text-center mb-2 mt-1">Change Password
                <button type="button" class="btn btn-close-mail float-right skippopup" data-dismiss="modal">X</button>
             </h4> 

        </div>
        <div class="modal-body">
		 <a href="#" class="float-left forgot-link my-2">Please Enter Your New Password </a>
			      <form name="force_reset" id="force_reset" novalidate="novalidate">
				 <div class="form-group mb-4">
					<input type="password" name="new_password" class="form-control" id="new_password" placeholder="New Password">
				</div>
				<div class="form-group mb-4">
					<input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="Confirm Password">
                </div>
            <input type="hidden" name="email" value="{{$session_val['pli_loginId']}}" class="form-control" id="email" >
				
				<div class="form-group clearfix">
					<button type="submit" ID="btnSuccess" runat="server"  class="btn btn-purple float-center">Submit</button>	
                  <span class="float-right btn-link text-capitalize cursor skippopup">Skip</span>					
				</div>
			</form>	

      </div>
      
    </div>
  </div>
</div>
</body>
</html>
<?php if($check_resetlogin->pli_firsttime_login==0){
    if(!isset($session_val['skip'])){
    ?>
<script type="text/javascript">

    $(document).ready(function(){
        $("#aap").hide();
      $('#force_reset_password').modal('show');
    });
    
    </script>
    <?php }} ?>

    