@extends('header')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Bid Report - Seller Wise</div>

                <div class="panel-body">
                    {!! $line->html() !!}
				</div>
					
            </div>
        </div>
    </div>
</div>
{!! Charts::scripts() !!}
{!! $line->script() !!}

@endsection
