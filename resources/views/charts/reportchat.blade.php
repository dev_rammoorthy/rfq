<?php 

 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;

$session = session()->all(); 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];

$event_data=EventController::reportCompletedEvent($loginid);

?>


@extends('event_manager/event-header')
@section('assets')
<link rel="stylesheet" type="text/css" href="{{asset('css/plugins/data-table/dataTables.bootstrap.min.css')}}?{{ time() }}">
<link rel="stylesheet" type="text/css" href="{{asset('css/page/tables.css')}}?{{ time() }}">
@endsection
@section('content')

<!--main content wrapper-->
<div class="content-wrapper">
        <!--creative states-->
            <div class="inner-page-img">
            <div class="container">
            <h3 class="page-title">Bidwise Chart Report</h3>
            </div>
            </div>
            
        <div class="box-container">
        <div class="row">            
        
        <div class="box-container">
		<div class="row">              
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3> Chart Reports</h3>
                            <div class="text-right">                               
                                <h6> * Search by Event name</h6>
                            </div>
						</div>
						
						<!--<form action="{{ action('EventController@reports') }}" method="POST"  class="event-detail-form" name="report" id="report-form">
                         -->
						 <form action="{{ action('EventController@chartsReprts') }}" method="POST" class="event_report_validate" id="event_report_validate" >

						 <br>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                    <h5 style="padding:7px"> Event Name </h5>
                                </div>

                                <div class="col-md-8 col-xs-8 col-sm-8 form-group">
								
							<?php	
								if(isset($searchedcharts))
								{
									foreach($searchedcharts as $name)
									{
										$a=$name->pea_event_id;
										$b=$name->pec_event_id.' - '.$name->pec_event_name.' [ '.$name->pec_event_start_dt.' - '.$name->pec_event_end_dt.' ] ';
										$c=$name->pea_event_spec;
										$d=$name->pea_id;
									}								
								}
							?>								
								
                                    <select class="form-control"  name="event_name_report" id="event_name_report">
																						
											 <?php if(isset($searchedcharts))
												 { ?> 
											 
											 <option value="{{$a}}">{{$b}}</option>
											 
											 <?php } else { ?>
											 
											 <option value="">-- Choose Your Events --</option>
											  <?php }  ?>
												 
											
											@foreach ($event_data as $item)
											   <?php if(isset($event_data)) { ?> 
												<option value="{{$item->pec_event_id}}">{{$item->pec_event_id}} - {{$item->pec_event_name}} [ {{$item->pec_event_start_dt}} - {{$item->pec_event_end_dt}} ]</option>
											   <?php } ?>
											   
												
											@endforeach											

									</select>
								</div>
								
                            </div>
							
							 <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                    <h5 style="padding:7px">Lot Name </h5>
                                </div>

                                <div class="col-md-8 col-xs-8 col-sm-8 form-group">
                                    <select class="form-control"  name="sub_lot" id="sub_lot">
										
										<?php if(isset($lotData) && $lotData != null) : ?>
                                            <option>-- Choose Your Lot --</option>
                                            
                                            @foreach ($lotData as $item)
                                            <option value="{{$item->pea_id}}" <?php if(isset($lotId) && $lotId == $item->pea_id) { echo 'selected'; } ?> >{{$item->pea_event_spec}}</option>
                                            
                                            @endforeach
                                        <?php endif; ?>									
										
									</select> 
								</div>
                            </div>

                            <input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
						
							<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
								<input type="submit" class="btn btn-purple" id="report-submit" name="SUBMIT">
							</div>    
						</form>
												
					</div>
				</div>
			</div>
		</div>
    </div>
	
	  <?php if(isset($line_chart)) : ?> 
			
            
	<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Line Chart</div>

                <div class="panel-body">

					{!! $line_chart->html() !!}
					
                </div>
            </div>
        </div>
    </div>
</div>
{!! Charts::scripts() !!}


{!! $line_chart->script() !!}

<?php endif; ?>




        </div>
        </div>
            <!--/main content wrapper-->
        </div>
        
@endsection


@section('scripts')
	<script type="text/javascript" src="{{asset('js/plugins/datatables/datatables.min.js')}}?{{ time() }}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/datatables/dataTables.buttons.min.js')}}?{{ time() }}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/datatables/jszip.min.js')}}?{{ time() }}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/datatables/buttons.html5.min.js')}}?{{ time() }}"></script>
@endsection
@section('inline-scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#mis_download').DataTable( {
			responsive: true,
			scrollX: true,
			ordering: false,
			searching: false,
			info: false,
			dom: 'Bfrtip',
			buttons: [
				'excel',
			],
		} );
	} );
</script>
@endsection 