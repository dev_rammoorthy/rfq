@extends('header')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Chart Demo</div>

                <div class="panel-body">
                    {!! $chart->html() !!}
				</div>

				<hr>	
					{!!$pie->html() !!}

				<hr>	
					{!!$line->html() !!}

				<hr>
					{!!$area->html() !!}

				<hr>
					{!!$areaspline->html() !!}

				<hr>
					{!!$geo->html() !!}

				<hr>
					{!!$percentage->html() !!}
					
            </div>
        </div>
    </div>
</div>
{!! Charts::scripts() !!}
{!! $chart->script() !!}

{!! $pie->script() !!}
{!! $line->script() !!}
{!! $area->script() !!}
{!! $areaspline->script() !!}
{!! $geo->script() !!}
{!! $percentage->script() !!}
@endsection
