	<div style="width:500px; background-color:#fff; margin:0px auto;">
		<div>
			<img src="http://35.200.233.222:8000/new-img/logo.png">
		</div>
		<div style="background-color:#fff; width:100%; box-shadow:0px 0px 10px 0px hsla(0, 0%, 49%, 0.47); border-radius:4px;">
			<div style="background-color:#146da0; color:#fff; padding:50px 0px; text-align:center; border-radius:4px 4px 0px 0px;">
				<div style="font-size:20px;font-weight:800; margin:0px">
					<p style="width: 50px; height: 50px; margin: 0px auto; background-color: #155f8a; border-radius: 50%; line-height: 50px; text-align: center;">&#10004;</p>
				</div>
				<p style="font-size:19px; font-weight:600; margin:10px 0px; letter-spacing:2px; text-transform:capitalize; font-family:sans-serif;">New Event Invite</p>
				<span style="font-size:13px; color:#add8f1; font-weight:500; margin:10px 0px; text-transform:capitalize; font-family:sans-serif;"></span>
			</div>
			
			<div style="padding:0px 20px; font-size:14px; font-family:sans-serif; line-height:28px; color:#333; font-weight:500; text-align:center;">
				<p style="margin:25px 0px;">Dear sir, <br/>Your are invited for the <span style="text-transform: capitalize;">{!! $pec_event_name !!}/{!! $pmc_cat_name !!} by {!! $pli_loginId !!} from {!! $pli_con_name !!}</span></p>
			</div>						
			<div style="border: 2px solid #112233;width:400px;margin: 0px auto;">							
				<p style="text-align:center;font-size:20px;display:block;background: #146da0;color: #fff;padding: 10px;margin: 0px; font-weight:600;">Event Details</p>
				<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Event Name :<b style="text-transform: capitalize;">{!! $pec_event_name !!}</b></p>
				<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Market Name :<b>{!! $pmc_market_name !!}</b></p>
				<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Category Name :<b>{!! $pmc_cat_name !!}</b></p>
				<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Sub Category :<b>{!! $pms_subcat_name !!}</b></p>
				<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Specification :<b>{!! $pea_event_spec !!}</b></p>
				<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Unit :<b>{!! $pea_event_unit_quantity !!} / {!! $pmu_unit_name !!}</b></p>
				@if ($event_type==2)
					
				<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Starting Price :<b>Rs. {!! $pea_event_start_price !!}</b></p>
					
				@endif
				<!--<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Expected Price :<b>Rs. { $pea_event_start_price !!}</b></p>-->
				<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Event Start Date :<b>{!! $pec_event_start_dt !!}</b></p>
				<p style="text-align:center;font-size:15px;display:block;border-bottom: 1px solid #123;">Event Start Date :<b>{!! $pec_event_end_dt !!}</b></p>
				<p style="text-align:center;font-size:15px;display:block;">Location :<b>{!! $pea_event_location !!}</b></p>							
			</div>

			<div style="padding:30px 20px; font-size:14px; font-family:sans-serif; line-height:28px; color:#333; font-weight:500; text-align:center;">
				<p style="margin:25px 0px;">To Accept The above event simply click the below button.</p>
				<a href="{{ URL::to('/event_accept') }}/{!! $enc1 !!}/1/{!! $enc !!}" title="Register Now" style="display:inline-block; background-color:#00c851; padding:10px 20px; text-decoration:none; font-size:14px;  font-weight:500; letter-spacing:1px; text-transform:uppercase; color:#fff; border-radius:4px;width:130px">Accept Event</a>
				<a href="{{ URL::to('/event_accept') }}/{!! $enc1 !!}/2/{!! $enc !!}" title="Register Now" style="display:inline-block; background-color:#ff3547; padding:10px 20px; text-decoration:none; font-size:14px;  font-weight:500; letter-spacing:1px; text-transform:uppercase; color:#fff; border-radius:4px;width:130px">Not Interest</a>
				<p style="margin: 8px 0px;"><a href="{{ URL::to('/need-clarification') }}/{!! $enc_buyer !!}/{!! $enc1 !!}/{!! $enc !!}" style="color: #f36123;">Need Clarification.?</a></p>
				<p style="color:#a5a5a5; margin-top:30px; margin-bottom:0px; line-height:10px; font-size:12px; font-weight:500; font-family:sans-serif;">Email Sent by PurchaseQuik</p>
				<p style="color:#a5a5a5; font-size:12px; margin:0px; line-height:20px; font-weight:500; font-family:sans-serif;">Copyright &copy; {!! $year !!} PurchaseQuik. All rights reserved.</p>
			</div>
		</div>
	</div>