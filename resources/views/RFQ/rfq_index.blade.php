<style>
.auction-collapsed-info
{
  width: 15% !important;
}
#ownv{
    font-weight: bold;
    margin-left: 40px;
}
.record{
	margin-top: 18px;
	margin-bottom: 75px;
}
</style>

<script type="text/javascript">
	function passdata(id){
		document.getElementById("pass_id").value =id;
	}
</script>

@extends('event_manager/event-headernew')

@section('content')
<div class="content-wrapper">
	<div class="inner-page-img">
		<div class="container">
			<h3 class="page-title">My RFQ</h3>
		</div>
	</div>

	
	<div class="box-container">
		<div class="row">
			@if(Session::has('msg'))
			<div style="font-weight: bold;font-size: 43px;">{{session('msg')}}</div>
			@endif
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
				<div class="auction-report-title">
					<div class="auction-collapsed-info">
						<span>RFQ Name</span>
					</div>
					<div class="auction-collapsed-info">
						<span>RFQ Desc</span>
					</div>
					<div class="auction-collapsed-info">
						<span>Event start Date</span>
					</div>
					<div class="auction-collapsed-info">
					    <span>Event end Date</span>
					</div>
					<div class="auction-collapsed-info">
					    <span>EMD</span>
					</div>
					<div class="auction-collapsed-info">
					    <span>RFQ Fees</span>
					</div>
				</div>

				@foreach ($fetch as $item)
					<div class="record">
						<div class="auction-collapsed-info">
							<span>{{$item['event_name']}}</span>
						</div>
						<div class="auction-collapsed-info">
							<span>{{$item['description']}}</span>
						</div>
						<div class="auction-collapsed-info">
							<span>{{$item['start_date_time']}}</span>
						</div>
						<div class="auction-collapsed-info">
						    <span>{{$item['end_date_time']}}</span>
						</div>
						<div class="auction-collapsed-info">
						    <span>{{$item['emd']}}</span>
						</div>
						<div class="auction-collapsed-info">
						    <span>{{$item['rfq_fees']}}</span>
						    <span><a href="#" id="ownv" class="btn btn-purple sub_btn" title="Own Vendor" data-toggle="modal" data-target="#addressFormv" onclick="passdata( {{$item['id']}} )">Inivte</a></span>
						</div>
					</div>
				@endforeach

			</div>
		</div>
	</div>

	<div class="modal fade" id="addressFormv" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content invite-popup-content">
        <div class="modal-header">
			<h4 class="modal-title">Invite Your Suppliers</h4>
        </div>
        <div class="modal-body">
			<form action="inviterfq" class="invite-vendorform" method="post">				
				<div class="pqvendor_mailinput form-group col-sm-6 col-xs-12 col-md-6">
					<label for="">Email</label>
					<input type="email" class="form-control" name="email">
					<input type="hidden" class="form-control" name="pass_id" id="pass_id">
					{{ csrf_field() }}
					<br>
					<input type="submit" class="btn btn-purple sub_btn" id="signupform">
				</div>
			</form>
        </div>
		
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>

	
@endsection