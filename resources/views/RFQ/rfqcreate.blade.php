@extends('event_manager/event-headernew')

@section('content')

<style type="text/css">
	.sub{
		text-transform: capitalize;
	}

	.movie_item_toolbar {
	    clear: both;
	    vertical-align: bottom;
	    width: 100%;
	    height: 66px;
	}

	.sub_btn {
	    padding: 8px 27px;
	    font-size: 16px;
	    font-weight: bold;
	}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js"></script>

<script type="text/javascript">

	function loadDoc() {
	  	var xhttp = new XMLHttpRequest();
		xhttp.open("POST", "/json-handler");
		xhttp.setRequestHeader("Content-Type", "application/json");
		var data = new FormData(document.querySelector('form'));
	  	xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		      	document.getElementById("show_data").innerHTML = this.responseText;
		    }
	  	};
	  	xhttp.open("POST", "api/pincode", true);
	  	xhttp.send(data);
	}

	var clicks = 0;

	var ExcelToJSON = function() {

      	this.parseExcel = function(file) {
        var reader = new FileReader();

        reader.onload = function(e) {
          var data = e.target.result;

          var workbook = XLSX.read(data, {
            type: 'binary'
          });
          workbook.SheetNames.forEach(function(sheetName) {
            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            var json_object = JSON.stringify(XL_row_object);
            var json_ary=JSON.parse(json_object);

            for (var i = 0; i < XL_row_object.length; i++) {
            	var criteria1 = document.createElement("INPUT");
				criteria1.setAttribute("type", "text");
				criteria1.setAttribute("Name", "criteria" + i);
				criteria1.setAttribute("id","criteria"+ i);
				criteria1.setAttribute('style','margin-bottom: 15px');
				criteria1.setAttribute('value',json_ary[i]['Example Technical & Quality Criteria']);
				document.getElementById("criteria1").appendChild(criteria1).classList.add("form-control");

				var estimated1 = document.createElement("INPUT");
				estimated1.setAttribute("type", "text");
				estimated1.setAttribute("Name", "estimated" + i);
				estimated1.setAttribute("id","estimated" + i);
				estimated1.setAttribute('style','margin-bottom: 15px');
				estimated1.setAttribute('value',json_ary[i]['Estimated Rate in (INR)']);
				document.getElementById("estimated1").appendChild(estimated1).classList.add("form-control");

				var rate1 = document.createElement("INPUT");
				rate1.setAttribute("type", "text");
				rate1.setAttribute("Name", "rate" + i);
				rate1.setAttribute("id","rate" + i);
				rate1.setAttribute('style','margin-bottom: 15px');
				rate1.setAttribute('value',json_ary[i]['Rate']);
				document.getElementById("rate1").appendChild(rate1).classList.add("form-control");
				document.getElementById("count_xls").value = i;
            }
          })
        };

        reader.onerror = function(ex) {
          console.log(ex);
        };

        reader.readAsBinaryString(file);
      	};
  	};

  	

	function ExcelBoxCreate(evt){
		var files = evt.target.files; // FileList object
	    var xl2json = new ExcelToJSON();
	    xl2json.parseExcel(files[0]);
	}

	function textBoxCreate(){
		clicks++;
		var criteria_form0 = document.createElement("INPUT");
		criteria_form0.setAttribute("type", "text");
		criteria_form0.setAttribute("Name", "criteria" + clicks);
		criteria_form0.setAttribute("id","criteria"+ clicks);
		criteria_form0.setAttribute('style','margin-bottom: 15px');
		criteria_form0.setAttribute('placeholder','Example Technical & Quality Criteria');
		document.getElementById("criteria_form0").appendChild(criteria_form0).classList.add("form-control");

		var estimated_form0 = document.createElement("INPUT");
		estimated_form0.setAttribute("type", "text");
		estimated_form0.setAttribute("Name", "estimated" + clicks);
		estimated_form0.setAttribute("id","estimated" + clicks);
		estimated_form0.setAttribute('style','margin-bottom: 15px');
		estimated_form0.setAttribute('placeholder','Estimated Rate in (INR)');
		document.getElementById("estimated_form0").appendChild(estimated_form0).classList.add("form-control");

		var rate_form0 = document.createElement("INPUT");
		rate_form0.setAttribute("type", "text");
		rate_form0.setAttribute("Name", "rate" + clicks);
		rate_form0.setAttribute("id","rate" + clicks);
		rate_form0.setAttribute('style','margin-bottom: 15px');
		rate_form0.setAttribute('placeholder','Rate');
		document.getElementById("rate_form0").appendChild(rate_form0).classList.add("form-control");

		document.getElementById("count_form").value = clicks;
	}


	var CommExcelToJSON = function() {

      	this.parseExcel = function(file) {
        var reader = new FileReader();

        reader.onload = function(e) {
          var data = e.target.result;

          var workbook = XLSX.read(data, {
            type: 'binary'
          });
          workbook.SheetNames.forEach(function(sheetName) {
            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            var json_object = JSON.stringify(XL_row_object);
            var json_ary=JSON.parse(json_object);
            for (var i = 0; i < XL_row_object.length; i++) {
            	var comm_item_num1 = document.createElement("INPUT");
				comm_item_num1.setAttribute("type", "text");
				comm_item_num1.setAttribute("Name", "comm_item_num_xls" + i);
				comm_item_num1.setAttribute("id","comm_item_num_xls"+ i);
				comm_item_num1.setAttribute('style','margin-bottom: 15px');
				comm_item_num1.setAttribute('value',json_ary[i]['Item no']);
				document.getElementById("comm_item_num1").appendChild(comm_item_num1).classList.add("form-control");

				var comm_item_desc1 = document.createElement("INPUT");
				comm_item_desc1.setAttribute("type", "text");
				comm_item_desc1.setAttribute("Name", "comm_item_desc_xls" + i);
				comm_item_desc1.setAttribute("id","comm_item_desc_xls"+ i);
				comm_item_desc1.setAttribute('style','margin-bottom: 15px');
				comm_item_desc1.setAttribute('value',json_ary[i]['Work / Item description']);
				document.getElementById("comm_item_desc1").appendChild(comm_item_desc1).classList.add("form-control");

				var comm_item_code1 = document.createElement("INPUT");
				comm_item_code1.setAttribute("type", "text");
				comm_item_code1.setAttribute("Name", "comm_item_code_xls" + i);
				comm_item_code1.setAttribute("id","comm_item_code_xls"+ i);
				comm_item_code1.setAttribute('style','margin-bottom: 15px');
				comm_item_code1.setAttribute('value',json_ary[i]['Item code / mark']);
				document.getElementById("comm_item_code1").appendChild(comm_item_code1).classList.add("form-control");

				var comm_item_quantity1 = document.createElement("INPUT");
				comm_item_quantity1.setAttribute("type", "text");
				comm_item_quantity1.setAttribute("Name", "comm_item_quantity_xls" + i);
				comm_item_quantity1.setAttribute("id","comm_item_quantity_xls"+ i);
				comm_item_quantity1.setAttribute('style','margin-bottom: 15px');
				comm_item_quantity1.setAttribute('value',json_ary[i]['Required Quantity']);
				document.getElementById("comm_item_quantity1").appendChild(comm_item_quantity1).classList.add("form-control");

				var comm_item_units1 = document.createElement("INPUT");
				comm_item_units1.setAttribute("type", "text");
				comm_item_units1.setAttribute("Name", "comm_item_units_xls" + i);
				comm_item_units1.setAttribute("id","comm_item_units_xls"+ i);
				comm_item_units1.setAttribute('style','margin-bottom: 15px');
				comm_item_units1.setAttribute('value',json_ary[i]['units UOM']);
				document.getElementById("comm_item_units1").appendChild(comm_item_units1).classList.add("form-control");

				var comm_item_rate1 = document.createElement("INPUT");
				comm_item_rate1.setAttribute("type", "text");
				comm_item_rate1.setAttribute("Name", "comm_item_rate_xls" + i);
				comm_item_rate1.setAttribute("id","comm_item_rate_xls"+ i);
				comm_item_rate1.setAttribute('style','margin-bottom: 15px');
				comm_item_rate1.setAttribute('value',json_ary[i]['Estimate Rate']);
				document.getElementById("comm_item_rate1").appendChild(comm_item_rate1).classList.add("form-control");

				document.getElementById("comm_count_xls").value = i;
            }
          })
        };

        reader.onerror = function(ex) {
          console.log(ex);
        };

        reader.readAsBinaryString(file);
      	};
  	};

  	function CommExcelBoxCreate(evt){
		var files = evt.target.files; // FileList object
	    var xl2json = new CommExcelToJSON();
	    xl2json.parseExcel(files[0]);
	}


	comclick=0;

	function CommBoxCreate(){
		comclick++;
		var comm_item_num = document.createElement("INPUT");
		comm_item_num.setAttribute("type", "text");
		comm_item_num.setAttribute("Name", "comm_item_num" + comclick);
		comm_item_num.setAttribute("id","comm_item_num"+ comclick);
		comm_item_num.setAttribute('style','margin-bottom: 15px');
		comm_item_num.setAttribute('placeholder','Commercial Item Number');
		document.getElementById("comm_item_num").appendChild(comm_item_num).classList.add("form-control");

		var comm_item_desc = document.createElement("INPUT");
		comm_item_desc.setAttribute("type", "text");
		comm_item_desc.setAttribute("Name", "comm_item_desc" + comclick);
		comm_item_desc.setAttribute("id","comm_item_desc" + comclick);
		comm_item_desc.setAttribute('style','margin-bottom: 15px');
		comm_item_desc.setAttribute('placeholder','Commercial Item Description');
		document.getElementById("comm_item_desc").appendChild(comm_item_desc).classList.add("form-control");

		var comm_item_code = document.createElement("INPUT");
		comm_item_code.setAttribute("type", "text");
		comm_item_code.setAttribute("Name", "comm_item_code" + comclick);
		comm_item_code.setAttribute("id","comm_item_code" + comclick);
		comm_item_code.setAttribute('style','margin-bottom: 15px');
		comm_item_code.setAttribute('placeholder','Commercial item Code');
		document.getElementById("comm_item_code").appendChild(comm_item_code).classList.add("form-control");

		var comm_item_quantity = document.createElement("INPUT");
		comm_item_quantity.setAttribute("type", "text");
		comm_item_quantity.setAttribute("Name", "comm_item_quantity" + comclick);
		comm_item_quantity.setAttribute("id","comm_item_quantity" + comclick);
		comm_item_quantity.setAttribute('style','margin-bottom: 15px');
		comm_item_quantity.setAttribute('placeholder','Commercial item Quantity');
		document.getElementById("comm_item_quantity").appendChild(comm_item_quantity).classList.add("form-control");

		var comm_item_units = document.createElement("INPUT");
		comm_item_units.setAttribute("type", "text");
		comm_item_units.setAttribute("Name", "comm_item_units" + comclick);
		comm_item_units.setAttribute("id","comm_item_units" + comclick);
		comm_item_units.setAttribute('style','margin-bottom: 15px');
		comm_item_units.setAttribute('placeholder','Commercial item Units');
		document.getElementById("comm_item_units").appendChild(comm_item_units).classList.add("form-control");

		var comm_item_rate = document.createElement("INPUT");
		comm_item_rate.setAttribute("type", "text");
		comm_item_rate.setAttribute("Name", "comm_item_rate" + comclick);
		comm_item_rate.setAttribute("id","comm_item_rate" + comclick);
		comm_item_rate.setAttribute('style','margin-bottom: 15px');
		comm_item_rate.setAttribute('placeholder','Commercial item Rate');
		document.getElementById("comm_item_rate").appendChild(comm_item_rate).classList.add("form-control");

		document.getElementById("comm_count_form").value = comclick;
	}


</script>

<div class="content-wrapper">

	<div class="inner-page-img">
		<div class="container">
			<h3 class="page-title">Create RFQ</h3>
		</div>
	</div>


	<div class="box-container">
		<div class="row">
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Add RFQ Details</h3>
						</div>
						<form class="event-add-items" id="event-add-items" enctype="multipart/form-data" method="post" action='/rfq'>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>Event Name</label>
								<input type="text" class="form-control" id="event_name" name="event_name" placeholder="Event Name">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>Description</label>
								<input type="text" class="form-control" name="description" id ="description" placeholder="Description">
							</div>							

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>RFQ Start date & time</label>
								<input type="text" class="form-control datetimes1" name="start_date_time">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>RFQ End date & time</label>
								<input type="text" class="form-control datetimes2" name="end_date_time">
							</div>

							<div class="col-md-3 col-xs-12 col-sm-3 form-group">
								<label>Location</label>
								<select class="form-control" name="location">
									<option value="0">Location</option>
								</select>
							</div>


							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>EMD</label>
								<input type="text" class="form-control" name="emd" id="emd" placeholder="EMD">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>RFQ Fees</label>
								<input type="text" class="form-control" name="rfq_fees" id="rfq_fees" placeholder="RFQ Fees">
							</div>

							<div class="col-xs-3 col-md-3 col-sm-3 form-group">
								<label>Catalogue</label>
								<input type="file" class="form-control" name="catalogue" id ="catalogue" placeholder="Catalogue">
							</div>

							<div class="event-full-details movie_item_toolbar">
								<h3>Technical Document</h3>
							</div>
							
							<div class="col-xs-4 col-md-4 col-sm-4 form-group" id="criteria1">
							</div>
							<div class="col-xs-4 col-md-4 col-sm-4 form-group" id="estimated1">
							</div>
							<div class="col-xs-4 col-md-4 col-sm-4 form-group" id="rate1">
							</div>

							<div class="col-xs-4 col-md-4 col-sm-4 form-group" id="criteria_form0">
							</div>
							<div class="col-xs-4 col-md-4 col-sm-4 form-group" id="estimated_form0">
							</div>
							<div class="col-xs-4 col-md-4 col-sm-4 form-group" id="rate_form0">
							</div>

							<div class="col-xs-4 col-md-4 col-sm-4 form-group">
								<input type="hidden" class="form-control" name="count_form" id="count_form">
							</div>

							<div class="col-xs-4 col-md-4 col-sm-4 form-group">
								<input type="hidden" class="form-control" name="count_xls" id="count_xls">
							</div>

							<div class="col-xs-2 col-md-2 col-sm-2 form-group">
								<input type="file" class="form-control" name="tech_document" id ="tech_document" style=" display: inline-block;">
							</div>

							<div class="col-xs-2 col-md-2 col-sm-2 form-group">
								<input type="button" class="btn btn-purple sub_btn valid" onclick="textBoxCreate()" value="+">
							</div>

							<div class="event-full-details movie_item_toolbar">
								<h3>Commercial Document</h3>
							</div>

							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_num1">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_desc1">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_code1">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_quantity1">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_units1">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_rate1">
							</div>

							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_num">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_desc">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_code">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_quantity">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_units">
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 form-group" id="comm_item_rate">
							</div>

							<div class="col-xs-4 col-md-4 col-sm-4 form-group">
								<input type="hidden" class="form-control" name="comm_count_form" id="comm_count_form">
							</div>

							<div class="col-xs-4 col-md-4 col-sm-4 form-group">
								<input type="hidden" class="form-control" name="comm_count_xls" id="comm_count_xls">
							</div>

							<div class="col-xs-2 col-md-2 col-sm-2 form-group">
								<label>Commercial Document</label>
								<input type="file" class="form-control" name="commercial_document" id="commercial_document" placeholder="Commercial Document">
							</div>

							<div align="center" class="col-xs-12 col-md-12 col-sm-12 form-group">
								<input type="button" class="btn btn-purple sub_btn valid" onclick="CommBoxCreate()" value="+">
							</div>

							<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
								<input type="submit" class="btn btn-purple sub_btn" id="signupform">
							</div>
						</form>

						<script>
						    document.getElementById('tech_document').addEventListener('change', ExcelBoxCreate, false);
						    document.getElementById('commercial_document').addEventListener('change', CommExcelBoxCreate, false);
						</script>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
