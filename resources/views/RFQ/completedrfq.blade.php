<style>
.auction-collapsed-info
{
  width: 15% !important;
}
</style>

<script type="text/javascript">
	function passdata(id){
		document.getElementById("pass_id").value =id;
	}
</script>

@extends('event_manager/event-headernew')

@section('content')
<div class="content-wrapper">
	<div class="inner-page-img">
		<div class="container">
			<h3 class="page-title">My RFQ</h3>
		</div>
	</div>

	

	<div class="box-container">
		<div class="row">
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
				<div class="auction-report-title">
					<div class="auction-collapsed-info">
						<span>RFQ Name</span>
					</div>
					<div class="auction-collapsed-info">
						<span>RFQ Desc</span>
					</div>
					<div class="auction-collapsed-info">
						<span>Event start Date</span>
					</div>
					<div class="auction-collapsed-info">
					    <span>Event end Date</span>
					</div>
					<div class="auction-collapsed-info">
					    <span>EMD</span>
					</div>
					<div class="auction-collapsed-info">
					    <span>RFQ Fees</span>
					</div>
				</div>

				@foreach ($get_data as $item)
					<div class="record">
						<div class="auction-collapsed-info">
							<span>{{$item['event_name']}}</span>
						</div>
						<div class="auction-collapsed-info">
							<span>{{$item['description']}}</span>
						</div>
						<div class="auction-collapsed-info">
							<span>{{$item['start_date_time']}}</span>
						</div>
						<div class="auction-collapsed-info">
						    <span>{{$item['end_date_time']}}</span>
						</div>
						<div class="auction-collapsed-info">
						    <span>{{$item['emd']}}</span>
						</div>
						<div class="auction-collapsed-info">
						    <span>{{$item['rfq_fees']}}</span>
						    <span><a href="detailrfq/{{encrypt($item['id'])}}" >RFQ view</a></span>
						</div>
					</div>
				@endforeach

			</div>
		</div>
	</div>
</div>

	
@endsection