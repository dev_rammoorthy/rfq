@extends('event_manager/event-headernew')

@section('assets')
<link rel="stylesheet" type="text/css" href="{{asset('css/plugins/data-table/dataTables.bootstrap.min.css')}}?{{ time() }}">
<link rel="stylesheet" type="text/css" href="{{asset('css/page/tables.css')}}?{{ time() }}">
@endsection

@section('content')

<div class="container">
  <br>
  <h2>Techincal Rating</h2>         
  <table class="table">
    <thead>
      <tr>
        <th>Technical Quality Criteria</th>
        <th>Estimated Rate</th>
        <th>Your Rate</th>
      </tr>
    </thead>
    <tbody>
    	@foreach($upload_tech as $techs)
      <tr>
        <td>{{$techs->technical_quality_criteria}}</td>
        <td>{{$techs['estimated_rate']}}</td>
        <td>{{$techs['rate']}}</td>
        <td></td>
      </tr>
      @endforeach
    </tbody>
  </table>

  @foreach ($get_tech as $bidd)
  <table class="table">
    <thead>
      <tr>
        <th>Seller: {{$bidd['username']}},&nbsp &nbsp &nbspInput</th>
        <th>Remarks</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      @foreach($bidd['tech'] as $set)
      <tr>
        <td>{{$set->input}}</td>
        <td>{{$set->remarks}}</td>
        <td>{{$set->rating}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

   @endforeach
</div>


<div class="container">
  <br>
  <h2>Commercial Compare</h2>         
  <table class="table">
    <thead>
      <tr>
        <th>Item no</th>
        <th>Item description</th>
        <th>Quantity</th>
        <th>Estimate Rate</th>
      </tr>
    </thead>
    <tbody>
      @foreach($upload_comm as $comm)
      <tr>
        <td>{{$comm->item_no}}</td>
        <td>{{$comm->item_description}}</td>
        <td>{{$comm->quantity}}</td>
        <td>{{$comm->estimate_rate}}</td>
        <td></td>
      </tr>
      @endforeach
    </tbody>
  </table>

  @foreach ($get_comm as $bidd_comm)
  <table class="table">
    <thead>
      <tr>
        <th>Seller: {{$bidd_comm['username']}},&nbsp &nbsp &nbspInput</th>
        <th>Remarks</th>
      </tr>
    </thead>
    <tbody>
      @foreach($bidd_comm['comm'] as $set_bidd)
      <tr>
        <td>{{$set_bidd->input}}</td>
        <td>{{$set_bidd->remarks}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

   @endforeach
</div>


<div class="container">
  <br>
  <h2>Final Approved</h2>
  <form action="/rfqaccept" class="invite-vendorform" method="post">
  @foreach ($final_acc as $set)
  <table class="table">
    <thead>
      <tr>
        <th>Seller</th>
        <th>Rating</th>
        <th>Input</th>
        <th>Total Percent</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><b>{{$set['username']}}</b></td>
        <td><b>{{$set['rating']}} / {{$set['tech_rat']}}</b></td>
        <td><b>{{$set['final']}} / {{$set['comm_rat']}}</b></td>
        <td><b>
          @if($recom == $set['tech_percent'])
            {{$set['tech_percent']}} *
          @else
            {{$set['tech_percent']}}
          @endif
        </b></td>
        <td><input type="radio" name="accept" value="{{$set['id']}}"> Approved</td>
      </tr>
    </tbody>
  </table>
   @endforeach
   <div align="right">
    <input type="submit" class="btn btn-purple sub_btn" id="signupform">
   </div>
   </form>
</div>


       
       
@endsection