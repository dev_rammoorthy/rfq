@extends('event_manager/event-headernew')

@section('content')

<style type="text/css">
	.sub{
		text-transform: capitalize;
	}

	.movie_item_toolbar {
	    clear: both;
	    vertical-align: bottom;
	    width: 100%;
	    height: 66px;
	}

	.sub_btn {
	    padding: 8px 27px;
	    font-size: 16px;
	    font-weight: bold;
	}
</style>

<div class="content-wrapper">

	<div class="inner-page-img">
	<div class="container">
	<h3 class="page-title">Logistic Details</h3>
	</div>
	</div>


	<div class="box-container">
		<div class="row">
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>RFQ Details</h3>
							<p class="sub">Read all information here</p>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                <h5 style="padding:7px"> RFQ Name </h5>
                            </div>

                            <div class="col-md-8 col-xs-8 col-sm-8 form-group">
								{{$fetch['event_name']}}
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row" >
                            <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                <h5 style="padding:7px"> RFQ Description </h5>
                            </div>

                            <div class="col-md-8 col-xs-8 col-sm-8 form-group">
								{{$fetch['description']}}
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                <h5 style="padding:7px"> RFQ Catalogue</h5>
                            </div>

                            <div class="col-md-8 col-xs-8 col-sm-8 form-group">
								{{$fetch['catalogue_path']}}
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                <h5 style="padding:7px"> RFQ Start Date Time</h5>
                            </div>

                            <div class="col-md-8 col-xs-8 col-sm-8 form-group">
								{{$fetch['start_date_time']}}
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                <h5 style="padding:7px"> RFQ End Date Time</h5>
                            </div>

                            <div class="col-md-8 col-xs-8 col-sm-8 form-group">
								{{$fetch['end_date_time']}}
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                <h5 style="padding:7px"> RFQ Location</h5>
                            </div>

                            <div class="col-md-8 col-xs-8 col-sm-8 form-group">
								{{$fetch['location']}}
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                <h5 style="padding:7px"> Pay EMD</h5>
                            </div>

                            <div class="col-md-8 col-xs-8 col-sm-8 form-group">
								{{$fetch['emd']}}
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                <h5 style="padding:7px"> Pay RFQ Fees</h5>
                            </div>

                            <div class="col-md-8 col-xs-8 col-sm-8 form-group">
								{{$fetch['rfq_fees']}}
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                <h5 style="padding:7px"> Payment Amount </h5>
                            </div>

                            <div class="col-md-2 col-xs-2 col-sm-2 form-group">
								<b>{{$fetch['emd']}} + {{$fetch['rfq_fees']}}</b>
							</div>

							<div class="col-md-2 col-xs-2 col-sm-2 form-group">
								{{$fetch['emd'] + $fetch['rfq_fees']}}
							</div>
							<div class="col-md-2 col-xs-2 col-sm-2 form-group">
								<form method="post" action="{{action('RFQ\LiveRFQController@paidlive')}}">
									{{ csrf_field() }}
								<input type="hidden" value="{{$fetch['emd'] + $fetch['rfq_fees']}}" name="get_amount">
								<input type="hidden" value="{{$get_invite['id']}}" name="id">
								@if($fetch['emd'] + $fetch['rfq_fees'] !=0)
									@if($get_invite['amount_paid'] =="true")
										<input type="submit" value="Pay Now" class="btn btn-purple sub_btn" disabled>
									@else
										<input type="submit" value="Pay Now" class="btn btn-purple sub_btn">
									@endif
								@else
									<input type="submit" value="Pay Now" class="btn btn-purple sub_btn">
								@endif
								</form>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="event-full-details">
							<h3>RFQ Documents</h3>
							<p class="sub">Read sample document and upload</p>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                            <form class="event-add-items" id="event-add-items" enctype="multipart/form-data" method="post" action='/uploaddocument'>
                            	<div class="col-xs-4 col-md-4 col-sm-4 form-group">
									<label>Upload support Document</label>
									<input type="file" class="form-control" id="support_document" name="support_document">
								</div>
								<div class="col-xs-4 col-md-4 col-sm-4 form-group">
									<label>Upload Technical Document</label>
									<a href="/downloadtech/{{$get_invite['id']}}" target="_blank" style="font-weight: bold;color: rebeccapurple;">Sample Doc</a>
									<input type="file" class="form-control" id="technical_document" name="technical_document">
								</div>
								<div class="col-xs-4 col-md-4 col-sm-4 form-group">
									<label>Upload Commercial Document</label>
									<a href="/downloadcomm/{{$get_invite['id']}}" target="_blank" style="font-weight: bold;color: rebeccapurple;">Sample Doc</a>
									<input type="file" class="form-control" id="commercial_document" name="commercial_document">
								</div>
								<input type="hidden" value="{{$get_invite['id']}}" name="id">

								@if($fetch['emd'] + $fetch['rfq_fees'] !=0)
									@if($get_invite['amount_paid'] =="false")
										<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
											<input type="submit" class="btn btn-purple sub_btn" id="signupform" disabled>
										</div>
									@else
										<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
											<input type="submit" class="btn btn-purple sub_btn" id="signupform">
										</div>
									@endif
								@else
									<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
											<input type="submit" class="btn btn-purple sub_btn" id="signupform">
										</div>
								@endif							
								
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
