<table width="100%" bgcolor="#f8f8f8" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;border-spacing:0">
  <tbody><tr>
    <td><table bgcolor="#ffffff" width="682" cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse;border-spacing:0">
        <tbody><tr>
          <td style="padding:0px;margin:0px"><table bgcolor="#ffffff" width="618" cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse;border-spacing:0">
              <tbody><tr>
                <td><table bgcolor="#ffffff" width="640" cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse;border-spacing:0">
                    <tbody><tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left" height="15">&nbsp;</td>
                    </tr>                    
                    <tr>
                      <td style="margin:0px;padding:0px"> 
                        
                        
                        <table width="320" align="left" border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            
                            <tr>
                              
                            <td style="vertical-align:middle;text-align:left"><a href="http://www.purchasequick.in/" rel="noreferrer" target="_blank"><img src="http://www.purchasequick.in/img/logo.png" alt="logo"></a></td>
                            </tr>
                            
                            
                          </tbody>
                        </table>
                        <table width="300" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0">
                          <tbody>
                            <tr>
                              <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left" height="10">&nbsp;</td>
                            </tr>
                            <tr>
                              <td style="margin:0;vertical-align:middle;color:#424242;font-size:14px;line-height:21px;font-family:Arial,Helvetica,sans-serif;font-style:normal;font-weight:normal;text-align:right"><img style="vertical-align:top" src="http://www.purchasequick.in/img/calender.png" alt="calender">{!!$date_format!!}</td>
                            </tr>
                          </tbody>
                        </table>
                        
                        </td>
                    </tr>
                    
                    
                    <tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left" height="15">&nbsp;</td>
                    </tr>
                    <tr>
                      <td style="padding:0px;margin:0px;background-color:#f1f1f1;border:1px solid #ebebeb"><table style="border-collapse:collapse;border-spacing:0" width="100%" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                            <tr>
                              <td width="15">&nbsp;</td>
                              <td><table style="border-collapse:collapse;border-spacing:0" width="100%" border="0">
                                  <tbody>
                                    <tr>
                                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left;margin:0px" height="15">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td style="margin:0;color:#000000;font-size:27px;line-height:31px;font-family:Arial,Helvetica,sans-serif;font-style:normal;font-weight:bold;text-align:center"><a style="text-decoration:none;color:#ff9800;text-transform: capitalize;" href="" rel="noreferrer" target="_blank" >{!! $event_name !!}</a></td>
                                    </tr>
                                    <tr>
                                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left;margin:0px" height="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td style="margin:0px;line-height:24px;text-align:center"><font color="#000000" face="Arial, Helvetica, sans-serif"><span style="font-size:18px">{!! $event_cat_name !!}</span></font><br></td>
                                    </tr>
                                    <tr>
                                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left;margin:0px" height="12">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                              <td width="15">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                      <tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left;margin:0px" height="25">&nbsp;</td>
                    </tr>
                    <tr>
                      <td style="line-height:24px;font-size:15px;vertical-align:top;padding:0px;text-align:left;font-weight:bold;color:#231f20;font-family:Arial,Helvetica,sans-serif">Dear {!! $seller_name !!}</td>
                    </tr>
                    <tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left;margin:0px" height="15">&nbsp;</td>
                    </tr>
                    <tr>
                      <td cstyle="line-height:24px;vertical-align:top;padding:0px;text-align:left"><font color="#231f20" face="Arial, Helvetica, sans-serif"><span style="font-size:15px">You have won in the event which has been created.</span></font><br></td></tr>
					  <tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left" height="15">&nbsp;</td>
                    </tr>
                    
                   <tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left;margin:0px" height="15">&nbsp;</td>
                    </tr>
                    
					<tr>
					</tr><tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left;margin:0px" height="8">&nbsp;</td>
                    </tr>
                      
                    
					<tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left" height="15">&nbsp;</td>
                    </tr>
					
                     <tr>
                      <td style="margin:0;padding:0;font-size:20px;text-align:center;color:#231f20;line-height:22px;font-family:Arial,Helvetica,sans-serif;font-weight:bold">Event Won</td>
                    </tr>
                    <tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left" height="15">&nbsp;</td>
                    </tr>
                    <tr>
                      <td style="line-height:0;font-size:0;background-color:#f1f1f1;vertical-align:top;padding:0px;text-align:left" height="15">&nbsp;</td>
                    </tr><tr>
                      <td style="margin:0;padding:0;font-size:14px;color:#231f20;line-height:16px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;text-align:center;background-color:#f1f1f1">your Event Prizes Worth Rs 3 Lakhs
					  </td>
                    </tr><tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left;background-color:#f1f1f1" height="10">&nbsp;</td>
                    </tr>
                 <tr><td style="padding:0px;text-align:center;background-color:#f1f1f1"><img src="http://www.purchasequick.in/img/prize.png" alt="prize" ></td></tr>   
                   <tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left;background-color:#f1f1f1" height="10">&nbsp;</td>
                    </tr>
                    <tr>
                      <td style="margin:0;padding:0;font-size:14px;color:#231f20;line-height:16px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;text-align:center;background-color:#f1f1f1">
					  <div style="text-align:center;font-size:20px;margin-top:5px;line-height:20px">{!! $seller_company !!}					
					  </div>
					  </td>
                    </tr>
                    <tr>
                      <td style="line-height:0;font-size:0;background-color:#f1f1f1;vertical-align:top;padding:0px;text-align:left" height="5">&nbsp;</td>
                    </tr><tr>
                      <td style="margin:0;padding:0;font-size:20px;color:#231f20;line-height:16px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;text-align:center;background-color:#f1f1f1">Rs.{!! $seller_bidamount !!}	
					  </td>
                    </tr>
                    <tr>
                      <td style="line-height:0;font-size:0;background-color:#f1f1f1;vertical-align:top;padding:0px;text-align:left" height="25">&nbsp;</td>
                    </tr>
                    
                     
                    <tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left" height="15">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center"><table style="border-collapse:collapse;border-spacing:0" width="200">
                          <tbody>
                            <tr>
                              <td style="background-color:#ff9800;text-align:center;color:#ffffff;border-radius:25px;font-size:12px;text-decoration:none;font-weight:bold"><a href="http://www.purchasequick.in/" style="margin:0;padding:0px 10px 0px 10px;display:block;color:#ffffff;font-size:17px;line-height:21px;font-family:Arial,Helvetica,sans-serif;text-align:center;font-weight:bold;text-align:center;text-decoration:none;border:10px solid #ff9800;border-radius:25px" rel="noreferrer" target="_blank" >Click here</a></td>
                            </tr>
                          </tbody>
                        </table></td>
						
						<tr>
                      <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left" height="15">&nbsp;</td>
                    </tr>
                    </tr>
                  </tbody></table></td>
              </tr>
            </tbody></table></td>
        </tr>
		
		
        

        
        <tr>
          <td bgcolor="#f4f4f4" colspan="3"><table width="100%" border="0" style="border-collapse:collapse;border-spacing:0">
              <tbody><tr>
                <td style="line-height:0;font-size:0" width="20">&nbsp;</td>
                <td width="640"><table width="100%" border="0" style="border-collapse:collapse;border-spacing:0">
                    <tbody><tr>
                      <td><table width="100%" border="0" style="border-collapse:collapse;border-spacing:0">
                          <tbody><tr>
                            <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:center" height="20">&nbsp;</td>
                          </tr>
                          <tr>
                            <td style="margin:0;color:#626061;font-size:11px;line-height:20px;font-family:Arial,Helvetica,sans-serif;font-style:normal;font-weight:normal;text-align:center">2018 Purchase quick |  Terms And Conditions apply<br> </td>
                          </tr>
                          <tr>
                            <td style="line-height:0;font-size:0;vertical-align:top;padding:0px;text-align:left" height="20">&nbsp;</td>
                          </tr>
                        </tbody></table></td>
                    </tr>
                  </tbody></table></td>
                <td style="line-height:0;font-size:0" width="20">&nbsp;</td>
              </tr>
            </tbody></table></td>
        </tr>
        
      </tbody></table></td>
  </tr>
</tbody></table>