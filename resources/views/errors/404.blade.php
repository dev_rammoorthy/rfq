<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="description" content="">
		<!--favicon icon-->
	<link rel="icon" type="image/png" href="{{asset ('img/purch-icon.png') }}">

	<title>Purchasequick Page Not Found</title>

		 <!--web fonts-->
		<link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
		<link href="{{asset ('css/bootstrap.min.css') }}" rel="stylesheet">
		<!--icon font-->
		<link href="{{asset ('css/font-awesome.min.css') }}" rel="stylesheet">
		<link href="{{asset ('css/custom.css') }}" rel="stylesheet">
		<link href="{{asset ('css/main.css') }}" rel="stylesheet">
    <!-- kuwy library CSS -->
   
    <title>404 error page</title> 
     <!-- kuwy library CSS -->
</head>
<body class="fixed-nav top-nav">
 <!--main content wrapper-->
  <div class="content-wrapper">
    <div class="text-center"> 
     <div class="container">
       <a class="text-center" href="#">
       <img class="pr-3"  src="{{asset('img/logo.png')}}" srcset="img/logo.png"  alt=""/>
      </a>
     </div>
  </div>

<div class="box-container">
   <div class="row">              
     <div class="col-xl-12 col-sm-12 col-md-12 col-lg-12">
       <div class="mb-4">
         <div class="card-body">
            <div class="event-full-details text-center">
              <img class="pr-3 img-fluid" src="img/matexnet-44.png" srcset="img/matexnet-44.png"  alt=""/>
               <p>Sorry, an error has occured, Requested page not found!</p>
                <p> <a href="{{ URL::to('/') }}" class="btn btn-primary">Back</a> </p>
            </div>

         </div>
        </div>
     </div>
   </div>
  </div>
   <!--/main content wrapper-->
</div>
</body>
</html>