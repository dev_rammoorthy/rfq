<?php 

 use \App\Http\Controllers\PurchaseController;
 use \App\Http\Controllers\EventController;

$session = session()->all(); 
$session_val = PurchaseController::CheckSession($session);
$loginid=$session['pli_loginId'];

//$event_data=EventController::buyerCompletedEvent1($loginid);

$page="Reports";
$pagetrack=PurchaseController::pageLogout($page);

?>

<style>

.auction-collapsed-info
{
    width: 15% !important;
}

/* .auction-report-title
{
    width: 93% !important;
} */

.event-detail-form
{
    width:auto !important;
    display: block !important;
}

/* #accordion
{
    width: 93% !important;
} */


.lot
{
	display:none;
}
.paging_simple_numbers
{
	display:none;
}
.dt-buttons
{
    margin-left: 90%
}

.auction-collapsed-info
{
	width: 15%!important;
}
#Excel
{
	width: 10%!important;
}

.dt-buttons .dt-button
{
    margin-right: 36px !important;
}

#mis_download_wrapper{
    width: 97% !important;
}

#report1-submit
{
    margin-top: 29px;
    position: absolute;
    margin-left: 84%;
    width: 50px;
    height: 34px;
}

</style>


@extends('event_manager/event-header')
@section('assets')
<link rel="stylesheet" type="text/css" href="{{asset('css/plugins/data-table/dataTables.bootstrap.min.css')}}?{{ time() }}">
<link rel="stylesheet" type="text/css" href="{{asset('css/page/tables.css')}}?{{ time() }}">
@endsection
@section('content')

<!--main content wrapper-->
<div class="content-wrapper">
        <!--creative states-->
            <div class="inner-page-img">
            <div class="container">
            <h3 class="page-title">Closed Bid Report</h3>
            </div>
            </div>    
        
        <div class="box-container">
        <div class="row">            
        
        <div class="box-container">
		<div class="row">              
			<div class="col-xl-12 col-sm-12 col-md-12 col-lg-12 profile-info-view">
				<div class="card card-shadow mb-4">
					<div class="card-body">
						<div class="event-full-details">
							<h3>Reports</h3>
                            <div class="text-right">                               
                                <h6> * Search by either Event name or Event dates</h6>
                            </div>
						</div>
						
						<form action="{{ action('ReportController@closereport') }}" method="POST"  class="event-detail-form" name="report" id="report-form">
							<br>
                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">
                                <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                    <h5 style="padding:7px"> Event Name </h5>
                                </div>

                                <div class="col-md-8 col-xs-8 col-sm-8 form-group">
                                    <input type="text" class="form-control" id="pec_event_name" name="pec_event_name" placeholder="Enter Event Name">
                                </div>
                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 form-group text-center">  
                                <h5> OR </h5>
                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 form-group row">  
                                <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                    <h5 style="padding:7px"> Event Start and End Date </h5> 
                                </div>

                                <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                    <input type="text" class="form-control reportdate" name="pec_event_start_dt" id="pec_event_start_dt" placeholder="Pick Event Start Date &amp; Time">
                                </div>

                                <div class="col-md-4 col-xs-4 col-sm-4 form-group">
                                    <input type="text" class="form-control reportdate" name="pec_event_end_dt" id="pec_event_end_dt" placeholder="Pick Event End Date &amp; Time">
                                </div>	
                            </div>	

                            <!-- <div class="col-md-5 col-xs-12 col-sm-5 form-group">
								<input type="text" class="form-control" id="pec_event_name" name="pec_event_name" placeholder="Event Name">
                            </div>
                            
                            <div class="col-md-1 col-xs-1 col-sm-1 form-group">
                            |
                            </div>
							
							<div class="col-md-3 col-xs-12 col-sm-3 form-group date">
								<input type="text" class="form-control reportdate" name="pec_event_start_dt" id="pec_event_start_dt" placeholder="Event Start Date &amp; Time">
                            </div>

							<div class="col-md-3 col-xs-12 col-sm-3 form-group date">
								<input type="text" class="form-control reportdate" name="pec_event_end_dt" id="pec_event_end_dt" placeholder="Event Start Date &amp; Time">
							</div>						 -->

                            <input type="hidden" id="loginid" name="loginid" value="{{$session['pli_loginId']}}">
						
							<div class="col-md-12 col-xs-12 col-sm-12 block-btn-submit">
								<input type="submit" class="btn btn-purple" id="report-submit" name="SUBMIT">
							</div>    
						</form>
						
					</div>
				</div>
			</div>
		</div>

        
            <form action="{{ action('EventController@reportsPDF') }}" method="POST"  class="event-detail-form" name="report" id="report-form"> 
                @if (isset($event_data) && $event_data != null)
                    @foreach ($event_data as $item)
                        <input type="hidden" id="hfArray" name="hfArrayEventId[]" value="<?= $item->pec_event_id ?>" />
                        <input type="hidden" id="hfArray" name="hfArrayEventName[]" value="<?= $item->pec_event_name ?>" />
                        <input type="hidden" id="hfArray" name="hfArrayStartDate[]" value="<?= $item->pec_event_start_dt ?>" />
                        <input type="hidden" id="hfArray" name="hfArrayEndDate[]" value="<?= $item->pec_event_end_dt ?>" />
                        <input type="hidden" id="hfArray" name="hfArrayEventType[]" value="<?php echo (($item->pec_event_type == 1) ? 'Closed Bid' : (($item->pec_event_type == 2) ? 'Reverse Bid' : 'Both Closed and Reverse Bid')); ?>" />
                        <input type="hidden" id="hfArray" name="hfArrayNoOfItems[]" value="<?= $item->no_item ?>" />
                    @endforeach
                    <input type="submit" style="float:right" value="PDF" class="btn btn-purple" id="report1-submit" name="SUBMIT">
                @endif              
            </form> 
       
            @if (isset($event_data) && $event_data != null)
                <table id="mis_download" class="table table-striped table-bordered lot" cellspacing="0" >
                    <thead>
                    <tr>
                            <th>  Event ID</th>
                            <th>  Event Name</th>
                            <th>  Event Start Date </th>
                            <th>  Event End Date </th>
                            <th>  Event Type </th>
                            <th> No.of Items </th>

                    </tr>
                    
                    </thead>
                    <tbody>
                        @foreach ($event_data as $item)
                        <tr>
                                <td> {{$item->pec_event_id}}</td>
                                <td>{{$item->pec_event_name}}</td>                            
                                <td>{{$item->pec_event_start_dt}}</td>                            
                                <td>{{$item->pec_event_end_dt}}</td>        
                                <td> <?php echo (($item->pec_event_type == 1) ? 'Closed Bid' : (($item->pec_event_type == 2) ? 'Reverse Bid' : 'Both Closed and Reverse Bid'));  ?></td>                     
                                <td>{{$item->no_item}}</td>    									
                        </tr>
                
                    @endforeach
                    </tbody>            
                </table>        
            @endif
        
    </div>

         @if (isset($event_data) && $event_data != null)
             
        
        <div class="col-xl-12 col-sm-12 col-md-12 col-lg-12  profile-info-view">
        <div class="auction-report-title">
        <div class="auction-collapsed-info">
        <span>Event ID</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event Name</span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event start Date </span>
        </div>
        <div class="auction-collapsed-info">
        <span>Event end Date </span>
        </div>
        <div class="auction-collapsed-info">
        <span>No.of Items</span>
        </div> 
        <div class="auction-collapsed-info">
        <span>Item Data</span>
        </div> 
        
        </div>
		
		
		   @foreach ($event_data as $item)

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php $eventItem = EventController::getAddedItems2($item->pec_event_id); ?>
                    <?php if(@count($eventItem) > 0) : ?>
                
                        <div class="panel panel-default">
                            <div class="panel-heading auction-accordion" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                        
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$item->pec_event_id}}" aria-expanded="false" aria-controls="collapseOne">
                                            <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_id}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                    <p>{{$item->pec_event_name}}</p>
                                                    </div>
                                                                                                    
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_start_dt}}</p>
                                                    </div>
                                                    
                                                    <div class="auction-collapsed-info">
                                                            <p>{{$item->pec_event_end_dt}}</p>
                                                    </div>
                                                    <div class="auction-collapsed-info">
                                                        <p>{{$item->no_item}}</p>
                                                    </div>
                                                    
                                                   
                                    </a>
                                    <div class="auction-collapsed-info">
                                        <!-- <form style=" position: absolute;margin-left: 82%; top: 14%;" action="{{ action('EventController@eventreport') }}" method="POST"  class="event-detail-form" name="report">
                
                                            <input type="hidden" name="hfEventIt" value="{{$item->pec_event_id}}" />
                                            <input type="submit" value="Excel" class="btn btn-purple" id="submit-event" name="SUBMIT">
                                            <input type="submit" value="PDF" class="btn btn-purple" id="submit-event" name="SUBMIT">
                
                                        </form> -->

                                        <a href="#" style="position: absolute;margin-left: 82%; top: 0;" data-id="{{$item->pec_event_id}}" id="bidwise-pop-export" onClick="bidwiseOverallPopFunction('{{$item->pec_event_id}}', '{{$item->pec_event_type}}')"> Export </a>

                                    </div>
                                    
                                </h4>
                            </div>
                            
                            
                            <div id="{{$item->pec_event_id}}"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="auction-inviter-fullinfo">
                                    <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Category</th>
                                            <th>Product Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Spec</th>
                                            <th>Units</th>
                                            <th>Qty</th>
                                            <th>Location</th>
                                            @if ($item->pec_event_type==2)
                                            <th>Start Price</th>
                                            <th>Min Dec.</th>
                                            @endif
                                            <th>No.of Bidder</th>
                                            <th>Bid Data</th>
                                            </tr>
                                        </thead>
                                        <tbody>                
                    

                                                    @foreach ($eventItem as $add_item)
                                            <tr class="clickable" data-toggle="collapse" data-target="#group-of-rows-{{$add_item->pea_id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                                                    <?php                                            
                                                                
                                                    $email_count= json_decode($add_item->pei_invite_details);
                                                    $invite_count=count( $email_count);
                                        
                                                        ?>  
                                                
                                                    <td>{{$add_item->pmc_cat_name}}</td>
                                                    <td>{{$add_item->pms_subcat_name}}</td>
                                                    <td>{{$add_item->pea_event_start_dt}}</td>
                                                    <td>{{$add_item->pea_event_end_dt}}</td>
                                                    <td>{{$add_item->pea_event_spec}}</td>
                                                    <td>{{$add_item->pmu_unit_name}}</td>
                                                    <td>{{$add_item->pea_event_unit_quantity}}</td>
                                                    <td>{{$add_item->pea_event_location}}</td>
                                                    @if ($item->pec_event_type==2)
                                                    <td>{{$add_item->pea_event_start_price}}</td>
                                                    <td>{{$add_item->pea_event_max_dec}}</td>
                                                    @endif
                                                <td>
                                                        <?php $sellerbid = EventController::sellerBidDetails($add_item->pea_id);
                                                                    
                                                        ?>
                                                <span class="lot-mail-req">No.of Acceptance : {{$add_item->pei_invite_accept_count}}</span>
                                                <a href="javascript:" id="hover-mail-list"><span class="mail-invite-list">{{count($sellerbid)}}</span></a>
                                                </td>

                                                <td>
                                                    <!-- <form action="{{ action('EventController@lotreport') }}" method="POST"  class="event-detail-form" name="report"> -->
                                                        <div class="auction-collapsed-info">
                                                            <input type="hidden" name="hfLotId" value="{{$add_item->pea_id}}" />
                                                            <a data-id="{{$add_item->pea_id}}" class="btn btn-purple" id="bidwise-pop-export" onClick="bidwisePopFunction('<?php echo $add_item->pea_id ?>','<?php echo $item->pec_event_id ?>')"> Export </a>
                                                            <!-- <input type="submit" value="Excel" class="btn btn-purple" id="submit-event" name="SUBMIT">
                                                            <input type="submit" value="PDF" class="btn btn-purple" id="submit-event" name="SUBMIT"> -->
                                                        </div>
                                                    <!-- </form> -->
                                                </td>
                                            </tr>
                                        
                                        </tbody>                               
                                    
                                        @endforeach    

                                    </table>
        </div>
        
        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
					 @endforeach
        
        </div>
        @else
            <!--inner-page-img End-->
            <?php if(isset($event_data)) : ?> 
			<div class="box-container">
			  <div class="row">
				  <div class="col-xl-12 text-center">
					 <img class="pr-3 img-fluid" src="{{asset('img/no-record.png')}}" alt="Purchase"/>
					 <!--<p> <a href="" class="btn btn-primary">Back</a> </p>-->
				  </div>
			   </div>
			 <!--row End-->
			 
             </div>
            <?php endif; ?>
			 <!--box-container End-->
        @endif
        </div>
        </div>
            <!--/main content wrapper-->
        </div>

        <div class="modal fade headShake" id="Bid-Export-Report" role="dialog">
            <div class="modal-dialog vendor-invite-popup">
            
            <!-- Modal content-->
            <div class="modal-content modal-content-amount invite-popup-content">
                <div class="modal-header modal-header-amount amount_bg" style="height:auto">
                    <h4 class="modal-title-amount">Select the Columns to export</h4>
                </div>
                <div class="modal-body modal-body-bg" style="height:auto">
                    <form action="{{ action('EventController@lotreport') }}" method="POST"  class="event-detail-form" name="report" id="bid-wise-report-form">
                        <div class="ownvendor_mailinput"> 
                            <p class="text-center" style="font-weight: 700;"> Bid Detail Columns </p>
                                <!-- <input id="biddetail" name="biddetail[]" type="checkbox" value="0"> All <br> -->
                                <div class="col-md-12">
                                    <div class="col-md-12 row" style="padding:10px">
                                        <div class="col-md-6">
                                            <a href="#" id="select_all"> <i class="fa fa-check" aria-hidden="true"></i> Select All </a> <br>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="#" id="unselect_all"> <i class="fa fa-times" aria-hidden="true"></i> Unselect All </a> <br>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding:20px; line-height: normal; background:white;">
                                        <input id="biddetail-default" name="biddetail-default[]" type="checkbox" value="1" disabled="true" checked> Item Id <br>
                                        <input id="biddetail-default" name="biddetail-default[]" type="checkbox" value="2" disabled="true" checked> Seller Company Name <br>
                                        <input id="biddetail-default" name="biddetail-default[]" type="checkbox" value="3" disabled="true" checked> Bid Amount <br>
                                        <input id="biddetail" name="biddetail[]" type="checkbox" value="4"> Bid Date <br>
                                        <input id="biddetail" name="biddetail[]" type="checkbox" value="5"> Auction Status <br>
                                    </div>
                                    <div class="amount-btn">
                                        <div style="margin-top:1em" class="float-left mb-4">
                                            <!-- <a id="confirm-pdf-model" class="btn-amount btn-primary text-center"> PDF </a>           
                                            <a id="confirm-excel-model" class="btn-amount btn-primary text-center"> EXCEL </a>  -->

                                            <input type="hidden" name="hfLotId" id="hfLotId" />
                                            <input type="hidden" name="hfEventId" id="hfEventId" />
                                            <input type="submit" value="Excel" class="btn-amount btn-purple text-center" id="submit-event" name="SUBMIT">
                                            <input type="submit" value="PDF" class="btn-amount btn-purple text-center" id="submit-event" name="SUBMIT">
                                            <a id="confirm-bid-cancel-model" class="btn-cancel btn-light text-center"> Cancel </a>
                                        </div>                                
                                    </div>  
                                </div>
                        </div>
                    </form>
                </div>
            </div>
            
            </div>
        </div>

    <div class="modal fade headShake" id="Bid-Overall-Export-Report" role="dialog">
        <div class="modal-dialog vendor-invite-popup">
        
        <!-- Modal content-->
        <div class="modal-content modal-content-amount invite-popup-content">
            <div class="modal-header modal-header-amount amount_bg" style="height:auto">
            <h4 class="modal-title-amount">Select the Columns to export</h4>
            </div>
            <div class="modal-body modal-body-bg" style="height:auto">
                <form action="{{ action('EventController@eventreport') }}" method="POST"  class="event-detail-form" name="report" id="bid-wise-report-form">
                    <div class="ownvendor_mailinput"> 
                        <p class="text-center" style="font-weight: 700;"> Lot Detail Columns </p>
                            <div class="col-md-12">
                            <!-- <input id="biddetail" name="biddetail[]" type="checkbox" value="0"> All <br> -->
                                <div class="col-md-12 row" style="padding:10px">
                                    <div class="col-md-6">
                                        <a href="#" id="all_select_all"> <i class="fa fa-check" aria-hidden="true"></i> Select All </a> <br>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#" id="all_unselect_all"> <i class="fa fa-times" aria-hidden="true"></i> Unselect All </a> <br>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding:20px; line-height: normal; background:white;">
                                    <input id="lotdetail-default" name="lotdetail-default[]" type="checkbox" value="1" disabled="true" checked> Event Id <br>
                                    <input id="lotdetail" name="lotdetail[]" type="checkbox" value="2"> Category Name <br>
                                    <input id="lotdetail" name="lotdetail[]" type="checkbox" value="3"> Sub Category Name <br>
                                    <input id="lotdetail-default" name="lotdetail-default[]" type="checkbox" value="4" disabled="true" checked> Start Date <br>
                                    <input id="lotdetail-default" name="lotdetail-default[]" type="checkbox" value="5" disabled="true" checked> End Date <br>
                                    <input id="lotdetail-default" name="lotdetail-default[]" type="checkbox" value="6" disabled="true" checked> Spec <br>
                                    <input id="lotdetail" name="lotdetail[]" type="checkbox" value="7"> Unit <br>
                                    <input id="lotdetail" name="lotdetail[]" type="checkbox" value="8"> Item Quantity <br>
                                    <input id="lotdetail" name="lotdetail[]" type="checkbox" value="9"> Start Price <br>
                                    <input id="lotdetail" name="lotdetail[]" type="checkbox" value="10"> Min Decrement <br>
                                    <input id="lotdetail" name="lotdetail[]" type="checkbox" value="11"> Floor Price <br>
                                    <input id="lotdetail" name="lotdetail[]" type="checkbox" value="12"> Location <br>
                                </div>
                                <div class="col-md-12">
                                    <label> Note: Start Price, Min Decrement, Floor Price not applicable to Closed Bid Auctions </label>
                                </div>
                                <div class="amount-btn">
                                    <div style="margin-top:1em" class="float-left mb-4">
                                        <!-- <a id="confirm-pdf-model" class="btn-amount btn-primary text-center"> PDF </a>           
                                        <a id="confirm-excel-model" class="btn-amount btn-primary text-center"> EXCEL </a>  -->

                                        <input type="hidden" name="hfEventIt" id="hfEventIt" />
                                        <input type="hidden" name="hfEventType" id="hfEventType" />
                                        <input type="submit" value="Excel" class="btn-amount btn-purple text-center" id="submit-event" name="SUBMIT">
                                        <input type="submit" value="PDF" class="btn-amount btn-purple text-center" id="submit-event" name="SUBMIT">
                                        <a id="confirm-bid-lot-cancel-model" class="btn-cancel btn-light text-center"> Cancel </a>
                                    </div>
                            
                                </div>  
                            </div>
                    </div>
                </form>
            </div>
        </div>
        
        </div>
    </div>
        
@endsection


@section('scripts')
	<script type="text/javascript" src="{{asset('js/plugins/datatables/datatables.min.js')}}?{{ time() }}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/datatables/dataTables.buttons.min.js')}}?{{ time() }}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/datatables/jszip.min.js')}}?{{ time() }}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/datatables/buttons.html5.min.js')}}?{{ time() }}"></script>
@endsection
@section('inline-scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#mis_download').DataTable( {
			responsive: true,
			scrollX: true,
			ordering: false,
			searching: false,
			info: false,
			dom: 'Bfrtip',
			buttons: [
				'excel',
			],
		} );
	} );
</script>
@endsection 