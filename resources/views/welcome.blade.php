@extends('header')


@section('content')
	<div class="login-bg">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 d-lg-flex align-items-center">
					<!--login form-->
					
					<div class="login-form">
						<div id="status"></div>
						<h4 class="text-uppercase text-purple text-center mb-5">Login</h4>
						<form name="buyerLogin" id="buyerLogin">

							<div class="form-group">
								<input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
							</div>
							<div class="form-group mb-4">
								<input type="password"  name="password" class="form-control" id="exampleInputPassword1" placeholder="Enter Password">
							</div>

							<div class="form-group clearfix">
								<a href="{{ url ('forgot-password') }}" class="float-left forgot-link my-2">Forgot Password ?</a>
								<button type="submit" class="btn btn-purple float-right">LOGIN</button>
							</div>
							<div class="text-center mt-4">
								<a href="{{ url ('register') }}" class="btn-link text-capitalize f12">Register As Buyer</a>
							</div>

						</form>
					</div>
					<!--/login form-->					 

					<!--login promo-->
					<div class="login-promo basic-gradient  text-white position-relative">
						<div class="login-promo-content text-center">
						   <div class="login-form-brand">
								<img src="{{ asset ('new-img/logo.png') }}" alt="PurchaseQick">
							</div>
									  
							<h1 class="text-white">Let&rsquo;s Start The Journey</h1>
							<p>Online Market Place</p>
							
						</div>
						<div class="seller-link-trigger">
							<a href="/sregister" class="btn-link f12"><i class="fa fa-user" aria-hidden="true"></i> Register As Seller</a>
						</div>
					</div>
					<!--/login promo-->
				</div>
			</div>
		</div>
	</div>
@endsection

<script>

	window.history.pushState(null, "", window.location.href);        
	window.onpopstate = function() {
		window.history.pushState(null, "", window.location.href);
	};
    
</script>
